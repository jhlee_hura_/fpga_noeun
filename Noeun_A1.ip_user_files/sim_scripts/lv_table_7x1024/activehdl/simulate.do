onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+lv_table_7x1024 -L xilinx_vip -L xil_defaultlib -L xpm -L dist_mem_gen_v8_0_13 -L xilinx_vip -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.lv_table_7x1024 xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {lv_table_7x1024.udo}

run -all

endsim

quit -force
