vlib questa_lib/work
vlib questa_lib/msim

vlib questa_lib/msim/xilinx_vip
vlib questa_lib/msim/xil_defaultlib
vlib questa_lib/msim/xpm
vlib questa_lib/msim/gtwizard_ultrascale_v1_7_6
vlib questa_lib/msim/blk_mem_gen_v8_4_3
vlib questa_lib/msim/xdma_v4_1_3

vmap xilinx_vip questa_lib/msim/xilinx_vip
vmap xil_defaultlib questa_lib/msim/xil_defaultlib
vmap xpm questa_lib/msim/xpm
vmap gtwizard_ultrascale_v1_7_6 questa_lib/msim/gtwizard_ultrascale_v1_7_6
vmap blk_mem_gen_v8_4_3 questa_lib/msim/blk_mem_gen_v8_4_3
vmap xdma_v4_1_3 questa_lib/msim/xdma_v4_1_3

vlog -work xilinx_vip -64 -sv -L xilinx_vip "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi4stream_vip_axi4streampc.sv" \
"C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi_vip_axi4pc.sv" \
"C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/xil_common_vip_pkg.sv" \
"C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi4stream_vip_pkg.sv" \
"C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi_vip_pkg.sv" \
"C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi4stream_vip_if.sv" \
"C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi_vip_if.sv" \
"C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/clk_vip_if.sv" \
"C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/rst_vip_if.sv" \

vlog -work xil_defaultlib -64 -sv -L xilinx_vip "+incdir+../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source" "+incdir+../../../ipstatic/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_fifo/hdl/xpm_fifo.sv" \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -64 -93 \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work gtwizard_ultrascale_v1_7_6 -64 "+incdir+../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source" "+incdir+../../../ipstatic/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_bit_sync.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gte4_drp_arb.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gthe4_delay_powergood.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gtye4_delay_powergood.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gthe3_cpll_cal.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gthe3_cal_freqcnt.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gthe4_cpll_cal.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gthe4_cpll_cal_rx.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gthe4_cpll_cal_tx.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gthe4_cal_freqcnt.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gtye4_cpll_cal.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gtye4_cpll_cal_rx.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gtye4_cpll_cal_tx.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gtye4_cal_freqcnt.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gtwiz_buffbypass_rx.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gtwiz_buffbypass_tx.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gtwiz_reset.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gtwiz_userclk_rx.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gtwiz_userclk_tx.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gtwiz_userdata_rx.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gtwiz_userdata_tx.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_reset_sync.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_reset_inv_sync.v" \

vlog -work xil_defaultlib -64 "+incdir+../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source" "+incdir+../../../ipstatic/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/ip_0/sim/gtwizard_ultrascale_v1_7_gtye4_channel.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/ip_0/sim/xdma_0_pcie4_ip_gt_gtye4_channel_wrapper.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/ip_0/sim/gtwizard_ultrascale_v1_7_gtye4_common.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/ip_0/sim/xdma_0_pcie4_ip_gt_gtye4_common_wrapper.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/ip_0/sim/xdma_0_pcie4_ip_gt_gtwizard_gtye4.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/ip_0/sim/xdma_0_pcie4_ip_gt_gtwizard_top.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/ip_0/sim/xdma_0_pcie4_ip_gt.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_gtwizard_top.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_phy_ff_chain.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_phy_pipeline.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_bram_16k_int.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_bram_16k.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_bram_32k.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_bram_4k_int.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_bram_msix.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_bram_rep_int.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_bram_rep.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_bram_tph.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_bram.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_gt_gt_channel.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_gt_gt_common.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_gt_phy_clk.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_gt_phy_rst.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_gt_phy_rxeq.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_gt_phy_txeq.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_sync_cell.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_sync.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_gt_cdr_ctrl_on_eidle.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_gt_receiver_detect_rxterm.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_gt_phy_wrapper.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_init_ctrl.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_pl_eq.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_vf_decode.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_pipe.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_phy_top.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_seqnum_fifo.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_sys_clk_gen_ps.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4_ip_pcie4_uscale_core_top.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/sim/xdma_0_pcie4_ip.v" \

vlog -work blk_mem_gen_v8_4_3 -64 "+incdir+../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source" "+incdir+../../../ipstatic/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../ipstatic/simulation/blk_mem_gen_v8_4.v" \

vlog -work xil_defaultlib -64 "+incdir+../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source" "+incdir+../../../ipstatic/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_1/sim/xdma_v4_1_3_blk_mem_64_reg_be.v" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_2/sim/xdma_v4_1_3_blk_mem_64_noreg_be.v" \

vlog -work xdma_v4_1_3 -64 -sv -L xilinx_vip "+incdir+../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source" "+incdir+../../../ipstatic/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../ipstatic/hdl/xdma_v4_1_vl_rfs.sv" \

vlog -work xil_defaultlib -64 -sv -L xilinx_vip "+incdir+../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/ip_0/source" "+incdir+../../../ipstatic/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/xdma_v4_1/hdl/verilog/xdma_0_dma_bram_wrap.sv" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/xdma_v4_1/hdl/verilog/xdma_0_dma_bram_wrap_1024.sv" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/xdma_v4_1/hdl/verilog/xdma_0_core_top.sv" \
"../../../../Noeun_A1.srcs/sources_1/ip/xdma_0/sim/xdma_0.sv" \

vlog -work xil_defaultlib \
"glbl.v"

