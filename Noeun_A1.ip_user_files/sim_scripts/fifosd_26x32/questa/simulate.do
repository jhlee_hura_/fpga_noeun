onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib fifosd_26x32_opt

do {wave.do}

view wave
view structure
view signals

do {fifosd_26x32.udo}

run -all

quit -force
