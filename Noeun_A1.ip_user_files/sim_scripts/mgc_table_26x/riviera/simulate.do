onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+mgc_table_26x -L xilinx_vip -L xil_defaultlib -L xpm -L dist_mem_gen_v8_0_13 -L xilinx_vip -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.mgc_table_26x xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {mgc_table_26x.udo}

run -all

endsim

quit -force
