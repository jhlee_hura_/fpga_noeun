onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib mgc_table_26x_opt

do {wave.do}

view wave
view structure
view signals

do {mgc_table_26x.udo}

run -all

quit -force
