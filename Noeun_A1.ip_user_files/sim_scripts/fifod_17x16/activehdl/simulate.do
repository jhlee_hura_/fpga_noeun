onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+fifod_17x16 -L xilinx_vip -L xil_defaultlib -L xpm -L fifo_generator_v13_2_4 -L xilinx_vip -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.fifod_17x16 xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {fifod_17x16.udo}

run -all

endsim

quit -force
