onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib fifod_17x16_opt

do {wave.do}

view wave
view structure
view signals

do {fifod_17x16.udo}

run -all

quit -force
