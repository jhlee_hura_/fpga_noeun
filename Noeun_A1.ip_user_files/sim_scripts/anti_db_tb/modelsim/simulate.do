onbreak {quit -f}
onerror {quit -f}

vsim -voptargs="+acc" -t 1ps -L xilinx_vip -L xil_defaultlib -L xpm -L dist_mem_gen_v8_0_13 -L xilinx_vip -L unisims_ver -L unimacro_ver -L secureip -lib xil_defaultlib xil_defaultlib.anti_db_tb xil_defaultlib.glbl

do {wave.do}

view wave
view structure
view signals

do {anti_db_tb.udo}

run -all

quit -force
