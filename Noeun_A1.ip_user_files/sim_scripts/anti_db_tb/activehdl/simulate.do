onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+anti_db_tb -L xilinx_vip -L xil_defaultlib -L xpm -L dist_mem_gen_v8_0_13 -L xilinx_vip -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.anti_db_tb xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {anti_db_tb.udo}

run -all

endsim

quit -force
