`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/12/01 10:19:03
// Design Name: 
// Module Name: bsc_pcie
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module bsc_pcie(
    input wire  rst,
    input wire  pcie_clk,

    input wire          regs_bsc_pcie_sel,
    input wire [46:0]   regs_adda,
    input wire          regs_adda_valid,
    output wire [31:0]  regs_bsc_pcie_data,
    output wire         regs_bsc_pcie_data_valid,
    input wire          regs_data_ready,
    
    input wire  [7:0]   c2h_sts_0,
    input wire  [7:0]   h2c_sts_0
        
    );
    
//Switch for DMA Test
wire			test_cnt_clr;
wire	[1:0]	data_sel;
wire	[127:0]	to_pcie_tdata;
wire	[15:0]	to_pcie_tkeep;
wire			to_pcie_tlast;

// Module for DMA
wire	[127:0]	o_dma_buf_data;
wire    [15:0]	o_dma_buf_keep;
wire            o_dma_buf_last;
wire            o_dma_buf_valid;
wire            o_dma_buf_ready;   

// ---------------------------------------------------------------------------------
//
// DMA Count
//
    reg  [31:0]  c2h_busy_clk_cnt ='b0;
	reg  [31:0]  c2h_run_clk_cnt  ='b0;
	reg  [31:0]  c2h_packet_cnt   ='b0;
	reg  [31:0]  c2h_desc_cnt     ='b0; 
// ---------------------------------------------------------------------------------
// Reg for pcie_clk
regs_bsc_pcie regs_bsc_pcie (
	.pcie_clk			(pcie_clk),					// I
	.sclr				(rst),						// I

	.regs_sel			(regs_bsc_pcie_sel),			// I
	.regs_adda			(regs_adda),				// I [46:0]
	.regs_adda_valid	(regs_adda_valid),			// I
	.regs_dout			(regs_bsc_pcie_data),		// O [31:0]
	.regs_dout_valid	(regs_bsc_pcie_data_valid),	// O
	// -------------------------------------------------------
	// -- register ? λ³?
	// DMA_DATA_SEL_REG
	.data_sel				(data_sel),				// O [1:0]
	.test_cnt_clr			(test_cnt_clr),			// O
	// DMA_FIFO_EMPTY_REG
	.dma_fifo_empty			(),		// I

	// PCIE_DMA_C2H_0_CTRL_REG
	.c2h_0_rst				(),			// O
	// PCIE_DMA_C2H_0_SRCADDR_REG
	.c2h_0_src_addr			(),		// O [31:0]
	// PCIE_DMA_C2H_0_LEN_REG
	.c2h_0_len				(),			// O [27:0]
	// PCIE_DMA_C2H_0_STAT_REG
	.c2h_0_sts				(c2h_sts_0),			// I [7:0]

	// PCIE_DMA_C2H_0_BUSY_CNT_REG
	.c2h_0_busy_clk_cnt		(c2h_busy_clk_cnt),	// I[31:0]
	// PCIE_DMA_C2H_0_RUN_CNT_REG
	.c2h_0_run_clk_cnt		(c2h_run_clk_cnt),	// I [31:0]
	// PCIE_DMA_C2H_0_PACKET_CNT_REG
	.c2h_0_packet_cnt		(c2h_packet_cnt),		// I [31:0]
	// PCIE_DMA_C2H_0_DESC_CNT_REG
	.c2h_0_desc_cnt			(c2h_desc_cnt),		// I [31:0]

	// PCIE_DMA_H2C_0_STAT_REG
	.h2c_0_sts				(h2c_sts_0)				// I [7:0]
);        
/*

pcie_dma_data_sel #(
	.DATA_WIDTH(128)
) pcie_dma_data_sel (
	.pcie_clk(pcie_clk),			// I
	.sclr(rst),					// I

	.test_cnt_clr(test_cnt_clr),	// I
	.data_sel(data_sel),			// I [1:0]

    .din_fifo_tdata(o_dma_buf_data),			// I [DATA_WIDTH-1:0]
    .din_fifo_tkeep(o_dma_buf_keep),			// I [KEEP_WIDTH-1:0]
    .din_fifo_tvalid(o_dma_buf_valid),		// I
    .din_fifo_tlast(o_dma_buf_last),			// I
    .din_fifo_tready(o_dma_buf_ready),		// O

	// to PCIE Axi streaming
	.to_pcie_tdata(to_pcie_tdata),			// O [DATA_WIDTH-1:0]
	.to_pcie_tkeep(to_pcie_tkeep),			// O [KEEP_WIDTH-1:0]
	.to_pcie_tlast(to_pcie_tlast),			// O
	.to_pcie_tvalid(),		// O
	.to_pcie_tready()			// I
);
*/

//========================================================================================================== DMA Count
    //always
    reg          c2h_busy_end_flag;
    reg          c2h_run_end_flag;
    wire         c2h_busy;
    wire         c2h_control_run;
    wire         packet_done;
    wire         descript_done;   


assign c2h_busy = c2h_sts_0[0];
assign c2h_control_run = c2h_sts_0[6];
assign packet_done = c2h_sts_0[4];
assign descript_done = c2h_sts_0[3];


always @(posedge pcie_clk) begin
	if (c2h_busy) begin
		c2h_busy_clk_cnt <= (c2h_busy_end_flag) ? 'b1 : c2h_busy_clk_cnt + 'b1;	// ?λ‘?κ²? ???λ©? cnt Reset
		c2h_busy_end_flag <= 1'b0;
	end
	else begin
		c2h_busy_clk_cnt <= c2h_busy_clk_cnt;			// busyκ°? ??? μ§?? κ°μ ? μ§??΄ Registerλ‘? κ°μ λ³΄λ.
		c2h_busy_end_flag <= 1'b1;
	end

	if (c2h_control_run) begin
		c2h_run_clk_cnt <= (c2h_run_end_flag) ? 'b1 : c2h_run_clk_cnt + 'b1;	// ?λ‘?κ²? ???λ©? cnt Reset
		c2h_run_end_flag <= 1'b0;

		c2h_packet_cnt <=	(c2h_run_end_flag) ? 'b0 :
							(packet_done) ? c2h_packet_cnt + 1'b1 : c2h_packet_cnt;
		c2h_desc_cnt <=		(c2h_run_end_flag) ? 'b0 :
							(descript_done) ? c2h_desc_cnt + 1'b1 : c2h_desc_cnt;
	end
	else begin
		c2h_run_clk_cnt <= c2h_run_clk_cnt;		// busyκ°? ??? μ§?? κ°μ ? μ§??΄ Registerλ‘? κ°μ λ³΄λ.
		c2h_packet_cnt <= c2h_packet_cnt;
		c2h_desc_cnt <= c2h_desc_cnt;
		c2h_run_end_flag <= 1'b1;
	end
end
endmodule
