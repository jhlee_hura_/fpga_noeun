`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/11/30 17:46:41
// Design Name: 
// Module Name: lv_table
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module lv_table(
    input wire        clk,	//sig_clk
	input wire        sclr,

	input wire        lv_cal_en,

	input wire [8:0]  freq_index,
	input wire [5:0]  freq_intp,

	input wire [4:0]  bw_idx,	
	input wire [4:0]  temp_idx,


	input wire        we_satt,
	input wire [13:0] addra_satt,
	input wire [15:0] dataa_satt,

	input wire        we_ln,
	input wire [8:0]  addra_ln,
	input wire [15:0] dataa_ln,
	
	input wire        we_gatt,
	input wire        addra_gatt_mode,
	input wire [8:0]  addra_gatt,
	input wire [6:0]  dataa_gatt,

	input wire        we_att_1,
	input wire [4:0]  addra_att_1,
	input wire [15:0] dataa_att_1,

	input wire        we_att_2,
	input wire [4:0]  addra_att_2,
	input wire [15:0] dataa_att_2,

	input wire        we_bw,
	input wire [4:0]  addra_bw,
	input wire [15:0] dataa_bw,

	input wire        we_temp,
	input wire [4:0]  addra_temp,
	input wire [15:0] dataa_temp,



	input wire [4:0]  satt_idx,
	input wire        lna_en,
	input wire [4:0]  att_1_idx,
	input wire [4:0]  att_2_idx,

	output wire [6:0] gatt_ctrl,
	output wire       gatt_ctrl_valid,
	output reg [8:0]  lv_cal_out,
	output reg [7:0]  anti_db_cal_out,
	output reg [17:0] sum_cal
);

// -----------------------------------------------------
//	LV_table read//
wire	[15:0]	doutb_satt;
wire	[15:0]	doutb_ln;
wire	[15:0]	doutb_att_1;
wire	[15:0]	doutb_att_2;
wire	[15:0]	doutb_bw;
wire	[15:0]	doutb_temp;
wire 	[6:0]	doutb_gatt;

//-------------------------------------------
// 

reg	[2:0]	freq_idx_state;
parameter	IDLE_STATE 			= 3'b000;
parameter 	ACTION0_WAIT		= 3'b001;
parameter	ACTION1_STATE 		= 3'b010;
parameter 	ACTION1_WAIT		= 3'b011;
parameter	ACTION2_STATE 		= 3'b100;

wire [13:0] satt_freq_index ={satt_idx, freq_index};
reg [13:0]  addrb_satt;
reg [15:0]	dout_satt_base;
reg [15:0]	satt_differ;

reg [8:0]   addrb_ln;
reg [15:0]	dout_ln_base;
reg [15:0]	ln_differ;

reg [9:0] 	addrb_gc;
reg [6:0]	dout_gc_base;
reg [7:0]	gc_differ;


reg [13:0]  satt_freq_index_old;
reg [5:0] 	freq_intp_old;
reg			lna_en_old;

reg			gc_differ_valid;

always @(posedge clk) begin
	if(sclr)  begin
		addrb_satt  	<= 	14'b0;
		dout_satt_base	<=	16'b0;
		satt_differ		<=	16'b0;

		addrb_ln		<= 	9'b0;
		dout_ln_base	<=	16'b0;
		ln_differ 		<=	16'b0;

		addrb_gc		<= 	10'b0;
		dout_gc_base 	<=	7'b0;
		gc_differ		<=	8'b0;
		gc_differ_valid	<= 1'b0;

		satt_freq_index_old	<=  14'h3fff;
		freq_intp_old	<= 6'b0;

		freq_idx_state 	<= IDLE_STATE;
		end
	else begin
		case (freq_idx_state)
			IDLE_STATE : begin // ln? λ³?κ²½ν?? ???κ²? ?? ;
				if (satt_freq_index == satt_freq_index_old && freq_intp == freq_intp_old && lna_en_old == lna_en) begin
					addrb_satt  	<= 	addrb_satt;
					dout_satt_base	<=	dout_satt_base;
					satt_differ		<=	satt_differ;

					addrb_ln		<= 	addrb_ln;
					dout_ln_base	<=	dout_ln_base;
					ln_differ  		<=	ln_differ;

					addrb_gc		<= 	addrb_gc;
					dout_gc_base 	<=	dout_gc_base;
					gc_differ		<=	gc_differ;
					gc_differ_valid	<= 1'b0;

					satt_freq_index_old	<= satt_freq_index_old;
					freq_intp_old <= freq_intp_old;
					lna_en_old		<= lna_en_old;

					freq_idx_state 	<= IDLE_STATE;
					end
				else begin
					addrb_satt  	<= 	satt_freq_index; 	// ?  λ²μ? μ§?? 
					dout_satt_base	<=	dout_satt_base;
					satt_differ		<=	satt_differ;

					addrb_ln		<= 	freq_index;	// ?  λ²μ? μ§?? 
					dout_ln_base	<=	dout_ln_base;
					ln_differ       <=	ln_differ;

					addrb_gc		<= 	{lna_en ,freq_index};	// ?  λ²μ? μ§?? 
					dout_gc_base	<=	dout_gc_base;
					gc_differ       <=	gc_differ;
					gc_differ_valid	<= ((lna_en != lna_en_old) || (freq_index != satt_freq_index_old[8:0]));

					satt_freq_index_old	<=  satt_freq_index_old;
					freq_intp_old 	<= freq_intp;
					lna_en_old		<= lna_en;

					freq_idx_state 	<=  ACTION0_WAIT;
					end

				end

			ACTION0_WAIT : begin	// ??΄?­ ??κΈ?
					addrb_satt  	<= 	addrb_satt;
					dout_satt_base	<=	dout_satt_base;
					satt_differ		<=	satt_differ;

					addrb_ln		<= 	addrb_ln;
					dout_ln_base	<=	dout_ln_base;
					ln_differ  		<=	ln_differ;

					addrb_gc		<= 	addrb_gc;
					dout_gc_base	<=	dout_gc_base;
					gc_differ  		<=	gc_differ;
					gc_differ_valid	<= 1'b0;

					satt_freq_index_old	<= satt_freq_index_old;

					freq_idx_state 		<= ACTION1_STATE;
				end


			ACTION1_STATE : begin
					addrb_satt  	<= 	addrb_satt+14'b1;  	// ?€? λ²μ? μ§?? 
					dout_satt_base	<=	doutb_satt;			// ?  λ²μ? λ°μ
					satt_differ		<=	satt_differ;

					addrb_ln		<= 	addrb_ln+9'b1;		// ?€?  λ²μ? μ§?? 
					dout_ln_base	<=	doutb_ln;			// ?  λ²μ? λ°μ
					ln_differ  		<=	ln_differ;

					addrb_gc		<= 	addrb_gc+1'b1;		// ?€?  λ²μ? μ§?? 
					dout_gc_base	<=	doutb_gatt;			// ?  λ²μ? λ°μ
					gc_differ  		<=	gc_differ;
					gc_differ_valid	<= 1'b0;

					satt_freq_index_old	<= addrb_satt;		// μ£Όν? ?Έ?±?€ λ³?κ²?

					freq_idx_state 	<= ACTION1_WAIT;
					end

			ACTION1_WAIT : begin	// ??΄?­ ??κΈ?
					addrb_satt  	<= 	addrb_satt;
					dout_satt_base	<=	dout_satt_base;
					satt_differ		<=	satt_differ;

					addrb_ln		<= 	addrb_ln;
					dout_ln_base	<=	dout_ln_base;
					ln_differ  		<=	ln_differ;

					addrb_gc		<= 	addrb_gc;
					dout_gc_base	<=	dout_gc_base;
					gc_differ  		<=	gc_differ;
					gc_differ_valid	<= 1'b0;

					satt_freq_index_old	<= satt_freq_index_old;

					freq_idx_state 		<= ACTION2_STATE;
					end

			ACTION2_STATE : begin
					addrb_satt  	<= 	addrb_satt;
					dout_satt_base	<=	dout_satt_base;
					satt_differ		<=	doutb_satt - dout_satt_base; 	// differ

					addrb_ln		<= 	addrb_ln;
					dout_ln_base	<=	dout_ln_base;
					ln_differ  		<=	doutb_ln - dout_ln_base;		// differ

					addrb_gc		<= 	addrb_gc;
					dout_gc_base	<=	dout_gc_base;
					gc_differ  		<=	doutb_gatt - dout_gc_base;
					gc_differ_valid	<= 1'b0;

					satt_freq_index_old	<= satt_freq_index_old;

					freq_idx_state 		<= IDLE_STATE;
					end

		endcase
	end
end


// -----------------------------------------------------
// Table
lv_table_16x16k LV_table_satt(
  .clka(clk),			// I
  .ena(we_satt),		// I
  .wea(we_satt),		// I [0 : 0]
  .addra(addra_satt),	// I [13 : 0] addra;
  .dina(dataa_satt),	// I [15 : 0] dina;
  .clkb(clk),			// I clkb;
  .addrb(addrb_satt),	// I [13 : 0] addrb;
  .doutb(doutb_satt)	// O [15 : 0] doutb;
);

lv_table_16x512 LV_table_ln (
  .a(addra_ln),        	// I [8 : 0]
  .d(dataa_ln),        	// I [15 : 0]
  .dpra(addrb_ln),  	// I [8 : 0]
  .clk(clk),    		// I
  .we(we_ln),      		// I
  .dpo(doutb_ln)   		// O [15 : 0]
);

lv_table_16x32 LV_table_att_1 (
  .a(addra_att_1),			// I [4 : 0]
  .d(dataa_att_1),			// I [15 : 0]
  .dpra(att_1_idx),  		// I [4 : 0]
  .clk(clk),    			// I
  .we(we_att_1),				// I
  .dpo(doutb_att_1)			// O [15 : 0]
);

lv_table_16x32 LV_table_att_2 (
  .a(addra_att_2),			// I [4 : 0]
  .d(dataa_att_2),			// I [15 : 0]
  .dpra(att_2_idx),  		// I [4 : 0]
  .clk(clk),    			// I
  .we(we_att_2),				// I
  .dpo(doutb_att_2)			// O [15 : 0]
);

lv_table_16x32 LV_table_bw (
  .a(addra_bw),				// I [4 : 0]
  .d(dataa_bw),				// I [15 : 0]
  .dpra(bw_idx),  			// I [4 : 0]
  .clk(clk),    			// I
  .we(we_bw),				// I
  .dpo(doutb_bw)			// O [15 : 0]
);

lv_table_16x32 LV_table_temp (
  .a(addra_temp),			// I [4 : 0]
  .d(dataa_temp),			// I [15 : 0]
  .dpra(temp_idx),  		// I [4 : 0]
  .clk(clk),    			// I
  .we(we_temp),				// I
  .dpo(doutb_temp)			// O [15 : 0]
);

lv_table_7x1024 CTRL_table_gatt (
  .a({addra_gatt_mode,addra_gatt}),	// I [9 : 0]
  .d(dataa_gatt),        			// I [6 : 0]
  .dpra(addrb_gc),  				// I [9 : 0]
  .clk(clk),    					// I
  .we(we_gatt),      				// I
  .dpo(doutb_gatt)    				// O [6 : 0]
);

// *****************************************************************************
// interpolation
// *****************************************************************************
wire[16:0]		satt_multi;
wire[16:0]		ln_multi;
wire[16:0]		gc_multi;

mult_differ multiplier_differ_satt (
  .CLK(clk),			// I
  .A(satt_differ),		// I [15 : 0]
  .B(freq_intp),		// I [5 : 0]
  .P(satt_multi)		// O [16 : 0]
); // latency =3

mult_differ multiplier_differ_ln (
  .CLK(clk),			// I
  .A(ln_differ),		// I [15 : 0]
  .B(freq_intp),		// I [5 : 0]
  .P(ln_multi)			// O [16 : 0]
); // latency =3

mult_differ multiplier_differ_gc (
	.CLK(clk),
    .A({gc_differ,8'b0}),
    .B(freq_intp),
    .P(gc_multi)
); // latency =3

reg			lv_cal_en_old;
reg [15:0]	satt_interp;
reg [15:0]	ln_interp;
reg [6:0]	gc_interp;
wire		gatt_c_valid;
wire [15:0]	ln_interp_out;

always @(posedge clk) begin
	satt_interp <= satt_multi[16:1] + satt_multi[0] + dout_satt_base;
	ln_interp <= ln_multi[16:1] + ln_multi[0] + dout_ln_base;
	gc_interp <= (gc_multi[15:9] + gc_multi[8] ) + dout_gc_base;
	lv_cal_en_old <= lv_cal_en;
end

delay_regi # (
	.DIN_WIDTH	(1),
	.DELAY		(8)
) delay_gatt_ctrl (
	.clk(clk),					// I
	.sclr(sclr),				// I
	.din(gc_differ_valid),			// I [DIN_WIDTH-1:0]
	.dout(gatt_c_valid)			// O [DIN_WIDTH-1:0]
);

assign  ln_interp_out = (lna_en) ? ln_interp : 16'b0;
assign  gatt_ctrl = (lv_cal_en) ? gc_interp[6:0] : 'b0;
assign	gatt_ctrl_valid = gatt_c_valid || (lv_cal_en_old != lv_cal_en);	// lv_cal_en λ³??? λ°μ?΄?Ό ?¨.

// *****************************************************************************
//adder
wire [17:0]		add_18bit_out1_1;
wire [17:0]		add_18bit_out1_2;
wire [17:0]		add_18bit_out1_3;
wire [17:0]		add_18bit_out2_1;
wire [17:0]		add_18bit_out3;

wire [17:0]		din_add_18bit_2_a;
wire [17:0]		din_add_18bit_2_b;
wire [17:0]		din_add_18bit_2_c;
wire [17:0]		din_add_18bit_3_a;

assign	din_add_18bit_2_a = add_18bit_out1_1;
assign	din_add_18bit_2_b = add_18bit_out1_2;
assign	din_add_18bit_2_c = add_18bit_out1_3;
assign	din_add_18bit_3_a = add_18bit_out2_1;

wire [17:0] agc_coeff_log2;
wire [17:0] do_gain_ctrl;
wire 		dov_gain_ctrl;
add_s18s18 add_18bit_1_1 (
  .A({satt_interp[15],satt_interp[15],satt_interp}),		// I [17 : 0]
  .B({ln_interp_out[15],ln_interp_out[15],ln_interp_out}),	// I [17 : 0]
  .CLK(clk),												// I
  .S(add_18bit_out1_1)										// O [17 : 0]
); // latency= 2

add_s18s18 add_18bit_1_2(
  .A({doutb_att_1[15],doutb_att_1[15],doutb_att_1}),		// I [17 : 0]
  .B({doutb_att_2[15],doutb_att_2[15],doutb_att_2}),		// I [17 : 0]
  .CLK(clk),											// I
  .S(add_18bit_out1_2)									// O [17 : 0]
);

add_s18s18 add_18bit_1_3(
  .A({doutb_bw[15],doutb_bw[15],doutb_bw}),				// I [17 : 0]
  .B({doutb_temp[15],doutb_temp[15],doutb_temp}),		// I [17 : 0]
  .CLK(clk),											// I
  .S(add_18bit_out1_3)									// O [17 : 0]
);

add_s18s18 add_18bit_2_1(
  .A(din_add_18bit_2_a),		// I [17 : 0]
  .B(din_add_18bit_2_b),		// I [17 : 0]
  .CLK(clk),					// I
  .S(add_18bit_out2_1)			// O [17 : 0]
);

add_s18s18 add_18bit_3_1(
  .A(din_add_18bit_3_a),		// I [17 : 0]
  .B(din_add_18bit_2_c),		// I [17 : 0]
  .CLK(clk),					// I
  .S(add_18bit_out3)			// O [17 : 0]
);


// *****************************************************************************
wire [7:0] dout_anti_db;

anti_db_tb anti_db (
  .a(add_18bit_out3[7:0]), // input [7 : 0] a
  .spo(dout_anti_db) // output [7 : 0] spo
);

always@(posedge clk) begin
	sum_cal			<= (lv_cal_en) ? add_18bit_out3 : 'b0;
	lv_cal_out		<= (lv_cal_en) ? add_18bit_out3[16:8] : 'b0; // ??? ? λ²¨λ³΄? κ°μΌλ‘?
	anti_db_cal_out <= (lv_cal_en) ? dout_anti_db : 'b0;
end
endmodule
