`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/11/30 20:52:34
// Design Name: 
// Module Name: bsc_sig_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module bsc_sig(
    input wire  rst,
    input wire  u_sig_clk,
    input wire  pcie_clk,

    input wire          regs_bsc_sig_sel,
    input wire [46:0]   regs_adda,
    input wire          regs_adda_valid,
    output wire [31:0]  regs_bsc_sig_data,
    output wire         regs_bsc_sig_data_valid,
    input wire          regs_data_ready,
        
    input wire		GPS_PPS
    );

// --------------------------------------------------------------------------------- Reg for sig_clk



// ---------------------------------------------------------------------------------
// 
// pps date
//
wire [15:0]	set_date;
wire 		set_date_valid;
wire [4:0]	set_time_H;
wire [5:0]	set_time_M;
wire [5:0]	set_time_S;
wire 		set_time_valid;
wire [15:0]	pps_date;
wire [4:0]	pps_time_H;
wire [5:0]	pps_time_M;
wire [5:0]	pps_time_S;
wire [6:0]	pps_time_10ms;
wire [31:0]	pps_count_syson;
wire [16:0]	sec_in_day;
wire [29:0]	nano_sec;
wire pps_trig;
// ---------------------------------------------------------------------------------
regs_bsc_sig regs_bsc_sig (
	.clk				(u_sig_clk),				// I
	.rst				(rst),					// I
	.pcie_clk			(pcie_clk),				// I

	.regs_sel			(regs_bsc_sig_sel),		// I
	.regs_adda			(regs_adda),			// I [46:0]
	.regs_adda_valid	(regs_adda_valid),		// I
	.regs_dout			(regs_bsc_sig_data),	// O [31:0]
	.regs_dout_valid	(regs_bsc_sig_data_valid),	// O
	.regs_dout_ready	(regs_data_ready),		// I
	// -------------------------------------------------------
	// -- register ?���?

	// DATE_REG
	.set_date(set_date),					// O [15:0]
	.set_date_valid(set_date_valid),		// O
	.pps_date(pps_date),					// I [15:0]
	// TIME_REG
	.set_time_H(set_time_H),				// O [4:0]
	.set_time_M(set_time_M),            	// O [5:0]
	.set_time_S(set_time_S),            	// O [5:0]
	.set_time_valid(set_time_valid),		// O
	.pps_time_H(pps_time_H),				// I [4:0]
	.pps_time_M(pps_time_M),				// I [5:0]
	.pps_time_S(pps_time_S),				// I [5:0]
	.pps_time_10ms(pps_time_10ms),			// I [6:0]
	// SYSONSEC_REG
	.pps_count_syson(pps_count_syson)		// I [31:0]

);


pps_time #(
	.CLKPERSEC(`CLKPERSEC)
) pps_time (
	.clk(u_sig_clk),
	.sclr(rst),						// I
	.GPS_PPS(GPS_PPS),					// I

	.set_date(set_date),				// I [15:0]
	.set_date_valid(set_date_valid),	// I

	.set_time_H(set_time_H),			// I [4:0]
	.set_time_M(set_time_M),			// I [5:0]
	.set_time_S(set_time_S),			// I [5:0]
	.set_time_valid(set_time_valid),	// I

	.pps_reged(pps_trig),				// O
	.pps_date(pps_date),				// O [15:0]
	.pps_time_H(pps_time_H),			// O [4:0]
	.pps_time_M(pps_time_M),			// O [5:0]
	.pps_time_S(pps_time_S),			// O [5:0]
	.pps_time_10ms(pps_time_10ms),		// O [6:0]
	.sec_in_day(sec_in_day),			// O [16:0]
	.nano_sec(nano_sec),				// O [29:0]
	.pps_count_syson(pps_count_syson)	// O [31:0]
);
endmodule
