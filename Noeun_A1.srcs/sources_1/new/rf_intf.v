`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/11/30 17:40:04
// Design Name: 
// Module Name: rf_intf
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module rf_intf(
    input wire clk,		//125Mhz
	input wire sclr,

//// RF - DC Board
	//DIGI. ATTEN. - PE43610(0,1,4) 
	output wire  	RF_PIN0,	output wire 	RF_PIN1,		output wire 	RF_PIN4,

	input wire 	[15:0]	dc_bd_din_att,	input wire			dc_bd_din_att_valid,	output wire			dc_bd_att_ready,	
	
	// LNA Switch (5)
	output wire 	RF_PIN5,	
	
	input wire			dc_bd_lna_sw,		
	
	//SR - SN74HCS595BQBR(2,3,6)	
	output wire 	RF_PIN2,	output wire 	RF_PIN3,	output wire 	RF_PIN6,

	input wire [15:0]	dc_bd_rf_sw,	input wire 			dc_bd_rf_sw_valid,
	input wire      dc_bd_ifbw_idx,	input wire			dc_bd_ifbw_idx_valid,
	
	// SR LOAD - SN74HCS165BQB(7,10,11)
	output wire 	RF_PIN7,		output wire 	RF_PIN10,	input wire 	RF_PIN11,
	
	input wire	dc_bd_din_sts_valid,	output wire	 dc_bd_din_sts_ready,	output wire [15:0]	dc_bd_dout_sts,	output wire	  dc_bd_dout_sts_valid,	
	//TMP102(8,9)
	output wire 	RF_PIN8,  inout wire 	RF_PIN9,	
	input wire 	[1:0] 	dc_bd_din_tmp102,	input wire 			dc_bd_din_tmp102_valid,
	output wire [15:0] 	dc_bd_dout_tmp102,	output wire 		dc_bd_busy_tmp102,	
	//LOG DETECTOR - LMV321AIDCKR(34,35)	
	input wire 		RF_PIN34,	input wire 		RF_PIN35,	
	output wire         dc_bd_log_detect1,    output wire         dc_bd_log_detect2,
//// 	
//// RF - SYN Board	
//// 
	//SR - SN74HCS595BQBR(14,15,18),	
	output wire 		RF_PIN14,	output wire 		RF_PIN15,	output wire 		RF_PIN18,
	input wire [15:0]	syn_bd_rf_sw,	input wire 	syn_bd_rf_sw_valid,	input wire syn_bd_ifbw_idx,	input wire	syn_bd_ifbw_idx_valid,
			
	// SR LOAD - SN74HCS165BQB(12,13,19),
	output wire 		RF_PIN12,	output wire 		RF_PIN13,	output wire 		RF_PIN19,	
	input wire	syn_bd_din_sts_valid,	output wire	 syn_bd_din_sts_ready,	output wire [15:0]	syn_bd_dout_sts,	output wire	  syn_bd_dout_sts_valid,		

	// LMX2594RHAT(16,17,20,21,22,23)
	output wire 		RF_PIN16,	output wire 		RF_PIN17,	output wire 		RF_PIN20,	
	output wire 		RF_PIN21,	output wire 		RF_PIN22,	output wire 		RF_PIN23,
	input wire	[1:0]	syn_bd_din_pll_cmd, //
	input wire	[23:0]	syn_bd_din_pll, input wire	 syn_bd_din_pll_valid,	
	input wire [15:0]	syn_bd_pll_ctrl_interval,output wire syn_bd_pll_last_cmd_in,
	output wire 	syn_bd_pll_cmd_done,output wire  [23:0]	syn_bd_dout_pll,output wire	syn_bd_dout_pll_valid,	

    //DIGI. ATTEN. - PE43610A-X(25,26,27) 
    output wire 		RF_PIN25,	output wire 		RF_PIN26,		output wire 		RF_PIN27,
 	input wire 	[15:0]	syn_bd_din_att,	input wire		syn_bd_din_att_valid,	output wire		syn_bd_att_ready,	   
    // LMK01010ISQ(30,31,32,33)
	output wire 	RF_PIN30,	output wire 	RF_PIN31,  output wire 	RF_PIN32,    inout wire 	RF_PIN33,    
	input wire [31:0] syn_bd_din_distributor, input wire syn_bd_din_distributor_valid, output wire syn_bd_distributor_ready,

    // NOT USE RF_PIN24    
	output wire 		RF_PIN24,	
	
    // TMP102(28,29)
	inout wire 		RF_PIN28,	output wire 		RF_PIN29,    
	input wire 	[1:0] 	syn_bd_din_tmp102,	input wire 			syn_bd_din_tmp102_valid,
	output wire [15:0] 	syn_bd_dout_tmp102,	output wire 		syn_bd_busy_tmp102
	

);


// RF Switch Serial Shiftreg
wire			rf1_sw_rclk;
wire			rf1_sw_clk;
wire			rf1_sw_data;

// RF-DC PE43610(PIN 0,1,4)
wire			adc_bd_att_le;
wire			adc_bd_att_clk;
wire			adc_bd_att_data;


// STS
wire			cm_cs;
wire			cm_clk;
wire			cm_data;

// LO PLL
wire			lo_pll_cs1;
wire			lo_pll_cs2;
wire			lo_pll_clk;
wire			lo_pll_sdo;
wire			lo_pll_sdi;
wire			lo_pll_sd_T;

// Temperature
wire 			tmp_scl;
wire 			tmp_sdi;
wire 			tmp_sdo;
wire			tmp_sda_T;
wire 			tmp_alert;

// ---------------------------------------------------------------------------------
// DC_Board PE43610(Digital Atten.) 
// ---------------------------------------------------------------------------------

CON_PE43610 DC_BD_M1_PE43610(
      .clk(clk),		//125Mhz
	  .sclr(sclr),
	  .din_att(dc_bd_din_att),   // [15:0] I
	  .din_att_valid(dc_bd_din_att_valid), // I
	  .att_ready(dc_bd_att_ready),	// O
	  .PIN_CS(RF_PIN0),    // O   
      .PIN_CLK(RF_PIN1),	 // O  
	  .PIN_DATA(RF_PIN4)       // O   
    );
// ---------------------------------------------------------------------------------
// DC_Board HMC981LP3E(LNA)  
// ---------------------------------------------------------------------------------

OBUF HMC981LP3E (
	.O(RF_PIN5), // 
	.I(dc_bd_lna_sw)
);
/*
CON_HMC981LP3E DC_BD_M2_HMC981LP3E(
    .lna_sw(dc_bd_lna_sw),
    .PIN_OUT(RF_PIN5)
);*/

// ---------------------------------------------------------------------------------
// DC_Board  SN74HCS595BQBR(Shift register) 
// ---------------------------------------------------------------------------------
	
CON_SN74HCS595BQBR DC_BD_M3_SN74HCS595BQBR(
      .clk(clk),		//125Mhz
	  .sclr(sclr),
	  .ifbw_idx(dc_bd_ifbw_idx),     // I
	  .ifbw_idx_valid(dc_bd_ifbw_idx_valid),     // I
	  .rf_sw(dc_bd_rf_sw), // I
	  .rf_sw_valid(dc_bd_rf_sw_valid),	// O
	  .PIN_RCLK(RF_PIN3),    // O     
      .PIN_CLK(RF_PIN6),	 // O     
	  .PIN_DATA(RF_PIN2)       // O   
    );
 // ---------------------------------------------------------------------------------
// DC_Board TMP102  RF_PIN 8,9 
// --------------------------------------------------------------------------------

CON_TMP102 DC_BD_M4_TMP102(
    .clk(clk),
    .sclr(sclr),
	.din_tmp102(dc_bd_din_tmp102), // I [1:0]
	.din_tmp102_valid(dc_bd_din_tmp102_valid), // I
	.dout_tmp102(dc_bd_dout_tmp102), // O [15:0]
	.busy_tmp102(dc_bd_busy_tmp102), // O
    .PIN_SCL(RF_PIN8), // O        
    .PIN_SDA_IO(RF_PIN9) // IO
);
// tmp102  - I2C
   
// ---------------------------------------------------------------------------------
// DC_Board  SN74HCS165BQB(Shift register - load) 
// ---------------------------------------------------------------------------------

CON_SN74HCS165BQB DC_BD_M5_SN74HCS165BQB(
    .clk(clk),		//125Mhz I
	.sclr(sclr),   // I
	.din_sts_valid(dc_bd_din_sts_valid),    // I
	.din_sts_ready(dc_bd_din_sts_ready),   // O
	.dout_sts(dc_bd_dout_sts),   // O [15:0]
	.dout_sts_valid(dc_bd_dout_sts_valid),  // O
  	.PIN_CS(RF_PIN7), // O      
	.PIN_CLK(RF_PIN10),	 // O      
	.PIN_DATA(RF_PIN11)      //I
);
// ---------------------------------------------------------------------------------
// DC_Board   LMV321AIDCKR_1(LOG_DETECTOR) 
// ---------------------------------------------------------------------------------

CON_LMV321AIDCKR DC_BD_M6_LMV321AIDCKR_1(
	.log_detect(dc_bd_log_detect1),   // O 
	.PIN_IN(RF_PIN34)      //I
);

// ---------------------------------------------------------------------------------
// DC_Board  LMV321AIDCKR_2(LOG_DETECTOR)
// ---------------------------------------------------------------------------------

CON_LMV321AIDCKR DC_BD_M7_LMV321AIDCKR_2(

	.log_detect(dc_bd_log_detect2),    // O
	.PIN_IN(RF_PIN35)      //I
);

// ---------------------------------------------------------------------------------
// SYN_Board  SN74HCS595BQBR(Shift register) RF_PIN 14,15,18 
// ---------------------------------------------------------------------------------
	
CON_SN74HCS595BQBR SYN_BD_M1_SN74HCS595BQBR(
      .clk(clk),		//125Mhz
	  .sclr(sclr),
	  .ifbw_idx(syn_bd_ifbw_idx),     // I
	  .ifbw_idx_valid(syn_bd_ifbw_idx_valid),     // I
	  .rf_sw(syn_bd_rf_sw), // I
	  .rf_sw_valid(syn_bd_rf_sw_valid),	// O
	  .PIN_RCLK(RF_PIN14),    // O     
      .PIN_CLK(RF_PIN15),	 // O         
	  .PIN_DATA(RF_PIN18)       // O       
    );
    
// ---------------------------------------------------------------------------------
// SYN_Board LMK01010ISQ(CLOCK BUFFER) RF_PIN 30,31,32,33
// --------------------------------------------------------------------------------
CON_LMK01010ISQ SYN_BD_M2_LMK01010ISQ(
        .clk (clk), // I //125MHz
        .sclr (sclr), // I
        
        .din_distributor (syn_bd_din_distributor), // I [31:0]
        .din_distributor_valid (syn_bd_din_distributor_valid), // I
        .distributor_ready (syn_bd_distributor_ready), // O
        
        .PIN_CS (RF_PIN30), // O        
        .PIN_CLK (RF_PIN31), // O       
        .PIN_DATA (RF_PIN32), // O      
        .PIN_SYNC (RF_PIN33) // O       
);
// ---------------------------------------------------------------------------------
// SYN_Board  SN74HCS165BQB(Shift register - load) RF_PIN 19,12,13 
// ---------------------------------------------------------------------------------

CON_SN74HCS165BQB SYN_BD_M3_SN74HCS165BQB(
    .clk(clk),		//125Mhz I
	.sclr(sclr),   // I
	.din_sts_valid(syn_bd_din_sts_valid),    // I
	.din_sts_ready(syn_bd_din_sts_ready),   // O
	.dout_sts(syn_bd_dout_sts),   // O [15:0]
	.dout_sts_valid(syn_bd_dout_sts_valid),  // O
	
  	.PIN_CS(RF_PIN19), // O
	.PIN_CLK(RF_PIN12),	 // O
	.PIN_DATA(RF_PIN13)      //I
);

// ---------------------------------------------------------------------------------
// SYN_Board LMX2594(PLL) RF_PIN 16,17,20,21,22,23
// --------------------------------------------------------------------------------
// LMX2594 PLL, - Timing 
CON_LMX2594RHAT SYN_BD_M4_LMX2594RHAT(
    .clk(clk),// I
    .sclr(sclr), // I
    
    .din_syn_cmd(syn_bd_din_pll_cmd), // I [1:0]     
    .din_syn(syn_bd_din_pll), // I [23:0]
    .din_syn_valid(syn_bd_din_pll_valid),	// I
    .syn_ctrl_interval(syn_bd_pll_ctrl_interval),	// I [15:0]
	.syn_last_cmd_in(syn_bd_pll_last_cmd_in), // O
	.syn_cmd_done(syn_bd_pll_cmd_done), // O
	.dout_syn(syn_bd_dout_pll), // O [23:0]
	.dout_syn_valid(syn_bd_dout_pll_valid), // O 
	
	.PIN_CS1_L(RF_PIN16), // O
	.PIN_CS1_H(RF_PIN17), // O 
	.PIN_CS2_A(RF_PIN20), // O
	.PIN_CS2_B(RF_PIN21), // O 	
	.PIN_CLK(RF_PIN22), // O
	.PIN_IO(RF_PIN23)	 // IO
);

// ---------------------------------------------------------------------------------
// SYN_Board PE43610(Digital Atten.) RF_PIN 25,26,27 
// ---------------------------------------------------------------------------------

CON_PE43610 SYN_BD_M5_PE43610(
      .clk(clk),		//125Mhz
	  .sclr(sclr),
	  .din_att(syn_bd_din_att),   // [15:0] I
	  .din_att_valid(syn_bd_din_att_valid), // I
	  .att_ready(syn_bd_att_ready),	// O
	  .PIN_CS(RF_PIN25),    // O
      .PIN_CLK(RF_PIN26),	 // O  
	  .PIN_DATA(RF_PIN27)       // O
    );

// ---------------------------------------------------------------------------------
// SYN_Board TMP102  RF_PIN 28,29 
// --------------------------------------------------------------------------------

CON_TMP102 SYN_BD_M6_TMP102(
    .clk(clk),
    .sclr(sclr),
	.din_tmp102(syn_bd_din_tmp102), // I [1:0]
	.din_tmp102_valid(syn_bd_din_tmp102_valid), // I
	.dout_tmp102(syn_bd_dout_tmp102), // O [15:0]
	.busy_tmp102(syn_bd_busy_tmp102), // O
    .PIN_SCL(RF_PIN29), // O
    .PIN_SDA_IO(RF_PIN28) // IO
);
// tmp102  - I2C
endmodule
