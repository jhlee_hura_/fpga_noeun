`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/11/30 17:50:28
// Design Name: 
// Module Name: gc_cntl_pack
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module gc_cntl_pack(
    input wire         clk,
	input wire         sclr,

	input wire			lna_en,
	input wire			lna_en_valid,
	input wire [4:0]	satt,
	input wire			satt_valid,
    input wire [6:0]	gatt1,
	input wire			gatt1_valid,
	input wire [6:0]	gatt2,
	input wire			gatt2_valid,

	input wire [4:0]	att_cha_1,
	input wire			att_cha_1_valid,
	input wire [4:0]	att_cha_2,
	input wire			att_cha_2_valid,
	
	input wire [4:0]	att_chb_1,
	input wire			att_chb_1_valid,	
	input wire [4:0]	att_chb_2,
	input wire			att_chb_2_valid,
	
	input wire 			att_ready,

	output reg			gc_lna,
	output reg [15:0]	gc_att,
	output reg			gc_att_valid
);


localparam WAIT_CNT_VALUE = 'd120;	// 8 * 120 => 960ns 

// ADDRESSS for ATT chip
localparam SATT	    	= 3'b000;	// [0:2] = 000;
localparam GATT1    	= 3'b001;	// [0:2] = 001;
localparam GATT2	    = 3'b010;	// [0:2] = 010;
localparam ATT_CHA_1	= 3'b011;	// [0:2] = 011;
localparam ATT_CHA_2	= 3'b100;   // [0:2] = 100;
localparam ATT_CHB_1	= 3'b101;   // [0:2] = 101;
localparam ATT_CHB_2	= 3'b110;	// [0:2] = 110;


// LN Switch
always @(posedge clk) begin
	if (sclr) begin
		gc_lna		<= 1'b0;

	end
	else begin
		gc_lna	<= (lna_en_valid) ? lna_en : gc_lna;
	end
end


// -------------------------------------------------------------------------
// ATT data get
reg [4:0]	r_satt;
reg			r_satt_flag;

reg [4:0]	r_att_cha_1;
reg			r_att_cha_1_flag;
reg [4:0]	r_att_cha_2;
reg			r_att_cha_2_flag;

reg [4:0]	r_att_chb_1;
reg			r_att_chb_1_flag;
reg [4:0]	r_att_chb_2;
reg			r_att_chb_2_flag;

reg [6:0]	r_gatt1;
reg			r_gatt1_flag;
reg [6:0]	r_gatt2;
reg			r_gatt2_flag;


reg	[9:0]	din_fifo;
reg			wr_en;

reg		    satt_write;
reg		    att_cha_1_write;
reg		    att_chb_1_write;
reg		    att_cha_2_write;
reg		    att_chb_2_write;
reg		    gatt1_write;
reg		    gatt2_write;


always @(posedge clk) begin
	if (sclr) begin
		r_satt <= 5'b0;
		r_satt_flag <= 1'b0;
		r_att_cha_1 <= 5'b0;
		r_att_cha_1_flag <= 1'b0;
		r_att_cha_2 <= 5'b0;
		r_att_cha_2_flag <= 1'b0;
		r_att_chb_1 <= 5'b0;
		r_att_chb_1_flag <= 1'b0;		
		r_att_chb_2 <= 5'b0;
		r_att_chb_2_flag <= 1'b0;
		
		r_gatt1 <= 7'b0;
		r_gatt1_flag <= 1'b0;
		r_gatt2 <= 7'b0;
		r_gatt2_flag <= 1'b0;

		din_fifo <= 10'b0;
		wr_en <= 1'b0;

	end
	else begin
		r_satt 			    <=	(satt_valid) ? satt : r_satt;
		r_satt_flag 	    <=	(satt_valid) ? 1'b1 :
							     (satt_write) ? 1'b0 : r_satt_flag;
		r_att_cha_1 		<=	(att_cha_1_valid) ? att_cha_1 : r_att_cha_1;
		r_att_cha_1_flag 	<=	(att_cha_1_valid) ? 1'b1 :
							     (att_cha_1_write) ? 1'b0 : r_att_cha_1_flag;
		r_att_cha_2 		<=	(att_cha_2_valid) ? att_cha_2 : r_att_cha_2;
		r_att_cha_2_flag 	<=	(att_cha_2_valid) ? 1'b1 :
							     (att_cha_2_write) ? 1'b0 : r_att_cha_2_flag;
		r_att_chb_1 		<=	(att_chb_1_valid) ? att_chb_1 : r_att_chb_1;
		r_att_chb_1_flag 	<=	(att_chb_1_valid) ? 1'b1 :
							     (att_chb_1_write) ? 1'b0 : r_att_chb_1_flag;							
		r_att_chb_2 		<=	(att_chb_2_valid) ? att_chb_2 : r_att_chb_2;
		r_att_chb_2_flag 	<=	(att_chb_2_valid) ? 1'b1 :
							     (att_chb_2_write) ? 1'b0 : r_att_chb_2_flag;
							
		r_gatt1			<=	(gatt1_valid) ? gatt1 : r_gatt1;
		r_gatt1_flag	<=	(gatt1_valid) ? 1'b1 :
							(gatt1_write) ? 1'b0 : r_gatt1_flag;
		r_gatt2			<=	(gatt2_valid) ? gatt2 : r_gatt2;
		r_gatt2_flag	<=	(gatt2_valid) ? 1'b1 :
							(gatt2_write) ? 1'b0 : r_gatt2_flag;

		if (r_satt_flag && ~satt_write) begin
			din_fifo <= {2'b0, r_satt[0],r_satt[1], r_satt[2],r_satt[3], r_satt[4], SATT};
			wr_en <= 1'b1;
			satt_write		    <= 1'b1;
			att_cha_1_write		<= 1'b0;
		    att_cha_2_write		<= 1'b0;			
			att_chb_1_write		<= 1'b0;
			att_chb_2_write		<= 1'b0;			
			gatt1_write		    <= 1'b0;
			gatt2_write		    <= 1'b0;

		end
		else if (r_att_cha_1_flag && ~att_cha_1_write) begin
			din_fifo <= {2'b0, r_att_cha_1[0],r_att_cha_1[1], r_att_cha_1[2],r_att_cha_1[3], r_att_cha_1[4], ATT_CHA_1};
			wr_en <= 1'b1;
			satt_write 		    <= 1'b0;
			att_cha_1_write 	<= 1'b1;
			att_cha_2_write 	<= 1'b0;			
			att_chb_1_write 	<= 1'b0;
			att_chb_2_write 	<= 1'b0;			
			gatt1_write 		<= 1'b0;
			gatt2_write 	    <= 1'b0;

		end
		else if (r_att_cha_2_flag && ~att_cha_2_write) begin
			din_fifo <= {2'b0, r_att_cha_2[0],r_att_cha_2[1], r_att_cha_2[2],r_att_cha_2[3], r_att_cha_2[4], ATT_CHA_2};
			wr_en <= 1'b1;
			satt_write 		    <= 1'b0;
			att_cha_1_write 	<= 1'b0;
			att_cha_2_write 	<= 1'b1;
			att_chb_1_write 	<= 1'b0;			
			att_chb_2_write 	<= 1'b0;			
			gatt1_write 		<= 1'b0;
			gatt2_write 	    <= 1'b0;

		end
		else if (r_att_chb_1_flag && ~att_chb_1_write) begin
			din_fifo <= {2'b0, r_att_chb_1[0],r_att_chb_1[1], r_att_chb_1[2],r_att_chb_1[3], r_att_chb_1[4], ATT_CHB_1};
			wr_en <= 1'b1;
			satt_write 		    <= 1'b0;
			att_cha_1_write 	<= 1'b0;
			att_cha_2_write 	<= 1'b0;
			att_chb_1_write 	<= 1'b1;			
			att_chb_2_write 	<= 1'b0;			
			gatt1_write 		<= 1'b0;
			gatt2_write 	    <= 1'b0;

		end	
		else if (r_att_chb_2_flag && ~att_chb_2_write) begin
			din_fifo <= {2'b0, r_att_chb_2[0],r_att_chb_2[1], r_att_chb_2[2],r_att_chb_2[3], r_att_chb_2[4], ATT_CHB_2};
			wr_en <= 1'b1;
			satt_write 		    <= 1'b0;
			att_cha_1_write 	<= 1'b0;
			att_cha_2_write 	<= 1'b0;
			att_chb_1_write 	<= 1'b0;			
            att_chb_2_write 	<= 1'b1;			
			gatt1_write 		<= 1'b0;
			gatt2_write 	    <= 1'b0;

		end				
		else if (r_gatt1_flag && ~gatt1_write) begin
			din_fifo <= {r_gatt1[0], r_gatt1[1], r_gatt1[2], r_gatt1[3], r_gatt1[4], r_gatt1[5], r_gatt1[6], GATT1};
			wr_en <= 1'b1;
			satt_write 		    <= 1'b0;
			att_cha_1_write 	<= 1'b0;
			att_cha_2_write 	<= 1'b0;			
			att_chb_1_write 	<= 1'b0;
			att_chb_2_write 	<= 1'b0;			
			gatt1_write 		<= 1'b1;
			gatt2_write 	    <= 1'b0;

		end
		else if (r_gatt2_flag && ~gatt2_write) begin
			din_fifo <= {r_gatt2[0], r_gatt2[1], r_gatt2[2], r_gatt2[3], r_gatt2[4], r_gatt2[5], r_gatt2[6], GATT2};
			wr_en <= 1'b1;
			satt_write 		    <= 1'b0;
			att_cha_1_write 	<= 1'b0;
			att_cha_2_write 	<= 1'b0;			
			att_chb_1_write 	<= 1'b0;
			att_chb_2_write 	<= 1'b0;			
			gatt1_write 		<= 1'b0;
			gatt2_write 	    <= 1'b1;

		end
		else begin
			wr_en <= 1'b0;
			satt_write 		    <= 1'b0;
			att_cha_1_write 	<= 1'b0;
			att_cha_2_write 	<= 1'b0;			
			att_chb_1_write 	<= 1'b0;
			att_chb_2_write 	<= 1'b0;	
			gatt1_write 	    <= 1'b0;
			gatt2_write 	    <= 1'b0;

		end
	end
end


// -------------------------------------------------------------------------
// ATT data fifo
reg        rd_en;
wire [9:0] dout_fifo;
wire       empty;
wire       ff_valid;

fifosd_10x16 fifo_attcmd (
  .clk(clk),				// I
  .rst(sclr),				// I
  .din(din_fifo),			// I [9 : 0]
  .wr_en(wr_en),			// I
  .rd_en(rd_en),  			// I
  .dout(dout_fifo),			// O [9 : 0]
  .full(),    				// O
  .empty(empty),  			// O
  .valid(ff_valid)  		// O
);

wire [15:0] att_data;
assign att_data = {dout_fifo[9:3],1'b0,dout_fifo[2:0],5'b0};


always @(posedge clk) begin
	gc_att			<= att_data;
	gc_att_valid	<= ff_valid;
end

localparam IDLE_STAT 		= 1'b0;
localparam WAIT_CMD_STAT 	= 1'b1;

reg [6:0] wait_cnt;
reg       state;

// ?��?���? ?��?�� cs ?��?�� 발생
always @(posedge clk) begin
	if (sclr) begin
		wait_cnt 	<= 'b0;
		rd_en		<= 1'b0;
		state 		<= IDLE_STAT;
	end
	else begin
		case(state)
			IDLE_STAT : begin
				wait_cnt 		<= 'b0;
				if (~empty && att_ready) begin	// user ?�� mgc ?��
					rd_en 		<= 1'b1;
					state 		<= WAIT_CMD_STAT;
				end
				else begin
					rd_en 		<= 1'b0;
					state 		<= IDLE_STAT;
				end
			end

			WAIT_CMD_STAT : begin
				rd_en		<= 1'b0;
				wait_cnt 	<= (att_ready) ? wait_cnt + 1'b1 : 'b0;
				if (wait_cnt >= WAIT_CNT_VALUE)
					state 		<= IDLE_STAT;
				else
					state 		<= WAIT_CMD_STAT;
			end
		endcase
	end
end
endmodule
