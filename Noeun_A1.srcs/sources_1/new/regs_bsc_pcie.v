`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/12/01 10:20:16
// Design Name: 
// Module Name: regs_bsc_pcie
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module regs_bsc_pcie(
	input	wire				pcie_clk,
	input	wire				sclr,

	input	wire				regs_sel,
	input	wire	[46:0]		regs_adda,
	input	wire				regs_adda_valid,
	output	wire	[31:0]		regs_dout,
	output	wire				regs_dout_valid,

	// -------------------------------------------------------
	// -- register ?���?
	// DMA_DATA_SEL_REG
	output reg		[1:0]		data_sel,
	output reg					test_cnt_clr,
	// DMA_FIFO_EMPTY_REG
	input wire					dma_fifo_empty,

	// PCIE_DMA_C2H_0_CTRL_REG
	output	wire				c2h_0_rst,
	// PCIE_DMA_C2H_0_SRCADDR_REG
	output	reg		[31:0]		c2h_0_src_addr = `PCIE_DMA_C2H_SRCADDR_DEF,
	// PCIE_DMA_C2H_0_LEN_REG
	output	reg		[27:0]		c2h_0_len = `DMA_LEN_DEF,
	// PCIE_DMA_C2H_0_STAT_REG
	input wire		[7:0]		c2h_0_sts,

	// PCIE_DMA_C2H_0_BUSY_CNT_REG
	input	wire	[31:0]		c2h_0_busy_clk_cnt,
	// PCIE_DMA_C2H_0_RUN_CNT_REG
	input	wire	[31:0]		c2h_0_run_clk_cnt,
	// PCIE_DMA_C2H_0_PACKET_CNT_REG
	input	wire	[31:0]		c2h_0_packet_cnt,
	// PCIE_DMA_C2H_0_DESC_CNT_REG
	input	wire	[31:0]		c2h_0_desc_cnt,

	// PCIE_DMA_H2C_0_STAT_REG
	input wire		[7:0]		h2c_0_sts


    );
// (* mark_debug = "true" *)
reg				regout_we;
reg				regout_we1;

wire	[15:0]	addr;
wire			wr_en;
wire	[31:0]	wr_d;
reg		[31:0]	rd_d;

assign regs_dout		= rd_d;
assign regs_dout_valid	= regout_we;

always @(posedge pcie_clk) begin
	regout_we1	<= ~regs_adda[46] & (regs_sel & regs_adda_valid);
	regout_we	<= regout_we1;
end

// reg_adda[46]= 1:wr_en, 0:rd_en
// reg_adda[45:32] = 14bits addr
// reg_adda[31:0] = 32bits data
assign wr_en	= regs_adda[46] & (regs_sel & regs_adda_valid);
assign addr		= {regs_adda[45:32], 2'b0};	// 14bits to 16bits address
assign wr_d		= regs_adda[31:0];

reg	c2h_0_rst_r;
always @(posedge pcie_clk ) begin
	if (sclr) begin
		rd_d			<= 32'b0;
		// DMA_DATA_SEL_REG
		data_sel		<= 'b0;
		test_cnt_clr	<= 1'b0;
		// PCIE_DMA_C2H_0_CTRL_REG
		c2h_0_rst_r	<= 1'b0;
		// PCIE_DMA_C2H_0_SRCADDR_REG
		c2h_0_src_addr	<= `PCIE_DMA_C2H_SRCADDR_DEF;
		// PCIE_DMA_C2H_0_LEN_REG
		c2h_0_len			<= `DMA_LEN_DEF;

	end
	else begin
		case (addr)
			`DMA_DATA_SEL_REG : begin
				rd_d[1:0] <= data_sel;
				rd_d[31:2]	<= 30'b0;
				if (wr_en) begin
					data_sel <= wr_d[1:0];
					test_cnt_clr <= wr_d[8];
				end
			end

			`DMA_FIFO_EMPTY_REG : begin
				rd_d[0] <= dma_fifo_empty;
				rd_d[31:1]	<= 31'b0;
			end

			`PCIE_DMA_C2H_0_CTRL_REG : begin
				rd_d[31:0] <= 32'b0;
				if (wr_en) begin
					c2h_0_rst_r <= wr_d[0];
				end
			end

			`PCIE_DMA_C2H_0_SRCADDR_REG : begin
				rd_d[31:0] <= c2h_0_src_addr;
				if (wr_en) begin
					c2h_0_src_addr <= wr_d[31:0];
				end
			end

			`PCIE_DMA_C2H_0_LEN_REG : begin
				rd_d[27:0] <= c2h_0_len;
				rd_d[31:28]	<= 4'b0;
				if (wr_en) begin
					c2h_0_len <= wr_d[27:0];
				end
			end

			`PCIE_DMA_C2H_0_STAT_REG : begin
				rd_d[7:0]	<= c2h_0_sts;
				rd_d[31:8]	<= 24'b0;
			end

			`PCIE_DMA_C2H_0_BUSY_CNT_REG : begin
				rd_d[31:0]	<= c2h_0_busy_clk_cnt;
			end

			`PCIE_DMA_C2H_0_RUN_CNT_REG : begin
				rd_d[31:0]	<= c2h_0_run_clk_cnt;
			end

			`PCIE_DMA_C2H_0_PACKET_CNT_REG : begin
				rd_d[31:0]	<= c2h_0_packet_cnt;
			end

			`PCIE_DMA_C2H_0_DESC_CNT_REG : begin
				rd_d[31:0]	<= c2h_0_desc_cnt;
			end

			`PCIE_DMA_H2C_0_STAT_REG : begin
				rd_d[7:0]	<= h2c_0_sts;
				rd_d[31:8]	<= 24'b0;
			end
                      			

			default: begin
				rd_d <= 32'b0;
			end
		endcase

		////////////////////////////////////////////////
		// ?��?�� ??�?.

		if (!wr_en) begin
			c2h_0_rst_r <= 1'b0;
			test_cnt_clr <= 1'b0;
		end
	end
end

pls_expander #(
	.COUNT(`C2H_RST_PCIECLK),
	.POLARITY(1)
) c2h_0_rst_ext(
	.sin(c2h_0_rst_r),
	.clk(pcie_clk),
	.sout(c2h_0_rst)
);
endmodule
