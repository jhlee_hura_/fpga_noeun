`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/11/30 14:01:23
// Design Name: 
// Module Name: Noeun_A1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Noeun_A1(
// HP Bank 64 (X)
//-------------------------------------	

// HP Bank 65 
//-------------------------------------	

	//  TMP102 IN
	output	wire 		TMP_SCL_IN,	inout	wire 		TMP_SDA_IN,
	//  TMP102 Out
	output	wire 		TMP_SCL_EX,	inout	wire 		TMP_SDA_EX,	
	
// HP Bank 66 (X)
//-------------------------------------		

	
// HP Bank 67 
//-------------------------------------		
    // ADS42JB49
    input wire  OVRA,    input wire  OVRB,
    input wire  ADC_SDOUT,    
    output wire ADC_CS_B, // ADC_SEN    
    output wire ADC_SCLK,
    output wire ADC_SDATA,
    //output wire SYNCINB_P,    output wire SYNCINB_N,
    
    // LMK04828 -> FPGA
    input wire  SYSREF_B_P,    input wire  SYSREF_B_N,    
    input wire  CLK_C_P,    input wire  CLK_C_N, 
    	
// HD Bank 84
//-------------------------------------
	
    // LMK04828	
	output wire    LMK_RESET,
	output wire    LMK_SYNC,	output wire    LMK_CS_B,	output wire    LMK_SCK,	inout wire     LMK_SDIO,
		
	// UART
	output wire    F_UART_TXD,	input  wire    F_UART_RXD,
	// FAN CTRL
	output wire    FAN_CTRL,
	// HMC 1033
	output wire    ADCCLK_SEN,	output wire    ADCCLK_SDI,	input wire     ADCCLK_SDO,	  output wire    ADCCLK_SCK,
	
    // Processor -> FPGA RST(?)	
	input wire    FPGA_INT,
		     
	//GPS 
	input wire		GPS_LOCK,	input wire		GPS_HOLDOVER,	input wire		GPS_PPS,

	// Ext. GPIO
	inout	wire [3:0]	EXT_GPIO,

	//LED  
	output wire   	SP_RUN0_N,	// Fault LED
	output wire    	SP_RUN1_N,	output wire    	SP_RUN2_N,	output wire    	SP_RUN3_N,
	
// HD Bank 86 
//-------------------------------------	
//PE43610 - Digi. atten
//SN74HCS595BQBR - 8bit shift register
//SN74HCS165BQB - 8bit parallel-load shift register
//LMV321AIDCKR - OPAMP
//LMK01010ISQ - CLK Buffer
//LMX2594RHAT - PLL
//PE43610A-X - Digi. atten
//TMP102 - Temperature
	// RF - ADC
	// PE43610(PIN 0,1,4) , SN74HCS595BQBR(RF CTRL 2,3,6)	, LNA Switch (5)
	output 	RF_PIN0,	output	RF_PIN1,	output	RF_PIN2,	output	RF_PIN3,
	output	RF_PIN4,	output	RF_PIN5,	output	RF_PIN6,	
	
	// SN74HCS595BQBR(CM 7,10,11), TMP102(8,9)
	output	RF_PIN7,	output	RF_PIN8,	inout	RF_PIN9,	output	RF_PIN10,	input	RF_PIN11,
	
	
	// RF - SN74HCS165BQB(12,13,19), SN74HCS595BQBR(14,15,18), LMX2594RHAT(16,17,20,21,22,23)
	output		RF_PIN12,	output		RF_PIN13,	output		RF_PIN14,	output		RF_PIN15,
	output		RF_PIN16,	output		RF_PIN17,	output		RF_PIN18,	output		RF_PIN19,
	output		RF_PIN20,	output		RF_PIN21,	output		RF_PIN22,	output		RF_PIN23,
	
// HD Bank 87
//-------------------------------------	
    // RF - PE43610A-X(25,26,27) TMP102(28,29) LMK01010ISQ(30,31,32,33) LMV321AIDCKR(34,35)
	output		RF_PIN24,	output		RF_PIN25,	output		RF_PIN26,	output		RF_PIN27,
	output		RF_PIN28,	inout		RF_PIN29,	output		RF_PIN30,	output		RF_PIN31,	
	output		RF_PIN32,	inout		RF_PIN33,	input		RF_PIN34,	input		RF_PIN35,	
	// Not used
	output		RF_PIN36,	inout		RF_PIN37,	output		RF_PIN38,	output		RF_PIN39,	
	output		RF_PIN40,	inout		RF_PIN41,	output		RF_PIN42,	output		RF_PIN43,	
	output		RF_PIN44,	inout		RF_PIN45,	output		RF_PIN46,	output		RF_PIN47,
							
// MGT Bank 224 (X)
//-------------------------------------	

// MGT Bank 225
//-------------------------------------	> Open Synthesized Design I/O ports
    input wire        sys_clk_p,
    input wire        sys_clk_n,
    input wire        sys_rst_n,
    
    input wire  [3:0] pci_exp_rxp,
    input wire  [3:0] pci_exp_rxn,
    output wire [3:0] pci_exp_txp,
    output wire [3:0] pci_exp_txn,
	

// MGT Bank 226 (X)
//-------------------------------------

// MGT Bank 227 
//-------------------------------------	> Open Synthesized Design I/O ports
	//ADS42JB49 -> FPGA
    input wire [3:0] SERDOUT_P,
    input wire [3:0] SERDOUT_N,
    
    input CLK_P,
    input CLK_N
	

    );


//==========================================================================================================
// Draft

	// -- to/from REGS
	wire [46:0]  regs_adda;
	wire         regs_adda_valid;
	wire         regs_bsc_sys_sel;
	wire         regs_bsc_sig_sel;
	wire         regs_bsc_pcie_sel;
	wire         regs_ud_sys_sel;
	wire         regs_ud_sig_sel;


	wire [31:0]  regs_bsc_sys_data;
	wire [31:0]  regs_bsc_sig_data;
	wire [31:0]  regs_bsc_pcie_data;
	wire [31:0]  regs_ud_sys_data;
	wire [31:0]  regs_ud_sig_data;
	wire         regs_data_valid;

    wire [7:0]   c2h_sts_0;
    wire [7:0]   h2c_sts_0;
    

    
    // AXI streaming ports
    wire [127:0] m_axis_h2c_tdata_0;
    wire         m_axis_h2c_tlast_0;
    wire         m_axis_h2c_tvalid_0;
    wire         m_axis_h2c_tready_0;
    wire [15:0]  m_axis_h2c_tkeep_0;
    wire [127:0] s_axis_c2h_tdata_0; 
    wire         s_axis_c2h_tlast_0;
    wire         s_axis_c2h_tvalid_0;
    wire         s_axis_c2h_tready_0;
    wire [15:0]  s_axis_c2h_tkeep_0; 

    wire         pcie_clk;
    wire         user_resetn;
    wire         user_lnk_up; 


//==========================================================================================================


  //----------------------------------------------------------------------------------------------------------------//
  //    System(SYS) Interface                                                                                       //
  //----------------------------------------------------------------------------------------------------------------//

    wire         sys_clk;
    wire         sys_clk_gt;
    wire [2:0]   msi_vector_width;
    wire         msi_enable;
    reg  		  usr_irq_req = 0;
    wire  		  usr_irq_ack;


//////////////////////////////////////////////////  LITE
   //-- AXI Master Write Address Channel
    wire [31:0]  m_axil_awaddr;
    wire [2:0]   m_axil_awprot;
    wire         m_axil_awvalid;
    wire         m_axil_awready;
    //-- AXI Master Write Data Channel
    wire [31:0]  m_axil_wdata;
    wire [3:0]   m_axil_wstrb;
    wire         m_axil_wvalid;
    wire         m_axil_wready;
    //-- AXI Master Write Response Channel
    wire         m_axil_bvalid;
    wire [1:0]   m_axil_bresp;    
    wire         m_axil_bready;
    //-- AXI Master Read Address Channel
    wire [31:0]  m_axil_araddr;
    wire [2:0]   m_axil_arprot;
    wire         m_axil_arvalid;
    wire         m_axil_arready;
    //-- AXI Master Read Data Channel
    wire [31:0]  m_axil_rdata;
    wire [1:0]   m_axil_rresp;
    wire         m_axil_rvalid;
    wire         m_axil_rready;




//==========================================================================================================



  // Ref clock buffer for DMA_PCIe Module
  IBUFDS_GTE4 refclk_ibuf (
      .O(sys_clk_gt), 
      .ODIV2(sys_clk), 
      .I(sys_clk_p), 
      .CEB(1'b0), 
      .IB(sys_clk_n)
  );
  
  // Core Top Level Wrapper
  xdma_0 PCIE_DMA_M 
     (
      //---------------------------------------------------------------------------------------//
      //  PCI Express (pci_exp) Interface                                                      //
      //---------------------------------------------------------------------------------------//
      .sys_clk          (sys_clk),          // I
      .sys_clk_gt       (sys_clk_gt),       // I
      .sys_rst_n        (1'b1),        // I sys_rst_n
      .user_lnk_up      (user_lnk_up),      // O
      
      // Tx
      .pci_exp_txn      (pci_exp_txn),      // O
      .pci_exp_txp      (pci_exp_txp),      // O
      
      // Rx
      .pci_exp_rxn      (pci_exp_rxn),      // I
      .pci_exp_rxp      (pci_exp_rxp),      // I
      
      //-- AXI Global
      .axi_aclk         (pcie_clk),         // O
      .axi_aresetn      (user_resetn),      // O 

      // IRQ, MSI
      .usr_irq_req      (usr_irq_req),      // I
      .usr_irq_ack      (usr_irq_ack),      // O
      .msi_enable       (msi_enable),       // O
      .msi_vector_width (msi_vector_width), // O
      
     
      // LITE interface   
      //-- AXI Master Write Address Channel
      .m_axil_awaddr    (m_axil_awaddr),    // O [31:0]
      .m_axil_awprot    (),    // O  [2:0]
      .m_axil_awvalid   (m_axil_awvalid),   // O
      .m_axil_awready   (m_axil_awready),   // I 
      //-- AXI Master Write Data Channel
      .m_axil_wdata     (m_axil_wdata),     // O [31:0]
      .m_axil_wstrb     (m_axil_wstrb),     // O [3:0]
      .m_axil_wvalid    (m_axil_wvalid),    // O
      .m_axil_wready    (m_axil_wready),    // I
      //-- AXI Master Write Response Channel
      .m_axil_bvalid    (m_axil_bvalid),    // I
      .m_axil_bresp     (m_axil_bresp),     // I [1:0]
      .m_axil_bready    (m_axil_bready),    // O
      //-- AXI Master Read Address Channel
      .m_axil_araddr    (m_axil_araddr),    // O [31:0]
      .m_axil_arprot    (),    // O [2:0]
      .m_axil_arvalid   (m_axil_arvalid),   // O 
      .m_axil_arready   (m_axil_arready),   // I
      //-- AXI Master Read Data Channel     
      .m_axil_rdata     (m_axil_rdata),     // I [31:0]
      .m_axil_rresp     (m_axil_rresp),     // I [1:0]
      .m_axil_rvalid    (m_axil_rvalid),    // I
      .m_axil_rready    (m_axil_rready),    // O
      

      // Config managemnet interface
      .cfg_mgmt_addr            (19'b0),
      .cfg_mgmt_write           (1'b0),
      .cfg_mgmt_write_data      (32'b0),
      .cfg_mgmt_byte_enable     (4'b0),
      .cfg_mgmt_read            (1'b0),
      .cfg_mgmt_read_data       (),
      .cfg_mgmt_read_write_done (),

      // AXI streaming ports
      .s_axis_c2h_tdata_0       (128'b0),  
      .s_axis_c2h_tlast_0       (1'b0),
      .s_axis_c2h_tvalid_0      (1'b0), 
      .s_axis_c2h_tready_0      (),
      .s_axis_c2h_tkeep_0       (16'b0),
      .m_axis_h2c_tdata_0       (),
      .m_axis_h2c_tlast_0       (),
      .m_axis_h2c_tvalid_0      (),
      .m_axis_h2c_tready_0      (1'b0),
      .m_axis_h2c_tkeep_0       (),

      // DMA Status
      .c2h_sts_0                (c2h_sts_0),    // O  [7:0]
      .h2c_sts_0                (h2c_sts_0)

  
    );
wire			regs_data_ready;
wire            regs_ud_data_valid;
wire            regs_bsc_data_valid;
// interface to axil - regs
assign regs_data_valid = regs_ud_data_valid |regs_bsc_data_valid;
axil2regs PCIE_2_REGS (
	.clk			(pcie_clk),			// I
	.sclr			(~user_resetn),		// I

	//-- AXI LITE
	//-- AXI Slave interface
	.s_axil_awaddr	(m_axil_awaddr[15:2]),	// I [13:0] write address, From PCIe
	.s_axil_awvalid	(m_axil_awvalid),		// I
	.s_axil_awready	(m_axil_awready),		// O

	.s_axil_wdata	(m_axil_wdata),			// I [31:0] write date, From PCIe
	.s_axil_wvalid	(m_axil_wvalid),		// I
	.s_axil_wready	(m_axil_wready),		// O

	.s_axil_bresp	(m_axil_bresp),			// O [1:0] write response, To PCIe
	.s_axil_bvalid	(m_axil_bvalid),		// O
	.s_axil_bready	(m_axil_bready),		// I

	.s_axil_araddr	(m_axil_araddr[15:2]),	// I [13:0] Read address, From PCIe
	.s_axil_arvalid	(m_axil_arvalid),		// I
	.s_axil_arready	(m_axil_arready),		// O

	.s_axil_rdata	(m_axil_rdata),			// O [31:0] Read data, To PCIe 
	.s_axil_rresp	(m_axil_rresp),			// O [1:0]
	.s_axil_rvalid	(m_axil_rvalid),		// O
	.s_axil_rready	(m_axil_rready),		// I

	// -------------------------------------------------------
	.regs_adda			(regs_adda),			// O [46:0]
	.regs_adda_valid	(regs_adda_valid),		// O
	.regs_bsc_sys_sel	(regs_bsc_sys_sel),		// O
	.regs_bsc_sig_sel	(regs_bsc_sig_sel),		// O
	.regs_bsc_pci_sel	(regs_bsc_pcie_sel),		// O
	.regs_ud_sys_sel	(regs_ud_sys_sel),		// O
	.regs_ud_sig_sel	(regs_ud_sig_sel),		// O


	.regs_bsc_sys_data	(regs_bsc_sys_data),	// I [31:0]
	.regs_bsc_sig_data	(regs_bsc_sig_data),	// I [31:0]
	.regs_bsc_pci_data	(regs_bsc_pcie_data),	// I [31:0]
	.regs_ud_sys_data	(regs_ud_sys_data),		// I [31:0]
	.regs_ud_sig_data	(32'h0000),		// I [31:0]

	.regs_data_valid	(regs_data_valid),		// I
	.regs_data_ready	(regs_data_ready)		// O	
);

//==========================================================================================================
wire u_sys_clk;
wire sys_clk_locked;

clk_wiz_b u_sys_clk_gen (
    // Clock out ports
    .clk_out1(),	// O
    .clk_out2(u_sys_clk),     		// O User Sys clock
    // Status and control signals
    .locked(sys_clk_locked),		// O
 // Clock in ports
    .clk_in1(pcie_clk)
);      		// I

bsc_saturn_top(

    .rst(1'b0),       // I
    .u_sys_clk(u_sys_clk), // I
    .u_sig_clk(u_sys_clk), // I
    .pcie_clk(pcie_clk), // I
    

    .regs_adda(regs_adda), // I [46:0]
    .regs_adda_valid(regs_adda_valid),   // I 
    .regs_bsc_data_valid(regs_bsc_data_valid),    // O
    .regs_data_ready(regs_data_ready),      // I
    
    .regs_bsc_sys_data(regs_bsc_sys_data),  // O [31:0]  
    .regs_bsc_sys_sel(regs_bsc_sys_sel), // I
    
    .regs_bsc_sig_data(regs_bsc_sig_data),  // O [31:0]    
    .regs_bsc_sig_sel(regs_bsc_sig_sel), // I    
    
    .regs_bsc_pcie_data(regs_bsc_pcie_data), // O [31:0]
    .regs_bsc_pcie_sel(regs_bsc_pcie_sel), // I   
    .c2h_sts_0(c2h_sts_0), // I [7:0]
    .h2c_sts_0(h2c_sts_0), // I [7:0]
// HP Bank 65 
//-------------------------------------	
	// TMP102 in 
	.TMP_SCL_IN(TMP_SCL_IN),   .TMP_SDA_IN(TMP_SDA_IN),	   // O    I
	// TMP102 out
	.TMP_SCL_EX(TMP_SCL_EX),   .TMP_SDA_EX(TMP_SDA_EX),	   // O   I	
	
// HP Bank 67 
//-------------------------------------		
        // ADS42JB49
      .OVRA(OVRA),   .OVRB(OVRB),   // I   I
      .ADC_SDOUT(ADC_SDOUT),    .ADC_CS_B(ADC_CS_B), // I     O, ADC_SEN
      .ADC_SCLK(ADC_SCLK),  .ADC_SDATA(ADC_SDATA), //   O     O
      //.SYNCINB_P(SYNCINB_P), .SYNCINB_N(SYNCINB_N), // O   O
    
        // LMK04828 -> FPGA
       .SYSREF_B_P(SYSREF_B_P), .SYSREF_B_N(SYSREF_B_N), // I    I    
       .CLK_C_P(CLK_C_P),  .CLK_C_N(CLK_C_N),    // I   I 

// HD Bank 84
//-------------------------------------
	
    // LMK04828	
	    .LMK_RESET(LMK_RESET), 	.LMK_SYNC(LMK_SYNC),   // O    O
        .LMK_CS_B(LMK_CS_B),    .LMK_SCK(LMK_SCK),    .LMK_SDIO(LMK_SDIO),  // O    O   IO
		
	// UART
	     .F_UART_TXD(F_UART_TXD),   .F_UART_RXD(F_UART_RXD),   // O    I
	// FAN CTRL
	     .FAN_CTRL(FAN_CTRL),  // O
	// HMC 1033
	     .ADCCLK_SEN(ADCCLK_SEN),  .ADCCLK_SDI(ADCCLK_SDI), .ADCCLK_SDO(ADCCLK_SDO),  .ADCCLK_SCK(ADCCLK_SCK),//   O   O   I   O	
	     
	//GPS 
	 		.GPS_LOCK(GPS_LOCK), .GPS_HOLDOVER(GPS_HOLDOVER),   .GPS_PPS(GPS_PPS),   // I   I   I
	// Ext. GPIO
	       .EXT_GPIO(EXT_GPIO), // I
	//LED  
	    	.SP_RUN0_N(SP_RUN0_N),	.SP_RUN1_N(SP_RUN1_N),  .SP_RUN2_N(SP_RUN2_N), 	.SP_RUN3_N(SP_RUN3_N)    //  O Fault LED    O   O   O       
	    	
	
 	  
);

ud_noeun_top(

    .rst(1'b0),       // I
    .u_sys_clk(u_sys_clk), // I
    .u_sig_clk(u_sys_clk), // I   
    .pcie_clk(pcie_clk), // I
    

    .regs_adda(regs_adda), // I [46:0]
    .regs_adda_valid(regs_adda_valid),   // I
    .regs_data_ready(regs_data_ready),      // I  
    .regs_data_valid(regs_ud_data_valid),    // O

    .regs_ud_sys_sel(regs_ud_sys_sel), // I    
    .regs_ud_sys_data(regs_ud_sys_data),  // O [31:0]
    
    .regs_ud_sig_sel(regs_ud_sig_sel), // I    
    .regs_ud_sig_data(regs_ud_sig_data),  // O [31:0]    
// HD Bank 86 
//-------------------------------------	

	// RF - ADC
	// PE43610(PIN 0,1,4) , SN74HCS595BQBR(RF CTRL 2,3,6)	, LNA Switch (5)
	 	.RF_PIN0(RF_PIN0),	.RF_PIN1(RF_PIN1),    .RF_PIN2(RF_PIN2),	.RF_PIN3(RF_PIN3), //O   O   O   O
	    .RF_PIN4(RF_PIN4),    .RF_PIN5(RF_PIN5),	   .RF_PIN6(RF_PIN6),      // O   O   O

	
	// SN74HCS595BQBR(CM 7,10,11), TMP102(8,9)
		.RF_PIN7(RF_PIN7),	 .RF_PIN8(RF_PIN8),	.RF_PIN9(RF_PIN9),	.RF_PIN10(RF_PIN10),  .RF_PIN11(RF_PIN11),  // O    O   IO	 O    I
   
		
	// RF - SN74HCS165BQB(12,13,19), SN74HCS595BQBR(14,15,18), LMX2594RHAT(16,17,20,21,22,23)
			.RF_PIN12(RF_PIN12), 	.RF_PIN13(RF_PIN13),   	.RF_PIN14(RF_PIN14),	.RF_PIN15(RF_PIN15),   // O    O    O    O  
	        .RF_PIN16(RF_PIN16),	.RF_PIN17(RF_PIN17),	.RF_PIN18(RF_PIN18),	.RF_PIN19(RF_PIN19),   // O    O    O    O
	        .RF_PIN20(RF_PIN20),    .RF_PIN21(RF_PIN21),	.RF_PIN22(RF_PIN22),	.RF_PIN23(RF_PIN23),   // O    O    O    O

	
// HD Bank 87
//-------------------------------------	
     // RF - PE43610A-X(25,26,27) TMP102(28,29) LMK01010ISQ(30,31,32,33) LMV321AIDCKR(34,35)
			.RF_PIN24(RF_PIN24),	.RF_PIN25(RF_PIN25),	.RF_PIN26(RF_PIN26),    .RF_PIN27(RF_PIN27), // O O O O
			.RF_PIN28(RF_PIN28),	.RF_PIN29(RF_PIN29),    .RF_PIN30(RF_PIN30),	.RF_PIN31(RF_PIN31), // O IO O O
			.RF_PIN32(RF_PIN32),    .RF_PIN33(RF_PIN33),	.RF_PIN34(RF_PIN34),	.RF_PIN35(RF_PIN35) //O IO I I    
);
    
    
    //yhj
endmodule
