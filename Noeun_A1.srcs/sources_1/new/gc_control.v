`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/11/30 17:46:56
// Design Name: 
// Module Name: gc_control
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module gc_control(
    input wire 			sys_clk,		//sys_clk
	input wire 			sclr,

	// table UpLoad from reg
	input wire 			we_mgc,
	input wire 			mgc_table_sel,
	input wire [6:0] 	addr_mgc,
	input wire [15:0] 	data_mgc,  /// SIZE CHECK POINT !

	// MGC from regs
	input wire			mgc_en,
	input wire [6:0]	mgc_idx,	// 27 ~ 127 까�? (dBuV (-80dBm ~ 20dBm))
	input wire			mgc_idx_valid,

	// User GC to/from regs
	input wire			user_lna_en,
	input wire			user_lna_en_valid,
	input wire [4:0]	user_satt,
	input wire			user_satt_valid,
	input wire [4:0]	user_att_cha_1,
	input wire			user_att_cha_1_valid,
	input wire [4:0]	user_att_cha_2,
	input wire			user_att_cha_2_valid,	
	input wire [4:0]	user_att_chb_1,
	input wire 			user_att_chb_1_valid,
	input wire [4:0]	user_att_chb_2,
	input wire 			user_att_chb_2_valid,	

	// GATT
	input wire [6:0]	table_gatt_ctrl,
	input wire			table_gatt_ctrl_valid,
	input wire [6:0]	user_gatt1,
	input wire			user_gatt1_valid,
	input wire [6:0]	user_gatt2,
	input wire			user_gatt2_valid,


	// Output GC
	input wire			att_ready,
	output wire			gc_lna,

	output wire [15:0]	gc_att,
	output wire			gc_att_valid,

	output wire			lna_en,
	output wire [4:0]	satt_idx,
	output wire [4:0]	att_cha_1_idx,
	output wire [4:0]	att_cha_2_idx,	
	output wire [4:0]	att_chb_1_idx,
	output wire [4:0]	att_chb_2_idx,	
	output wire [6:0]	gatt1_val,
	output wire [6:0]	gatt2_val

);

wire		rf_lna_en;
wire		rf_lna_en_valid;
wire [4:0]	rf_satt;
wire		rf_satt_valid;
wire [4:0]	rf_att_cha_1;
wire		rf_att_cha_1_valid;
wire [4:0]	rf_att_cha_2;
wire		rf_att_cha_2_valid;
wire [4:0]	rf_att_chb_1;
wire		rf_att_chb_1_valid;
wire [4:0]	rf_att_chb_2;
wire		rf_att_chb_2_valid;
wire [6:0]	rf_gatt1_ctrl;
wire		rf_gatt1_ctrl_valid;
wire [6:0]	rf_gatt2_ctrl;
wire		rf_gatt2_ctrl_valid;


assign lna_en = rf_lna_en;
assign satt_idx = rf_satt;
assign att_cha_1_idx = rf_att_cha_1;
assign att_cha_2_idx = rf_att_cha_2;
assign att_chb_1_idx = rf_att_chb_1;
assign att_chb_2_idx = rf_att_chb_2;
assign gatt1_val = rf_gatt1_ctrl;
assign gatt2_val = rf_gatt2_ctrl;

// -----------------------------------------------------------------
// SYS ATT,ATT1A, ATT2A, ATT1B, ATT2B, LNA On/Off -> MGC Table? or User input?
rf_mgc rf_mgc(
	.clk                   (sys_clk),				       // I // sys_clk
	.sclr                  (sclr),					       // I

	// table UpLoad from reg
	.we_mgc                (we_mgc && mgc_table_sel=='b0), // I
	.addr_mgc              (addr_mgc),			           // I [6:0]
	.data_mgc              (data_mgc),			           // I [15:0]

	// MGC from regs
	.mgc_en                (mgc_en),				       // I
	.mgc_idx               (mgc_idx),				       // I [6:0]	// 27 ~ 127 까�? (dBuV (-80dBm ~ 20dBm))
	.mgc_idx_valid         (mgc_idx_valid),	               // I

	// User GC to/from regs
	.user_lna_en           (user_lna_en),			       // I
	.user_lna_en_valid     (user_lna_en_valid),	           // I
	.user_satt             (user_satt),				       // I [4:0]
	.user_satt_valid       (user_satt_valid),		       // I
	.user_att_cha_1        (user_att_cha_1),		       // I [4:0]
	.user_att_cha_1_valid  (user_att_cha_1_valid),	       // I
	.user_att_cha_2        (user_att_cha_2),		       // I [4:0]
	.user_att_cha_2_valid  (user_att_cha_2_valid),	       // I
	.user_att_chb_1        (user_att_chb_1),		       // I [4:0]
	.user_att_chb_1_valid  (user_att_chb_1_valid),	       // I	
	.user_att_chb_2        (user_att_chb_2),		       // I [4:0]
	.user_att_chb_2_valid  (user_att_chb_2_valid),	       // I
	
	// to/from rf_intf
	.rf_lna_en             (rf_lna_en),				       // O
	.rf_lna_en_valid       (rf_lna_en_valid),	           // O
	.rf_satt               (rf_satt),				       // O [4:0]
	.rf_satt_valid         (rf_satt_valid),		           // O
	.rf_att_cha_1          (rf_att_cha_1),			       // O [4:0]
	.rf_att_cha_1_valid    (rf_att_cha_1_valid),	       // O
	.rf_att_cha_2          (rf_att_cha_2),			       // O [4:0]
	.rf_att_cha_2_valid    (rf_att_cha_2_valid),	       // O
	.rf_att_chb_1          (rf_att_chb_1),			       // O [4:0]
	.rf_att_chb_1_valid    (rf_att_chb_1_valid),	       // O	
	.rf_att_chb_2          (rf_att_chb_2),			       // O [4:0]
	.rf_att_chb_2_valid    (rf_att_chb_2_valid)		       // O	
);


// -----------------------------------------------------------------
// GATT
rf_gatt rf_gatt(
	.clk                   (sys_clk),				// I

	.user_gatt1            (user_gatt1),			// I [6:0]
	.user_gatt1_valid      (user_gatt1_valid),		// I
	.user_gatt2            (user_gatt2),			// I [6:0]
	.user_gatt2_valid      (user_gatt2_valid),		// I


	.table_gatt_ctrl       (table_gatt_ctrl),		// I [6:0]
	.table_gatt_ctrl_valid (table_gatt_ctrl_valid), // I

	.rf_gatt1_ctrl         (rf_gatt1_ctrl),			// O [6:0]
	.rf_gatt1_ctrl_valid   (rf_gatt1_ctrl_valid),	// O
	.rf_gatt2_ctrl         (rf_gatt2_ctrl),			// O [6:0]
	.rf_gatt2_ctrl_valid   (rf_gatt2_ctrl_valid)	// O
);


gc_cntl_pack gc_cntl_pack(
	.clk			       (sys_clk),				// I // sys_clk
	.sclr			       (sclr),					// I


	.lna_en			       (rf_lna_en),			    // I
	.lna_en_valid	       (rf_lna_en_valid),		// I
    .satt			       (rf_satt),				// I [4:0]
	.satt_valid		       (rf_satt_valid),		    // I
	
	.gatt1			       (rf_gatt1_ctrl),		    // I [6:0]
	.gatt1_valid	       (rf_gatt1_ctrl_valid),	// I
	.gatt2			       (rf_gatt2_ctrl),		    // I [6:0]
	.gatt2_valid	       (rf_gatt2_ctrl_valid),	// I
	
	.att_cha_1			   (rf_att_cha_1),			// I [4:0]
	.att_cha_1_valid	   (rf_att_cha_1_valid),	// I
    .att_cha_2			   (rf_att_cha_2),			// I [4:0]
	.att_cha_2_valid	   (rf_att_cha_2_valid),	// I
	.att_chb_1			   (rf_att_chb_1),			// I [4:0]
	.att_chb_1_valid	   (rf_att_chb_1_valid),	// I	
	.att_chb_2			   (rf_att_chb_2),			// I [4:0]
	.att_chb_2_valid	   (rf_att_chb_2_valid),	// I
	
	.att_ready		       (att_ready),			    // I
	
	.gc_lna			       (gc_lna),				// O

	.gc_att			       (gc_att),				// O [15:0]
	.gc_att_valid	       (gc_att_valid)			// O

);
endmodule
