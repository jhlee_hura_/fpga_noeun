`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/11/30 17:02:20
// Design Name: 
// Module Name: bsc_saturn_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module bsc_saturn_top(
    input wire  rst,
    input wire  u_sys_clk,
    input wire  u_sig_clk,
    input wire  pcie_clk,
    


    input wire [46:0]   regs_adda,
    input wire          regs_adda_valid,
    output wire         regs_bsc_data_valid,
    input wire          regs_data_ready,  
    
     // bsc sys     
    input wire          regs_bsc_sys_sel,
    output wire [31:0]  regs_bsc_sys_data,

     // bsc sig     
    input wire          regs_bsc_sig_sel,
    output wire [31:0]  regs_bsc_sig_data,

     // bsc pcie     
    input wire          regs_bsc_pcie_sel,
    output wire [31:0]  regs_bsc_pcie_data,
    input wire [7:0] c2h_sts_0,
    input wire [7:0] h2c_sts_0,
	//  TMP102 IN
	output	wire 	TMP_SCL_IN,	inout	wire 	TMP_SDA_IN,
	//  TMP102 Out
	output	wire 	TMP_SCL_EX,	inout	wire 	TMP_SDA_EX,	

    // ADS42JB49
    input wire  OVRA,    input wire  OVRB,
    input wire  ADC_SDOUT,    
    output wire ADC_CS_B, // ADC_SEN    
    output wire ADC_SCLK,
    output wire ADC_SDATA,
    
    // LMK04828 -> FPGA
    input wire  SYSREF_B_P,    input wire  SYSREF_B_N,    
    input wire  CLK_C_P,    input wire  CLK_C_N, 
    	
    // LMK04828	
	output wire    LMK_RESET,
	output wire    LMK_SYNC,	output wire    LMK_CS_B,	output wire    LMK_SCK,	inout wire     LMK_SDIO,
		
	// UART
	output wire    F_UART_TXD,	input  wire    F_UART_RXD,
	// FAN CTRL
	output wire    FAN_CTRL,
	// HMC 1033
	output wire    ADCCLK_SEN,	output wire    ADCCLK_SDI,	input wire     ADCCLK_SDO,	  output wire    ADCCLK_SCK,
	
		     
	//GPS 
	input wire		GPS_LOCK,	input wire		GPS_HOLDOVER,	input wire		GPS_PPS,

	// Ext. GPIO
	inout	wire [3:0]	EXT_GPIO,
	    
	//LED  
	output wire   	SP_RUN0_N,	// Fault LED
	output wire    	SP_RUN1_N,	output wire    	SP_RUN2_N,	output wire    	SP_RUN3_N       
    );
wire regs_bsc_sys_data_valid;
wire regs_bsc_sig_data_valid;     
wire regs_bsc_pcie_data_valid;
assign regs_bsc_data_valid =  regs_bsc_sys_data_valid |   regs_bsc_sig_data_valid | regs_bsc_pcie_data_valid;

bsc_sys(

    .rst(rst),       // I
    .u_sys_clk(u_sys_clk), // I
    .pcie_clk(pcie_clk), // I
    
    .regs_bsc_sys_sel(regs_bsc_sys_sel), // I
    .regs_adda(regs_adda), // I [46:0]
    .regs_adda_valid(regs_adda_valid),   // I
    .regs_bsc_sys_data(regs_bsc_sys_data),  // O [31:0]
    .regs_bsc_sys_data_valid(regs_bsc_sys_data_valid),    // O
    .regs_data_ready(regs_data_ready),      // I
    
// HP Bank 65 
//-------------------------------------	
	// TMP102 in 
	.TMP_SCL_IN(TMP_SCL_IN),   .TMP_SDA_IN(TMP_SDA_IN),	   // O    I
	// TMP102 out
	.TMP_SCL_EX(TMP_SCL_EX),   .TMP_SDA_EX(TMP_SDA_EX),	   // O   I	
	
// HP Bank 67 
//-------------------------------------		
        // ADS42JB49
      .OVRA(OVRA),   .OVRB(OVRB),   // I   I
      .ADC_SDOUT(ADC_SDOUT),    .ADC_CS_B(ADC_CS_B), // I     O, ADC_SEN
      .ADC_SCLK(ADC_SCLK),  .ADC_SDATA(ADC_SDATA), //   O     O
      //.SYNCINB_P(SYNCINB_P), .SYNCINB_N(SYNCINB_N), // O   O
    
        // LMK04828 -> FPGA
       .SYSREF_B_P(SYSREF_B_P), .SYSREF_B_N(SYSREF_B_N), // I    I    
       .CLK_C_P(CLK_C_P),  .CLK_C_N(CLK_C_N),    // I   I 

// HD Bank 84
//-------------------------------------
	
    // LMK04828	
	    .LMK_RESET(LMK_RESET), 	.LMK_SYNC(LMK_SYNC),   // O    O
        .LMK_CS_B(LMK_CS_B),    .LMK_SCK(LMK_SCK),    .LMK_SDIO(LMK_SDIO),  // O    O   IO
		
	// UART
	     .F_UART_TXD(F_UART_TXD),   .F_UART_RXD(F_UART_RXD),   // O    I
	// FAN CTRL
	     .FAN_CTRL(FAN_CTRL),  // O
	// HMC 1033
	     .ADCCLK_SEN(ADCCLK_SEN),  .ADCCLK_SDI(ADCCLK_SDI), .ADCCLK_SDO(ADCCLK_SDO),  .ADCCLK_SCK(ADCCLK_SCK),//   O   O   I   O	
	     
	//GPS 
	 		.GPS_LOCK(GPS_LOCK), .GPS_HOLDOVER(GPS_HOLDOVER),  // I   I   
	// Ext. GPIO
	       .EXT_GPIO(EXT_GPIO), // I
	//LED  
	    	.SP_RUN0_N(SP_RUN0_N),	.SP_RUN1_N(SP_RUN1_N),  .SP_RUN2_N(SP_RUN2_N), 	.SP_RUN3_N(SP_RUN3_N)    //  O Fault LED    O   O   O       	    	 	  
);  
bsc_sig(  
    .rst(rst),       // I
    .u_sig_clk(u_sig_clk), // I
    .pcie_clk(pcie_clk), // I
    
    .regs_bsc_sig_sel(regs_bsc_sig_sel), // I
    .regs_adda(regs_adda), // I [46:0]
    .regs_adda_valid(regs_adda_valid),   // I
    .regs_bsc_sig_data(regs_bsc_sig_data),  // O [31:0]
    .regs_bsc_sig_data_valid(regs_bsc_sig_data_valid),    // O
    .regs_data_ready(regs_data_ready),      // I
    
    
    .GPS_PPS(GPS_PPS)                   // I
);

bsc_pcie(  
    .rst(rst),       // I
    .pcie_clk(pcie_clk), // I
    

    .regs_adda(regs_adda), // I [46:0]
    .regs_adda_valid(regs_adda_valid),   // I
    
    .regs_bsc_pcie_sel(regs_bsc_pcie_sel), // I    
    .regs_bsc_pcie_data(regs_bsc_pcie_data),  // O [31:0]
    .regs_bsc_pcie_data_valid(regs_bsc_pcie_data_valid),    // O
    .regs_data_ready(regs_data_ready),      // I
    
    .c2h_sts_0(c2h_sts_0),   // I [7:0]
    .h2c_sts_0(h2c_sts_0)   // I [7:0]
);

endmodule
