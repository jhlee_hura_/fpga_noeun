`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/12/01 11:24:02
// Design Name: 
// Module Name: ud_sig
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ud_sig(
   input wire  rst,
    input wire  u_sig_clk,
    input wire  pcie_clk,
    

    input wire [46:0]   regs_adda,
    input wire          regs_adda_valid,
    input wire          regs_data_ready, 
     
    input wire          regs_ud_sig_sel,    
    output wire [31:0]  regs_ud_sig_data,
    output wire         regs_ud_sig_data_valid

    );
    
// ---------------------------------------------------------------------------------
// Reg for sig_clk
regs_ud_sig regs_ud_sig (
	.clk(u_sig_clk),						// I
	.rst(rst),						// I
	.pcie_clk(pcie_clk),				// I

	.regs_sel			(regs_ud_sig_sel),			// I
	.regs_adda			(regs_adda),				// I [46:0]
	.regs_dout_ready	(regs_data_ready),			// I
		
	.regs_adda_valid	(regs_adda_valid),			// I
	.regs_dout			(regs_ud_sig_data),			// O [31:0]
	.regs_dout_valid	(regs_ud_sig_data_valid)	// O


	// -------------------------------------------------------
	// -- register ?���?

);        
endmodule
