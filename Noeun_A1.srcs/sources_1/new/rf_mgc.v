`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/11/30 17:49:14
// Design Name: 
// Module Name: rf_mgc
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module rf_mgc(
 input wire 			clk,		//sys_clk
	input wire 			sclr,

	// table UpLoad from reg
	input wire 			we_mgc,
	input wire [6:0] 	addr_mgc,
	input wire [25:0] 	data_mgc,

	// MGC from regs
	input wire			mgc_en,
	input wire [6:0]	mgc_idx,	// 27 ~ 127 ê¹ì? (dBuV (-80dBm ~ 20dBm))
	input wire			mgc_idx_valid,

	// User GC to/from regs
	input wire			user_lna_en,
	input wire			user_lna_en_valid,
	input wire [4:0]	user_satt,
	input wire			user_satt_valid,
	input wire [4:0]	user_att_cha_1,
	input wire			user_att_cha_1_valid,
	input wire [4:0]	user_att_cha_2,
	input wire			user_att_cha_2_valid,	
	input wire [4:0]	user_att_chb_1,
	input wire 			user_att_chb_1_valid,
	input wire [4:0]	user_att_chb_2,
	input wire 			user_att_chb_2_valid,
	
	// to/from rf_intf
	output reg			rf_lna_en,
	output reg			rf_lna_en_valid,
	output reg [4:0]	rf_satt,
	output reg			rf_satt_valid,
	output reg [4:0]	rf_att_cha_1,
	output reg			rf_att_cha_1_valid,
	output reg [4:0]	rf_att_cha_2,
	output reg			rf_att_cha_2_valid,	
	output reg [4:0]	rf_att_chb_1,
	output reg			rf_att_chb_1_valid,
	output reg [4:0]	rf_att_chb_2,
	output reg			rf_att_chb_2_valid	
);

// ----------------------------------------------------------------------------
// User mode and MGC mode Mux (?ë¡? ?? ¥?ì§? ??¼ë©? ê·¸ë?ë¡? ? ì§?.)
wire [25:0]	dout_mgc;
reg 		mgc_valid_tmp = 1'b0;

always @(posedge clk) begin
	if (sclr) begin
		mgc_valid_tmp 	    <= 1'b0;
		rf_lna_en 		    <= 1'b0;
		rf_lna_en_valid 	<= 1'b0;
		rf_satt			    <= 5'b0;
		rf_satt_valid 	    <= 1'b0;
		rf_att_cha_1		<= 5'b0;
		rf_att_cha_1_valid 	<= 1'b0;
		rf_att_cha_2		<= 5'b0;
		rf_att_cha_2_valid 	<= 1'b0;		
		rf_att_chb_1		<= 5'b0;
		rf_att_chb_1_valid	<= 1'b0;
		rf_att_chb_2		<= 5'b0;
		rf_att_chb_2_valid	<= 1'b0;		
	end
	else begin
		mgc_valid_tmp <= mgc_idx_valid;		// mgc table read 1 clk
		if (mgc_en) begin
			rf_lna_en 	        <= dout_mgc[25];
			rf_satt		        <= dout_mgc[24:20];
			rf_att_cha_1		<= dout_mgc[19:15];
			rf_att_cha_2		<= dout_mgc[9:5];
			rf_att_chb_1		<= dout_mgc[14:10];			
			rf_att_chb_2		<= dout_mgc[4:0];			
			rf_lna_en_valid	    <= mgc_valid_tmp;
			rf_satt_valid	    <= mgc_valid_tmp;
			rf_att_cha_1_valid	<= mgc_valid_tmp;
			rf_att_cha_2_valid	<= mgc_valid_tmp;			
			rf_att_chb_1_valid	<= mgc_valid_tmp;
			rf_att_chb_2_valid	<= mgc_valid_tmp;			
		end
		else begin		// user_gc_en
			rf_lna_en 	        <= user_lna_en;
			rf_satt 	        <= user_satt;
			rf_att_cha_1 	    <= user_att_cha_1;
			rf_att_cha_2 	    <= user_att_cha_2;
			rf_att_chb_1 	    <= user_att_chb_1;			
			rf_att_chb_2 	    <= user_att_chb_2;			
			rf_lna_en_valid	    <= user_lna_en_valid;
			rf_satt_valid	    <= user_satt_valid;
			rf_att_cha_1_valid	<= user_att_cha_1_valid;
			rf_att_cha_2_valid	<= user_att_cha_2_valid;			
			rf_att_chb_1_valid	<= user_att_chb_1_valid;
			rf_att_chb_2_valid	<= user_att_chb_2_valid;			
		end
	end
end

//-----------------------------------------------------------------------------
// 27 ë³´ë¤ ??? ê²ì? ëª¨ë 0?¼ë¡?
// 27ë³´ë¤ ?° ê²ì? MGC ë²ì? 27 dBuV ~ 127dBuVê¹ì??.
wire [6:0] mgc_idx_1;
assign	mgc_idx_1 = (mgc_idx > 7'd27) ? (mgc_idx - 7'd27) : 7'd0;

mgc_table_26x mgc_table (
  .a(addr_mgc),			// I [6 : 0]
  .d(data_mgc),			// I [25 : 0]
  .dpra(mgc_idx_1),		// I [6 : 0]
  .clk(clk),    		// I
  .we(we_mgc),			// I
  .dpo(dout_mgc)		// O [25 : 0]
);
endmodule
