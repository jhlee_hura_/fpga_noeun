`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/12/01 09:30:06
// Design Name: 
// Module Name: regs_bsc_sig
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module regs_bsc_sig(
input 	wire 				clk,		// sig_clk
	input 	wire 				rst,		// because of multi-clk
	input	wire				pcie_clk,

	input	wire  				regs_sel,
	input	wire	[46:0]		regs_adda,
	input	wire  				regs_adda_valid,
	output	wire	[31:0]		regs_dout,
	output	wire				regs_dout_valid,
	input	wire				regs_dout_ready,
	// DATE_REG
	output	reg		[15:0]		set_date,
	output	reg		 			set_date_valid,
	input	wire 	[15:0]		pps_date,
	// TIME_REG
	output	reg		[4:0]		set_time_H,
	output	reg		[5:0]		set_time_M,
	output	reg		[5:0]		set_time_S,
	output	reg		 			set_time_valid,
	input	wire	[4:0]		pps_time_H,
	input	wire	[5:0]		pps_time_M,
	input	wire	[5:0]		pps_time_S,
	input	wire	[6:0]		pps_time_10ms,
	// SYSONSEC_REG
	input	wire	[31:0]		pps_count_syson	
    );
    

wire 	[46:0]	fifo_dout;
wire			regin_empty;
wire			regout_empty;
reg				regout_we;

wire	[15:0]	addr;
wire			wr_en;
wire	[31:0]	wr_d;
reg 	[31:0]	rd_d;
wire			regin_valid;

// from pcie block
fifo_regin_47x16 regin_fifo (
  .rst(rst),        					// I
  .wr_clk(pcie_clk),  					// I
  .rd_clk(clk),  						// I
  .din(regs_adda),        				// I [46 : 0]
  .wr_en(regs_sel & regs_adda_valid),   // I
  .rd_en(~regin_empty),    				// I
  .dout(fifo_dout),      				// O [46 : 0]
  .full(),      						// O
  .empty(regin_empty),    				// O
  .valid(regin_valid)    				// O
);

// to pcie block
fifo_regout_32x16 regout_fifo (
  .rst(rst),        					// I
  .wr_clk(clk),  						// I
  .rd_clk(pcie_clk),  					// I
  .din(rd_d),        					// I [31 : 0]
  .wr_en(regout_we),    				// I
  .rd_en(~regout_empty & regs_dout_ready),    			// I
  .dout(regs_dout),      				// O [31 : 0]
  .full(),      						// O
  .empty(regout_empty),    				// O
  .valid(regs_dout_valid)    			// O
);

always @(posedge clk) begin
	regout_we 	<= regin_valid & ~fifo_dout[46];
end

// fifo[46]= 1:wr_en, 0:rd_en
// fifo[45:32] = 14bits addr
// fifo[31:0] = 32bits data
assign wr_en 	= regin_valid & fifo_dout[46];
assign addr 	= {fifo_dout[45:32], 2'b0};
assign wr_d 	= fifo_dout[31:0];

always @(posedge clk) begin
	if (rst)	begin
		rd_d 				<= 32'b0;

		// DATE_REG
		set_date			<= 16'b0;
		set_date_valid		<= 1'b0;
		// TIME_REG
		set_time_H			<= 5'b0;
		set_time_M			<= 6'b0;
		set_time_S			<= 6'b0;
		set_time_valid		<= 1'b0;
	end
	else begin
		case (addr)


			`DATE_REG : begin
				rd_d[15:0]	<= pps_date;
				rd_d[31:16] <= 'b0;
				if (wr_en) begin
					set_date <= wr_d[15:0];
					set_date_valid <= 1'b1;
				end
			end

			`TIME_REG : begin
				rd_d[31:29]	<= 'b0;
				rd_d[28:24]	<= pps_time_H;
				rd_d[23:22]	<= 'b0;
			    rd_d[21:16] <= pps_time_M;
				rd_d[15:14]	<= 'b0;
			    rd_d[13:8]	<= pps_time_S;
				rd_d[7]		<= 'b0;
				rd_d[6:0]	<= pps_time_10ms;
				if (wr_en) begin
					set_time_H <= wr_d[28:24];
					set_time_M <= wr_d[21:16];
					set_time_S <= wr_d[13:8];
					set_time_valid <= 1'b1;
				end
			end

			`SYSONSEC_REG : begin
				rd_d <= pps_count_syson;
			end

			default : begin
				rd_d <= 32'b0;
			end
		endcase

		////////////////////////////////////////////////
		// ?��?�� ??�?.
		if (!wr_en) begin
			set_date_valid			<= 1'b0;
			set_time_valid			<= 1'b0;
		end


	end
end    
endmodule
