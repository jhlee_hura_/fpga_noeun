`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/12/01 11:05:49
// Design Name: 
// Module Name: ud_sys
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ud_sys(
   input wire  rst,
    input wire  u_sys_clk,
    input wire  pcie_clk,
    

    input wire [46:0]   regs_adda,
    input wire          regs_adda_valid,
    input wire          regs_data_ready,
        
    input wire          regs_ud_sys_sel,    
    output wire [31:0]  regs_ud_sys_data,
    output wire         regs_ud_sys_data_valid,


    // HD Bank 86 
    //-------------------------------------	
    //PE43610 - Digi. atten
    //SN74HCS595BQBR - 8bit shift register
    //SN74HCS165BQB - 8bit parallel-load shift register
    //LMV321AIDCKR - OPAMP

	// RF - ADC
	// PE43610(PIN 0,1,4) , SN74HCS595BQBR(RF CTRL 2,3,6)	, LNA Switch (5)
	output wire	 RF_PIN0,	output	wire  RF_PIN1,	output	wire    RF_PIN2,	output	wire    RF_PIN3,
	output	wire    RF_PIN4,	output	wire    RF_PIN5,	output	wire    RF_PIN6,	
	
	// SN74HCS595BQBR(CM 7,10,11), TMP102(8,9)
	output	wire    RF_PIN7,	output	wire    RF_PIN8,	inout	wire RF_PIN9,	output	wire    RF_PIN10,	input	wire    RF_PIN11,
	
	
	// RF - SN74HCS165BQB(12,13,19), SN74HCS595BQBR(14,15,18), LMX2594RHAT(16,17,20,21,22,23)
	output		wire   RF_PIN12,	output		wire  RF_PIN13,	output		RF_PIN14,	output		wire RF_PIN15,
	output		wire   RF_PIN16,	output		wire  RF_PIN17,	output		RF_PIN18,	output		wire RF_PIN19,
	output		wire   RF_PIN20,	output		wire  RF_PIN21,	output		RF_PIN22,	output		wire    RF_PIN23,
	
// HD Bank 87
//-------------------------------------	
    // RF - PE43610A-X(25,26,27) TMP102(28,29) LMK01010ISQ(30,31,32,33) LMV321AIDCKR(34,35)
	output		wire   RF_PIN24,	output		wire  RF_PIN25,   	output	wire   RF_PIN26, output wire   RF_PIN27,
	
	inout		wire   RF_PIN28,	output		wire   RF_PIN29,	output	wire  RF_PIN30,	  output wire 	RF_PIN31,	
	
	output		wire   RF_PIN32,	inout		wire   RF_PIN33,	input		wire  RF_PIN34,	  input wire	RF_PIN35    
    );



//==========================================================================================================  regs_ud_sys
// 
// Level Table
// 

//==================================================================================================
//deploy_yhj


// to Lv_Table from regs_ud_sys
wire        lv_cal_en;

wire [8:0]  freq_index;
wire [8:0]  user_freq_index;
wire [5:0]  freq_intp;
wire [5:0]  user_freq_intp;

wire [4:0]  temp_idx_cha;
wire [4:0]  temp_idx_chb;
wire [4:0]  bw_idx_sysclk_cha;
wire [4:0]  bw_idx_sysclk_chb;

wire        we_satt;
wire [13:0] addra_satt;
wire [15:0] dataa_satt;

wire        we_lna;
wire [8:0]  addra_lna;
wire [15:0] dataa_lna;

wire        we_gatt;
wire        addra_gatt_mode;
wire [8:0]  addra_gatt;
wire [6:0]  dataa_gatt;

wire        we_att_cha_1;
wire [4:0]  addra_att_cha_1;
wire [15:0] dataa_att_cha_1;

wire        we_att_cha_2;
wire [4:0]  addra_att_cha_2;
wire [15:0] dataa_att_cha_2;

wire        we_bw_cha;
wire [4:0]  addra_bw_cha;
wire [15:0] dataa_bw_cha;

wire        we_temp_cha;
wire [4:0]  addra_temp_cha;
wire [15:0] dataa_temp_cha;

wire        we_att_chb_1;
wire [4:0]  addra_att_chb_1;
wire [15:0] dataa_att_chb_1;

wire        we_att_chb_2;
wire [4:0]  addra_att_chb_2;
wire [15:0] dataa_att_chb_2;

wire        we_bw_chb;
wire [4:0]  addra_bw_chb;
wire [15:0] dataa_bw_chb;

wire        we_temp_chb;
wire [4:0]  addra_temp_chb;
wire [15:0] dataa_temp_chb;

assign freq_index  =  user_freq_index;
assign freq_intp   =  user_freq_intp;



// to Lv_Table from regs_ud_sys
wire [4:0]	lvtb_satt_idx;              // from gc control to LV_Table
wire 		lvtb_lna_en;                // from gc control to LV_Table

wire [4:0] 	satt_idx;                   // from gc control to LV_Table
wire 		lna_en;                     // from gc control to LV_Table

wire [4:0]	lvtb_att_idx_cha_1;             // from gc control to LV_Table
wire [4:0]	lvtb_att_idx_cha_2;             // from gc control to LV_Table
wire [4:0]	lvtb_att_idx_chb_1;             // from gc control to LV_Table
wire [4:0]	lvtb_att_idx_chb_2;             // from gc control to LV_Table

wire [4:0] 	att_idx_cha_1;                  // from gc control to LV_Table
wire [4:0] 	att_idx_cha_2;                  // from gc control to LV_Table
wire [4:0] 	att_idx_chb_1;                  // from gc control to LV_Table
wire [4:0] 	att_idx_chb_2;                  // from gc control to LV_Table

wire [17:0]	sum_cal_cha;
wire [8:0] lv_cal_out_cha;
wire [7:0] anti_db_cal_out_cha;

wire [17:0]	sum_cal_chb;
wire [8:0] lv_cal_out_chb;
wire [7:0] anti_db_cal_out_chb;

wire [6:0] 	lv_table_gatt_ctrl_cha;       // ----> DELETE POINT CHECK
wire		lv_table_gatt_ctrl_valid_cha; // ----> DELETE POINT CHECK
wire [6:0] 	lv_table_gatt_ctrl_chb;       // ----> DELETE POINT CHECK
wire		lv_table_gatt_ctrl_valid_chb; // ----> DELETE POINT CHECK


assign lvtb_satt_idx  =  satt_idx;  // from gc control to LV_Table
assign lvtb_lna_en    =  lna_en;    // from gc control to LV_Table
assign lvtb_att_idx_cha_1 =  att_idx_cha_1; // from gc control to LV_Table
assign lvtb_att_idx_cha_2 =  att_idx_cha_2; // from gc control to LV_Table
assign lvtb_att_idx_chb_1 =  att_idx_chb_1; // from gc control to LV_Table
assign lvtb_att_idx_chb_2 =  att_idx_chb_2; // from gc control to LV_Table




// ---------------------------------------------------------------------------------
// 
// GC Control
// 
wire 		we_mgc;
wire 		mgc_table_sel;
wire [6:0] 	addr_mgc;
wire [15:0] data_mgc;

wire		mgc_en;
wire [6:0]	mgc_idx;	// 27 ~ 127 까�? (dBuV (-80dBm ~ 20dBm))
wire		mgc_idx_valid;

wire		user_lna_en;
wire		user_lna_en_valid;
wire [4:0]	user_satt;
wire		user_satt_valid;
wire [4:0]	user_att_cha_1;
wire		user_att_cha_1_valid;
wire [4:0]	user_att_cha_2;
wire		user_att_cha_2_valid;
wire [4:0]	user_att_chb_1;
wire		user_att_chb_1_valid;
wire [4:0]	user_att_chb_2;
wire		user_att_chb_2_valid;


wire [6:0]	user_gatt1;
wire		user_gatt1_valid;
wire [6:0]	user_gatt2;
wire		user_gatt2_valid;

wire [6:0]	gatt1_val;  
wire [6:0]	gatt2_val;  

wire			att_ready;
wire			gc_lna;
wire [15:0]		gc_att;
wire			gc_att_valid;

//==================================================================================================






// ---------------------------------------------------------------------------------
// 
// RF Interface
// 

// DC BD ---------------------------------------------------------------------------
// DC BD - M1
wire [15:0] dc_bd_din_att; 
wire dc_bd_din_att_valid; 
wire dc_bd_att_ready;

// DC BD - M2
wire dc_bd_lna_sw;

// DC BD - M3
wire [15:0] dc_bd_rf_sw; 
wire dc_bd_rf_sw_valid; 
wire dc_bd_ifbw_idx; 
wire dc_bd_ifbw_idx_valid;

// DC BD - M4
wire [1:0]	dc_bd_din_tmp102; 
wire	dc_bd_din_tmp102_valid; 
wire [15:0] dc_bd_dout_tmp102; 
wire dc_bd_busy_tmp102;

// DC BD - M5
wire	 dc_bd_din_sts_valid; 

wire    [15:0] dc_bd_dout_sts; 


// DC BD - M6, M7	  
wire    dc_bd_log_detect1;
 wire    dc_bd_log_detect2;
// SYN BD ---------------------------------------------------------------------------
// SYN BD - M1
wire [15:0] syn_bd_rf_sw;
wire syn_bd_rf_sw_valid; 
wire syn_bd_ifbw_idx; 
wire syn_bd_ifbw_idx_valid;

// SYN BD - M2

wire [31:0] syn_bd_din_distributor;
wire syn_bd_din_distributor_valid;
wire syn_bd_distributor_ready ;
// SYN BD - M3
wire	        syn_bd_din_sts_valid;
wire           syn_bd_din_sts_ready;
wire    [15:0] syn_bd_dout_sts;
wire           syn_bd_dout_sts_valid;

// SYN BD - M4
wire [1:0]       syn_bd_din_pll_cmd;  // need to be added!!!!!!!!!!!!!!!!!
wire [23:0] syn_bd_din_pll;
wire        syn_bd_din_pll_valid;
wire [15:0] syn_bd_pll_ctrl_interval;
wire        syn_bd_pll_last_cmd_in; 
wire        syn_bd_pll_cmd_done;
wire [23:0] syn_bd_dout_pll;
wire        syn_bd_dout_pll_valid;	 

// SYN BD - M5
wire [15:0] syn_bd_din_att;
wire        syn_bd_din_att_valid;
wire        syn_bd_att_ready;

wire [6:0]syn_bd_att1;  // O [6:0]
wire syn_bd_att1_valid; // O
wire [6:0]syn_bd_att2;  // O [6:0]
wire syn_bd_att2_valid; // O
wire [6:0]syn_bd_att3;  // O [6:0]
wire syn_bd_att3_valid; // O
wire [6:0]syn_bd_att4;  // O [6:0]
wire syn_bd_att4_valid; // O

localparam SYN_BD_ATT1	= 3'b000;	// [0:2] = 000;
localparam SYN_BD_ATT2	= 3'b001;	// [0:2] = 001;
localparam SYN_BD_ATT3	= 3'b010;	// [0:2] = 010;
localparam SYN_BD_ATT4	= 3'b011;	// [0:2] = 011;


assign syn_bd_din_att = (syn_bd_att1_valid)? {syn_bd_att1[0], syn_bd_att1[1], syn_bd_att1[2], syn_bd_att1[3], syn_bd_att1[4], syn_bd_att1[5], syn_bd_att1[6],1'b0, SYN_BD_ATT1, 5'b0} : 
                        (syn_bd_att2_valid)? {syn_bd_att2[0], syn_bd_att2[1], syn_bd_att2[2], syn_bd_att2[3], syn_bd_att2[4], syn_bd_att2[5], syn_bd_att2[6],1'b0, SYN_BD_ATT2, 5'b0} : 
                        (syn_bd_att3_valid)? {syn_bd_att3[0], syn_bd_att3[1], syn_bd_att3[2], syn_bd_att3[3], syn_bd_att3[4], syn_bd_att3[5], syn_bd_att3[6],1'b0, SYN_BD_ATT3, 5'b0} :
                        (syn_bd_att4_valid)?  {syn_bd_att4[0], syn_bd_att4[1], syn_bd_att4[2], syn_bd_att4[3], syn_bd_att4[4], syn_bd_att4[5], syn_bd_att4[6],1'b0, SYN_BD_ATT4, 5'b0} :0 ;        
                        
assign syn_bd_din_att_valid = syn_bd_att1_valid | syn_bd_att2_valid | syn_bd_att3_valid | syn_bd_att4_valid;                                       
// SYN BD - M6
wire [1:0]		syn_bd_din_tmp102;
wire			syn_bd_din_tmp102_valid;
wire [15:0]		syn_bd_dout_tmp102;
wire           syn_bd_busy_tmp102;



// ---------------------------------------------------------------------------------
// Reg for sys_clk
regs_ud_sys regs_ud_sys (
	.clk(u_sys_clk),						// I
	.rst(rst),						// I
	.pcie_clk(pcie_clk),				// I

	.regs_sel			(regs_ud_sys_sel),			// I
	.regs_adda			(regs_adda),				// I [46:0]
	.regs_adda_valid	(regs_adda_valid),			// I
	.regs_dout			(regs_ud_sys_data),			// O [31:0]
	.regs_dout_valid	(regs_ud_sys_data_valid),	// O
	.regs_dout_ready	(regs_data_ready),			// I
	// -------------------------------------------------------
	// -- register ?���?


//==================================================================================================

	// RF_MGC_EN_REG
	.mgc_en               (mgc_en),				  // O
	// RF_MGC_REG
	.mgc_idx              (mgc_idx),			  // O [6:0]
	.mgc_idx_valid        (mgc_idx_valid),		  // O
	// RF_MGC_TB_REG
	.we_mgc               (we_mgc),				  // O
	.mgc_table_sel        (mgc_table_sel),		  // O
	.addr_mgc             (addr_mgc),			  // O [6:0]
	.data_mgc             (data_mgc),			  // O [15:0]

	// RF_GC_USER_LNASW_REG
	.user_lna_en          (user_lna_en),		  // O
	.user_lna_en_valid    (user_lna_en_valid),	  // O
	// RF_GC_USER_SATT_REG
	.user_satt            (user_satt),			  // O [4:0]
	.user_satt_valid      (user_satt_valid),	  // O
	// RF_GC_USER_ATT_CHA_1_REG
	.user_att_cha_1       (user_att_cha_1),		  // O [4:0]
	.user_att_cha_1_valid (user_att_cha_1_valid), // O
	// RF_GC_USER_ATT_CHA_2_REG
	.user_att_cha_2       (user_att_cha_2),		  // O [4:0]
	.user_att_cha_2_valid (user_att_cha_2_valid), // O
	// RF_GC_USER_ATT_CHB_1_REG
	.user_att_chb_1       (user_att_chb_1),		  // O [4:0]
	.user_att_chb_1_valid (user_att_chb_1_valid), // O
	// RF_GC_USER_ATT_CHB_2_REG
	.user_att_chb_2       (user_att_chb_2),		  // O [4:0]
	.user_att_chb_2_valid (user_att_chb_2_valid), // O	
	
	// RF_GATT_REG
	.user_gatt1           (user_gatt1),			  // O [6:0]
	.user_gatt1_valid     (user_gatt1_valid),     // O
	// RF_GATT2_REG
	.user_gatt2           (user_gatt2),			  // O [6:0]
	.user_gatt2_valid     (user_gatt2_valid),	  // O

//==================================================================================================



	.busy_dc_bd_att(~dc_bd_att_ready),						// I

// --------- SYN Register ---------
 	// RF_INTF_SYNBD_M4_PLL_CMD_REG
	.syn_bd_din_pll_cmd(syn_bd_din_pll_cmd),						// O [1:0]
	// RF_INTF_SYNBD_M4_PLL_WR_REG
	.syn_bd_din_pll(syn_bd_din_pll),					// O [23:0]
	.syn_bd_din_pll_valid(syn_bd_din_pll_valid),		// O
	// RF_INTF_SYNBD_M4_PLL_RD_REG
	.syn_bd_dout_pll(syn_bd_dout_pll),					// I [23:0]

	// RF_SYN_CTRL_INTERVAL_REG
	.syn_bd_pll_ctrl_interval(syn_bd_pll_ctrl_interval), 	// O [15:0]
	
	//RF_INTF_SYNBD_M2_SYNC_DISTRIBUTOR_REG
    .syn_bd_din_distributor (syn_bd_din_distributor),  // O [31:0], 
    .syn_bd_din_distributor_valid (syn_bd_din_distributor_valid), // O
    .syn_bd_distributor_ready (syn_bd_distributor_ready), // I
    //RF_INTF_SYNBD_M5_PLL_ATT1_REG
    .syn_bd_att1(syn_bd_att1),  // O [6:0]
    .syn_bd_att1_valid(syn_bd_att1_valid), // O
    //RF_INTF_SYNBD_M5_PLL_ATT2_REG
    .syn_bd_att2(syn_bd_att2),  // O [6:0]
    .syn_bd_att2_valid(syn_bd_att2_valid), // O
    //RF_INTF_SYNBD_M5_PLL_ATT3_REG
    .syn_bd_att3(syn_bd_att3),  // O [6:0] 
    .syn_bd_att3_valid(syn_bd_att3_valid),   // O
    //RF_INTF_SYNBD_M5_PLL_ATT4_REG
     .syn_bd_att4(syn_bd_att4),  // O [6:0]      
     .syn_bd_att4_valid(syn_bd_att4_valid), // O
     
     .busy_syn_bd_att(syn_bd_att_ready), // I
// --------- Frequency Switching & Detect Register ---------
	// RF_INTF_DCBD_M3_SR_WR_REG
	.dc_bd_rf_sw(dc_bd_rf_sw),						// O [15:0]
	.dc_bd_rf_sw_valid(dc_bd_rf_sw_valid),			// O

	// RF_INTF_SYNBD_M1_SR_WR_REG
	.syn_bd_rf_sw(syn_bd_rf_sw),						// O [15:0]
	.syn_bd_rf_sw_valid(syn_bd_rf_sw_valid),			// O
	//RF_INTF_DCBD_M6_DETECT_RD_REG
	.dc_bd_log_detect1(dc_bd_log_detect1),             // I
	//RF_INTF_DCBD_M7_DETECT_RD_REG
	.dc_bd_log_detect2(dc_bd_log_detect2),             // I
	
// --------- Status control ---------  
	// RF_INTF_SYNBD_M3_SRL_WR_REG
	.dc_bd_din_sts_valid(dc_bd_din_sts_valid),					// O
	// RF_INTF_SYNBD_M3_SRL_RD_REG
	.dc_bd_dout_sts(dc_bd_dout_sts),					// I [15:0]

	// RF_INTF_SYNBD_M3_SRL_WR_REG
	.syn_bd_din_sts_valid(syn_bd_din_sts_valid),					// O
	// RF_INTF_SYNBD_M3_SRL_RD_REG
	.syn_bd_dout_sts(syn_bd_dout_sts),					// I [15:0]
	
// --------- RF_I2C control ---------	
	// RF_INTF_DCBD_M4_TMP_AD_REG
	.dc_bd_din_tmp102(dc_bd_din_tmp102),				// O [1:0]
	.dc_bd_din_tmp102_valid(dc_bd_din_tmp102_valid),	// O
	// RF_INTF_DCBD_M4_TMP_RD_REG
	.dc_bd_dout_tmp102(dc_bd_dout_tmp102),			// I [15:0]

	// RF_INTF_SYNBD_M6_TMP_AD_REG
	.syn_bd_din_tmp102(syn_bd_din_tmp102),				// O [1:0]
	.syn_bd_din_tmp102_valid(syn_bd_din_tmp102_valid),	// O
	// RF_INTF_SYNBD_M6_TMP_RD_REG
	.syn_bd_dout_tmp102(syn_bd_dout_tmp102),			// I [15:0]





//==================================================================================================
//deploy_yhj
	// level calibation table
	
	// LV_CAL_EN_REG
	.lv_cal_en          (lv_cal_en),	       // O
	// LV_FREQ_IDX_REG
	.freq_index         (user_freq_index),     // O [8:0]
	.freq_intp          (user_freq_intp),      // O [5:0]
	// LV_TEMP_IDX_CHA_REG
	.temp_idx_cha       (temp_idx_cha),		   // O [4:0]
	// LV_TEMP_IDX_CHB_REG
	.temp_idx_chb       (temp_idx_chb),		   // O [4:0]
	
	
	// LV_TB_SATT_REG
	.we_satt            (we_satt),		       // O
	.addra_satt         (addra_satt),	       // O [13:0]
	.dataa_satt         (dataa_satt),	       // O [15:0]
	// LV_TB_LNA_REG
	.we_lna             (we_lna),		       // O
	.addra_lna          (addra_lna),		       // O [8:0]
	.dataa_lna          (dataa_lna),		       // O [15:0]
	// LV_TB_GATT_REG
	.we_gatt            (we_gatt),		       // O
	.addr_gatt_mode     (addra_gatt_mode),     // O
	.addr_gatt          (addra_gatt),	       // O [8:0]
	.data_gatt          (dataa_gatt),	       // O [6:0]
	// LV_TB_ATT_CHA_1_REG
	.we_att_cha_1       (we_att_cha_1),		   // O
	.addra_att_cha_1    (addra_att_cha_1),	   // O [4:0]
	.dataa_att_cha_1    (dataa_att_cha_1),	   // O [15:0]
	// LV_TB_ATT_CHA_2_REG
	.we_att_cha_2       (we_att_cha_2),		   // O
	.addra_att_cha_2    (addra_att_cha_2),	   // O [4:0]
	.dataa_att_cha_2    (dataa_att_cha_2),	   // O [15:0]
	// LV_TB_BW_CHA_REG
	.we_bw_cha          (we_bw_cha),		   // O
	.addra_bw_cha       (addra_bw_cha),		   // O [4:0]
	.dataa_bw_cha       (dataa_bw_cha),	       // O [15:0]
	// LV_TB_TEMP_CHA_REG
	.we_temp_cha        (we_temp_cha),		   // O
	.addra_temp_cha     (addra_temp_cha),	   // O [4:0]
	.dataa_temp_cha     (dataa_temp_cha),	   // O [15:0]
	// LV_TB_ATT_CHB_1_REG
	.we_att_chb_1       (we_att_chb_1),		   // O
	.addra_att_chb_1    (addra_att_chb_1),	   // O [4:0]
	.dataa_att_chb_1    (dataa_att_chb_1),	   // O [15:0]
	// LV_TB_ATT_CHB_2_REG
	.we_att_chb_2       (we_att_chb_2),		   // O
	.addra_att_chb_2    (addra_att_chb_2),	   // O [4:0]
	.dataa_att_chb_2    (dataa_att_chb_2),	   // O [15:0]
	// LV_TB_BW_CHB_REG
	.we_bw_chb          (we_bw_chb),		   // O
	.addra_bw_chb       (addra_bw_chb),		   // O [4:0]
	.dataa_bw_chb       (dataa_bw_chb),	       // O [15:0]
	// LV_TB_TEMP_CHB_REG
	.we_temp_chb        (we_temp_chb),		   // O
	.addra_temp_chb     (addra_temp_chb),	   // O [4:0]
	.dataa_temp_chb     (dataa_temp_chb),	   // O [15:0]






	// LV_SATT_IDX_REG
	.satt_idx           (3),				   // I [4:0]	
	// LV_LNA_REG	
	.lna_en             (lvtb_lna_en),	       // I
	// LV_GATT_VALUE_REG
	.gatt1_val          (gatt1_val),		   // I [6:0]
	.gatt2_val          (gatt2_val),		   // I [6:0]
	// LV_ATT_IDX_CHA_1_REG
	.att_idx_cha_1      (att_idx_cha_1),	   // I [4:0]
	// LV_ATT_IDX_CHA_2_REG
	.att_idx_cha_2      (att_idx_cha_2),	   // I [4:0]
	// LV_ATT_IDX_CHB_1_REG
	.att_idx_chb_1      (att_idx_chb_2),	   // I [4:0]
	// LV_ATT_IDX_CHB_2_REG
	.att_idx_chb_2      (att_idx_chb_2),	   // I [4:0]
	// LV_SUM_CAL_CHA_REG
	.sum_cal_cha        (sum_cal_cha),		   // I [17:0]
    // LV_SUM_CAL_CHB_REG
	.sum_cal_chb        (sum_cal_chb)		   // I [17:0]

	
//==================================================================================================

	
);    


//==================================================================================================
//deploy_yhj
lv_table lv_table_cha(
    .clk             (u_sys_clk),			         // I
	.sclr            (rst),					         // I

    // to Table from Regs_ud_sys
//--------------------------------------------------------
	.lv_cal_en       (lv_cal_en),			         // I
    
    .freq_index      (freq_index),		             // I [8:0]
	.freq_intp       (freq_intp),			         // I [5:0]
	
	.bw_idx          (bw_idx_sysclk_cha),		     // I [4:0]
	.temp_idx        (temp_idx_cha),			     // I [4:0]
//--------------------------------------------------------
	.we_satt         (we_satt),			             // I
	.addra_satt      (addra_satt),		             // I [13:0]
	.dataa_satt      (dataa_satt),		             // I [15:0]

	.we_ln           (we_lna),				         // I
	.addra_ln        (addra_lna),			         // I [8:0]
	.dataa_ln        (dataa_lna),			         // I [15:0]
	
	.we_gatt         (we_gatt),				         // I
	.addra_gatt_mode (addra_gatt_mode),	             // I
	.addra_gatt      (addra_gatt),			         // I [8:0]
	.dataa_gatt      (dataa_gatt),			         // I [6:0]
	
	.we_att_1        (we_att_cha_1),			     // I
	.addra_att_1     (addra_att_cha_1),		         // I [4:0]
	.dataa_att_1     (dataa_att_cha_1),		         // I [15:0]

	.we_att_2        (we_att_cha_2),			     // I
	.addra_att_2     (addra_att_cha_2),		         // I [4:0]
	.dataa_att_2     (dataa_att_cha_2),		         // I [15:0]

	.we_bw           (we_bw_cha),				     // I
	.addra_bw        (addra_bw_cha),			     // I [4:0]
	.dataa_bw        (dataa_bw_cha),			     // I [15:0]

	.we_temp         (we_temp_cha),				     // I
	.addra_temp      (addra_temp_cha),			     // I [4:0]
	.dataa_temp      (dataa_temp_cha),			     // I [15:0]
//--------------------------------------------------------

    // to Table from gc_control
	.satt_idx        (lvtb_satt_idx),		     // I [4:0]
	.lna_en          (lvtb_lna_en),			     // I	
	
	.att_1_idx       (lvtb_att_idx_cha_1),		     // I [4:0]
	.att_2_idx       (lvtb_att_idx_cha_2),		     // I [4:0]
	
	
    // output
	.gatt_ctrl       (lv_table_gatt_ctrl_cha),		 // O [6:0]
	.gatt_ctrl_valid (lv_table_gatt_ctrl_valid_cha), // O
	.lv_cal_out      (lv_cal_out_cha),			     // O [8:0]
	.anti_db_cal_out (anti_db_cal_out_cha),	         // O [7:0]
	.sum_cal         (sum_cal_cha)				     // O [17:0]

);



lv_table lv_table_chb(
    .clk             (u_sys_clk),			         // I
	.sclr            (rst),					         // I

    // to Table from Regs_ud_sys
//--------------------------------------------------------
	.lv_cal_en       (lv_cal_en),			     // I
    
    .freq_index      (freq_index),		         // I [8:0]
	.freq_intp       (freq_intp),			     // I [5:0]
	
	.temp_idx        (temp_idx_chb),			     // I [4:0]
	.bw_idx          (bw_idx_sysclk_chb),		     // I [4:0]
//--------------------------------------------------------

	.we_satt         (we_satt),			         // I
	.addra_satt      (addra_satt),		         // I [13:0]
	.dataa_satt      (dataa_satt),		         // I [15:0]

	.we_ln           (we_lna),				     // I
	.addra_ln        (addra_lna),			     // I [8:0]
	.dataa_ln        (dataa_lna),			     // I [15:0]
	
	.we_gatt         (we_gatt),				     // I
	.addra_gatt_mode (addra_gatt_mode),	         // I
	.addra_gatt      (addra_gatt),			     // I [8:0]
	.dataa_gatt      (dataa_gatt),			     // I [6:0]
	
	.we_att_1        (we_att_chb_1),			     // I
	.addra_att_1     (addra_att_chb_1),		         // I [4:0]
	.dataa_att_1     (dataa_att_chb_1),		         // I [15:0]

	.we_att_2        (we_att_chb_2),			     // I
	.addra_att_2     (addra_att_chb_2),		         // I [4:0]
	.dataa_att_2     (dataa_att_chb_2),		         // I [15:0]

	.we_bw           (we_bw_chb),				     // I
	.addra_bw        (addra_bw_chb),			     // I [4:0]
	.dataa_bw        (dataa_bw_chb),			     // I [15:0]

	.we_temp         (we_temp_chb),				     // I
	.addra_temp      (addra_temp_chb),			     // I [4:0]
	.dataa_temp      (dataa_temp_chb),			     // I [15:0]
//--------------------------------------------------------

    // to Table from gc_control
	.satt_idx        (lvtb_satt_idx),		     // I [4:0]
	.lna_en          (lvtb_lna_en),			     // I	
	
	.att_1_idx       (lvtb_att_idx_chb_1),		     // I [4:0]
	.att_2_idx       (lvtb_att_idx_chb_2),		     // I [4:0]
	
	
    // output
	.gatt_ctrl       (lv_table_gatt_ctrl_chb),		 // O [6:0]    //NC
	.gatt_ctrl_valid (lv_table_gatt_ctrl_valid_chb), // O          //NC
	.lv_cal_out      (lv_cal_out_chb),			     // O [8:0]
	.anti_db_cal_out (anti_db_cal_out_chb),	         // O [7:0]
	.sum_cal         (sum_cal_chb)				     // O [17:0]

);
//==================================================================================================





//==================================================================================================
//deploy_yhj

gc_control gc_control(
	.sys_clk               (u_sys_clk),			           // I //125Mhz
	.sclr                  (rst),				           // I

	// Ach -----------------------------
	// table UpLoad Ach from reg
	.we_mgc                (we_mgc),				       // I
	.mgc_table_sel         (mgc_table_sel),		           // I
	.addr_mgc              (addr_mgc),			           // I [6:0]
	.data_mgc              (data_mgc),			           // I [15:0]

	// MGC from regs
	.mgc_en                (mgc_en),				       // I
	.mgc_idx               (mgc_idx),				       // I [6:0] // 27 ~ 127 까�? (dBuV (-80dBm ~ 20dBm))
	.mgc_idx_valid         (mgc_idx_valid),	               // I

	// User GC to/from regs
	.user_lna_en           (user_lna_en),				   // I
	.user_lna_en_valid     (user_lna_en_valid),	           // I
	.user_satt             (user_satt),					   // I [4:0]
	.user_satt_valid       (user_satt_valid),		       // I
	.user_att_cha_1        (user_att_cha_1),				   // I [4:0]
	.user_att_cha_1_valid  (user_att_cha_1_valid),		       // I
	.user_att_cha_2        (user_att_cha_2),				   // I [4:0]
	.user_att_cha_2_valid  (user_att_cha_2_valid),		       // I	
	.user_att_chb_1        (user_att_chb_1),				   // I [4:0]
	.user_att_chb_1_valid  (user_att_chb_1_valid),		       // I
	.user_att_chb_2        (user_att_chb_2),				   // I [4:0]
	.user_att_chb_2_valid  (user_att_chb_2_valid),		       // I
	
	.table_gatt_ctrl       (lv_table_gatt_ctrl_cha),	   // I [6:0]
	.table_gatt_ctrl_valid (lv_table_gatt_ctrl_valid_cha), // I
	.user_gatt1            (user_gatt1),				   // I [6:0]
	.user_gatt1_valid      (user_gatt1_valid),		       // I
	.user_gatt2            (user_gatt2),				   // I [6:0]
	.user_gatt2_valid      (user_gatt2_valid),	           // I


	// to/from rf_intf
	.att_ready             (dc_bd_att_ready),				// I
	.gc_lna                (dc_bd_lna_sw),					// O
	.gc_att                (dc_bd_din_att),					// O [15:0]
	.gc_att_valid          (dc_bd_din_att_valid),			// O

//--------------------------------------------------------
	.lna_en                (lna_en),		                // O
	.satt_idx              (satt_idx),		                // O [4:0]
	.att_cha_1_idx         (att_idx_cha_1),	                // O [4:0]
	.att_cha_2_idx         (att_idx_cha_2),	                // O [4:0]	
	.att_chb_1_idx         (att_idx_chb_1),    	            // O [4:0]
	.att_chb_2_idx         (att_idx_chb_2),	                // O [4:0]	
	.gatt1_val             (gatt1_val),	                    // O [6:0]
	.gatt2_val             (gatt2_val)    	                // O [6:0]


);    

wire 	full_lv_cal_cha;
wire 	empty_lv_cal_cha;
wire   lv_cal_out_cha_sig;
wire   anti_db_cal_out_cha_sig;

fifod_17x16 lv_cal_cha (
  .rst(rst),        						// input wire rst
  .wr_clk(u_sys_clk),  						// input wire wr_clk
  .rd_clk(u_sys_clk),  						// input wire rd_clk    
  .din({lv_cal_out_cha,anti_db_cal_out_cha}),	// input wire [16 : 0] din
  .wr_en(~full_lv_cal_cha),					// input wire wr_en
  .rd_en(~empty_lv_cal_cha),					// input wire rd_en
  .dout({lv_cal_out_cha_sig,anti_db_cal_out_cha_sig}),		// output wire [16 : 0] dout
  .full(full_lv_cal_cha),      // output wire full
  .empty(empty_lv_cal_cha),    // output wire empty
  .valid()    // output wire valid
);

wire 	full_lv_cal_chb;
wire 	empty_lv_cal_chb;
wire   lv_cal_out_chb_sig;
wire   anti_db_cal_out_chb_sig;

fifod_17x16 lv_cal_chb (
  .rst(rst),        						// input wire rst
  .wr_clk(u_sys_clk),  						// input wire wr_clk
  .rd_clk(u_sys_clk),  						// input wire rd_clk    
  .din({lv_cal_out_chb,anti_db_cal_out_chb}),	// input wire [16 : 0] din
  .wr_en(~full_lv_cal_chb),					// input wire wr_en
  .rd_en(~empty_lv_cal_chb),					// input wire rd_en
  .dout({lv_cal_out_chb_sig,anti_db_cal_out_chb_sig}),		// output wire [16 : 0] dout
  .full(full_lv_cal_chb),      // output wire full
  .empty(empty_lv_cal_chb),    // output wire empty
  .valid()    // output wire valid
);



//==================================================================================================


rf_intf rf_intf (
	.clk(u_sys_clk),			// I //125Mhz
	.sclr(rst),			// I

//// RF - DC Board
	// M1 - PE43610(PIN 0,1,4) 
	 	.RF_PIN0(RF_PIN0),	.RF_PIN1(RF_PIN1),  .RF_PIN4(RF_PIN4),  //O,   O,   O 
	 	.dc_bd_din_att(dc_bd_din_att), .dc_bd_din_att_valid(dc_bd_din_att_valid), .dc_bd_att_ready(dc_bd_att_ready), // I[15:0], I, O
    // M2 - LNA Switch (5)
        .RF_PIN5(RF_PIN5), .dc_bd_lna_sw(dc_bd_lna_sw), // O I
        
    // M3 - SN74HCS595BQBR(RF CTRL 2,3,6)	
	 	 .RF_PIN2(RF_PIN2),	.RF_PIN3(RF_PIN3), .RF_PIN6(RF_PIN6),   // O O O
	   .dc_bd_rf_sw(dc_bd_rf_sw),	.dc_bd_rf_sw_valid(dc_bd_rf_sw_valid), // I [15:0], I, 
	   .dc_bd_ifbw_idx(dc_bd_ifbw_idx),	.dc_bd_ifbw_idx_valid(dc_bd_ifbw_idx_valid), // I, I
	 	 
	// M4 - SN74HCS165BQB(CM 7,10,11)
		.RF_PIN7(RF_PIN7),	 	.RF_PIN10(RF_PIN10),  .RF_PIN11(RF_PIN11),  // O,    O,   I	 
        .dc_bd_din_sts_valid(dc_bd_din_sts_valid), .dc_bd_din_sts_ready(),// I, O
        .dc_bd_dout_sts(dc_bd_dout_sts), .dc_bd_dout_sts_valid(),   // O[15:0],  O
        
    // M5 - TMP102(8,9)
        .RF_PIN8(RF_PIN8),	.RF_PIN9(RF_PIN9),
	   .dc_bd_din_tmp102(dc_bd_din_tmp102),	   .dc_bd_din_tmp102_valid(dc_bd_din_tmp102_valid), // I [1:0], I
	   .dc_bd_dout_tmp102(dc_bd_dout_tmp102),	.dc_bd_busy_tmp102(dc_bd_busy_tmp102),	   // O[15:0], O
    // M6, M7 - LOG DETECTOR - LMV321AIDCKR(34,35)	
        .RF_PIN34(RF_PIN34),    .RF_PIN35(RF_PIN35), // I, I
    	.dc_bd_log_detect1(dc_bd_log_detect1), 	.dc_bd_log_detect2(dc_bd_log_detect2), // O, O
    	
//// RF - SYN Board

	// M1 - SN74HCS595BQBR(14,15,18), 
		.RF_PIN14(RF_PIN14), .RF_PIN15(RF_PIN15), .RF_PIN18(RF_PIN18),   // O,   O,   O      
	    .syn_bd_rf_sw(syn_bd_rf_sw),	.syn_bd_rf_sw_valid(syn_bd_rf_sw_valid), // I [15:0], I, 
	    .syn_bd_ifbw_idx(syn_bd_ifbw_idx),	.syn_bd_ifbw_idx_valid(syn_bd_ifbw_idx_valid), // I, I

	// M2 - LMK01010ISQ(30,31,32,33)
        .RF_PIN30(RF_PIN30),	.RF_PIN31(RF_PIN31),  .RF_PIN32(RF_PIN32),    .RF_PIN33(RF_PIN33),  // O, O, O, O
        .syn_bd_din_distributor (syn_bd_din_distributor),   .syn_bd_din_distributor_valid (syn_bd_din_distributor_valid), // I [31:0], I
        .syn_bd_distributor_ready (syn_bd_distributor_ready), // O
	// M3 - SN74HCS165BQB(12,13, 19)
		.RF_PIN12(RF_PIN12), .RF_PIN13(RF_PIN13),  .RF_PIN19(RF_PIN19),	// O, I, O
        .syn_bd_din_sts_valid(syn_bd_din_sts_valid), .syn_bd_din_sts_ready(syn_bd_din_sts_ready),// I O
        .syn_bd_dout_sts(syn_bd_dout_sts), .syn_bd_dout_sts_valid(syn_bd_dout_sts_valid),   // O O		

	// M4 - LMX2594RHAT(16,17,20,21,22,23)   
	    .RF_PIN16(RF_PIN16), .RF_PIN17(RF_PIN17), .RF_PIN20(RF_PIN20), .RF_PIN21(RF_PIN21),	// O, O, O, O
	    .RF_PIN22(RF_PIN22),	.RF_PIN23(RF_PIN23), // O, O
	    .syn_bd_din_pll_cmd(syn_bd_din_pll_cmd), .syn_bd_din_pll(syn_bd_din_pll), .syn_bd_din_pll_valid(syn_bd_din_pll_valid), // I [1:0] , I[23:0], I 
	    .syn_bd_pll_ctrl_interval(syn_bd_pll_ctrl_interval), .syn_bd_pll_last_cmd_in(syn_bd_pll_last_cmd_in), // I[15:0], O
    	.syn_bd_pll_cmd_done(syn_bd_pll_cmd_done), .syn_bd_dout_pll(syn_bd_dout_pll), .syn_bd_dout_pll_valid(syn_bd_dout_pll_valid),	  // O, O[23:0], O  
    		             	
	// M5 -  PE43610A-X(25,26,27)
        .RF_PIN25(RF_PIN25),	.RF_PIN26(RF_PIN26),    .RF_PIN27(RF_PIN27), // O, O, O
	 	.syn_bd_din_att(syn_bd_din_att), .syn_bd_din_att_valid(syn_bd_din_att_valid), .syn_bd_att_ready(syn_bd_att_ready), // I[15:0], O, O

	// M6 - TMP102(28,29)  
	   .RF_PIN28(RF_PIN28),	.RF_PIN29(RF_PIN29),    //  IO O
	   .syn_bd_din_tmp102(syn_bd_din_tmp102),	  .syn_bd_din_tmp102_valid(syn_bd_din_tmp102_valid), // I [1:0], I
	   .syn_bd_dout_tmp102(syn_bd_dout_tmp102),	  .syn_bd_busy_tmp102(syn_bd_busy_tmp102)	   // O[15:0], O
		
);
endmodule
