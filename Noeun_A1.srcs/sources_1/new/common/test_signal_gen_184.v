//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2015/12/09 16:40:05
// Design Name: HURA
// Module Name: test_signal_gen
// Project Name: HURA 2ch test-bed
// Target Devices: Kintex7 410T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision: build 1(2015/12/09)
// Additional Comments:
//
// Copyright 2018 (c) HURA. All rights reserved.
//////////////////////////////////////////////////////////////////////////////////
module test_signal_gen_184(
	input	wire       			sigclk,
	input	wire				ce,
	input	wire				sclr,
	input	wire 	[31:0] 		pinc_in,
	input	wire 	[31:0] 		poff_in,
	input	wire 	[15:0]    	tsig_scale,
	output	wire	[15:0] 		test_signal
);

wire [17:0]	  test_sine;

wire [47:0] m_axis_data_tdata;
assign test_sine = m_axis_data_tdata[17:0];

test_dds_184 test_dds (
  .aclk(sigclk),							// I
  .aclken(ce),								// I
  .aresetn(~sclr),							// I
  .s_axis_phase_tvalid(1'b1),				// I
  .s_axis_phase_tdata({poff_in,pinc_in}),	// I [63 : 0]
  .m_axis_data_tvalid(),					// O
  .m_axis_data_tdata(m_axis_data_tdata)		// O [47 : 0]
);

mult_s18s16 mag_control (
  .CLK(sigclk),			// I
  .A(test_sine),		// I [17 : 0]
  .B(tsig_scale),		// I [15 : 0]
  .P(test_signal)		// O [15 : 0]
);

endmodule
