//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2015/08/15 16:40:05
// Design Name:
// Module Name: data_merge2to1_v2
// Project Name:
// Target Devices: Kintex7 325T, EdgeRF(Guam)
// Tool Versions: Vivado 2020.2
// Description: N bits 신호를 2*N bits로 변경, din_valid duration 을 2배로
//
// Dependencies:
//
// Revision:
//				build 2(2018/07/16) : port 이름 변경
//				build 3(2020/12/18) : v2 - last 신호 반영
// Additional Comments:
//
// Copyright 2015-2020 (c) HURA. All rights reserved.
//////////////////////////////////////////////////////////////////////////////////
module data_merge2to1_v2 #(
	parameter NBITS = 32
) (
    input wire clk,
    input wire sclr,
    input wire [NBITS-1:0] din,
    input wire din_valid,
    input wire din_last,
	input wire endian,				// 0 : little endian, 1 : big endian
	output reg [2*NBITS-1:0] dout,	// {[31:0],[31:0]}
	output reg dout_valid,
	output reg dout_last,
	output reg dout_hv				// half valid : last 신호일때 1개만 valid일 때 1
);

reg [NBITS-1:0]	pre_data;
reg				pre_data_valid;
// getand hold last value of accum. or dout_valid signal
always @(posedge clk) begin
	if (sclr) begin
		dout <= 'b0;
		pre_data <= 'b0;
		pre_data_valid <= 1'b0;
		dout_valid <= 1'b0;
		dout_last <= 1'b0;
		dout_hv <= 1'b0;
	end
	else begin
		if (din_valid) begin
			if (din_last && pre_data_valid) begin
				if (endian)
					dout <= {pre_data,din};
				else
					dout <= {din,pre_data};
				dout_valid <= 1'b1;
				dout_last <= 1'b1;
				dout_hv <= 1'b0;
				pre_data <= 'b0;
				pre_data_valid <= 1'b0;
			end
			else if (din_last && !pre_data_valid) begin
				if (endian)
					dout <= {din,pre_data};	// 이전 데이터는 없기에 바로 넣고 더미(0)을 넣어줌
				else
					dout <= {pre_data,din};	// 이전 데이터는 없기에 바로 넣고 더미(0)을 넣어줌
				dout_valid <= 1'b1;
				dout_last <= 1'b1;
				dout_hv <= 1'b1;
				pre_data <= 'b0;
				pre_data_valid <= 1'b0;
			end
			else if (pre_data_valid) begin			// last 가 아닐때
				if (endian)
					dout <= {pre_data,din};
				else
					dout <= {din,pre_data};
				dout_valid <= 1'b1;
				dout_last <= 1'b0;
				dout_hv <= 1'b0;
				pre_data <= 'b0;			// 일단 더미(0) 넣어 last 신호에 대비
				pre_data_valid <= 1'b0;
			end
			else begin		// last도 아니고, count_r도 아닐때.
				dout <= dout;
				dout_valid <= 1'b0;
				dout_last <= 1'b0;
				dout_hv <= 1'b0;
				pre_data <= din;
				pre_data_valid <= 1'b1;
			end

		end
		else begin
			dout <= dout;
			dout_valid <= 1'b0;
			dout_last <= 1'b0;
			dout_hv <= 1'b0;
			pre_data <= pre_data;
			pre_data_valid <= pre_data_valid;
		end
	end
end

endmodule
