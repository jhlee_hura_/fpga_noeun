//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2018/07/30 12:58:00
// Design Name: pci express DMA
// Module Name: pls_expander
// Project Name: test pci expressDMA
// Target Devices: Kintex7 410T
// Tool Versions: Vivado 2018.1
// Description: Pulse 확장
//
// Dependencies:
//
// Revision: build 2. port 명 변경
// Additional Comments:
//
// Copyright 2015-2018 (c) HURA. All rights reserved.
//////////////////////////////////////////////////////////////////////////////////
// POLARITY가 0 이면 sin 이 High 일때 Low 펄스가 길게..
// POLARITY가 1 이면 sin 이 High 일때 High 펄스가 길게..

module pls_expander #(
	parameter COUNT = 10,		// count는 0에서 부터 시작 갯수-1개.
	parameter POLARITY = 1'b1 	// output wave form
) (
   	input wire sin,
	input wire clk,
	output reg sout
);

localparam NBITS = 	(COUNT <= 2) ? 1 :
					(COUNT <= 4) ? 2 :
					(COUNT <= 8) ? 3 :
					(COUNT <= 16) ? 4 :
					(COUNT <= 32) ? 5 :
					(COUNT <= 64) ? 6 :
					(COUNT <= 128) ? 7 :
					(COUNT <= 256) ? 8 :
					(COUNT <= 512) ? 9 :
					(COUNT <= 1024) ? 10 :
					(COUNT <= 2048) ? 11 :
					(COUNT <= 4096) ? 12 :
					(COUNT <= 8192) ? 13 :
					(COUNT <= 16384) ? 14 :
					(COUNT <= 32768) ? 15 :
					(COUNT <= 65536) ? 16 : 32;

reg [NBITS-1:0] clk_num;

always @(posedge clk) begin
	if (sin) begin
		clk_num <= COUNT-1'b1;
		sout <= POLARITY;
	end
	else if(clk_num != 0) begin
		clk_num <= clk_num - 1'b1;
		sout <= POLARITY;
	end
	else begin
		clk_num <= 0;
		sout <= ~POLARITY;
	end
end

endmodule
