//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2018/07/26 12:00:00
// Design Name: delay_regi
// Module Name: delay_regi.v
// Project Name:
// Target Devices: any Devices
// Tool Versions: Vivado 2018.1
// Description: signal Delay
//
// Dependencies:
//
// Revision: build.1
// Revision: build.2 : 36 delay가 36 넘어가면 에러나는 문제 해결
// Additional Comments:
//
// Copyright 2018 (c) HURA. All rights reserved.
//////////////////////////////////////////////////////////////////////////////////
module delay_regi # (
	parameter DIN_WIDTH = 1,
	parameter DELAY = 1
) (
	input wire clk,
	input wire sclr,
	input wire [DIN_WIDTH-1: 0] din,
	output wire [DIN_WIDTH-1: 0] dout
);


reg  [DIN_WIDTH-1:0] din_r [DELAY-1:0];  // 2d


always @(posedge clk) begin
	if (sclr)
		din_r[0] <= 'b0;
	else
		din_r[0] <= din;
end

genvar i;
generate
	for (i=0; i < (DELAY-1); i=i+1) begin : srl
		always @(posedge clk) begin
			if (sclr)
				din_r[i+1] <= 'b0;
			else begin
				din_r[i+1] <= din_r[i];
			end
		end
    end
endgenerate

assign dout = din_r[DELAY-1];

/* reg  [DELAY-1:0] din_r [DIN_WIDTH-1:0];  // 2d

genvar i;
generate
    for (i=0; i < DIN_WIDTH; i=i+1) begin : srl
		always @(posedge clk) begin
			if (sclr)
				din_r[i] <= 'b0;
			else
				din_r[i] <= {din_r[i][DELAY-2:0], din[i]};
		end

		assign dout[i] = din_r[i][DELAY-1];
    end
endgenerate */

endmodule