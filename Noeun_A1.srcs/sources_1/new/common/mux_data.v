//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2016/05/26 08:53:37
// Design Name:
// Module Name: mux_data.v
// Project Name:
// Target Devices: Kintex7 410T
// Tool Versions: Vivado 2015.2
// Description:
//
// Dependencies:
//
// Revision : build 1(2016/05/26)
// Additional Comments:
//
// Copyright 2016 (c) HURA. All rights reserved.
//////////////////////////////////////////////////////////////////////////////////
module mux_data #(
	parameter DATA_WIDTH = 16,
	parameter NUM_DIN = 2,
	localparam MSB_NUM_DIN = 	(NUM_DIN <= 2) ? 1 :
								(NUM_DIN > 2 && NUM_DIN <= 4) ? 2 :
								(NUM_DIN > 4 && NUM_DIN <= 8) ? 3 :
								(NUM_DIN > 8 && NUM_DIN <= 16) ? 4 :
								(NUM_DIN > 16 && NUM_DIN <= 32) ? 5 :
								(NUM_DIN > 32 && NUM_DIN <= 64) ? 6 : 0
) (
	input [(DATA_WIDTH*NUM_DIN)-1:0] din,
	input [MSB_NUM_DIN-1:0] sel,
	output [DATA_WIDTH-1:0] dout
);

assign 	dout = 	din >>	(sel*DATA_WIDTH);

endmodule