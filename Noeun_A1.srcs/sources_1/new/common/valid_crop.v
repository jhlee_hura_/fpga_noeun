//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/07/01 19:40:05
// Design Name: Valid Crop
// Module Name: valid_crop.v
// Project Name:
// Target Devices: Kintex7 410T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision: build 1(2016/11/22)
// Revision: build 2(2019/04/13) : din_start 신호 추가
// Additional Comments:
//
// Copyright 2019 (c) HURA. All rights reserved.
//////////////////////////////////////////////////////////////////////////////////
module valid_crop #(
	parameter NBIT_DATA = 31,
	parameter NBIT_OFFSET = 8,
	parameter NBIT_LEN = 12
) (
	input wire clk,
//	input wire sclr,

	input wire [NBIT_OFFSET-1:0] st_offset,
	input wire [NBIT_LEN-1:0]	length,

	input wire [NBIT_DATA-1:0]	din,
	input wire					din_valid,
	input wire					din_last,
//	input wire					din_start,

	output reg [NBIT_DATA-1:0]	dout='b0,
	output reg					dout_valid='b0,
	output reg					dout_last='b0,
	output reg					dout_start='b0
); // latency = 1;


reg [NBIT_OFFSET-1:0] cnt_offset='b0;
reg [NBIT_LEN-1:0] cnt_len='b0;

reg [NBIT_OFFSET-1:0] st_offset_r='b0;
reg [NBIT_LEN-1:0] length_r='b1;
reg	reset_flag='b0;

always @(posedge clk) begin
	dout <= din;

	if (din_last) begin
		st_offset_r <= st_offset;
		length_r <= length;
	end

	if (din_valid) begin
		if (cnt_offset == st_offset_r) begin
			cnt_offset <= (din_last) ? 'b0 : cnt_offset;
			if (cnt_len < length_r) begin
				dout_valid <= 1'b1;
				cnt_len <= (din_last) ? 'b0 : cnt_len + 1'b1;
				dout_start <= (cnt_len == 'b0) ? 1'b1 : 1'b0;
				dout_last <= (cnt_len == (length_r-1'b1)) ? 1'b1 : 1'b0;
			end
			else begin
				cnt_len <= (din_last) ? 'b0 : cnt_len;
				dout_start <= 1'b0;
				dout_last <= 1'b0;
				dout_valid <= 1'b0;
			end
		end
		else begin
			cnt_offset <= (din_last) ? 'b0 : cnt_offset + 1'b1;
			dout_start <= 1'b0;
			dout_last <= 1'b0;
			dout_valid <= 1'b0;
		end
	end
	else begin
		dout_start <= 1'b0;
		dout_last <= 1'b0;
		dout_valid <= 1'b0;
	end

end

endmodule