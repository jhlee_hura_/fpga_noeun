//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/09/07 11:13:48
// Design Name: register for MARS X1 Board Support Configuration(BSC)
// Module Name: uart_lite (using axi_uartlite)
// Project Name:
// Target Devices: Kintex7 410T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// Additional Comments:
//
// Copyright 2019 (c) HURA. All rights reserved.
//////////////////////////////////////////////////////////////////////////////////
module uart_lite (
	input	wire 				clk,
	input	wire				sclr,

	input	wire				UART1_RXD,
	output	wire				UART1_TXD,

	input	wire [7:0]			w_data,
	input	wire 				w_data_valid,
	input	wire 				rd_en,
	output	reg [7:0]			r_data,
	output	reg					r_data_valid,

	input	wire				tx_fifo_rst,
	input	wire				rx_fifo_rst,
	input	wire				en_interrupt,
	input	wire				cfg_valid,

	input	wire				status_req,
	output	reg	[7:0]			status
);


localparam RX_FIFO_REG = 'h0;
localparam TX_FIFO_REG = 'h4;
localparam STATUS_REG = 'h8;
localparam CFG_REG = 'hC;

wire [31:0] s_axi_wdata;
wire [31:0] s_axi_rdata;
reg [3:0]	s_axi_awaddr;
reg			s_axi_awvalid;
wire		s_axi_awready;
reg [7:0]	axi_wdata;
reg			s_axi_wvalid;
reg [3:0]	s_axi_araddr;
reg			s_axi_arvalid;

always @(posedge clk) begin
	s_axi_awvalid <=	(s_axi_awvalid && s_axi_awready) ? 'b0 :
						(w_data_valid || cfg_valid) ? 1'b1 : s_axi_awvalid;

	s_axi_wvalid <=		(s_axi_wvalid && s_axi_wready) ? 'b0 :
						(w_data_valid || cfg_valid) ? 1'b1 : s_axi_wvalid;

	s_axi_awaddr <= 	(w_data_valid) ? TX_FIFO_REG :
						(cfg_valid) ? CFG_REG : s_axi_awaddr;

	axi_wdata <=		(w_data_valid) ? w_data :
						(cfg_valid) ? {3'b0,en_interrupt,2'b0,rx_fifo_rst,tx_fifo_rst} : axi_wdata;

	s_axi_arvalid <=	(s_axi_arvalid && s_axi_arready) ? 'b0 :
						(rd_en || status_req) ? 1'b1 : s_axi_arvalid;

	s_axi_araddr <=		(rd_en) ? RX_FIFO_REG :
						(status_req) ? STATUS_REG : s_axi_araddr;

	r_data <=		(s_axi_rvalid && s_axi_araddr == RX_FIFO_REG ) ? s_axi_rdata[7:0] : r_data;

	status <=		(s_axi_rvalid && s_axi_araddr == STATUS_REG ) ? s_axi_rdata[7:0] : r_data;

	r_data_valid <= (s_axi_rvalid && s_axi_araddr == RX_FIFO_REG );
end

assign s_axi_wdata = {24'b0, axi_wdata};

axi_uartlite_0 axi_uartlite_0 (
  .s_axi_aclk(clk),					// I
  .s_axi_aresetn(~sclr),			// I
  .interrupt(),						// O
  .s_axi_awaddr(s_axi_awaddr),		// I [3 : 0]
  .s_axi_awvalid(s_axi_awvalid),	// I
  .s_axi_awready(s_axi_awready),	// O
  .s_axi_wdata(s_axi_wdata),		// I [31 : 0]
  .s_axi_wstrb(4'hf),      			// I [3 : 0]
  .s_axi_wvalid(s_axi_wvalid),		// I
  .s_axi_wready(s_axi_wready),		// O
  .s_axi_bresp(),					// O [1 : 0]
  .s_axi_bvalid(),    				// O
  .s_axi_bready(1'b1),    			// I
  .s_axi_araddr(s_axi_araddr),		// I [3 : 0]
  .s_axi_arvalid(s_axi_arvalid),	// I
  .s_axi_arready(s_axi_arready),	// O
  .s_axi_rdata(s_axi_rdata),		// O [31 : 0]
  .s_axi_rresp(),					// O [1 : 0]
  .s_axi_rvalid(s_axi_rvalid),		// O
  .s_axi_rready(1'b1),    			// I
  .rx(UART1_RXD),					// I
  .tx(UART1_TXD)					// O
);


endmodule