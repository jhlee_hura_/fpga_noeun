`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/07/27 17:45:45
// Design Name: 
// Module Name: board_perip_intf
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module board_perip_intf(
	input	wire		clk,	//sys_clk
	input	wire		sclr,

	// ADC SPI A
	//output	wire		adcA_sck,
	//output	wire		adcA_scs,
	//inout	wire		adcA_sdio,
	
    input wire  ADC_SDOUT,    
    output wire ADC_CS_B, // ADC_SEN    
    output wire ADC_SCLK,
    output wire ADC_SDATA,
    
	input	wire [23:0] din_adcA_spi,
	input	wire		div_adcA_spi,
	output	wire		dir_adcA_spi,
	output	wire [23:0] do_adcA_spi,
	output	wire		dov_adcA_spi,

    // LMK04821
    output wire    LMK_CS_B,	
    output wire    LMK_SCK,	
    inout wire     LMK_SDIO,

	// TMP102 Temp Sensor IN
	output	wire 		TMP_SCL_IN,
	inout	wire 		TMP_SDA_IN,
	output wire [15:0] 	dout_tmp102_in,
	output wire 		busy_tmp102_in,
	
	// TMP102 Temp Sensor EX
	output	wire 		TMP_SCL_EX,
	inout	wire 		TMP_SDA_EX,
	output wire [15:0] 	dout_tmp102_ex,
	output wire 		busy_tmp102_ex,
		
	input wire 	[1:0] 	din_tmp102,
	input wire 			din_tmp102_valid,

	// HMC1033 PLL SPI
	output	wire		hmcpll_sck,
	output	wire		hmcpll_scs,
	output	wire		hmcpll_sdo,
	input	wire		hmcpll_sdi,

	input	wire [31:0] din_hmcpll_spi,
	input	wire		div_hmcpll_spi,
	output	wire		dir_hmcpll_spi,
	output	wire [31:0] do_hmcpll_spi,
	output	wire		dov_hmcpll_spi,

	// PWR FAN Speed control
	output	wire		pwr_fan,
	input wire	[15:0]	pwr_fan_high_dura,
	input wire	[15:0]	pwr_fan_low_dura

);
//assign hmcpll_sck = 1'b1;
//assign hmcpll_scs = 1'b1;
//assign hmcpll_sdo = 1'b1;
// ---------------------------------------------------------------------------------
//	ADC 1 SPI Port
// ---------------------------------------------------------------------------------

spi_intf #(
	.SCLK_PERIOD(4),
	.CLK_START_END(2),
	.HIGH_CLK(2),
	.CLK_OFFSET(1),
	.CONT_WIDTH(16),
	.DATA_WIDTH(8),
	.RW_FLAG_BIT(23),
	.SCS_N_MODE(0),
	.SCLK_IDLE_MODE(0)
) spi_intf_adc_ads42jb49 (
	.clk		(clk),				// I
	.sclr		(sclr),				// I

	.sclk		(ADC_SCLK),			// O
	.scs_n		(ADC_CS_B),			// O
	.sdo		(ADC_SDATA),			// O
	.sdi		(ADC_SDOUT),			// I
	.iobuf_T	(),		// O

	.din		(din_adcA_spi),		// I [WORD_WIDTH-1:0]
	.din_valid	(div_adcA_spi),		// I
	.din_ready	(dir_adcA_spi),		// O
	.dout		(do_adcA_spi),		// O [WORD_WIDTH-1:0]
	.dout_valid	(dov_adcA_spi)		// O
);

// --------------------------------------------------------------------------------
// tmp102 A -> internal
wire [39:0] din_tmp102_a;
assign din_tmp102_a = {5'b10010,din_tmp102,1'b0,8'b0,5'b10010,din_tmp102,1'b1,16'b0};

wire tmp_sdi_a;
wire tmp_sdo_a;
wire tmp_sda_T_a;
IOBUF  i2c_tmp102_iobuf_a (
	.O(tmp_sdi_a),		// Buffer output
	.IO(TMP_SDA_IN),		// Buffer inout port (connect directly to top-level port)
	.I(tmp_sdo_a),		// Buffer input
	.T(tmp_sda_T_a)		// 3-state enable input, high=input, low=output
);
i2c_intf #(
	.SCLK_PERIOD(320),		// (2?΄?) //tmp102 400kHz. 240?Όλ‘? ?΄?Ό ?¨. μ§?κΈμ? ??€?Έ
	.CLK_START_END(160), 	// (SCLK_PERIOD-1)?? CLK_START_END λ³΄λ€ ?° κ°μ λΉνΈ.
	.HIGH_CLK(105),
	.CLK_OFFSET(105), 		// SCLK_PERIOD-HIGH_CLK λ³΄λ€ ???Ό?¨.(? ?Έ? λΉν΄ HIGHκ°? λ°?λ¦°λ)
	.CHIPSEL_WIDTH(8),		// slave address bit width
	.CONT_WIDTH(8),			// (0 κ°??₯) control
	.RCHIPSEL_WIDTH(8),		// (0 κ°??₯) read slave address bit width
	.DATA_WIDTH(16),		//
	.RW_FLAG_BIT(16),
	.DATA_NUM(1)				// data bit? λ°λ³΅
)
i2c_tmp102_a(
	.clk(clk),					// I
	.sclr(sclr),				// I

	.sclk(TMP_SCL_IN),				// O
	.sdo(tmp_sdo_a),				// O
	.sdi(tmp_sdo_a),				// I
	.iobuf_T(tmp_sda_T_a),		// O

	.din(din_tmp102_a),				// I [WORD_WIDTH-1:0]
	.din_valid(din_tmp102_valid),	// I
	.dout(dout_tmp102_in),				// O [WORD_WIDTH-1:0]
	.dout_valid(),					// O

	.s_busy(busy_tmp102_in)			// O
); //total latency

// --------------------------------------------------------------------------------
// tmp102 B -> external
wire [39:0] din_tmp102_b;
assign din_tmp102_b = {5'b10010,din_tmp102,1'b0,8'b0,5'b10010,din_tmp102,1'b1,16'b0};

wire tmp_sdi_b;
wire tmp_sdo_b;
wire tmp_sda_T_b;
IOBUF  i2c_tmp102_iobuf (
	.O(tmp_sdi_b),		// Buffer output
	.IO(TMP_SDA_EX),		// Buffer inout port (connect directly to top-level port)
	.I(tmp_sdo_b),		// Buffer input
	.T(tmp_sda_T_b)		// 3-state enable input, high=input, low=output
);
i2c_intf #(
	.SCLK_PERIOD(320),		// (2?΄?) //tmp102 400kHz. 240?Όλ‘? ?΄?Ό ?¨. μ§?κΈμ? ??€?Έ
	.CLK_START_END(160), 	// (SCLK_PERIOD-1)?? CLK_START_END λ³΄λ€ ?° κ°μ λΉνΈ.
	.HIGH_CLK(105),
	.CLK_OFFSET(105), 		// SCLK_PERIOD-HIGH_CLK λ³΄λ€ ???Ό?¨.(? ?Έ? λΉν΄ HIGHκ°? λ°?λ¦°λ)
	.CHIPSEL_WIDTH(8),		// slave address bit width
	.CONT_WIDTH(8),			// (0 κ°??₯) control
	.RCHIPSEL_WIDTH(8),		// (0 κ°??₯) read slave address bit width
	.DATA_WIDTH(16),		//
	.RW_FLAG_BIT(16),
	.DATA_NUM(1)				// data bit? λ°λ³΅
)
i2c_tmp102_b(
	.clk(clk),					// I
	.sclr(sclr),				// I

	.sclk(TMP_SCL_EX),				// O
	.sdo(tmp_sdo_b),				// O
	.sdi(tmp_sdi_b),				// I
	.iobuf_T(tmp_sda_T_b),		// O

	.din(din_tmp102_b),				// I [WORD_WIDTH-1:0]
	.din_valid(din_tmp102_valid),	// I
	.dout(dout_tmp102_ex),				// O [WORD_WIDTH-1:0]
	.dout_valid(busy_tmp102_ex),					// O

	.s_busy()			// O
); //total latency

// ---------------------------------------------------------------------------------
//	Clock PLL SPI Port (HMC1033)
// ---------------------------------------------------------------------------------
 spi_intf #(
	.SCLK_PERIOD(12),
	.CLK_START_END(6),
	.HIGH_CLK(6),
	.CLK_OFFSET(3),
	.CONT_WIDTH(0),
	.DATA_WIDTH(32),
	.RW_FLAG_BIT(0),
	.SCS_N_MODE(1),
	.SCLK_IDLE_MODE(1)
) spi_intf_syn (
	.clk		(clk),					// I
	.sclr		(sclr),					// I

	.sclk		(hmcpll_sck),			// O hmcpll_sck
	.scs_n		(hmcpll_scs),			// O hmcpll_scs
	.sdo		(hmcpll_sdo),			// O hmcpll_sdo
	.sdi		(hmcpll_sdi),			// I
	.iobuf_T	(),						// O

	.din		(din_hmcpll_spi),		// I [WORD_WIDTH-1:0]
	.din_valid	(div_hmcpll_spi),		// I
	.din_ready	(dir_hmcpll_spi),		// O
	.dout		(do_hmcpll_spi),		// O [WORD_WIDTH-1:0]
	.dout_valid	(dov_hmcpll_spi)		// O

);

// ---------------------------------------------------------------------------------
//	Fan speed Control (PWM)
// ---------------------------------------------------------------------------------
pwm #(
	.MAX_CLK_CNT_BIT(16)
) pwm_pwrfan(
	.clk(clk),					// I
	.sclr(sclr),				// I

	.high_dura(pwr_fan_high_dura),	// I [MAX_CLK_CNT_BIT-1:0]
	.low_dura(pwr_fan_low_dura),	// I [MAX_CLK_CNT_BIT-1:0]

	.pwm_out(pwr_fan)		// O
);


endmodule