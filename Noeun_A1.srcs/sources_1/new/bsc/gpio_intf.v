//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/09/16 13:37:37
// Design Name:
// Module Name: GPIO interface
// Project Name: MERCURY A3 board support configuration
// Target Devices: Kintex7 410T, Mercury A3 board
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build 1 (2019/09/16)
// Additional Comments:
//
// Copyright 2019 (c) HURA. All rights reserved.
//////////////////////////////////////////////////////////////////////////////////
module gpio_intf #(
	parameter NBIT = 4
) (
	inout wire [NBIT-1:0]	GPIO,

	input wire [NBIT-1:0]	din,
	input wire [NBIT-1:0]	d_T,
	output wire	[NBIT-1:0]	dout
);

  genvar          l;
  generate
	for (l = 0; l <= (NBIT-1); l = l + 1) begin : gpio_if
		IOBUF GPIO_iobuf (
			.O(dout[l]),	// Buffer output
			.IO(GPIO[l]),	// Buffer inout port (connect directly to top-level port)
			.I(din[l]),		// Buffer input
			.T(d_T[l])		// 3-state enable input, high=input, low=output
		);
	end
  endgenerate

endmodule