`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer:  LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2018/04/25 21:04:48
// Design Name: register for MARS X1 Board Support Configuration(BSC)
// Module Name: axil2regs (bridge to regs from axil master in XDMA_PCIE)
// Project Name: HURA MARS X1 EVM
// Target Devices: ku060-ffva1156, MARS X1 board
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// Additional Comments:
//
// Copyright 2018, All rights reserved by HURA
//////////////////////////////////////////////////////////////////////////////////
module axil2regs (
	input	wire 				clk,	// pcie_clk
	input	wire				sclr,

	//-- AXI LITE
	//-- AXI Slave interface
	input 	wire [13:0]			s_axil_awaddr,
	input	wire				s_axil_awvalid,
	output	reg					s_axil_awready = 1'b0,

	input  	wire [31:0] 		s_axil_wdata,
//	input  	wire [3:0] 			s_axil_wstrb,
	input  	wire				s_axil_wvalid,
	output 	reg					s_axil_wready = 1'b0,

	output 	reg [1:0]			s_axil_bresp,
	output 	reg         		s_axil_bvalid, // write response valid
	input  	wire         		s_axil_bready,

	input  	wire [13:0] 		s_axil_araddr,
	input  	wire				s_axil_arvalid,
	output 	reg					s_axil_arready = 1'b0,

	output 	reg [31:0]			s_axil_rdata,
	output 	reg [1:0]			s_axil_rresp,
	output 	reg					s_axil_rvalid = 1'b0,
	input  	wire				s_axil_rready,

	// -------------------------------------------------------
	output	reg		[46:0]		regs_adda = 47'b0,
	output	reg					regs_adda_valid = 1'b0,
	output	reg					regs_bsc_sys_sel = 1'b0,
	output	reg					regs_bsc_sig_sel = 1'b0,
	output	reg					regs_bsc_pci_sel = 1'b0,
	output	reg					regs_ud_sys_sel = 1'b0,
	output	reg     			regs_ud_sig_sel = 1'b0,


	input	wire	[31:0]		regs_bsc_sys_data,
	input	wire	[31:0]		regs_bsc_sig_data,
	input	wire	[31:0]		regs_bsc_pci_data,
	input	wire	[31:0]		regs_ud_sys_data,
	input	wire	[31:0]		regs_ud_sig_data,


	input	wire				regs_data_valid,
	output	wire				regs_data_ready	

);

// ---------------------------------------------------------------------------------
// Function
// ---------------------------------------------------------------------------------
function			f_addr_dec;
	input 	[15:0] 		a;
	input 	[15:0] 		a_from;
	input 	[15:0] 		a_to;

	begin
		f_addr_dec = (a_from <= a) & (a <= a_to);
	end
endfunction


wire 	[13:0]		awaddr14;
wire 	[13:0]		araddr14;
reg 	[13:0] 		awaddr14_r		= 'b0;
reg 	[13:0] 		araddr14_r		= 'b0;
reg		[31:0]		wdata_r			= 'b0;
reg					awaddr_asset	= 1'b0;
reg					wdata_asset		= 1'b0;
reg					araddr_asset	= 1'b0;
reg					rdata_wait		= 1'b0;

assign 			awaddr14 = s_axil_awaddr;//s_axil_awaddr[15:2];
assign 			araddr14 = s_axil_araddr;//s_axil_araddr[15:2];
assign 			regs_data_ready = ~s_axil_rvalid;
always @(posedge clk) begin
	if (sclr) begin
		s_axil_awready	<= 1'b0;
		s_axil_wready	<= 1'b0;
		s_axil_bresp 	<= 2'b00;
		s_axil_bvalid	<= 1'b0;
		s_axil_arready	<= 1'b0;
		s_axil_rdata	<= 32'b0;
		s_axil_rresp	<= 2'b0;
		s_axil_rvalid	<= 1'b0;

		regs_adda			<= 47'b0;
		regs_adda_valid		<= 1'b0;
		regs_bsc_sys_sel	<= 1'b0;
		regs_bsc_sig_sel	<= 1'b0;
		regs_bsc_pci_sel	<= 1'b0;
		regs_ud_sys_sel		<= 1'b0;
		regs_ud_sig_sel		<= 1'b0;


		awaddr_asset	<= 1'b0;
		wdata_asset		<= 1'b0;
		araddr_asset	<= 1'b0;
		wdata_r			<= 32'b0;
		rdata_wait		<= 1'b0;
	end
	else begin
		// write address bus
		if (s_axil_awvalid && s_axil_awready) begin
			s_axil_awready 	<= 1'b0;
			awaddr_asset	<= 1'b1;
			awaddr14_r		<= awaddr14;
		end
		else begin
			if (~s_axil_awready && ~awaddr_asset) begin	// μ΄κΈ° ???Ό? ready asset
				s_axil_awready <= 1'b1;
			end
		end

		// write data bus
		if (s_axil_wvalid && s_axil_wready) begin
			s_axil_wready	<= 1'b0;
			wdata_asset		<= 1'b1;
			wdata_r			<= s_axil_wdata;
		end
		else begin
			if (~s_axil_wready && ~wdata_asset) begin	// μ΄κΈ° ???Ό? ready asset
				s_axil_wready <= 1'b1;
			end
		end

		// bresp data bus
		if (s_axil_bvalid && s_axil_bready) begin
			s_axil_bvalid 	<= 1'b0;
		end

		// read address bus
		if (s_axil_arvalid && s_axil_arready) begin
			araddr14_r 		<= araddr14;
			araddr_asset	<= 1'b1;
			rdata_wait		<= 1'b1;	// rdata λ₯? λ³΄λΌ ?κΉμ? arreadyλ₯? 0?Όλ‘? ?κΈ? ?? wait flag
			s_axil_arready 	<= 1'b0;
		end
		else begin
			if (~s_axil_arvalid && ~araddr_asset && ~rdata_wait) begin	// μ΄κΈ° ???Ό? ready asset
				s_axil_arready <= 1'b1;
			end
		end
		// read data response 
		if (s_axil_rvalid && s_axil_rready) begin
			s_axil_rvalid <= 1'b0;
			s_axil_rresp <= 2'b0;
		end	
		// write addr setting  // araddr, awaddrκ°? ?? ? κ·Όμ? write ?°? 
		if (awaddr_asset && wdata_asset) begin
			regs_adda		<= {1'b1,awaddr14_r,wdata_r};
			regs_adda_valid	<= 1'b1;
			regs_bsc_sys_sel	<= f_addr_dec({awaddr14_r,2'b0}, `BSC_REG1_ADDR_START, `BSC_REG1_ADDR_END); //UD_REG2_ADDR_END
			regs_bsc_sig_sel 	<= f_addr_dec({awaddr14_r,2'b0}, `BSC_REG2_ADDR_START, `BSC_REG2_ADDR_END);
			regs_bsc_pci_sel  	<= f_addr_dec({awaddr14_r,2'b0}, `BSC_PCIE_ADDR_START, `BSC_PCIE_ADDR_END);
			regs_ud_sys_sel 	<= f_addr_dec({awaddr14_r,2'b0}, `UD_REG1_ADDR_START, `UD_REG1_ADDR_END);
			regs_ud_sig_sel 	<= f_addr_dec({awaddr14_r,2'b0}, `UD_REG2_ADDR_START, `UD_REG2_ADDR_END);
//			regs_ud_reg3_sel 	<= f_addr_dec({awaddr14_r,2'b0}, `UD_REG3_ADDR_START, `UD_REG3_ADDR_END);
//			regs_ud_reg4_sel 	<= f_addr_dec({awaddr14_r,2'b0}, `UD_REG4_ADDR_START, `UD_REG4_ADDR_END);
			s_axil_bvalid	<= 1'b1;
			s_axil_awready 	<= 1'b1;
			s_axil_wready	<= 1'b1;
			awaddr_asset	<= 1'b0;
			wdata_asset		<= 1'b0;
		end
		// read address
		else if (araddr_asset) begin
			regs_adda 		<= {1'b0,araddr14_r,32'b0};
			regs_adda_valid	<= 1'b1;
			regs_bsc_sys_sel 	<= f_addr_dec({araddr14_r,2'b0}, `BSC_REG1_ADDR_START, `BSC_REG1_ADDR_END); //BSC_REG1_ADDR_END
			regs_bsc_sig_sel 	<= f_addr_dec({araddr14_r,2'b0}, `BSC_REG2_ADDR_START, `BSC_REG2_ADDR_END);
			regs_bsc_pci_sel  	<= f_addr_dec({araddr14_r,2'b0}, `BSC_PCIE_ADDR_START, `BSC_PCIE_ADDR_END);
			regs_ud_sys_sel 	<= f_addr_dec({araddr14_r,2'b0}, `UD_REG1_ADDR_START, `UD_REG1_ADDR_END);
			regs_ud_sig_sel 	<= f_addr_dec({araddr14_r,2'b0}, `UD_REG2_ADDR_START, `UD_REG2_ADDR_END);
//			regs_ud_reg3_sel 	<= f_addr_dec({araddr14_r,2'b0}, `UD_REG3_ADDR_START, `UD_REG3_ADDR_END);
//			regs_ud_reg4_sel 	<= f_addr_dec({araddr14_r,2'b0}, `UD_REG4_ADDR_START, `UD_REG4_ADDR_END);
			araddr_asset	<= 1'b0;
		end
		else begin
			regs_adda_valid	<= 1'b0;
		end

		// read regs
		if (regs_data_valid) begin
			s_axil_rdata <= (regs_bsc_sys_sel)	? regs_bsc_sys_data :
							(regs_bsc_sig_sel)	? regs_bsc_sig_data :
							(regs_bsc_pci_sel)	? regs_bsc_pci_data :
							(regs_ud_sys_sel)	? regs_ud_sys_data :
							(regs_ud_sig_sel)	? regs_ud_sig_data :32'h0;
			s_axil_rvalid <= 1'b1;
			rdata_wait <= 1'b0;
		end
		else begin
		      s_axil_rdata <= 32'b0;
		      s_axil_rvalid <= 1'b0;   
		end
		
	
	end
end

endmodule