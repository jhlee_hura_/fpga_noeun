`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/12/01 09:36:28
// Design Name: 
// Module Name: pps_time
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module pps_time#(
	parameter CLKPERSEC = 245760000
) (
	input wire 			clk,
	input wire			sclr,

	input wire 			GPS_PPS,

	input wire [15:0] 	set_date,
	input wire 			set_date_valid,

	input wire 	[4:0]	set_time_H,
	input wire 	[5:0]	set_time_M,
	input wire 	[5:0]	set_time_S,
	input wire 			set_time_valid,

	output reg			pps_reged,
	output reg [15:0]	pps_date = 15'd0,
	output reg [4:0]	pps_time_H = 5'd1,
	output reg [5:0]	pps_time_M = 6'd0,
	output reg [5:0]	pps_time_S = 6'd0,
	output reg [6:0]	pps_time_10ms = 7'd0,
	output reg [16:0]	sec_in_day = 17'd0,
	output reg [29:0]	nano_sec = 30'd0,
	output reg [31:0]	pps_count_syson
);

// κ³±νκΈ? ?κ³? ?¬??Έ ?κ³?
localparam MYLT_NANOx32 =		( CLKPERSEC == 245760000 ) ? 8'd130 :
								( CLKPERSEC == 184320000 ) ? 8'd174 : 8'd174;
localparam MAX_NANO = CLKPERSEC * MYLT_NANOx32;


localparam CLKPER10mSEC = CLKPERSEC / 100;
localparam CLK_BIT =	(CLKPERSEC <= 268435456) ? 28 :
						(CLKPERSEC <= 536870912) ? 29 : 32;
localparam CLK10m_BIT =	(CLKPER10mSEC <= 2097152) ? 21 :
						(CLKPER10mSEC <= 4194304) ? 22 :
						(CLKPER10mSEC <= 8388608) ? 23 : 24;

localparam PPS_DELAY_MARGIN = 'd100;

reg 		gps_pps_tmp = 'b0;
reg 		gps_pps_tmp_d ='b0;

wire gps_pps_trig;		// pps_reged?? ?κΈ°ν ??΄?Ό ?¨.
assign gps_pps_trig	= gps_pps_tmp &  ~gps_pps_tmp_d; // ?΄?ΈλΆ? ?΄?­? κΈ°μ? ?? .
always @(posedge clk) begin
	gps_pps_tmp 		<= GPS_PPS;
	gps_pps_tmp_d		<= gps_pps_tmp;
end

wire delay_gps_pps_trig;	// ? μ§? ??΄κ°?κ±°λ ??΄κ°?μ§? ?? λΆ?λΆ? ?΄κ²°μ ??΄ Delay
delay_regi # (
	.DIN_WIDTH	(1),
	.DELAY		(PPS_DELAY_MARGIN-2)
) delay_pps_reged (
	.clk(clk),						// I
	.sclr(sclr),					// I
	.din(gps_pps_trig),			// I [DIN_WIDTH-1:0]
	.dout(delay_gps_pps_trig)		// O [DIN_WIDTH-1:0]
);

reg [31:0] cnt_in_sec ='b0;	// pps time 0 = PPS riging (inside count/ output is 1 clk delayed)

reg [CLK10m_BIT-1:0]	pps_clk_10ms_cnt ='b0; // 0 ?Ό ? pps_reged κ°? ??΄?.
always @(posedge clk) begin
	if (sclr) begin
		cnt_in_sec <= 'b0;
		pps_reged <= 'b0;
		pps_clk_10ms_cnt <= 'b0;
		pps_count_syson <= 'b0;
	end
	else begin
		cnt_in_sec <=	(gps_pps_trig && cnt_in_sec >= (CLKPERSEC>>1)) ? 'b0 :
						(delay_gps_pps_trig) ? PPS_DELAY_MARGIN :
						(cnt_in_sec >= (CLKPERSEC-1)) ? 'b0 :
						cnt_in_sec + 1'b1;

		pps_reged <= (cnt_in_sec == 'b0) ? 'b1 : 'b0;		// GPS??

		pps_clk_10ms_cnt <= ((cnt_in_sec == 'b0) || (pps_clk_10ms_cnt >= (CLKPER10mSEC-1))) ? 'b0 :  pps_clk_10ms_cnt + 1'b1;

		pps_count_syson <= (cnt_in_sec == 'b0) ? pps_count_syson + 1'b1 : pps_count_syson;
	end
end

wire [31:0] din_mult_u32u8;
wire [39 : 0] P;
assign din_mult_u32u8 = (cnt_in_sec >= (CLKPERSEC - 'd3)) ? cnt_in_sec - (CLKPERSEC - 'd3) : (cnt_in_sec + 'd3);

mult_u32u8 multl_u32u8 (
  .CLK(clk),			// I
  .A(MYLT_NANOx32),	// I [7 : 0]
  .B(din_mult_u32u8),	// I [31 : 0]
  .P(P)					// O [39 : 0]
); // latency = 3;

wire [39:0] P_d;
assign P_d = P >> 5;

always @(posedge clk) begin
	nano_sec 	<=  P_d[29:0];
end


wire [16:0] pps_sec;
always @(posedge clk) begin
	if (sclr) begin
		pps_time_H	<= 'b0; //set_time[28:24];
		pps_time_M	<= 'b0; //set_time[21:16];
		pps_time_S	<= 'b0; //set_time[13:8];
		sec_in_day <= 'b0;
		pps_time_10ms <= 'b0;
	end
	else if (set_time_valid) begin
		pps_time_H	<= set_time_H; //set_time[28:24];
		pps_time_M	<= set_time_M; //set_time[21:16];
		pps_time_S	<= set_time_S; //set_time[13:8];

	end
	else begin
		pps_time_10ms <= 	(cnt_in_sec == 'b0) ? 'b0 :
							(pps_clk_10ms_cnt == (CLKPER10mSEC-1)) ? pps_time_10ms + 1'b1 :
							pps_time_10ms;

		pps_time_S <= 	((cnt_in_sec == 'b0) && pps_time_S == 'd59 ) ? 'b0 :
						(cnt_in_sec == 'b0) ? pps_time_S + 1'b1 :
						pps_time_S;
		pps_time_M <= 	((cnt_in_sec == 'b0) && pps_time_S == 'd59 && pps_time_M == 'd59) ? 'b0 :
						((cnt_in_sec == 'b0) && pps_time_S == 'd59) ? pps_time_M + 1'b1 :
						pps_time_M;
		pps_time_H <= 	((cnt_in_sec == 'b0) && pps_time_S == 'd59 && pps_time_M == 'd59 && pps_time_H == 'd23) ? 'b0 :
						((cnt_in_sec == 'b0) && pps_time_S == 'd59 && pps_time_M == 'd59) ? pps_time_H + 1'b1 :
						pps_time_H;
		sec_in_day <=	(cnt_in_sec == 'b0 && pps_sec == 'd86399) ? 'b0 :
						(cnt_in_sec == 'b0) ? pps_sec + 1'b1 : sec_in_day;


	end
end

wire [31:0] sec_HH;
mult_s16s16 hour (
  .CLK(clk),  				// I
  .A({11'b0,pps_time_H}),	// I [15 : 0]
  .B({16'd3600}),			// I [15 : 0]
  .P(sec_HH)				// O [31 : 0]
); // letency = 4

wire [31:0] sec_MM;
mult_s16s16 minu (
  .CLK(clk),				// I
  .A({10'b0,pps_time_M}),	// I [15 : 0]
  .B({16'd60}),				// I [15 : 0]
  .P(sec_MM)				// O [31 : 0]
); // letency = 4

wire [5:0] sec_SS;
delay_regi # (
	.DIN_WIDTH	(6),
	.DELAY		(4)
) delay_pps_time_S (
	.clk(clk),					// I
	.sclr(sclr),				// I
	.din(pps_time_S),			// I [DIN_WIDTH-1:0]
	.dout(sec_SS)				// O [DIN_WIDTH-1:0]
);

assign pps_sec = sec_HH[16:0] + sec_MM[11:0] + sec_SS;


always @(posedge clk) begin
	if (set_date_valid) begin
		pps_date	<= set_date[15:0];
	end
	else begin
		pps_date	<= ((cnt_in_sec == 'b0) && pps_time_S == 'd59 && pps_time_M == 'd59 && pps_time_H == 'd23) ? pps_date + 1'b1 : pps_date;
	end
end

endmodule
