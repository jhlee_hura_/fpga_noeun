`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/11/29 10:04:14
// Design Name: 
// Module Name: regs_bsc_sys
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module regs_bsc_sys(
    input 	wire 				clk,		// sys_clk
	input 	wire 				rst,		// because of multi-clk
	input	wire				pcie_clk,

	input	wire  				regs_sel,
	input	wire	[46:0]		regs_adda,
	input	wire  				regs_adda_valid,
	output	wire	[31:0]		regs_dout,
	output	wire				regs_dout_valid,
	input	wire				regs_dout_ready,	
	// -- register ?���?
	// CLK_MUX_REG
	output wire				clk_sel_rst,

	// PCB_VER_REG
	input wire		[3:0]	pcb_ver,
	// BOARD_STAT_REG
	input wire		[31:0]	board_stat,
	// BSC_LED_REG
	output reg				led_test,
	output reg		[3:0]	led_on,	

	// ADCA_SPI_WR_REG
	output 	reg 	[23:0] 	adcA_spi_wdata = 'b0,
	output 	reg 			adcA_spi_wd_valid = 'b0,
	// ADCA_SPI_RD_REG
	input 	wire 	[23:0] 	adcA_spi_rdata,
	input 	wire 			adcA_spi_busy,

	// TMP_AD_REG
	output	reg [1:0]		tmp102_addr,
	output	reg				tmp102_addr_valid,
	// TMP_RD_IN_REG
	input	wire [15:0]		tmp102_rdata_in,
	
	// TMP_RD_EX_REG
	input	wire [15:0]		tmp102_rdata_ex,
	
	// HMCPLL_SPI_WR_REG
	output 	reg		[31:0] 	hmcpll_spi_wdata = 'b0,
	output 	reg 			hmcpll_spi_wd_vaild = 'b0,
	// HMCPLL_SPI_RD_REG
	input 	wire 	[31:0] 	hmcpll_spi_rdata,

	// GPSDO_INFO_REG
	input	wire	[1:0]	gpsdo_info,


	// PWR_FAN_HIGH_DURA_REG
	output	reg		[15:0]	pwr_fan_high_dura,
	// PWR_FAN_LOW_DURA_REG
	output	reg		[15:0]	pwr_fan_low_dura,

	// EXT_GPIO_DATA_REG
	input	wire	[3:0]	gpio_rd,
	output	reg		[3:0]	gpio_wr,
	// EXT_GPIO_DIR_REG
	output	reg		[3:0]	gpio_dir,

	// UART_RD_REG
	input	wire	[7:0]	uart_rd,
	output	reg				uart_rd_en,
	// UART_WR_REG
	output	reg		[7:0]	uart_wd,
	output	reg				uart_wd_valid,
	// UART_STATUS_REG
	input	wire	[7:0]	uart_status,
	output	reg				uart_status_req,
	// UART_CFG_REG
	output	reg				uart_tx_fifo_rst,
	output	reg				uart_rx_fifo_rst,
	output	reg				uart_en_interrupt,
	output	reg				uart_cfg_valid
    );

wire 	[46:0]	fifo_dout;
wire			regin_empty;
wire			regout_empty;
reg				regout_we;
reg				regout_we1;

wire	[15:0]	addr;
wire			wr_en;
wire	[31:0]	wr_d;
reg 	[31:0]	rd_d;
wire			regin_valid;

// from pcie block
fifo_regin_47x16 regin_fifo (
  .rst(rst),        					// I
  .wr_clk(pcie_clk),  					// I
  .rd_clk(clk),  						// I
  .din(regs_adda),        				// I [46 : 0]
  .wr_en(regs_sel & regs_adda_valid),	// I
  .rd_en(~regin_empty),    				// I
  .dout(fifo_dout),      				// O [46 : 0]
  .full(),      						// O
  .empty(regin_empty),    				// O
  .valid(regin_valid)    				// O
);

// to pcie block	// Standard
fifo_regout_32x16 regout_fifo (
  .rst(rst),        					// I
  .wr_clk(clk),  						// I
  .rd_clk(pcie_clk),  					// I
  .din(rd_d),        					// I [31 : 0]
  .wr_en(regout_we),    				// I
  .rd_en(~regout_empty & regs_dout_ready),    			// I
  .dout(regs_dout),      				// O [31 : 0]
  .full(),      						// O
  .empty(regout_empty),    				// O
  .valid(regs_dout_valid)    			// O
);

always @(posedge clk) begin
	regout_we1 	<= regin_valid & ~fifo_dout[46];
	regout_we 	<= regout_we1;
end

// fifo[46]= 1:wr_en, 0:rd_en
// fifo[45:32] = 14bits addr
// fifo[31:0] = 32bits data
assign wr_en 	= regin_valid & fifo_dout[46];
assign addr 	= {fifo_dout[45:32], 2'b0}; // 14bits to 16bits address
assign wr_d 	= fifo_dout[31:0];

//BSC_VERSION_REG
reg [31:0]	version_date=32'b0;
reg [31:0]	version_dat;
//TEST_PAD_REG
reg [31:0]	pad_reg;

reg clk_sel_rst_r = 1'b0;   


always @(posedge clk ) begin
	if (rst)	begin
		rd_d 			<= 32'b0;
		// BSC_VERSION_REG
		version_dat		<= 0;
		// TEST_PAD_REG
		pad_reg			<= 32'b0;
		// CLK_MUX_REG
		clk_sel_rst_r 	<= 1'b0;
		// BSC_LED_REG
		led_test				<= 1'b0;
		led_on					<= 4'b0;

		// ADCA_SPI_WR_REG
		adcA_spi_wdata 	<= 'b0;
		adcA_spi_wd_valid <= 'b0;
		// TMP_AD_REG
		tmp102_addr			<= 2'b0;
		tmp102_addr_valid	<= 1'b0;
		// HMCPLL_SPI_WR_REG
		hmcpll_spi_wdata <= 'b0;
		hmcpll_spi_wd_vaild <= 1'b0;


		// PWR_FAN_HIGH_DURA_REG
		pwr_fan_high_dura <= `FAN_HIGH_DURA_DEF;
		// PWR_FAN_LOW_DURA_REG
		pwr_fan_low_dura <= `FAN_LOW_DURA_DEF;
		// EXT_GPIO_DATA_REG
		gpio_wr			<= 4'b0;
		// EXT_GPIO_DIR_REG
		gpio_dir		<= 4'b1111;		// read
		// UART_RD_REG
		uart_rd_en		<= 1'b0;
		// UART_WR_REG
		uart_wd			<= 8'b0;
		uart_wd_valid	<= 1'b0;
		// UART_STATUS_REG
		uart_status_req <= 1'b0;
		// UART_CFG_REG
		uart_tx_fifo_rst <= 1'b0;
		uart_rx_fifo_rst <= 1'b0;
		uart_en_interrupt <= 1'b0;
		uart_cfg_valid	<= 1'b0;
	end
	else begin
		case (addr)
			`BOARD_MAKER_REG : begin
				rd_d[31:0] <= `BOARD_MAKER;
			end
			
			`BSC_VERSION_REG : begin
				rd_d[31:0] <= version_date;
				if (wr_en)
					version_dat <= wr_d;
			end

			`BOARD_NAME_REG : begin
				rd_d[31:0] <= `BOARD_NAME;
			end

			`BOARD_NAME1_REG : begin
				rd_d[31:0] <= `BOARD_NAME1;
			end

			`BOARD_NAME2_REG : begin
				if (pcb_ver=='h0)
					rd_d[31:0] <= `BOARD_NAME2_C1;
				else if (pcb_ver=='h1)
					rd_d[31:0] <= `BOARD_NAME2_C1;
				else
					rd_d[31:0] <= `BOARD_NAME2;
			end

			`BOARD_NAME3_REG : begin
				rd_d[31:0] <= `BOARD_NAME3;
			end

			`BOARD_NAME4_REG : begin
				rd_d[31:0] <= `BOARD_NAME4;
			end

			`BOARD_NAME5_REG : begin
				rd_d[31:0] <= `BOARD_NAME5;
			end

			`BOARD_NAME6_REG : begin
				rd_d[31:0] <= `BOARD_NAME6;
			end

			`BOARD_NAME7_REG : begin
				rd_d[31:0] <= `BOARD_NAME7;
			end

			`BOARD_NAME8_REG : begin
				if (pcb_ver=='h0)
					rd_d[31:0] <= `BOARD_NAME8_C1;
				else if (pcb_ver=='h1)
					rd_d[31:0] <= `BOARD_NAME8_C1;
				else
					rd_d[31:0] <= `BOARD_NAME8;
			end

			`BOARD_NAME9_REG : begin
				if (pcb_ver=='h0)
					rd_d[31:0] <= `BOARD_NAME9_01;
				else if (pcb_ver=='h1)
					rd_d[31:0] <= `BOARD_NAME9_02;
				else
					rd_d[31:0] <= `BOARD_NAME9;
			end

			`BOARD_NAME10_REG : begin
				rd_d[31:0] <= `BOARD_NAME10;
			end

			`BOARD_NAME11_REG : begin
				rd_d[31:0] <= `BOARD_NAME11;
			end

			`CLK_MUX_REG : begin
				rd_d[31:0] <= 22'b0;
				if (wr_en) begin
					clk_sel_rst_r <= 1'b1;
				end
			end

			`TEST_PAD_REG : begin
				rd_d[31:0] <= pad_reg;
				if (wr_en)
					pad_reg <= ~wr_d;
			end

			`PCB_VER_REG : begin
				rd_d[31:4] <= 'b0;
				rd_d[3:0] <= pcb_ver;
			end

			`BOARD_STAT_REG : begin
				rd_d[31:0] <= board_stat;
			end
			`BSC_LED_REG	: begin
				rd_d[0] <= led_test;
				rd_d[4:1] <= led_on;
				rd_d[31:5] <= 27'b0;
				if (wr_en) begin
					led_test <= wr_d[0];
					led_on <= wr_d[4:1];
				end
			end

			`ADCA_SPI_WR_REG : begin
				rd_d[23:0] <= adcA_spi_wdata;
				rd_d[31:24] <= 8'b0;
				if (wr_en) begin
					adcA_spi_wdata <= wr_d[23:0];
					adcA_spi_wd_valid <= 1'b1;
				end
			end

			`ADCA_SPI_RD_REG : begin
				rd_d[23:0] <= adcA_spi_rdata;
				rd_d[30:24] <= 7'b0;
				rd_d[31] <= adcA_spi_busy;
			end

			`TMP_AD_REG : begin
				rd_d[1:0] <= tmp102_addr;
				rd_d[31:2] <= 'b0;
				if (wr_en) begin
					tmp102_addr <= wr_d[1:0];
					tmp102_addr_valid <= 1'b1;
				end
			end

			`TMP_IN_RD_REG : begin
				rd_d[15:0] <= tmp102_rdata_in;
				rd_d[31:16] <= 16'b0;
			end

			`TMP_EX_RD_REG : begin
				rd_d[15:0] <= tmp102_rdata_ex;
				rd_d[31:16] <= 16'b0;
			end
			
			`HMCPLL_SPI_WR_REG : begin
				rd_d[31:0] <= hmcpll_spi_wdata;
				if (wr_en) begin
					hmcpll_spi_wdata <= wr_d[31:0];
					hmcpll_spi_wd_vaild <= 1'b1;
				end
			end

			`HMCPLL_SPI_RD_REG : begin
				rd_d[31:0] <= hmcpll_spi_rdata;
			end

			`GPSDO_INFO_REG : begin
				rd_d[1:0] <= gpsdo_info;
				rd_d[31:2] <= 30'b0;
			end



			`PWR_FAN_HIGH_DURA_REG : begin
				rd_d[15:0] <= pwr_fan_high_dura;
				rd_d[31:16] <='b0;
				if (wr_en) begin
					pwr_fan_high_dura <= wr_d[15:0];
				end
			end

			`PWR_FAN_LOW_DURA_REG : begin
				rd_d[15:0] <= pwr_fan_low_dura;
				rd_d[31:16] <='b0;
				if (wr_en) begin
					pwr_fan_low_dura <= wr_d[15:0];
				end
			end

			`EXT_GPIO_DATA_REG : begin
				rd_d[3:0] <= gpio_rd;
				rd_d[31:4] <= 'b0;
				if (wr_en) begin
					gpio_wr <= wr_d[3:0];
				end
			end

			`EXT_GPIO_DIR_REG : begin
				rd_d[3:0] <= gpio_dir;
				rd_d[31:4] <= 'b0;
				if (wr_en) begin
					gpio_dir <= wr_d[3:0];
				end
			end

			`UART_RD_REG : begin
				uart_rd_en <= regin_valid & ~fifo_dout[46];
				rd_d[7:0] <= uart_rd;
				rd_d[31:8] <= 'b0;
			end

			`UART_WR_REG : begin
				rd_d[7:0] <= uart_wd;
				rd_d[31:8] <= 'b0;
				if (wr_en) begin
					uart_wd <= wr_d[7:0];
					uart_wd_valid <= 1'b1;
				end
			end

			`UART_STATUS_REG : begin
				rd_d[7:0] <= uart_status;
				rd_d[31:8] <= 'b0;
				if (wr_en) begin
					uart_status_req <= 1'b1;
				end
			end

			`UART_CFG_REG : begin
				rd_d[3:0] <= 'b0;
				rd_d[4] <= uart_en_interrupt;
				rd_d[31:5] <= 'b0;
				if (wr_en) begin
					uart_tx_fifo_rst <=  wr_d[0];
					uart_rx_fifo_rst <=  wr_d[1];
					uart_en_interrupt <=  wr_d[4];
					uart_cfg_valid	<= 1'b1;
				end
			end			
			default: begin
				rd_d <= 32'b0;
			end
		endcase

		////////////////////////////////////////////////

		if (!wr_en) begin
			version_dat	    		<= 32'b0;
			clk_sel_rst_r			<= 1'b0;
			adcA_spi_wd_valid		<= 1'b0;
			tmp102_addr_valid		<= 1'b0;
			hmcpll_spi_wd_vaild		<= 1'b0;
			uart_rd_en				<= 1'b0;
			uart_wd_valid			<= 1'b0;
			uart_status_req			<= 1'b0;
			uart_cfg_valid			<= 1'b0;
			uart_tx_fifo_rst		<= 1'b0;
			uart_rx_fifo_rst		<= 1'b0;
		end
	end
end



// -------------------------------------------------------------------------
parameter idle_state = 0;
parameter ready_state = 1;
parameter length_message = 2047; // length of rom data

reg state;
reg rom_en;
reg [10:0] addra;
wire [7:0] douta;

always @(posedge clk)
begin
	if (rst) begin
		rom_en	<= 1'b0;
		addra <= 11'h7ff;
		version_date <= 32'hffff_ffff;
		state <= idle_state;
	end
	else
		case (state)
		idle_state : begin
			if (version_dat == 32'h279) begin
				rom_en <= 1'b1;
				addra <= 11'd0;
				version_date <= {24'b0, douta};
				state <= ready_state;
			end
			else begin
				rom_en <= 1'b1;
				addra <= 11'd0;
				version_date <= `BSC_VERSION_DATE_DATA;
				state <= idle_state;
			end

		end
		ready_state : begin
			if (addra > length_message) begin
				rom_en <=1'b0;
				addra <= 11'd0;
				version_date <= {24'b0, douta};
				state <= idle_state;
			end
			else if (regin_valid == 1'b1 && addr == `BSC_VERSION_REG) begin
				rom_en <= 1'b1;
				addra <= addra + 11'b1;
				version_date <= {24'b0, douta};
				state <= ready_state;
			end
			else if (regin_valid == 1'b1 && addr != `BSC_VERSION_REG) begin
				rom_en <=1'b0;
				addra <= 11'd0;
				version_date <= `BSC_VERSION_DATE_DATA;
				state <= idle_state;
			end
			else begin
				rom_en <= 1'b1;
				addra <= addra;
				version_date <= version_date;
				state <= ready_state;
			end

		end
		endcase
end

rom_cr rom_cr(
  .clka(clk), 		// I
  .ena(rom_en), 	// I
  .addra(addra), 	// I [10 : 0]
  .douta(douta) 	// O [7 : 0]
);
 
endmodule
