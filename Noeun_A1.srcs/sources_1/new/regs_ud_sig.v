`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/12/01 11:30:02
// Design Name: 
// Module Name: regs_ud_sig
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module regs_ud_sig(
 input	wire				clk,
	input	wire				rst,
	input	wire				pcie_clk,

	input	wire  				regs_sel,
	input	wire	[46:0]		regs_adda,
	input	wire  				regs_adda_valid,
	output	wire	[31:0]		regs_dout,
	output	wire				regs_dout_valid,
	input	wire				regs_dout_ready
	// -------------------------------------------------------
	// -- register ?���?
	// temp


	
);

wire 	[46:0]	fifo_dout;
wire			regin_empty;
wire			regout_empty;
reg				regout_we;

wire	[15:0]	addr;
wire			wr_en;
wire	[31:0]	wr_d;
reg 	[31:0]	rd_d;
wire			regin_valid;

// from pcie block
fifo_regin_47x16 regin_fifo (
  .rst(rst),        						// I
  .wr_clk(pcie_clk),  						// I
  .rd_clk(clk),  							// I
  .din(regs_adda),        					// I [46 : 0]
  .wr_en(regs_sel & regs_adda_valid),		// I
  .rd_en(~regin_empty),    					// I
  .dout(fifo_dout),      					// O [46 : 0]
  .full(),      							// O
  .empty(regin_empty),    					// O
  .valid(regin_valid)    					// O
);

// to pcie block	// Standard
fifo_regout_32x16 regout_fifo (
  .rst(rst),        						// I
  .wr_clk(clk),  							// I
  .rd_clk(pcie_clk),  						// I
  .din(rd_d),        						// I [31 : 0]
  .wr_en(regout_we),    					// I
  .rd_en(~regout_empty & regs_dout_ready),	// I
  .dout(regs_dout),      					// O [31 : 0]
  .full(),      							// O
  .empty(regout_empty),    					// O
  .valid(regs_dout_valid)    				// O
);

always @(posedge clk) begin
	regout_we 	<= regin_valid & ~fifo_dout[46];
end

// fifo[46]= 1:wr_en, 0:rd_en
// fifo[45:32] = 14bits addr
// fifo[31:0] = 32bits data
assign wr_en 	= regin_valid & fifo_dout[46];
assign addr 	= {fifo_dout[45:32], 2'b0};
assign wr_d 	= fifo_dout[31:0];

reg		aud_fifo_rst_r;
reg		common_fifo_rst_r;

always @(posedge clk) begin
	if (rst)	begin
		rd_d 					<= 32'b0;



	end
	else begin
		case (addr)
 	

			default : begin
				rd_d <= 32'b0;
			end
		endcase

		////////////////////////////////////////////////
		// Delay�? ?��?�� ??�?.
		if (!wr_en) begin

		
		end


	end
end



endmodule
