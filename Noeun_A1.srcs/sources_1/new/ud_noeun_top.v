`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/11/30 17:02:50
// Design Name: 
// Module Name: ud_noeun_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ud_noeun_top(
    input wire  rst,
    input wire  u_sys_clk,
    input wire  u_sig_clk,
    input wire  pcie_clk,
    

    input wire [46:0]   regs_adda,
    input wire          regs_adda_valid,
    input wire          regs_data_ready,   
    output wire         regs_data_valid,    
    
    input wire          regs_ud_sys_sel,    
    output wire [31:0]  regs_ud_sys_data,
    
    input wire          regs_ud_sig_sel,    
    output wire [31:0]  regs_ud_sig_data,


	
    //PE43610 - Digi. atten
    //SN74HCS595BQBR - 8bit shift register
    //SN74HCS165BQB - 8bit parallel-load shift register
    //LMV321AIDCKR - OPAMP

	// RF - ADC
	// PE43610(PIN 0,1,4) , SN74HCS595BQBR(RF CTRL 2,3,6)	, LNA Switch (5)
	output wire	 RF_PIN0,	output	wire  RF_PIN1,	output	wire    RF_PIN2,	output	wire    RF_PIN3,
	output	wire    RF_PIN4,	output	wire    RF_PIN5,	output	wire    RF_PIN6,	
	
	// SN74HCS595BQBR(CM 7,10,11), TMP102(8,9)
	output	wire    RF_PIN7,	output	wire    RF_PIN8,	inout	wire RF_PIN9,	output	wire    RF_PIN10,	input	wire    RF_PIN11,
	
	
	// RF - SN74HCS165BQB(12,13,19), SN74HCS595BQBR(14,15,18), LMX2594RHAT(16,17,20,21,22,23)
	output		wire   RF_PIN12,	output		wire  RF_PIN13,	output		RF_PIN14,	output		wire RF_PIN15,
	output		wire   RF_PIN16,	output		wire  RF_PIN17,	output		RF_PIN18,	output		wire RF_PIN19,
	output		wire   RF_PIN20,	output		wire  RF_PIN21,	output		RF_PIN22,	output		wire    RF_PIN23,
	
// HD Bank 87
//-------------------------------------	
    // RF - PE43610A-X(25,26,27) TMP102(28,29) LMK01010ISQ(30,31,32,33) LMV321AIDCKR(34,35)
	output		wire   RF_PIN24,	output		wire  RF_PIN25,   	output	wire   RF_PIN26, output wire   RF_PIN27,
	
	inout		wire   RF_PIN28,	output		wire   RF_PIN29,	output	wire  RF_PIN30,	  output wire 	RF_PIN31,	
	
	output		wire   RF_PIN32,	inout		wire   RF_PIN33,	input		wire  RF_PIN34,	  input wire	RF_PIN35    
    );
wire regs_ud_sys_data_valid;
wire regs_ud_sig_data_valid;
assign regs_data_valid = regs_ud_sys_data_valid | regs_ud_sig_data_valid;

ud_sys(

    .rst(rst),       // I
    .u_sys_clk(u_sys_clk), // I
    .pcie_clk(pcie_clk), // I
    

    .regs_adda(regs_adda), // I [46:0]
    .regs_adda_valid(regs_adda_valid),   // I
     .regs_data_ready(regs_data_ready),      // I   
    
    .regs_ud_sys_sel(regs_ud_sys_sel), // I    
    .regs_ud_sys_data(regs_ud_sys_data),  // O [31:0]
    .regs_ud_sys_data_valid(regs_ud_sys_data_valid),    // O


// HD Bank 86 
//-------------------------------------	

	// RF - ADC
	// PE43610(PIN 0,1,4) , SN74HCS595BQBR(RF CTRL 2,3,6)	, LNA Switch (5)
	 	.RF_PIN0(RF_PIN0),	.RF_PIN1(RF_PIN1),    .RF_PIN2(RF_PIN2),	.RF_PIN3(RF_PIN3), //O   O   O   O
	    .RF_PIN4(RF_PIN4),    .RF_PIN5(RF_PIN5),	   .RF_PIN6(RF_PIN6),      // O   O   O

	
	// SN74HCS595BQBR(CM 7,10,11), TMP102(8,9)
		.RF_PIN7(RF_PIN7),	 .RF_PIN8(RF_PIN8),	.RF_PIN9(RF_PIN9),	.RF_PIN10(RF_PIN10),  .RF_PIN11(RF_PIN11),  // O    O   IO	 O    I
   
		
	// RF - SN74HCS165BQB(12,13,19), SN74HCS595BQBR(14,15,18), LMX2594RHAT(16,17,20,21,22,23)
			.RF_PIN12(RF_PIN12), 	.RF_PIN13(RF_PIN13),   	.RF_PIN14(RF_PIN14),	.RF_PIN15(RF_PIN15),   // O    O    O    O  
	        .RF_PIN16(RF_PIN16),	.RF_PIN17(RF_PIN17),	.RF_PIN18(RF_PIN18),	.RF_PIN19(RF_PIN19),   // O    O    O    O
	        .RF_PIN20(RF_PIN20),    .RF_PIN21(RF_PIN21),	.RF_PIN22(RF_PIN22),	.RF_PIN23(RF_PIN23),   // O    O    O    O

	
// HD Bank 87
//-------------------------------------	
     // RF - PE43610A-X(25,26,27) TMP102(28,29) LMK01010ISQ(30,31,32,33) LMV321AIDCKR(34,35)
			.RF_PIN24(RF_PIN24),	.RF_PIN25(RF_PIN25),	.RF_PIN26(RF_PIN26),    .RF_PIN27(RF_PIN27), // O O O O
			.RF_PIN28(RF_PIN28),	.RF_PIN29(RF_PIN29),    .RF_PIN30(RF_PIN30),	.RF_PIN31(RF_PIN31), // O IO O O
			.RF_PIN32(RF_PIN32),    .RF_PIN33(RF_PIN33),	.RF_PIN34(RF_PIN34),	.RF_PIN35(RF_PIN35) //O IO I I    
);
ud_sig(
    .rst(rst),       // I
    .u_sig_clk(u_sig_clk), // I
    .pcie_clk(pcie_clk), // I
    

    .regs_adda(regs_adda), // I [46:0]
    .regs_adda_valid(regs_adda_valid),   // I
    .regs_data_ready(regs_data_ready),      // I       
    
    .regs_ud_sig_sel(regs_ud_sig_sel), // I    
    .regs_ud_sig_data(regs_ud_sig_data),  // O [31:0]
    .regs_ud_sig_data_valid(regs_ud_sig_data_valid)    // O

);
endmodule
