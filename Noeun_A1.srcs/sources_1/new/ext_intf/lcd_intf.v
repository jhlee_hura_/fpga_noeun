//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/03/28 13:37:37
// Design Name:
// Module Name: LCD interface
// Project Name: MERCURY A3 board support configuration
// Target Devices: Kintex7 410T, Mercury A3 board
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build 1 (2019/03/28)
// Additional Comments:
//
// Copyright 2019 (c) HURA. All rights reserved.
//////////////////////////////////////////////////////////////////////////////////
module lcd_intf (
	input wire 				clk,		// sys_clk
	input wire				sclr,

	output reg 				rs,
	output reg 				rw,
	output reg 				e,
	inout wire [7:0]		d,

	input wire [9:0] 		din,
	input wire 				din_valid,
	output reg [7:0]		dout,
	output reg				dout_valid
);

wire [7:0]	d_in;
reg [7:0]	d_out;

genvar	l;
generate
	for (l = 0; l <= 7; l = l + 1) begin : lcd_data
		IOBUF lcd_iobuf (
			.O(d_in[l]),		// Buffer output
			.IO(d[l]),			// Buffer inout port (connect directly to top-level port)
			.I(d_out[l]),		// Buffer input
			.T(rw)			// 3-state enable input, high=input, low=output
		);
	end
endgenerate


wire empty;
reg rd_en;
wire [9:0] ff_data;

fifosd_10x128 lcd_fifo (
  .clk(clk),			// I
  .srst(sclr),			// I
  .din(din),			// I [9 : 0]
  .wr_en(din_valid),	// I
  .rd_en(rd_en),		// I
  .dout(ff_data),		// O [9 : 0]
  .full(),				// O
  .empty(empty),		// O
  .valid()				// O
);


localparam IDLE_STATE = 	2'b00;
localparam SET_STATE = 		2'b01;
localparam RISE_STATE = 	2'b10;
localparam FALL_STATE = 	2'b11;


reg [1:0] stat;
reg [7:0] cnt;
always @(posedge clk) begin
	if (sclr) begin
		rs		<= 1'b0;
		rw		<= 1'b0;
		e		<= 1'b0;
		rd_en	<= 1'b0;
		d_out	<= 8'b0;
		cnt		<= 'd127;
		dout	<= 8'b0;
		dout_valid <= 1'b0;
		stat <= IDLE_STATE;
	end
	else begin
		case(stat)
			IDLE_STATE : begin
				cnt		<= 'd16;
				e   	<= 1'b0;
				dout_valid <= 1'b0;
				if (!empty) begin
					rd_en <= 1'b1;
					stat <= SET_STATE;
				end
				else begin
					rd_en <= 1'b0;
					stat <= IDLE_STATE;
				end
			end
			SET_STATE : begin
				rd_en	<= 1'b0;
				rs		<= ff_data[9];
				rw		<= ff_data[8];
				d_out	<= ff_data[7:0];
				e   	<= 1'b0;
				if (cnt == 0) begin
					cnt <= 'd80;
					stat <= RISE_STATE;
				end
				else begin
					cnt <= cnt - 1'b1;
					stat <= SET_STATE;
				end
			end
			RISE_STATE : begin
				e   	<= 1'b1;
				if (cnt == 0) begin
					cnt <= 'd80;
					dout		<= (rw) ? d_in : dout;
					dout_valid <= (rw) ? 1'b1 : 1'b0;
					stat <= FALL_STATE;
				end
				else begin
					cnt <= cnt - 1'b1;
					stat <= RISE_STATE;
				end
			end
			FALL_STATE : begin
				e   	<= 1'b0;
				dout_valid <= 1'b0;
				if (cnt == 0) begin
					cnt <= 'd80;
					stat <= IDLE_STATE;
				end
				else begin
					cnt <= cnt - 1'b1;
					stat <= FALL_STATE;
				end
			end

		endcase

	end
end

endmodule
