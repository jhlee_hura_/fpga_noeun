//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2016/05/27 13:00:00
// Design Name:
// Module Name: spi_intf
// Project Name:
// Target Devices: Any Xilinx FPGA Device
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision: build 1(2016/05/27)
// 			 build 2(2017/03/09) : dout_valid 추가, s_trg -> din_valid 로 변경
//			 build 3(2018/07/18) : s_busy => din_ready 및 극성 반대로 변경. parameter 중 SCLK_PERIOD_BIT 제거
// Additional Comments:
//
// Copyright 2016-2018 (c) HURA. All rights reserved.
//////////////////////////////////////////////////////////////////////////////////
module spi_intf #(
	parameter SCLK_PERIOD = 8,
	parameter CLK_START_END = 4,	// CS 후 동작(클럭플럭 시작)까지 시간

	parameter HIGH_CLK = 2,			// LOW=(SCLK_PERIOD-HIGH_CLK)
	parameter CLK_OFFSET = 2,  		// SCLK_PERIOD-HIGH_CLK 보다 작거나 같아야함.(신호에 비해 HIGH가 밀린량)

	parameter CONT_WIDTH = 16, 		// control bit width (0가능)
	parameter DATA_WIDTH = 8,  		// data bit width
	parameter RW_FLAG_BIT = 23, 	// Read/Write Flag bit가  High(Read)이면 Data 출력은 high impedance를 보냄 //0 이면 Write Only

	parameter SCS_N_MODE = 0, 		// 0 = active low 	1 = active high
	parameter SCLK_IDLE_MODE = 0,	// 0 = high idle 	1 = low idle
	localparam	WORD_WIDTH = CONT_WIDTH + DATA_WIDTH // * DATA_NUM;
) (
	input wire clk,
	input wire sclr,

	output reg sclk,	//Phy
	output reg scs_n,
	output wire sdo,
	input wire sdi,
	output wire iobuf_T,

	input wire [WORD_WIDTH-1:0] din,
	input wire 					din_valid,
	output reg 					din_ready,
	output reg [WORD_WIDTH-1:0] dout,
	output reg					dout_valid
);
// ---------------------------------------------------------------------------------
// Paramater
// (SCLK_PERIOD-1)와 CLK_START_END 보다 큰 값의 비트.
localparam SCLK_PERIOD_BIT = 	(SCLK_PERIOD <= 2 && CLK_START_END <= 1) ? 1 :
								(SCLK_PERIOD <= 4 && CLK_START_END <= 3) ? 2 :
								(SCLK_PERIOD <= 8 && CLK_START_END <= 7) ? 3 :
								(SCLK_PERIOD <= 16 && CLK_START_END <= 15) ? 4 :
								(SCLK_PERIOD <= 32 && CLK_START_END <= 31) ? 5 :
								(SCLK_PERIOD <= 64 && CLK_START_END <= 63) ? 6 :
								(SCLK_PERIOD <= 128 && CLK_START_END <= 127) ? 7 :
								(SCLK_PERIOD <= 256 && CLK_START_END <= 255) ? 8 :
								(SCLK_PERIOD <= 512 && CLK_START_END <= 511) ? 9 :
								(SCLK_PERIOD <= 1024 && CLK_START_END <= 1023) ? 10 :
								(SCLK_PERIOD <= 2048 && CLK_START_END <= 2047) ? 11 :
								(SCLK_PERIOD <= 4096 && CLK_START_END <= 4095) ? 12 :
								(SCLK_PERIOD <= 8192 && CLK_START_END <= 8191) ? 13 :
								16;

// (CONT_WIDTH-1)와 (DATA_WIDTH-1) 보다 큰 값의 비트.
localparam MAX_OF_WIDTH_BIT = 	(CONT_WIDTH <= 2 && DATA_WIDTH <=2) ? 1 :
                                (CONT_WIDTH <= 4 && DATA_WIDTH <= 4) ? 2 :
                                (CONT_WIDTH <= 8 && DATA_WIDTH <= 8) ? 3 :
                                (CONT_WIDTH <= 16 && DATA_WIDTH <= 16) ? 4 :
                                (CONT_WIDTH <= 32 && DATA_WIDTH <= 32) ? 5 :
                                (CONT_WIDTH <= 64 && DATA_WIDTH <= 64) ? 6 :
                                (CONT_WIDTH <= 128 && DATA_WIDTH <= 128) ? 7 :
								8;

// ---------------------------------------------------------------------------------
// read/write signal
wire sig_rw;
assign sig_rw = (RW_FLAG_BIT == 0) ? 1'b0 : din[RW_FLAG_BIT];  // low : write, high : read

// ---------------------------------------------------------------------------------
// SCLK Making
// CLK_OFFSET 만큼 LOW, HIGH 갯수만큼 HIGH, 나머지는 LOW
wire [SCLK_PERIOD-1:0] clk_shape;
//CLK_OFFSET == 0 이면 clk_shape 에러이므로 방지 구문
generate
	if (CLK_OFFSET !=0)
		assign clk_shape[SCLK_PERIOD-1:SCLK_PERIOD-CLK_OFFSET] = 0;
endgenerate

assign clk_shape[SCLK_PERIOD-CLK_OFFSET-1:SCLK_PERIOD-CLK_OFFSET-HIGH_CLK] = ~0;

//SCLK_PERIOD == (CLK_OFFSET + HIGH_CLK) 이면 clk_shape 에러이므로 방지 구문
generate
	if (SCLK_PERIOD != (CLK_OFFSET + HIGH_CLK))
		assign clk_shape[SCLK_PERIOD-CLK_OFFSET-HIGH_CLK-1:0] = 0;
endgenerate

// ---------------------------------------------------------------------------------
// Write SPI
localparam state_idle = 3'b000;
localparam state_cs = 	3'b001;
localparam state_cont = 3'b010;
localparam state_data = 3'b011;
localparam state_end = 	3'b100;
localparam state_done = 3'b101;

reg [2:0] s_spi;
reg sdo_we;		// write en
reg sdo_r;

assign sdo = (sdo_we) ? sdo_r : 1'bz;
assign iobuf_T = ~sdo_we;

reg [SCLK_PERIOD_BIT-1:0] clk_count;
reg [MAX_OF_WIDTH_BIT-1:0] bit_count;
reg [WORD_WIDTH-1:0] shift_reg;

always @(posedge clk) begin
	if (sclr) begin
		scs_n		<= ~SCS_N_MODE;
		sclk		<= ~SCLK_IDLE_MODE;
		sdo_r		<= 1'b0;
		shift_reg 	<= 'b0;
		din_ready	<= 1'b0;
		sdo_we		<= 1'b0;
		clk_count	<= CLK_START_END;
		bit_count	<= CONT_WIDTH - 1;			// 먼저 CONT_WORD 먼저
		s_spi		<= state_idle;
	end else begin
		case(s_spi)
			state_idle : begin
				if (din_valid) begin			// din_valid가 오면 시작
					if (CONT_WIDTH != 0)
						sdo_we <= 1'b1;
					else						// CONTROL 이 없을때
						sdo_we <= (sig_rw) ? 1'b0 : 1'b1;
					din_ready <= 1'b0;
					shift_reg <= din;
					sdo_r <= 1'b0;
					s_spi <= state_cs;
				end
				else begin
					din_ready <= 1'b1;
					sdo_we <= 1'b0;
					shift_reg <= 0;
					sdo_r <= 1'b0;
					s_spi <= state_idle;
				end
				scs_n <= ~SCS_N_MODE;
				sclk <= ~SCLK_IDLE_MODE;
				bit_count <= CONT_WIDTH - 1;		// 먼저 CONT_WORD 먼저
				clk_count <= CLK_START_END;
			end
			state_cs : begin					// ChipSel part
				if (clk_count > 0) begin
					clk_count <= clk_count - 1'b1;
					sclk <= sclk;
					shift_reg <= shift_reg;
					sdo_r <= sdo_r;
					s_spi <= state_cs;
				end
				else begin
					if (CONT_WIDTH != 0) begin
						clk_count <= SCLK_PERIOD - 1;
						bit_count <= CONT_WIDTH - 1;
						sclk <= clk_shape[SCLK_PERIOD - 1];
						shift_reg <= {shift_reg[WORD_WIDTH-2:0],1'b0};
						sdo_r <= shift_reg[WORD_WIDTH-1];
						s_spi <= state_cont;
					end
					else begin					// CONTROL 이 없을때 바로 state_data로.
						clk_count <= SCLK_PERIOD - 1;
						bit_count <= DATA_WIDTH - 1;
						sclk <= clk_shape[SCLK_PERIOD - 1];
						shift_reg <= {shift_reg[WORD_WIDTH-2:0],1'b0};
						sdo_r <= shift_reg[WORD_WIDTH-1];
						s_spi <= state_data;
					end
				end
				scs_n <= SCS_N_MODE; //1'b0;
				din_ready <= din_ready;
				sdo_we <= sdo_we;

			end
			state_cont : begin		// Control(or Addr) part
				if (bit_count > 0 ) begin
					if (clk_count > 0) begin
						clk_count <= clk_count - 1'b1;
						bit_count <= bit_count;
						sclk <= clk_shape[clk_count-1];
						shift_reg <= shift_reg;
						sdo_r <= sdo_r;
						sdo_we <= sdo_we;
						s_spi <= state_cont;
					end
					else begin
						clk_count <= SCLK_PERIOD - 1;
						bit_count <= bit_count - 1'b1;
						sclk <= clk_shape[SCLK_PERIOD - 1];
						shift_reg <= {shift_reg[WORD_WIDTH-2:0],1'b0};
						sdo_r <= shift_reg[WORD_WIDTH-1];
						sdo_we <= sdo_we;
						s_spi <= state_cont;
					end
				end else begin
					if (clk_count > 0) begin
						clk_count <= clk_count - 1'b1;
						bit_count <= bit_count;
						sclk <= clk_shape[clk_count-1];
						shift_reg <= shift_reg;
						sdo_r <= sdo_r;
						sdo_we <= sdo_we;
						s_spi <= state_cont;
					end
					else begin
						clk_count <= SCLK_PERIOD - 1;
						bit_count <= DATA_WIDTH -1;  // DATA 준비
						sclk <= clk_shape[SCLK_PERIOD - 1];
						shift_reg <= {shift_reg[WORD_WIDTH-2:0],1'b0};
						sdo_r <= shift_reg[WORD_WIDTH-1];
						sdo_we <= (sig_rw) ? 1'b0 : 1'b1;
						s_spi <= state_data;
					end
				end
				scs_n <= scs_n;
				din_ready <=din_ready;
			end

			state_data : begin			// DATA part
				if (bit_count > 0 ) begin
					if (clk_count > 0) begin
						clk_count <= clk_count -1'b1;
						bit_count <= bit_count;
						sclk <= clk_shape[clk_count-1];
						shift_reg <= shift_reg;
						sdo_r <= sdo_r;
						s_spi <= state_data;
					end
					else begin
						clk_count <= SCLK_PERIOD - 1;
						bit_count <= bit_count - 1'b1;
						sclk <= clk_shape[SCLK_PERIOD - 1];
						shift_reg <= {shift_reg[WORD_WIDTH-2:0],1'b0};
						sdo_r <= shift_reg[WORD_WIDTH-1];
						s_spi <= state_data;
					end
				end
				else begin
					if (clk_count > 0) begin
						clk_count <= clk_count - 1'b1;
						bit_count <= bit_count;
						sclk <= clk_shape[clk_count -1];
						shift_reg <= shift_reg;
						sdo_r <= sdo_r;
						s_spi <= state_data;
					end
					else begin
						clk_count <= CLK_START_END;
						bit_count <= 0;
						sclk <= 1'b0;
						shift_reg <= {shift_reg[WORD_WIDTH-2:0],1'b0};
						sdo_r <= shift_reg[WORD_WIDTH-1];
						s_spi <= state_end;
					end
				end
				scs_n <= scs_n;
				din_ready <=din_ready;
				sdo_we <= (sig_rw) ? 1'b0 : 1'b1;
			end
			state_end : begin			// 완료
				if (clk_count > 0) begin
					clk_count <= clk_count - 1'b1;
					scs_n <= scs_n;
					s_spi <= state_end;
				end
				else begin
					clk_count <= CLK_START_END;
					scs_n <= ~SCS_N_MODE;
					s_spi <= state_done;
				end
				sclk <= sclk;
				sdo_r <= sdo_r;
				din_ready <=din_ready;
				sdo_we <= sdo_we;
				bit_count <= bit_count;
			end
			state_done : begin			// 완료
				if (clk_count > 0) begin
					clk_count <= clk_count - 1'b1;
					scs_n <= scs_n;
					s_spi <= state_done;
				end
				else begin
					clk_count <= clk_count;
					scs_n <= ~SCS_N_MODE;
					s_spi <= state_idle;
				end
				sclk <= sclk;
				sdo_r <= sdo_r;
				din_ready <=din_ready;
				sdo_we <= sdo_we;
				bit_count <= bit_count;
			end
		endcase
	end
end

// ---------------------------------------------------------------------------------
// Read SPI data
reg d_sclk;
wire data_en;
assign data_en = ~d_sclk & sclk & (scs_n == SCS_N_MODE);  // data 에 clk으로 동기화

always @(posedge clk) begin
	if (sclr) begin
		d_sclk <= 1'b1;
		dout <= 0;
		dout_valid <= 1'b0;
	end else begin
		if (data_en) begin			// sclk의 처음에서 데이터 GET
			dout[0] <= sdi;
			dout[WORD_WIDTH-1:1] <= dout[WORD_WIDTH-2:0];
			d_sclk <= sclk;
			dout_valid <= (bit_count ==  0) ? 1'b1 : 1'b0;
		end	else begin
			dout <= dout;
			d_sclk <= sclk;
			dout_valid <= 1'b0;
		end
	end
end

endmodule