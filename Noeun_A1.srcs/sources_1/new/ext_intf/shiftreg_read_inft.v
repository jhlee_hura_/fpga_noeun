//////////////////////////////////////////////////////////////////////////////////
// Company: HURA (Hub of Radio Technology)
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2018/10/06 13:00:00
// Design Name:
// Module Name: shiftreg_read_intf
// Project Name:
// Target Devices: Kintex7 410T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision: build 1(2018/10/06)
// Additional Comments:
//
// Copyright 2018 (c) HURA. All rights reserved.
//////////////////////////////////////////////////////////////////////////////////
module shiftreg_read_intf # (
	parameter SCLK_PERIOD = 8,
	parameter HIGH_CLK = 2,			// LOW=(SCLK_PERIOD-HIGH_CLK)
	parameter CLK_OFFSET = 2,  		// SCLK_PERIOD-HIGH_CLK 보다 작거나 같아야함.(신호에 비해 HIGH가 밀린량)

	parameter EDGE = 0,				// date get edge : 0 : Falling 1:Rising
	parameter DATA_WIDTH = 8  		// data bit width
) (
	input wire clk,
	input wire sclr,

	// To/from pin-out(pad)
	output reg 		sclk,
	output reg 		ld_n,				// 1 clk (parallel data save to send serial)
	input wire 		sdi,

	// From/To Register
	input wire 					load,		// 1clk enable
	output wire 				din_ready,
	output reg [DATA_WIDTH-1:0]	dout,
	output reg 					dout_valid
);

// ---------------------------------------------------------------------------------
// Paramater
// (SCLK_PERIOD-1)의 비트.
localparam SCLK_PERIOD_BIT = 	(SCLK_PERIOD <= 2) ? 1 :
								(SCLK_PERIOD <= 4) ? 2 :
								(SCLK_PERIOD <= 8) ? 3 :
								(SCLK_PERIOD <= 16) ? 4 :
								(SCLK_PERIOD <= 32) ? 5 :
								(SCLK_PERIOD <= 64) ? 6 :
								(SCLK_PERIOD <= 128) ? 7 :
								(SCLK_PERIOD <= 256) ? 8 :
								(SCLK_PERIOD <= 512) ? 9 :
								(SCLK_PERIOD <= 1024) ? 10 :
								(SCLK_PERIOD <= 2048) ? 11 :
								(SCLK_PERIOD <= 4096) ? 12 :
								(SCLK_PERIOD <= 8192) ? 13 :
								16;

localparam MAX_OF_WIDTH_BIT = 	(DATA_WIDTH <=2) ? 1 :
                                (DATA_WIDTH <= 4) ? 2 :
                                (DATA_WIDTH <= 8) ? 3 :
                                (DATA_WIDTH <= 16) ? 4 :
                                (DATA_WIDTH <= 32) ? 5 :
                                (DATA_WIDTH <= 64) ? 6 :
                                (DATA_WIDTH <= 128) ? 7 :
								8;
localparam SCLK_IDLE_MODE = 1;	// 0= high idle 1=low idle
// ---------------------------------------------------------------------------------
// SCLK Making
// CLK_OFFSET 만큼 LOW, HIGH 갯수만큼 HIGH, 나머지는 LOW
wire [SCLK_PERIOD-1:0] clk_shape;
//CLK_OFFSET == 0 이면 clk_shape 에러이므로 방지 구문
generate
	if (CLK_OFFSET !=0)
		assign clk_shape[SCLK_PERIOD-1:SCLK_PERIOD-CLK_OFFSET] = 0;
endgenerate

assign clk_shape[SCLK_PERIOD-CLK_OFFSET-1:SCLK_PERIOD-CLK_OFFSET-HIGH_CLK] = ~0;

//SCLK_PERIOD == (CLK_OFFSET + HIGH_CLK) 이면 clk_shape 에러이므로 방지 구문
generate
	if (SCLK_PERIOD != (CLK_OFFSET + HIGH_CLK))
		assign clk_shape[SCLK_PERIOD-CLK_OFFSET-HIGH_CLK-1:0] = 0;
endgenerate

// ---------------------------------------------------------------------------------
// Clock Generator
localparam state_idle = 	2'b00;
localparam state_ld = 		2'b01;
localparam state_read = 	2'b10;
localparam state_done = 	2'b11;

reg [1:0]	state;
reg [SCLK_PERIOD_BIT-1:0] clk_count;
reg [MAX_OF_WIDTH_BIT-1:0] bit_count;
//reg [DATA_WIDTH-1:0] shift_reg;

assign din_ready = (state == state_idle);

always @(posedge clk) begin
	if (sclr) begin
		sclk		<= ~SCLK_IDLE_MODE;
		clk_count 	<= SCLK_PERIOD - 1;
		bit_count 	<= DATA_WIDTH - 1;
		ld_n		<= 1'b1;
		state		<= state_idle;
	end
	else begin
		case(state)
			state_idle : begin
				if (load) begin
					ld_n	<= 1'b0;
					state	<= state_ld;
				end
				else begin
					ld_n	<= 1'b1;
					state	<= state_idle;
				end
				sclk	<= ~SCLK_IDLE_MODE;
				clk_count 	<= SCLK_PERIOD - 1;
				bit_count 	<= DATA_WIDTH - 1;
			end
			state_ld : begin
				if (clk_count > 0) begin
					clk_count	<= clk_count - 1'b1;
					ld_n		<= ld_n;
					sclk 		<= ~SCLK_IDLE_MODE;
					state 		<= state_ld;
				end
				else begin
					clk_count	<= SCLK_PERIOD - 1;
					ld_n		<= 1'b1;
					sclk		<= clk_shape[SCLK_PERIOD - 1];
					state 		<= state_read;
				end
				bit_count <= DATA_WIDTH - 1;
			end
			state_read : begin
				if (clk_count > 0) begin
					clk_count	<= clk_count - 1'b1;
					bit_count 	<= bit_count;
					sclk 		<= clk_shape[clk_count-1];
					state 		<= state_read;
				end
				else begin
					if (bit_count == 0 ) begin
						bit_count 	<= bit_count;
						state 		<= state_done;
					end
					else begin
						bit_count 	<= bit_count - 1'b1;
						state 		<= state_read;
					end
					clk_count 	<= SCLK_PERIOD - 1;
					sclk 		<= clk_shape[SCLK_PERIOD-1];
				end

			end
			state_done : begin
				state 		<= state_idle;
			end
		endcase
	end
end

// ---------------------------------------------------------------------------------
// Read SPI data
reg d_sclk;
wire data_en;
generate
if (EDGE) // rising edge
	assign data_en = ~d_sclk & sclk;
else	// falling edge
	assign data_en = d_sclk & ~sclk;
endgenerate

always @(posedge clk) begin
	if (sclr) begin
		d_sclk <= 1'b1;
		dout <= 0;
		dout_valid <= 1'b0;
	end else begin
		if (data_en) begin			// sclk의 처음 edge 데이터 GET
			dout[0] <= sdi;
			dout[DATA_WIDTH-1:1] <= dout[DATA_WIDTH-2:0];
			d_sclk <= sclk;
			dout_valid <= (bit_count ==  0) ? 1'b1 : 1'b0;
		end	else begin
			dout <= dout;
			d_sclk <= sclk;
			dout_valid <= 1'b0;
		end
	end
end

endmodule