`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/11/30 17:49:49
// Design Name: 
// Module Name: rf_gatt
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module rf_gatt(
input wire 			clk,		//sys_clk

	input wire [6:0]	user_gatt1,
	input wire  		user_gatt1_valid,
	input wire [6:0]	user_gatt2,
	input wire  		user_gatt2_valid,


	input wire [6:0]	table_gatt_ctrl,
	input wire  		table_gatt_ctrl_valid,

	output reg [6:0] 	rf_gatt1_ctrl,
	output reg 			rf_gatt1_ctrl_valid,
	output reg [6:0] 	rf_gatt2_ctrl,
	output reg 			rf_gatt2_ctrl_valid

);


	always @(posedge clk) begin
		rf_gatt1_ctrl <= 	(user_gatt1_valid) ? user_gatt1 :
							(table_gatt_ctrl_valid) ? table_gatt_ctrl :
							rf_gatt1_ctrl;
		rf_gatt1_ctrl_valid <= user_gatt1_valid | table_gatt_ctrl_valid;

		rf_gatt2_ctrl <= 	(user_gatt2_valid) ? user_gatt2 :
							(table_gatt_ctrl_valid) ? table_gatt_ctrl :
							rf_gatt2_ctrl;
		rf_gatt2_ctrl_valid <= user_gatt2_valid | table_gatt_ctrl_valid;

	end

endmodule
