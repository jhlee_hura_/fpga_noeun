`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/11/30 16:48:45
// Design Name: 
// Module Name: regs_ud_sys
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module regs_ud_sys(
    input	wire				clk,
	input	wire				rst,
	input	wire				pcie_clk,

	input	wire  				regs_sel,
	input	wire	[46:0]		regs_adda,
	input	wire  				regs_adda_valid,
	output	wire	[31:0]		regs_dout,
	output	wire				regs_dout_valid,
	input	wire				regs_dout_ready,
	// -------------------------------------------------------
	// -- register


//==================================================================================================
//deploy_yhj

	// RF_MGC_EN_REG
	output	reg				mgc_en = `MGC_EN_DEF,
	// RF_MGC_REG
	output	reg		[6:0]	mgc_idx,
	output	reg				mgc_idx_valid,
	// RF_MGC_TB_REG
	output	reg 			we_mgc,
	output	reg				mgc_table_sel,
	output	reg 	[6:0] 	addr_mgc,
	output	reg 	[15:0] 	data_mgc,

	// RF_GC_USER_LNASW_REG
	output	reg				user_lna_en,
	output	reg				user_lna_en_valid,
	// RF_GC_USER_SATT_REG
	output	reg [4:0]		user_satt,
	output	reg				user_satt_valid,
	// RF_GC_USER_ATT_CHA_1_REG
	output	reg [4:0]		user_att_cha_1,
	output	reg				user_att_cha_1_valid,
	// RF_GC_USER_ATT_CHA_2_REG
	output	reg [4:0]		user_att_cha_2,
	output	reg				user_att_cha_2_valid,
	// RF_GC_USER_ATT_CHB_1_REG
	output	reg [4:0]		user_att_chb_1,
	output	reg				user_att_chb_1_valid,	
	// RF_GC_USER_ATT_CHB_2_REG
	output	reg [4:0]		user_att_chb_2,
	output	reg				user_att_chb_2_valid,	

	// RF_GATT1_REG
	output	reg [6:0]		user_gatt1,
	output	reg				user_gatt1_valid,
	// RF_GATT2_REG
	output	reg [6:0]		user_gatt2,
	output	reg				user_gatt2_valid,
	

//==================================================================================================


	input	wire			busy_dc_bd_att,
//---------------------------------------------------------------------------------------
 	// RF_INTF_SYNBD_M4_PLL_CMD_REG
	output	reg [1:0]		syn_bd_din_pll_cmd,
	// RF_INTF_SYNBD_M4_PLL_WR_REG
	output  reg [23:0]		syn_bd_din_pll,
	output  reg 			syn_bd_din_pll_valid,
	// RF_INTF_SYNBD_M4_PLL_RD_REG
	input	wire [23:0]		syn_bd_dout_pll,
	// RF_INTF_SYNBD_M4_PLL_CTRL_INTERVAL_REG
	output	reg	 [15:0]		syn_bd_pll_ctrl_interval,
	
	//RF_INTF_SYNBD_M2_SYNC_DISTRIBUTOR_WR_REG	
    output reg [31:0] syn_bd_din_distributor, // O [31:0], 
    output reg syn_bd_din_distributor_valid, // O
	//RF_INTF_SYNBD_M2_SYNC_DISTRIBUTOR_RD_REG	    
    input wire syn_bd_distributor_ready, // I	
    
    //RF_INTF_SYNBD_M5_PLL_ATT1_REG    
    output reg [6:0]syn_bd_att1,
    output reg syn_bd_att1_valid ,			
    //RF_INTF_SYNBD_M5_PLL_ATT2_REG     
    output reg [6:0]syn_bd_att2,    
    output reg syn_bd_att2_valid ,  
    //RF_INTF_SYNBD_M5_PLL_ATT3_REG     
    output reg [6:0]syn_bd_att3,          
    output reg syn_bd_att3_valid ,			
    //RF_INTF_SYNBD_M5_PLL_ATT4_REG     
    output reg [6:0]syn_bd_att4,            
    output reg syn_bd_att4_valid ,
    
	input	wire	busy_syn_bd_att,    
// --------- Frequency Switching & Detect Register ---------
	// RF_INTF_DCBD_M3_SR_WR_REG
	output  reg  [15:0]		dc_bd_rf_sw,
	output  reg 			dc_bd_rf_sw_valid,
	
	// RF_INTF_SYNBD_M1_SR_WR_REG
	output  reg  [15:0]		syn_bd_rf_sw,
	output  reg 			syn_bd_rf_sw_valid,

    input wire  dc_bd_log_detect1,
    input wire  dc_bd_log_detect2,
    
// --------- Status control ---------    
	// RF_INTF_DCBD_M5_SRL_WR_REG
	output reg 				dc_bd_din_sts_valid,
	// RF_INTF_DCBD_M5_SRL_RD_REG
	input wire [15:0]		dc_bd_dout_sts,
		
	// RF_INTF_SYNBD_M3_SRL_WR_REG 
	output reg 				syn_bd_din_sts_valid,
	// RF_INTF_SYNBD_M3_SRL_RD_REG 
	input wire [15:0]		syn_bd_dout_sts,
	
// --------- RF_I2C control ---------	
	// RF_INTF_DCBD_M4_TMP_AD_REG
	output	reg [1:0]		dc_bd_din_tmp102,
	output	reg				dc_bd_din_tmp102_valid,
	// RF_INTF_DCBD_M4_TMP_RD_REG
	input	wire [15:0]		dc_bd_dout_tmp102,
	
	// RF_INTF_SYNBD_M6_TMP_AD_REG
	output	reg [1:0]		syn_bd_din_tmp102,
	output	reg				syn_bd_din_tmp102_valid,
	// RF_INTF_SYNBD_M6_TMP_RD_REG
	input	wire [15:0]		syn_bd_dout_tmp102,


//---------------------------------------------------------------------------------------

//==================================================================================================
//deploy_yhj

	// LV_CAL_EN_REG
	output reg					lv_cal_en,
	// LV_FREQ_IDX_REG
	output reg 		[8:0] 		freq_index,      // satt, lna, gatt Read and Merge address
	output reg 		[5:0] 		freq_intp,       // satt, lna, gatt Read and Merge address
	// LV_TEMP_IDX_CHA_REG
	output reg 		[4:0] 		temp_idx_cha,    // temp Lv_Table Read address
	// LV_TEMP_IDX_CHB_REG
	output reg 		[4:0] 		temp_idx_chb,    // temp Lv_Table Read address
	
	
	// LV_TB_SATT_REG
	output reg 					we_satt,         // satt Lv_Table Write enable
	output reg		[13:0] 		addra_satt,      // satt Lv_Table Port A Write address
	output reg		[15:0]		dataa_satt,      // satt Lv_Table Port A Write data
	// LV_TB_LNA_REG
	output reg 					we_lna,          // lna Lv_Table Write enable
	output reg 		[8:0] 		addra_lna,       // lna Lv_Table Port A Write address
	output reg 		[15:0] 		dataa_lna,       // lna Lv_Table Port A Write data
    // LV_TB_GATT_REG
	output reg 				    we_gatt,         // gatt Lv_Table Write enable
	output reg 		 		    addr_gatt_mode,  // gatt Lv_Table Port A Write address
	output reg 	    [8:0] 		addr_gatt,       // gatt Lv_Table Port A Write address
	output reg 	    [6:0] 		data_gatt,       // gatt Lv_Table Port A Write data
	// LV_TB_ATT_CHA_1_REG
	output reg 					we_att_cha_1,    // att_ch1_a Lv_Table enable
	output reg 		[4:0] 		addra_att_cha_1, // att_ch1_a Lv_Table Port A Write address
	output reg 		[15:0] 		dataa_att_cha_1, // att_ch1_a Lv_Table Port A Write data
	// LV_TB_ATT_CHA_2_REG
	output reg 					we_att_cha_2,    // att_ch1_b Lv_Table enable
	output reg 		[4:0] 		addra_att_cha_2, // att_ch1_b Lv_Table Port A Write address
	output reg 		[15:0] 		dataa_att_cha_2, // att_ch1_b Lv_Table Port A Write data
	// LV_TB_BW_CHA_REG
	output reg 					we_bw_cha,       // bw Lv_Table enable
	output reg 		[4:0] 		addra_bw_cha,    // bw Lv_Table port A Write address
	output reg 		[15:0] 		dataa_bw_cha,    // bw Lv_Table port A Write data
	// LV_TB_TEMP_CHA_REG
	output reg 					we_temp_cha,     // temp Lv_Table enable
	output reg 		[4:0] 		addra_temp_cha,  // temp Lv_Table Port A Write address
	output reg 		[15:0] 		dataa_temp_cha,  // temp Lv_Table Port A Write address
	// LV_TB_ATT_CHB_1_REG
	output reg 					we_att_chb_1,    // att_ch2_a Lv_Table enable
	output reg 		[4:0] 		addra_att_chb_1, // att_ch2_a Lv_Table Port A Write address
	output reg 		[15:0] 		dataa_att_chb_1, // att_ch2_a Lv_Table Port A Write data
	// LV_TB_ATT_CHB_2_REG
	output reg 					we_att_chb_2,    // att_ch2_b Lv_Table enable
	output reg 		[4:0] 		addra_att_chb_2, // att_ch2_b Lv_Table Port A Write address
	output reg 		[15:0] 		dataa_att_chb_2, // att_ch2_b Lv_Table Port A Write data
	// LV_TB_BW_CHB_REG
	output reg 					we_bw_chb,       // bw Lv_Table enable
	output reg 		[4:0] 		addra_bw_chb,    // bw Lv_Table port A Write address
	output reg 		[15:0] 		dataa_bw_chb,    // bw Lv_Table port A Write data
	// LV_TB_TEMP_CHB_REG
	output reg 					we_temp_chb,     // temp Lv_Table enable
	output reg 		[4:0] 		addra_temp_chb,  // temp Lv_Table Port A Write address
	output reg 		[15:0] 		dataa_temp_chb,  // temp Lv_Table Port A Write address




	// LV_SATT_IDX_REG
	input wire 		[4:0] 		satt_idx,
	// LV_LNA_REG
	input wire 					lna_en,
	// LV_GATT_VALUE_REG
	input wire 		[6:0] 		gatt1_val,
	input wire 		[6:0] 		gatt2_val,
	// LV_ATT_IDX_CHA_1_REG
	input wire 		[4:0] 		att_idx_cha_1,
	// LV_ATT_IDX_CHA_2_REG
	input wire 		[4:0] 		att_idx_cha_2,	
	// LV_ATT_IDX_CHB_1_REG
	input wire 		[4:0] 		att_idx_chb_1,
	// LV_ATT_IDX_CHB_2_REG
	input wire 		[4:0] 		att_idx_chb_2,
	// LV_SUM_CAL_CHA_REG
	input wire		[17:0]		sum_cal_cha,
	// LV_SUM_CAL_CHB_REG
	input wire		[17:0]		sum_cal_chb

//==================================================================================================

);

wire 	[46:0]	fifo_dout;
wire			regin_empty;
wire			regout_empty;
reg				regout_we;
reg				regout_we1;

wire	[15:0]	addr;
wire			wr_en;
wire	[31:0]	wr_d;
reg 	[31:0]	rd_d;
wire			regin_valid;

// from pcie block
fifo_regin_47x16 regin_fifo (
  .rst(rst),        					// I
  .wr_clk(pcie_clk),  					// I
  .rd_clk(clk),  						// I
  .din(regs_adda),        				// I [46 : 0]
  .wr_en(regs_sel & regs_adda_valid),	// I
  .rd_en(~regin_empty),    				// I
  .dout(fifo_dout),      				// O [46 : 0]
  .full(),      						// O
  .empty(regin_empty),    				// O
  .valid(regin_valid)    				// O
);

// to pcie block	// Standard
fifo_regout_32x16 regout_fifo (
  .rst(rst),        						// I
  .wr_clk(clk),  							// I
  .rd_clk(pcie_clk),  						// I
  .din(rd_d),        						// I [31 : 0]
  .wr_en(regout_we),    					// I
  .rd_en(~regout_empty & regs_dout_ready),	// I
  .dout(regs_dout),      					// O [31 : 0]
  .full(),      							// O
  .empty(regout_empty),    					// O
  .valid(regs_dout_valid)    				// O
);

always @(posedge clk) begin
	regout_we1 	<= regin_valid & ~fifo_dout[46];
	regout_we 	<= regout_we1;	
end

// fifo[46]= 1:wr_en, 0:rd_en
// fifo[45:32] = 14bits addr
// fifo[31:0] = 32bits data
assign wr_en 	= regin_valid & fifo_dout[46];
assign addr 	= {fifo_dout[45:32], 2'b0};
assign wr_d 	= fifo_dout[31:0];

//BYTESWAP_REG
reg [31:0]	pad_reg;


always @(posedge clk ) begin
	if (rst)	begin
		rd_d 			<= 32'b0;
		// BYTESWAP_REG
		pad_reg			<= 32'b0;

		// RF_MGC_EN_REG
		mgc_en			<= `MGC_EN_DEF;
		// RF_MGC_REG
		mgc_idx			<= `MGC_DEF;
		mgc_idx_valid	<= 1'b0;
		// RF_MGC_TB_REG
		we_mgc 			<= 1'b0;
		mgc_table_sel	<= 1'b0;
		addr_mgc 		<= 7'b0;
		data_mgc 		<= 16'b0;

		// RF_GC_USER_LNASW_REG
		user_lna_en			<= 1'b0;
		user_lna_en_valid	<= 1'b0;
		// RF_GC_USER_SATT_REG
		user_satt			<= 5'b0;
		user_satt_valid		<= 1'b0;
		// RF_GC_USER_ATT_CHA_1_REG
		user_att_cha_1			<= 5'b0;
		user_att_cha_1_valid		<= 1'b0;
		// RF_GC_USER_ATT_CHA_2_REG
		user_att_cha_2			<= 5'b0;
		user_att_cha_2_valid		<= 1'b0;		
		// RF_GC_USER_ATT_CHB_1_REG
		user_att_chb_1			<= 5'b0;
		user_att_chb_1_valid		<= 1'b0;
		// RF_GC_USER_ATT_CHB_2_REG
		user_att_chb_2			<= 5'b0;
		user_att_chb_2_valid		<= 1'b0;
		
		// RF_GATT1_REG
		user_gatt1			<= 7'b0;
		user_gatt1_valid		<= 1'b0;
		// RF_GATT2_REG
		user_gatt2			<= 7'b0;
		user_gatt2_valid	<= 1'b0;

// --------- SYN Register ---------
		// RF_INTF_SYNBD_M4_PLL_CMD_REG
		syn_bd_din_pll_cmd				<= 2'b0;
		// RF_INTF_SYNBD_M4_PLL_WR_REG
		syn_bd_din_pll			<= 24'b0;
		syn_bd_din_pll_valid		<= 1'b0;

		// RF_INTF_SYNBD_M4_PLL_CTRL_INTERVAL_REG
		syn_bd_pll_ctrl_interval	<= `RF_SYN_CTRL_INTERVAL_DEF;

	   //RF_INTF_SYNBD_M2_SYNC_DISTRIBUTOR_REG	
        syn_bd_din_distributor <= 32'b0; 
        syn_bd_din_distributor_valid <= 1'b0; 
    
        //RF_INTF_SYNBD_M5_PLL_ATT1_REG    
        syn_bd_att1 <= 6'b0;
        syn_bd_att1_valid <= 1'b0;	

        //RF_INTF_SYNBD_M5_PLL_ATT2_REG    
        syn_bd_att2 <= 6'b0;
        syn_bd_att2_valid <= 1'b0;	
        
         //RF_INTF_SYNBD_M5_PLL_ATT3_REG    
        syn_bd_att3 <= 6'b0;
        syn_bd_att3_valid <= 1'b0;	
        
         //RF_INTF_SYNBD_M5_PLL_ATT4_REG    
        syn_bd_att4 <= 6'b0;
        syn_bd_att4_valid <= 1'b0;	                      
// --------- Frequency Switching & Detect Register ---------    
		// RF_INTF_DCBD_M3_SR_WR_REG
		dc_bd_rf_sw				<= 16'b0;
		dc_bd_rf_sw_valid		<= 1'b0;

 
 		// RF_SYNBD_M1_REG
		syn_bd_rf_sw				<= 16'b0;
		syn_bd_rf_sw_valid		<= 1'b0;
		       
		// RF_INTF_DCBD_M5_SRL_WR_REG
		dc_bd_din_sts_valid			<= 1'b0;

		// RF_INTF_DCBD_M4_TMP_AD_REG
		dc_bd_din_tmp102			<= 2'b0;
		dc_bd_din_tmp102_valid	<= 1'b0;
		
		
		// RF_INTF_SYNBD_M6_TMP_AD_REG
		syn_bd_din_tmp102			<= 2'b0;
		syn_bd_din_tmp102_valid	<= 1'b0;
		
		
		
//==================================================================================================
//deploy_yhj		
		
		// LV_CAL_EN_REG
		lv_cal_en			    <= 1'b1;
		// LV_FREQ_IDX_REG
		freq_index 			    <= 9'b0;  
		freq_intp		        <= 6'b0;
		// LV_TB_SATT_REG
		we_satt 			    <= 1'b0;
		addra_satt 			    <= 14'b0;
		dataa_satt			    <= 16'b0;
		// LV_TB_LNA_REG
		we_lna 				    <= 1'b0;
		addra_lna 			    <= 9'b0;
		dataa_lna 			    <= 16'b0;
		// LV_TB_GATT_REG
		we_gatt 			    <= 1'b0;
		addr_gatt_mode		    <= 1'b0;
		addr_gatt			    <= 9'b0;
		data_gatt			    <= 7'b0;
		// LV_TB_ATT_CHA_1_REG
		we_att_cha_1 			<= 1'b0;
		addra_att_cha_1 		<= 5'b0;
		dataa_att_cha_1 		<= 16'b0;
		// LV_TB_ATT_CHA_2_REG
		we_att_cha_2 			<= 1'b0;
		addra_att_cha_2 		<= 5'b0;
		dataa_att_cha_2 		<= 16'b0;
		// LV_TB_BW_CHA_REG
		we_bw_cha 				<= 1'b0;
		addra_bw_cha 			<= 5'b0;
		dataa_bw_cha 			<= 16'b0;
		// LV_TB_TEMP_CHA_REG
		we_temp_cha 			<= 1'b0;
		addra_temp_cha 			<= 5'b0;
		dataa_temp_cha 			<= 16'b0;
		// LV_TEMP_IDX_CHA_REG
		temp_idx_cha 			<= 5'b0;
		// LV_TB_ATT_CHB_1_REG
		we_att_chb_1 			<= 1'b0;
		addra_att_chb_1 		<= 5'b0;
		dataa_att_chb_1 		<= 16'b0;
		// LV_TB_ATT_CHB_2_REG
		we_att_chb_2 			<= 1'b0;
		addra_att_chb_2 		<= 5'b0;
		dataa_att_chb_2 		<= 16'b0;
		// LV_TB_BW_CHB_REG
		we_bw_chb 				<= 1'b0;
		addra_bw_chb 			<= 5'b0;
		dataa_bw_chb 			<= 16'b0;
		// LV_TB_TEMP_CHB_REG
		we_temp_chb 			<= 1'b0;
		addra_temp_chb 			<= 5'b0;
		dataa_temp_chb 			<= 16'b0;
		// LV_TEMP_IDX_CHB_REG
		temp_idx_chb 			<= 5'b0;

	

		
//==================================================================================================
		

	end
	else begin
		case (addr)
			`COPYRIGHTER_REG : begin
				rd_d[31:0] <= `COPYRIGHTER;
			end

			`VERSION_REG : begin
				rd_d[31:0] <= `VERSION_DATE_DATA;
			end

			`PRODUCT_REG : begin
				rd_d[31:0] <= `PRODUCT;
			end

			`PRODUCT1_REG : begin
				rd_d[31:0] <= `PRODUCT1;
			end

			`PRODUCT2_REG : begin
				rd_d[31:0] <= `PRODUCT2;
			end

			`PRODUCT3_REG : begin
				rd_d[31:0] <= `PRODUCT3;
			end

			`PRODUCT4_REG : begin
				rd_d[31:0] <= `PRODUCT4;
			end

			`PRODUCT5_REG : begin
				rd_d[31:0] <= `PRODUCT5;
			end

			`PRODUCT6_REG : begin
				rd_d[31:0] <= `PRODUCT6;
			end

			`PRODUCT7_REG : begin
				rd_d[31:0] <= `PRODUCT7;
			end

			`PRODUCT8_REG : begin
				rd_d[31:0] <= `PRODUCT8;
			end

			`PRODUCT9_REG : begin
				rd_d[31:0] <= `PRODUCT9;
			end

			`PRODUCT10_REG : begin
				rd_d[31:0] <= `PRODUCT10;
			end

			`PRODUCT11_REG : begin
				rd_d[31:0] <= `PRODUCT11;
			end

			`BYTESWAP_REG : begin
				rd_d[31:0] <= pad_reg;
				if (wr_en)
					pad_reg <= {wr_d[7:0], wr_d[15:8], wr_d[23:16], wr_d[31:24]};
			end

			`SYS_TYPE_REG : begin
				rd_d[31:0] <= `EDGERF_TYPE;
			end

			`FUNCTION_REG : begin
				rd_d[31:0] <= `EDGERF_FUNC;
			end

//==================================================================================================
//deploy_yhj

			`RF_MGC_EN_REG : begin
				rd_d[0]	<= mgc_en;
				rd_d[31:1]	<= 31'b0;
				if (wr_en) begin
					mgc_en <= wr_d[0];
				end
			end

			`RF_MGC_REG : begin
				rd_d[6:0]	<= mgc_idx;
				rd_d[31:7]	<= 25'b0;
				if (wr_en) begin
					mgc_idx <= wr_d[6:0];
					mgc_idx_valid <= 1'b1;
				end
			end

			`RF_MGC_TB_REG : begin
				rd_d[31:0] <= 	32'b0;
				if (wr_en) begin
					we_mgc <= 1'b1;
					mgc_table_sel <= wr_d[28];
					addr_mgc <= wr_d[22:16];
					data_mgc <= wr_d[15:0];
				end
			end

			`RF_GC_USER_LNASW_REG : begin
				rd_d[0] <= user_lna_en;
				rd_d[31:1] <= 31'b0;
				if (wr_en) begin
					user_lna_en <= wr_d[0];
					user_lna_en_valid <= 1'b1;
				end
			end

			`RF_GC_USER_SATT_REG : begin
				rd_d[4:0] <= user_satt;
				rd_d[30:5] <= 26'b0;
				rd_d[31] <= busy_dc_bd_att;
				if (wr_en) begin
					user_satt <= wr_d[4:0];
					user_satt_valid <= 1'b1;
				end
			end

			`RF_GC_USER_ATT_CHA_1_REG : begin
				rd_d[4:0] <= user_att_cha_1;
				rd_d[30:5] <= 26'b0;
				rd_d[31] <= busy_dc_bd_att;
				if (wr_en) begin
					user_att_cha_1 <= wr_d[4:0];
					user_att_cha_1_valid <= 1'b1;
				end
			end

			`RF_GC_USER_ATT_CHA_2_REG : begin
				rd_d[4:0] <= user_att_cha_2;
				rd_d[30:5] <= 26'b0;
				rd_d[31] <= busy_dc_bd_att;
				if (wr_en) begin
					user_att_cha_2 <= wr_d[4:0];
					user_att_cha_2_valid <= 1'b1;
				end
			end
						

			`RF_GC_USER_ATT_CHB_1_REG : begin
				rd_d[4:0] <= user_att_chb_1;
				rd_d[30:5] <= 26'b0;
				rd_d[31] <= busy_dc_bd_att;
				if (wr_en) begin
					user_att_chb_1 <= wr_d[4:0];
					user_att_chb_1_valid <= 1'b1;
				end
			end

			`RF_GC_USER_ATT_CHB_2_REG : begin
				rd_d[4:0] <= user_att_chb_2;
				rd_d[30:5] <= 26'b0;
				rd_d[31] <= busy_dc_bd_att;
				if (wr_en) begin
					user_att_chb_2 <= wr_d[4:0];
					user_att_chb_2_valid <= 1'b1;
				end
			end

			`RF_GATT1_REG : begin
				rd_d[6:0] <= user_gatt1;
				rd_d[30:7] <= 'b0;
				rd_d[31] <= busy_dc_bd_att;
				if (wr_en) begin
					user_gatt1 <= wr_d[6:0];
					user_gatt1_valid <= 1'b1;
				end
			end

			`RF_GATT2_REG : begin
				rd_d[6:0] <= user_gatt2;
				rd_d[30:7] <= 'b0;
				rd_d[31] <= busy_dc_bd_att;
				if (wr_en) begin
					user_gatt2 <= wr_d[6:0];
					user_gatt2_valid <= 1'b1;
				end
			end


//==================================================================================================


// --------- SYN Register ---------
			`RF_INTF_SYNBD_M4_PLL_CMD_REG : begin
				rd_d[1:0] <= syn_bd_din_pll_cmd;
				rd_d[31:2] <= 30'b0;
				if (wr_en) begin
					syn_bd_din_pll_cmd <= wr_d[1:0];
				end
			end

			`RF_INTF_SYNBD_M4_PLL_WR_REG : begin
				rd_d[23:0] <= syn_bd_din_pll;
				if (wr_en) begin
					syn_bd_din_pll <= wr_d[23:0];
					syn_bd_din_pll_valid <= 1'b1;
				end
			end

			`RF_INTF_SYNBD_M4_PLL_RD_REG : begin
				rd_d[23:0] <= syn_bd_dout_pll;
				rd_d[31:24] <= 8'b0;
			end

			`RF_INTF_SYNBD_M4_PLL_CTRL_INTERVAL_REG : begin
				rd_d[15:0] <= syn_bd_pll_ctrl_interval;
				rd_d[31:16] <='b0;
				if (wr_en) begin
					syn_bd_pll_ctrl_interval <= wr_d[15:0];
				end
			end
			`RF_INTF_SYNBD_M2_SYNC_DISTRIBUTOR_REG : begin
				rd_d[31:0] <= syn_bd_din_distributor;
				if (wr_en) begin
					syn_bd_din_distributor <= wr_d[31:0];
					syn_bd_din_distributor_valid <= 1'b1;
				end
			end			
			`RF_INTF_SYNBD_M2_SYNC_DISTRIBUTOR_RD_REG : begin
				rd_d[0:0] <= syn_bd_distributor_ready;
				rd_d[31:1] <= 8'b0;
			end		
			
			`RF_INTF_SYNBD_M5_PLL_ATT1_REG : begin
				rd_d[6:0] <= syn_bd_att1;
				rd_d[30:7] <= 'b0;
				rd_d[31] <= busy_syn_bd_att;
				if (wr_en) begin
					syn_bd_att1 <= wr_d[6:0];
					syn_bd_att1_valid <= 1'b1;
				end
			end			
				
			`RF_INTF_SYNBD_M5_PLL_ATT2_REG : begin
				rd_d[6:0] <= syn_bd_att2;
				rd_d[30:7] <= 'b0;
				rd_d[31] <= busy_syn_bd_att;
				if (wr_en) begin
					syn_bd_att2 <= wr_d[6:0];
					syn_bd_att2_valid <= 1'b1;
				end
			end			

			`RF_INTF_SYNBD_M5_PLL_ATT3_REG : begin
				rd_d[6:0] <= syn_bd_att3;
				rd_d[30:7] <= 'b0;
				rd_d[31] <= busy_syn_bd_att;
				if (wr_en) begin
					syn_bd_att3 <= wr_d[6:0];
					syn_bd_att3_valid <= 1'b1;
				end
			end		
			
			`RF_INTF_SYNBD_M5_PLL_ATT4_REG : begin
				rd_d[6:0] <= syn_bd_att4;
				rd_d[30:7] <= 'b0;
				rd_d[31] <= busy_syn_bd_att;
				if (wr_en) begin
					syn_bd_att4 <= wr_d[6:0];
					syn_bd_att4_valid <= 1'b1;
				end
			end					
// --------- Frequency Switching & Detect Register ---------
			`RF_INTF_DCBD_M3_SR_WR_REG : begin
				rd_d[15:0] <= dc_bd_rf_sw;
				rd_d[31:16] <='b0;
				if (wr_en) begin
					dc_bd_rf_sw <= wr_d[15:0];
					dc_bd_rf_sw_valid <= 1'b1;
				end
			end
								
			`RF_INTF_SYNBD_M1_SR_WR_REG : begin
				rd_d[15:0] <= syn_bd_rf_sw;
				rd_d[31:16] <='b0;
				if (wr_en) begin
					syn_bd_rf_sw <= wr_d[15:0];
					syn_bd_rf_sw_valid <= 1'b1;
				end
			end

			`RF_INTF_DCBD_M6_DETECT_RD_REG	: begin
				rd_d[0] <= dc_bd_log_detect1;
				rd_d[31:1] <= 31'b0;
			end
			
			`RF_INTF_DCBD_M7_DETECT_RD_REG	: begin
				rd_d[0] <= dc_bd_log_detect2;
				rd_d[31:1] <= 31'b0;
			end			
// --------- Status control ---------			
			`RF_INTF_DCBD_M5_SRL_WR_REG	: begin
				rd_d[31:0] <= 'b0;
				if (wr_en) begin
					dc_bd_din_sts_valid <= 1'b1;
				end
			end

			`RF_INTF_DCBD_M5_SRL_RD_REG	: begin
				rd_d[15:0] <= dc_bd_dout_sts[15:0];
				rd_d[31:16] <= 16'b0;
			end
			
			`RF_INTF_SYNBD_M3_SRL_WR_REG	: begin
				rd_d[31:0] <= 'b0;
				if (wr_en) begin
					syn_bd_din_sts_valid <= 1'b1;
				end
			end

			`RF_INTF_SYNBD_M3_SRL_RD_REG	: begin
				rd_d[15:0] <= syn_bd_dout_sts[15:0];
				rd_d[31:16] <= 16'b0;
			end
// --------- RF_I2C control ---------			
			`RF_INTF_DCBD_M4_TMP_AD_REG : begin
				rd_d[1:0] <= dc_bd_din_tmp102;
				rd_d[31:2] <= 'b0;
				if (wr_en) begin
					dc_bd_din_tmp102 <= wr_d[1:0];
					dc_bd_din_tmp102_valid <= 1'b1;
				end
			end

			`RF_INTF_DCBD_M4_TMP_RD_REG : begin
				rd_d[15:0] <= dc_bd_dout_tmp102;
				rd_d[31:16] <= 16'b0;
			end			
			`RF_INTF_SYNBD_M6_TMP_AD_REG : begin
				rd_d[1:0] <= syn_bd_din_tmp102;
				rd_d[31:2] <= 'b0;
				if (wr_en) begin
					syn_bd_din_tmp102 <= wr_d[1:0];
					syn_bd_din_tmp102_valid <= 1'b1;
				end
			end

			`RF_INTF_SYNBD_M6_TMP_RD_REG : begin
				rd_d[15:0] <= syn_bd_dout_tmp102;
				rd_d[31:16] <= 16'b0;
			end
			
			
			
			
//==================================================================================================
//deploy_yhj			
// --------- LV Control and Information ---------
			`LV_CAL_EN_REG : begin
				rd_d[0]		 <= lv_cal_en;
				rd_d[31:1]	 <= 31'b0;
				if (wr_en) begin
					lv_cal_en <= wr_d[0];
				end
			end
			
			`LV_FREQ_IDX_REG : begin
				rd_d[8:0]   <= 	freq_index;
				rd_d[15:9]  <= 	7'b0;
				rd_d[21:16] <= 	freq_intp;
				rd_d[27:22] <= 	6'b0;
				rd_d[28] 	<= 	0;
				rd_d[31:29] <= 	3'b0;
				if (wr_en) begin
					freq_index <= wr_d[8:0];
					freq_intp  <= wr_d[21:16];
				end
			end

			`LV_TB_SATT_REG : begin
				rd_d[31:0]  <= 	32'b0;
				if (wr_en) begin
					we_satt    <= 1'b1;
					addra_satt <= wr_d[29:16];
					dataa_satt <= wr_d[15:0];
				end
			end
			`LV_TB_LNA_REG : begin
				rd_d[31:0]  <= 	32'b0;
				if (wr_en) begin
					we_lna    <= 1'b1;
					addra_lna <= wr_d[24:16];
					dataa_lna <= wr_d[15:0];
				end
			end
			
			`LV_TB_GATT_REG : begin
				rd_d[31:0]  <= 	32'b0;
				if (wr_en) begin
					we_gatt <= 1'b1;
					addr_gatt_mode <= wr_d[25];
					addr_gatt      <= wr_d[24:16];
					data_gatt      <= wr_d[6:0];
				end
			end

			`LV_TB_ATT_CHA_1_REG : begin
				rd_d[31:0]  <= 	32'b0;
				if (wr_en) begin
					we_att_cha_1    <= 1'b1;
					addra_att_cha_1 <= wr_d[20:16];
					dataa_att_cha_1 <= wr_d[15:0];
				end
			end

			`LV_TB_ATT_CHA_2_REG : begin
				rd_d[31:0]  <= 	32'b0;
				if (wr_en) begin
					we_att_cha_2    <= 1'b1;
					addra_att_cha_2 <= wr_d[20:16];
					dataa_att_cha_2 <= wr_d[15:0];
				end
			end

			`LV_TB_BW_CHA_REG : begin
				rd_d[31:0]  <= 	32'b0;
				if (wr_en) begin
					we_bw_cha    <= 1'b1;
					addra_bw_cha <= wr_d[20:16];
					dataa_bw_cha <= wr_d[15:0];
				end
			end

			`LV_TB_TEMP_CHA_REG : begin
				rd_d[31:0]  <= 	32'b0;
				if (wr_en) begin
					we_temp_cha    <= 1'b1;
					addra_temp_cha <= wr_d[20:16];
					dataa_temp_cha <= wr_d[15:0];
				end
			end
			
			`LV_TEMP_IDX_CHA_REG : begin
				rd_d[4:0]  <= 	temp_idx_cha;
				rd_d[31:5] <= 	27'b0;
				if (wr_en) begin
					temp_idx_cha <= wr_d[4:0];
				end
			end

			`LV_TB_ATT_CHB_1_REG : begin
				rd_d[31:0]  <= 	32'b0;
				if (wr_en) begin
					we_att_chb_1    <= 1'b1;
					addra_att_chb_1 <= wr_d[20:16];
					dataa_att_chb_1 <= wr_d[15:0];
				end
			end

			`LV_TB_ATT_CHB_2_REG : begin
				rd_d[31:0]  <= 	32'b0;
				if (wr_en) begin
					we_att_chb_2    <= 1'b1;
					addra_att_chb_2 <= wr_d[20:16];
					dataa_att_chb_2 <= wr_d[15:0];
				end
			end

			`LV_TB_BW_CHB_REG : begin
				rd_d[31:0]  <= 	32'b0;
				if (wr_en) begin
					we_bw_chb    <= 1'b1;
					addra_bw_chb <= wr_d[20:16];
					dataa_bw_chb <= wr_d[15:0];
				end
			end

			`LV_TB_TEMP_CHB_REG : begin
				rd_d[31:0]  <= 	32'b0;
				if (wr_en) begin
					we_temp_chb    <= 1'b1;
					addra_temp_chb <= wr_d[20:16];
					dataa_temp_chb <= wr_d[15:0];
				end
			end
			
			`LV_TEMP_IDX_CHB_REG : begin
				rd_d[4:0]  <= 	temp_idx_chb;
				rd_d[31:5] <= 	27'b0;
				if (wr_en) begin
					temp_idx_chb <= wr_d[4:0];
				end
			end			
		
		
			
			`LV_SATT_IDX_REG : begin
				rd_d[4:0] <= 	satt_idx;
				rd_d[31:5] <= 	27'b0;
			end

			`LV_LNA_REG : begin
				rd_d[0] <= 		lna_en;
				rd_d[31:1] <= 	31'b0;
			end


			`LV_GATT_VALUE_REG : begin
				rd_d[6:0]	<= gatt1_val;
				rd_d[7]		<= 	1'b0;
				rd_d[14:8]	<= 7'b0;
				rd_d[15]	<= 	1'b0;
				rd_d[22:16] <= gatt2_val;
				rd_d[31:23] <= 	'b0;
			end

			`LV_ATT_IDX_CHA_1_REG : begin
				rd_d[4:0] <= 	att_idx_cha_1;
				rd_d[31:5] <= 	27'b0;
			end

			`LV_ATT_IDX_CHA_2_REG : begin
				rd_d[4:0] <= 	att_idx_cha_2;
				rd_d[31:5] <= 	27'b0;
			end

			`LV_ATT_IDX_CHB_1_REG : begin
				rd_d[4:0] <= 	att_idx_chb_1;
				rd_d[31:5] <= 	27'b0;
			end

			`LV_ATT_IDX_CHB_2_REG : begin
				rd_d[4:0] <= 	att_idx_chb_2;
				rd_d[31:5] <= 	27'b0;
			end
						
			`LV_SUM_CAL_CHA_REG : begin
				rd_d[17:0] <= 	sum_cal_chb;
				rd_d[31:18] <= 	{14{sum_cal_cha[17]}};
			end
				
			`LV_SUM_CAL_CHB_REG : begin
				rd_d[17:0] <= 	sum_cal_chb;
				rd_d[31:18] <= 	{14{sum_cal_chb[17]}};
			end


//==================================================================================================



			default: begin
				rd_d <= 32'b0;
			end
		endcase

		////////////////////////////////////////////////
		// Delay�? ?��?�� ??�?.
		if (!wr_en) begin
			mgc_idx_valid			<= 1'b0;
			we_mgc					<= 1'b0;
			user_lna_en_valid		<= 1'b0;
			user_satt_valid			<= 1'b0;
			user_att_cha_1_valid			<= 1'b0;
			user_att_cha_2_valid			<= 1'b0;			
			user_att_chb_1_valid			<= 1'b0;
			user_att_chb_2_valid			<= 1'b0;			
			user_gatt1_valid			<= 1'b0;
			user_gatt2_valid		<= 1'b0;

			syn_bd_din_pll_valid			<= 1'b0;
            syn_bd_din_distributor_valid	<= 1'b0;			
			dc_bd_rf_sw_valid			<= 1'b0;					
			syn_bd_rf_sw_valid			<= 1'b0;	
			
            syn_bd_att1_valid <= 1'b0;				
            syn_bd_att2_valid <= 1'b0;			
            syn_bd_att3_valid <= 1'b0;				
            syn_bd_att4_valid <= 1'b0;


				
			syn_bd_din_sts_valid		<= 1'b0;
			dc_bd_din_sts_valid			<= 1'b0;		
					
			dc_bd_din_tmp102_valid		<= 1'b0;				
			syn_bd_din_tmp102_valid		<= 1'b0;	
			
//==================================================================================================
//deploy_yhj			
						
			we_satt 			    <= 1'b0;
			we_lna 				    <= 1'b0;
			we_gatt				    <= 1'b0;
			we_att_cha_1 			<= 1'b0;
			we_att_cha_2 			<= 1'b0;
			we_bw_cha 				<= 1'b0;
			we_temp_cha 			<= 1'b0;
			we_att_chb_1 			<= 1'b0;
			we_att_chb_2 			<= 1'b0;
			we_bw_chb 				<= 1'b0;
			we_temp_chb 			<= 1'b0;
			
//==================================================================================================
		
			
		end

	end
end
endmodule
