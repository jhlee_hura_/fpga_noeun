`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/10/12 19:27:28
// Design Name: 
// Module Name: CON_LMX2594RHAT
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module CON_LMX2594RHAT(
    input wire  clk,
    input wire  sclr,
    
	input wire	[1:0]	din_syn_cmd,     
	input wire	[23:0]	din_syn,
	input wire			din_syn_valid,	
	input wire [15:0]	syn_ctrl_interval,	
	output reg			syn_last_cmd_in,
	output reg			syn_cmd_done,
	output wire  [23:0]	dout_syn,
	output wire			dout_syn_valid,
	
	output wire		PIN_CS1_L,
	output wire		PIN_CS1_H,	
	output wire		PIN_CS2_A,
	output wire		PIN_CS2_B,		
	output wire		PIN_CLK,	
	output wire		PIN_IO	
    );
    
 
wire [23:0] ff_do_syn;
wire [1:0]	ff_do_syn_cmd;
wire		ff_do_syn_valid;
wire		ff_do_syn_ready;
wire		ff_do_syn_empty;
reg			syn_ready;
wire		spi_scs;

fifosd_26x32 Syn_bd_LMX2594_fifo (
  .clk(clk),				// I
  .rst(sclr),				// I
  .din({din_syn_cmd,din_syn}),			// I [25 : 0]
  .wr_en(din_syn_valid),	// I
  .rd_en(~ff_do_syn_empty && ~ff_do_syn_valid && syn_ready),		// I
  .dout({ff_do_syn_cmd,ff_do_syn}),			// O [25 : 0]
  .full(),					// O
  .empty(ff_do_syn_empty),  // O
  .valid(ff_do_syn_valid)	// O
);

wire			lo_pll_cs1_l;
wire			lo_pll_cs1_h;
wire			lo_pll_cs2_a;
wire			lo_pll_cs2_b;
wire			lo_pll_clk;
wire			lo_pll_sdo;
wire			lo_pll_sdi;
wire			lo_pll_sd_T;
spi_intf #(
	.SCLK_PERIOD(6),			// (몇개 clk?���? 구성) 2?��?��
	.CLK_START_END(4),
	.HIGH_CLK(2),
	.CLK_OFFSET(3),  			// SCLK_PERIOD > HIGH_CLK+CLK_OFFSET (?��?��?�� 비해 HIGH�? �?린량)
	.CONT_WIDTH(8),
	.DATA_WIDTH(16),
	.RW_FLAG_BIT(23),			// Write Only
	.SCS_N_MODE(0),				// 0 = active low 1= active high
	.SCLK_IDLE_MODE(0)			// 0= high idle 1=low idle
)
 LMX2594(
	.clk(clk),						// I
	.sclr(sclr),					// I

	.sclk(lo_pll_clk),				// O
	.scs_n(spi_scs),				// O
	.sdo(lo_pll_sdo),				// O
	.sdi(lo_pll_sdi),				// I
	.iobuf_T(lo_pll_sd_T),			// O

	.din(ff_do_syn),				// I [WORD_WIDTH-1:0]
	.din_valid(ff_do_syn_valid),	// I
	.din_ready(ff_do_syn_ready),	// O
	.dout(dout_syn),				// O [WORD_WIDTH-1:0]
	.dout_valid(dout_syn_valid)		// O
);

assign lo_pll_cs1_l = (ff_do_syn_cmd == 2'b00) ? spi_scs : 1'b1;
assign lo_pll_cs1_h = (ff_do_syn_cmd == 2'b01) ? spi_scs : 1'b1;
assign lo_pll_cs2_a = (ff_do_syn_cmd == 2'b10) ? spi_scs : 1'b1;
assign lo_pll_cs2_b = (ff_do_syn_cmd == 2'b11) ? spi_scs : 1'b1;

localparam IDLE_STAT = 2'b00;
localparam DIN_STAT = 2'b01;
localparam TERM_STAT = 2'b10;
localparam LAST_STAT = 2'b11;

reg [23:0] ff_do_syn_old;
reg [23:0] ff_do_syn_r;
reg [15:0] rdy_cnt;
reg [1:0] stat;
always @(posedge clk) begin	// sys_clk
	if (sclr) begin
		rdy_cnt <= 'b0;
		syn_ready <= 1'b0;
		ff_do_syn_old <= 'b0;
		ff_do_syn_r	<= 'b0;
		syn_cmd_done <= 1'b0;
		stat <= IDLE_STAT;
	end
	else begin
		case(stat)
			IDLE_STAT : begin
				syn_ready <= (ff_do_syn_valid) ? 1'b0 : 1'b1;
				rdy_cnt <= syn_ctrl_interval;
				ff_do_syn_r <= ff_do_syn;
				ff_do_syn_old <= ff_do_syn_old;
				syn_cmd_done <= 1'b0;
				stat <= (ff_do_syn_valid) ? DIN_STAT : IDLE_STAT;
			end
			DIN_STAT : begin
				syn_ready <= 1'b0;
				rdy_cnt <= rdy_cnt;
				syn_cmd_done <= 1'b0;
				stat <= (ff_do_syn_ready) ? TERM_STAT : DIN_STAT;
			end
			TERM_STAT : begin
				syn_ready <= 1'b0;
				rdy_cnt <= rdy_cnt - 1'b1;
				syn_cmd_done <= 1'b0;
				stat <= (rdy_cnt == 'b0) ? LAST_STAT : TERM_STAT;
			end
			LAST_STAT : begin
				syn_ready <= 1'b0;
				rdy_cnt <= rdy_cnt;
				ff_do_syn_r <= ff_do_syn_r;
				ff_do_syn_old <= ff_do_syn_r;
				syn_cmd_done <= ((ff_do_syn_cmd==1'b1) && (ff_do_syn_r == 24'h00241c) && (ff_do_syn_old == 24'h002414)) ? 1'b1 : 1'b0;
				stat <= IDLE_STAT;
			end
		endcase
	end
end

localparam STAT_0 = 1'b0;
localparam STAT_1 = 1'b1;
reg		last_cmd_stat;
always @(posedge clk) begin	// sys_clk
	if (sclr) begin
		syn_last_cmd_in <= 1'b0;
		last_cmd_stat <= STAT_0;
	end
	else begin
		case(last_cmd_stat)
			STAT_0 : begin
				syn_last_cmd_in <= 1'b0;
				last_cmd_stat  <= (din_syn_valid && din_syn_cmd && (din_syn ==  24'h002414)) ? STAT_1 : STAT_0;
			end

			STAT_1 : begin
				if (din_syn_valid) begin
					syn_last_cmd_in <= (din_syn_cmd && (din_syn ==  24'h00241C)) ? 1'b1 : 1'b0;
					last_cmd_stat <= STAT_0;
				end
				else begin
					syn_last_cmd_in <= 1'b0;
					last_cmd_stat <= STAT_1;
				end
			end
		endcase
	end
end


OBUF IOBUF_0 (
	.O(PIN_CS1_L),
	.I(lo_pll_cs1_l)
);
OBUF IOBUF_1 (
	.O(PIN_CS1_H),
	.I(lo_pll_cs1_h)
);
OBUF IOBUF_2 (
	.O(PIN_CS2_A),
	.I(lo_pll_cs2_a)
);
OBUF IOBUF_3 (
	.O(PIN_CS2_B),
	.I(lo_pll_cs2_b)
);
OBUF IOBUF_4 (
	.O(PIN_CLK),
	.I(lo_pll_clk)
);
IOBUF IOBUF_5 (
	.O(lo_pll_sdi),		// O  (to FPGA internal)
	.IO(PIN_IO),		// IO (connect directly to top-level port)
	.I(lo_pll_sdo),		// I  (from FPGA internal)
	.T(lo_pll_sd_T)		// I  high=input(to FPGA intenal) , low=output(to port)
);

endmodule
