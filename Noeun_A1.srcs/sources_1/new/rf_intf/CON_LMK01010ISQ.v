`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/10/13 17:38:44
// Design Name: 
// Module Name: CON_LMK01010ISQ
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module CON_LMK01010ISQ(

input wire clk,
input wire sclr,

input wire [31:0] din_distributor,
input wire din_distributor_valid,
output wire distributor_ready,

output wire PIN_CS,
output wire PIN_CLK,
output wire PIN_DATA,
output wire PIN_SYNC
);





wire distributor_le;
wire distributor_clk;
wire distributor_data;


//----------------------------------------------------------------------
OBUF IOBUF_0 (
.O(PIN_CS),
.I(distributor_le)
);
OBUF IOBUF_1 (
.O(PIN_CLK),
.I(distributor_clk)
);
OBUF IOBUF_2 (
.O(PIN_DATA),
.I(distributor_data)
);
OBUF IOBUF_3 (
.O(PIN_SYNC),
.I(1'b1)
);
//----------------------------------------------------------------------





//----------------------------------------------------------------------
shiftreg_intf #(
.SCLK_PERIOD(64),
.CLK_START_END(32),
.HIGH_CLK(32),
.CLK_OFFSET(32),
.DATA_WIDTH(32),
.SCS_N_MODE(0),
.SCLK_IDLE_MODE(1)
) LMK01010 (
.clk (clk), // I
.sclr (sclr), // I



.din (din_distributor), // I [DATA_WIDTH-1:0]
.din_valid (din_distributor_valid), // I
.din_ready (distributor_ready), // O



.sclk (distributor_clk), // O
.scs_n (), // O
.latch_en (distributor_le), // O
.sdo (distributor_data) // O
); //total latency
//----------------------------------------------------------------------






endmodule
