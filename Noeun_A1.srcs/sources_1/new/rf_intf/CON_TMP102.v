`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/10/12 19:30:21
// Design Name: 
// Module Name: CON_TMP102
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module CON_TMP102(
    input wire  clk,
    input wire  sclr,
	input wire 	[1:0] 	din_tmp102,
	input wire 			din_tmp102_valid,
	output wire [15:0] 	dout_tmp102,
	output wire 		busy_tmp102,
    output wire PIN_SCL,
    inout wire PIN_SDA_IO

    );
    
wire [39:0] din_tmp102_c;
assign din_tmp102_c = {5'b10010,din_tmp102,1'b0,8'b0,5'b10010,din_tmp102,1'b1,16'b0};

wire 			tmp_scl;
wire 		tmp_sdi;
wire 			tmp_sdo;
wire			tmp_sda_T;
i2c_intf #(
	.SCLK_PERIOD(320),		// (2?΄?) //tmp102 400kHz. 240?Όλ‘? ?΄?Ό ?¨. μ§?κΈμ? ??€?Έ
	.CLK_START_END(160), 	// (SCLK_PERIOD-1)?? CLK_START_END λ³΄λ€ ?° κ°μ λΉνΈ.
	.HIGH_CLK(105),
	.CLK_OFFSET(105), 		// SCLK_PERIOD-HIGH_CLK λ³΄λ€ ???Ό?¨.(? ?Έ? λΉν΄ HIGHκ°? λ°?λ¦°λ)
	.CHIPSEL_WIDTH(8),		// slave address bit width
	.CONT_WIDTH(8),			// (0 κ°??₯) control
	.RCHIPSEL_WIDTH(8),		// (0 κ°??₯) read slave address bit width
	.DATA_WIDTH(16),		//
	.RW_FLAG_BIT(16),
	.DATA_NUM(1)				// data bit? λ°λ³΅
)
Syn_tmp102(
	.clk(clk),					// I
	.sclr(sclr),				// I

	.sclk(tmp_scl),				// O
	.sdo(tmp_sdo),				// O
	.sdi(tmp_sdi),				// I
	.iobuf_T(tmp_sda_T),		// O

	.din(din_tmp102_c),				// I [WORD_WIDTH-1:0]
	.din_valid(din_tmp102_valid),	// I
	.dout(dout_tmp102),				// O [WORD_WIDTH-1:0]
	.dout_valid(),					// O

	.s_busy(busy_tmp102)			// O
); //total latency    

// TMP102(Temp.) ------------------------------
OBUF RF28_iobuf (
	.O(PIN_SCL),
	.I(tmp_scl)
);

IOBUF RF29_iobuf (
	.O(tmp_sdi),	// O  (to FPGA internal)
	.IO(PIN_SDA_IO),	// IO (connect directly to top-level port)
	.I(tmp_sdo),	// I  (from FPGA internal)
	.T(tmp_sda_T)	// I  high=input(to FPGA intenal) , low=output(to port)
);
endmodule
