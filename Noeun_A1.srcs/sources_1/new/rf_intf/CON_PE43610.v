`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/10/12 13:09:03
// Design Name: 
// Module Name: CON_PE43610
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module CON_PE43610(
    input wire clk,		//125Mhz
	input wire sclr,
	input wire 	[15:0]	din_att,
	input wire			din_att_valid,
	output wire			att_ready,	
	output wire		PIN_CS,
	output wire		PIN_CLK,	
	output wire		PIN_DATA
    );



// ---------------------------------------------------------------------------------
//  PE43610(Digital Atten.)
// ---------------------------------------------------------------------------------

// ATT CTRL shiftreg_intf

// PE43610(Att.) ATT Control
wire			att_le;
wire			att_clk;
 wire			att_data;
shiftreg_intf #(
 	.SCLK_PERIOD(15),			// 2?΄?
	.CLK_START_END(10),
	.HIGH_CLK(7),
	.CLK_OFFSET(7),			// SCLK_PERIOD-HIGH_CLK λ³΄λ€ ???Ό?¨.(? ?Έ? λΉν΄ HIGHκ°? λ°?λ¦°λ)
	.DATA_WIDTH(16),
	.SCS_N_MODE(0),
	.SCLK_IDLE_MODE(1)
) PE43610 (
	.clk		(clk),				// I
	.sclr		(sclr),				// I

	.din		(din_att),			// I [DATA_WIDTH-1:0]
	.din_valid	(din_att_valid),	// I
	.din_ready	(att_ready),		// O

	.sclk		(att_clk),			// O
	.scs_n		(),					// O
	.latch_en	(att_le),			// O
	.sdo		(att_data)			// O
); //total latency
    
// PE43610(Att.)------------------------------------------------
OBUF IOBUF_0 (
	.O(PIN_CS),
	.I(att_le)
);
OBUF IOBUF_1 (
	.O(PIN_CLK),
	.I(att_clk)
);
OBUF IOBUF_2 (
	.O(PIN_DATA),
	.I(att_data)
);    
endmodule
