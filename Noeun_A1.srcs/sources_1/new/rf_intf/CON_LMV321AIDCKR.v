`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/10/13 09:46:06
// Design Name: 
// Module Name: CON_LMV321AIDCKR
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module CON_LMV321AIDCKR(
  input wire  PIN_IN,
    output wire log_detect
    );
IBUF IOBUF_0 (
	.O(log_detect),
	.I(PIN_IN)
    );
endmodule
