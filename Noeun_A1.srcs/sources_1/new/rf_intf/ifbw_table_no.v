`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/10/06 17:03:13
// Design Name: 
// Module Name: ifbw_table_no
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ifbw_table_no(
	input wire 			clk,		// sys
	input wire         	ifbw_idx,
	input wire 			ifbw_idx_valid,

	output reg         	ifbw_sw,
	output reg 			ifbw_sw_valid
);

//ifbw_idx
// 0 = 70M
// 1 = 10M


//**************  Index table ********//
always @(posedge clk)	begin
	ifbw_sw_valid <= ifbw_idx_valid;
	case (ifbw_idx)
        2'd0 	: begin	ifbw_sw <= 1'b1;
						end	// ifbw = 70MHz
        2'd1 	: begin	ifbw_sw <= 1'b0;
						end	// ifbw = 10MHz

        default	: begin	ifbw_sw <= 1'b1;
						end	// ifbw = 70MHz
	endcase
end
endmodule
