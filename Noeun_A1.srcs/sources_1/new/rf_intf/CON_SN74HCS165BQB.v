`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/10/12 16:02:29
// Design Name: 
// Module Name: CON_SN74HCS165BQB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module CON_SN74HCS165BQB(
    input wire clk,		//125Mhz
	input wire sclr,
	input wire			din_sts_valid,
	output wire			din_sts_ready,
	output wire [15:0]	dout_sts,
	output wire			dout_sts_valid,
  	output wire		PIN_CS,
	output wire		PIN_CLK,	
	output wire		PIN_DATA   	
    );
    
// ---------------------------------------------------------------------------------
//  SN74HCS165BQB(Shift register - load) 
// ---------------------------------------------------------------------------------
//  STS Parallel to Shiftreg

wire			cm_cs;
wire			cm_clk;
wire			cm_data;
shiftreg_read_intf #(
	.SCLK_PERIOD(48),
	.HIGH_CLK(24),
	.CLK_OFFSET(12),
	.EDGE(1),
	.DATA_WIDTH(16)
) SN74HCS165BQB(
	.clk(clk),						// I
	.sclr(sclr),					// I

	.sclk(cm_clk),					// O
	.ld_n(cm_cs),					// O
	.sdi(cm_data),					// I

	.load(din_sts_valid),			// I
	.din_ready(din_sts_ready),		// O
	.dout(dout_sts),				// O [DATA_WIDTH-1:0]
	.dout_valid(dout_sts_valid)		// O
);

OBUF IOBUF_0 (
	.O(PIN_CS),
	.I(cm_cs)
);
OBUF IOBUF_1 (
	.O(PIN_CLK),
	.I(cm_clk)
);
IBUF IOBUF_2 (
	.O(cm_data),
	.I(PIN_DATA)
);
endmodule
