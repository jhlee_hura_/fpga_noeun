`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/10/12 15:35:33
// Design Name: 
// Module Name: CON_SN74HCS595BQBR
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module CON_SN74HCS595BQBR(

    input wire clk,		//125Mhz
	input wire sclr,
	input wire        	ifbw_idx,
	input wire			ifbw_idx_valid,	
	input wire [15:0]	rf_sw,
	input wire 			rf_sw_valid,
  	output wire		PIN_RCLK,
	output wire		PIN_CLK,	
	output wire		PIN_DATA    
    );
    

// ---------------------------------------------------------------------------------
//  BW to IF_BW
// ---------------------------------------------------------------------------------
wire     		ifbw_sw;
wire			ifbw_sw_valid;

ifbw_table_no ifbw_table (
	.clk(clk),						// I
	.ifbw_idx(ifbw_idx),				// I 
	.ifbw_idx_valid(ifbw_idx_valid),	// I

	.ifbw_sw(ifbw_sw),				// O 
	.ifbw_sw_valid(ifbw_sw_valid)	// O
);

// ---------------------------------------------------------------------------------
//   SN74HCS595BQBR(Shift register) RF_PIN 2,3,6 
// ---------------------------------------------------------------------------------
wire [15:0] 	rf_sw_1;
wire 			rf_sw_1_valid;
//assign rf_sw_1 = {rf1_sw[15:8],ifbw_sw,rf1_sw[6:0]};
assign rf_sw_1 = rf_sw;
assign rf_sw_1_valid = rf_sw_valid;

//  Switch RF1 (RF_CTRL)

wire [16:0] ff_rf1_sw;
wire ff_rf1_valid;
wire ff_rf1_empty;
wire rf1_sw_ready;
fifosd_17x16 adc_bd_SN74HCS595BQBR_fifo (
  .clk(clk),				// I
  .rst(sclr),				// I
  .din({rf_sw_1_valid,rf_sw_1}),			// I [16 : 0]
  .wr_en(rf_sw_1_valid),	// I
  .rd_en(~ff_rf1_empty && ~ff_rf1_valid && rf1_sw_ready),		// I
  .dout(ff_rf1_sw),			// O [16 : 0]
  .full(),					// O
  .empty(ff_rf1_empty),  // O
  .valid(ff_rf1_valid)	// O
);

// RF Switch Serial Shiftreg
wire			rf1_sw_rclk;
wire			rf1_sw_clk;
wire			rf1_sw_data;

shiftreg_intf #(
	.SCLK_PERIOD(24),		// (몇개 clk?���? 구성) 2?��?��
	.CLK_START_END(8),
	.HIGH_CLK(10),
	.CLK_OFFSET(10),  		// SCLK_PERIOD > HIGH_CLK+CLK_OFFSET (?��?��?�� 비해 HIGH�? �?린량)
	.DATA_WIDTH(16),
	.SCS_N_MODE(0),			// 0 = active low 1= active high
	.SCLK_IDLE_MODE(1) 		// 0= high idle 1=low idle
) SN74HCS595BQBR (
	.clk(clk),						// I
	.sclr(sclr),					// I

	// From/To Register
	.din(ff_rf1_sw[15:0]),					// I [WORD_WIDTH-1:0]
	.din_valid(ff_rf1_valid),		// I
	.din_ready(rf1_sw_ready),		// O

	// To pin-out(pad)
	.sclk(rf1_sw_clk),				// O
	.scs_n(),						// O
	.latch_en(rf1_sw_rclk),			// O
	.sdo(rf1_sw_data)				// O
);


// SN74HCS595BQBR(Write)--------------------------------------------------------------------
OBUF IOBUF_0 (
	.O(PIN_CLK),
	.I(rf1_sw_clk)
);
OBUF IOBUF_1 (
	.O(PIN_RCLK),
	.I(rf1_sw_rclk)
);
OBUF IOBUF_2 (
	.O(PIN_DATA),
	.I(rf1_sw_data)
);
endmodule
