`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/11/30 20:52:52
// Design Name: 
// Module Name: bsc_sys_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module bsc_sys(
    input wire  rst,
    input wire  u_sys_clk,
    input wire  pcie_clk,
    
    input wire          regs_bsc_sys_sel,
    input wire [46:0]   regs_adda,
    input wire          regs_adda_valid,
    output wire [31:0]  regs_bsc_sys_data,
    output wire         regs_bsc_sys_data_valid,
    input wire          regs_data_ready,

	//  TMP102 IN
	output	wire 	TMP_SCL_IN,	inout	wire 	TMP_SDA_IN,
	//  TMP102 Out
	output	wire 	TMP_SCL_EX,	inout	wire 	TMP_SDA_EX,	

    // ADS42JB49
    input wire  OVRA,    input wire  OVRB,
    input wire  ADC_SDOUT,    
    output wire ADC_CS_B, // ADC_SEN    
    output wire ADC_SCLK,
    output wire ADC_SDATA,

    
    // LMK04828 -> FPGA
    input wire  SYSREF_B_P,    input wire  SYSREF_B_N,    
    input wire  CLK_C_P,    input wire  CLK_C_N, 
    	
    // LMK04828	
	output wire    LMK_RESET,
	output wire    LMK_SYNC,	output wire    LMK_CS_B,	output wire    LMK_SCK,	inout wire     LMK_SDIO,
		
	// UART
	output wire    F_UART_TXD,	input  wire    F_UART_RXD,
	// FAN CTRL
	output wire    FAN_CTRL,
	// HMC 1033
	output wire    ADCCLK_SEN,	output wire    ADCCLK_SDI,	input wire     ADCCLK_SDO,	  output wire    ADCCLK_SCK,
	
		     
	//GPS 
	input wire		GPS_LOCK,	input wire		GPS_HOLDOVER,	

	// Ext. GPIO
	inout	wire [3:0]	EXT_GPIO,
	    
	//LED  
	output wire   	SP_RUN0_N,	// Fault LED
	output wire    	SP_RUN1_N,	output wire    	SP_RUN2_N,	output wire    	SP_RUN3_N       
    );
    
//==========================================================================================================  regs_bsc_sys
// 
//  board peripheral (adcA spi, HMC Ref Osc, Fan)
// 
wire [23:0] adcA_spi_wdata;
wire [23:0] adcA_spi_rdata;
wire 		adcA_spi_wd_vaild;
wire 		adcA_spi_din_ready;
wire [1:0]	tmp102_addr;
wire		tmp102_addr_valid;
wire [15:0]	tmp102_rdata_in;
wire [15:0]	tmp102_rdata_ex;
wire [31:0] hmcpll_spi_wdata;
wire [31:0] hmcpll_spi_rdata;
wire 		hmcpll_spi_wd_vaild;
wire 		hmcpll_spi_din_ready;
wire [15:0]	pwr_fan_high_dura;
wire [15:0]	pwr_fan_low_dura;

// ---------------------------------------------------------------------------------
// 
// EXT GPIO Control
// 
wire [3:0] gpio_rd;
wire [3:0] gpio_wr;
wire [3:0] gpio_dir;
// ---------------------------------------------------------------------------------
// 
// UART Control
// 
wire [7:0]	uart_wd;
wire		uart_wd_valid;
wire		uart_rd_en;
wire [7:0]	uart_rd;
wire		uart_rd_valid;
wire		uart_tx_fifo_rst;
wire		uart_rx_fifo_rst;
wire		uart_en_interrupt;
wire		uart_cfg_valid;
wire		uart_status_req;
wire [7:0]	uart_status;
// ---------------------------------------------------------------------------------
// 
// LED
// 
wire 	[3:0]led_on;
wire    led_test;

// ---------------------------------------------------------------------------------
// 
// GPSDO INFO
// 
reg [1:0] 	gpsdo_info;
 regs_bsc_sys regs_bsc_sys(
	.clk				(u_sys_clk),				// I 	// sys_clk
	.rst				(rst),					// I
	.pcie_clk			(pcie_clk),				// I

	.regs_sel			(regs_bsc_sys_sel),		// I
	.regs_adda			(regs_adda),			// I	[46:0]
	.regs_adda_valid	(regs_adda_valid),	    // I
	.regs_dout			(regs_bsc_sys_data),	// O	[31:0]
	.regs_dout_valid	(regs_bsc_sys_data_valid),		// O
	.regs_dout_ready	(regs_data_ready),		// I

	// -------------------------------------------------------
	// -- register ?���?
	// CLK_MUX_REG
	.clk_sel_rst(),		// O

	// PCB_VER_REG
	.pcb_ver(4'b0000),				// I [3:0]
	// BOARD_STAT_REG
	.board_stat(32'b0),		// I [31:0]
    //BSC_LED_REG
    .led_test(led_test),    // O
    .led_on(led_on),    // O [3:0]
    
	// ADCA_SPI_WR_REG
	.adcA_spi_wdata(adcA_spi_wdata),			// O [23:0]
	.adcA_spi_wd_valid(adcA_spi_wd_vaild),   	// O
	// ADCA_SPI_RD_REG
	.adcA_spi_rdata(adcA_spi_rdata),			// I [23:0]
	.adcA_spi_busy(~adcA_spi_din_ready),		// I

	// TMP_AD_REG
	.tmp102_addr(tmp102_addr),					// O [1:0]
	.tmp102_addr_valid(tmp102_addr_valid),		// O
	// TMP_IN_RD_REG
	.tmp102_rdata_in(tmp102_rdata_in),				// I [15:0]
	// TMP_EX_RD_REG
	.tmp102_rdata_ex(tmp102_rdata_ex),				// I [15:0]
	
	// HMCPLL_SPI_WR_REG
	.hmcpll_spi_wdata(hmcpll_spi_wdata),			// O [31:0]
	.hmcpll_spi_wd_vaild(hmcpll_spi_wd_vaild),		// O
	// HMCPLL_SPI_RD_REG
	.hmcpll_spi_rdata(hmcpll_spi_rdata),			// I [31:0]

	// GPSDO_INFO_REG
	.gpsdo_info(gpsdo_info),						// I [1:0]


	// PWR_FAN_HIGH_DURA_REG
	.pwr_fan_high_dura(pwr_fan_high_dura),			// O [15:0]
	// PWR_FAN_LOW_DURA_REG
	.pwr_fan_low_dura(pwr_fan_low_dura),			// O [15:0]

	// EXT_GPIO_DATA_REG
	.gpio_rd(gpio_rd),								// I [3:0]
	.gpio_wr(gpio_wr),								// O [3:0]
	// EXT_GPIO_DIR_REG
	.gpio_dir(gpio_dir),							// O [3:0]

	// UART_RD_REG
	.uart_rd(uart_rd),							// I [7:0]
	.uart_rd_en(uart_rd_en),                	// O
	// UART_WR_REG
	.uart_wd(uart_wd),							// O [7:0]
	.uart_wd_valid(uart_wd_valid),				// O
	// UART_STATUS_REG
	.uart_status(uart_status),					// I [7:0]
	.uart_status_req(uart_status_req),			// O
	// UART_CFG_REG
	.uart_tx_fifo_rst(uart_tx_fifo_rst),		// O
	.uart_rx_fifo_rst(uart_rx_fifo_rst),		// O
	.uart_en_interrupt(uart_en_interrupt),		// O
	.uart_cfg_valid(uart_cfg_valid)				// O    
);    

//==========================================================================================================  board_perip_intf
//
//
board_perip_intf board_perip_intf(
	.clk			(u_sys_clk),					// I
	.sclr			(rst),						// I

	// ADC A SPI
    .ADC_SDOUT      (ADC_SDOUT),                // I	
	.ADC_SCLK		(ADC_SCLK),					// O
	.ADC_CS_B		(ADC_CS_B),					// O
	.ADC_SDATA		(ADC_SDATA),				// O  

	.din_adcA_spi	(adcA_spi_wdata), 			// I [23:0]
	.div_adcA_spi	(adcA_spi_wd_vaild),		// I
	.dir_adcA_spi	(adcA_spi_din_ready), 		// O
	.do_adcA_spi	(adcA_spi_rdata),			// O [23:0]
	.dov_adcA_spi	(), 						// O

    .LMK_CS_B(LMK_CS_B),     // O
    .LMK_SCK( LMK_SCK),     // O
    .LMK_SDIO(LMK_SDIO),    //  IO
    
	// TMP Sensor IN
	.TMP_SCL_IN(TMP_SCL_IN),						// O
	.TMP_SDA_IN(TMP_SDA_IN),						// IO
	.dout_tmp102_in(tmp102_rdata_in),				// O [15:0]
	.busy_tmp102_in(),							// O
	
	// TMP Sensor EX
	.TMP_SCL_EX(TMP_SCL_EX),						// O
	.TMP_SDA_EX(TMP_SDA_EX),						// IO
	.dout_tmp102_ex(tmp102_rdata_ex),				// O [15:0]
	.busy_tmp102_ex(),							// O
		
	.din_tmp102(tmp102_addr),				// I [1:0]
	.din_tmp102_valid(tmp102_addr_valid),	// I

	
	// HMC PLL
	.hmcpll_sck		(ADCCLK_SCK),				// O
	.hmcpll_scs		(ADCCLK_SEN),				// O
	.hmcpll_sdo		(ADCCLK_SDI),				            // O
	.hmcpll_sdi		(ADCCLK_SDO),				// I

	.din_hmcpll_spi	(hmcpll_spi_wdata), 		// I [31:0]
	.div_hmcpll_spi	(hmcpll_spi_wd_vaild),		// I
	.dir_hmcpll_spi	(hmcpll_spi_din_ready),		// O
	.do_hmcpll_spi	(hmcpll_spi_rdata),			// O [31:0]
	.dov_hmcpll_spi	(),							// O

	// FAN Speed control
	.pwr_fan			(FAN_CTRL),				// O
	.pwr_fan_high_dura	(pwr_fan_high_dura),	// I [15:0]
	.pwr_fan_low_dura	(pwr_fan_low_dura)		// I [15:0]
);
      
//==========================================================================================================  GPIO
//
//
gpio_intf #(
	.NBIT(4)
) gpio_intf (
	.GPIO(EXT_GPIO),	// IO [NBIT-1:0]

	.din(gpio_wr),		// I [NBIT-1:0]
	.d_T(gpio_dir),		// I [NBIT-1:0]
	.dout(gpio_rd)		// O [NBIT-1:0]
);
//==========================================================================================================  UART
//
//
uart_lite uart_lite (
	.clk(u_sys_clk),			// I
	.sclr(rst),			// I

	.UART1_RXD(F_UART_RXD),	// I
	.UART1_TXD(F_UART_TXD),	// O

	.w_data(uart_wd),				// I [7:0]
	.w_data_valid(uart_wd_valid),	// I
	.rd_en(uart_rd_en),             // I
	.r_data(uart_rd),            	// O [7:0]
	.r_data_valid(),				// O

	.tx_fifo_rst(uart_tx_fifo_rst),		// I
	.rx_fifo_rst(uart_rx_fifo_rst),		// I
	.en_interrupt(uart_en_interrupt),	// I
	.cfg_valid(uart_cfg_valid),			// I

	.status_req(uart_status_req),		// I
	.status(uart_status)				// O [7:0]
);
//==========================================================================================================  LED Module
//
//

OBUF   led_3_obuf (.O(SP_RUN3_N), .I(~led_on[3]));				// sys_clk & power on
OBUF   led_2_obuf (.O(SP_RUN2_N), .I(~led_on[2]));				// user design part
OBUF   led_1_obuf (.O(SP_RUN1_N), .I(~led_on[1]));				// PCIE Transfer
OBUF   led_0_obuf (.O(SP_RUN0_N), .I(~led_on[0]));			// Fault LED
assign 	led_on = led_on;

//==========================================================================================================  GPSDO Info


always @(posedge u_sys_clk) begin
	gpsdo_info[0] 	<= GPS_HOLDOVER;
	gpsdo_info[1] 	<= GPS_LOCK;
end    
endmodule
