// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Thu Oct  7 10:35:28 2021
// Host        : HURA-JUNHO running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               C:/Users/hura/Documents/Xilinx_xfile/Noeun/noeun_a_19ver_01/noeun_a_19ver.srcs/sources_1/ip/lv_table_7x1024/lv_table_7x1024_stub.v
// Design      : lv_table_7x1024
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku5p-ffvb676-1-i
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "dist_mem_gen_v8_0_13,Vivado 2019.1" *)
module lv_table_7x1024(a, d, dpra, clk, we, dpo)
/* synthesis syn_black_box black_box_pad_pin="a[9:0],d[6:0],dpra[9:0],clk,we,dpo[6:0]" */;
  input [9:0]a;
  input [6:0]d;
  input [9:0]dpra;
  input clk;
  input we;
  output [6:0]dpo;
endmodule
