// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Thu Oct  7 10:35:28 2021
// Host        : HURA-JUNHO running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               C:/Users/hura/Documents/Xilinx_xfile/Noeun/noeun_a_19ver_01/noeun_a_19ver.srcs/sources_1/ip/lv_table_7x1024/lv_table_7x1024_sim_netlist.v
// Design      : lv_table_7x1024
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku5p-ffvb676-1-i
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "lv_table_7x1024,dist_mem_gen_v8_0_13,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "dist_mem_gen_v8_0_13,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module lv_table_7x1024
   (a,
    d,
    dpra,
    clk,
    we,
    dpo);
  input [9:0]a;
  input [6:0]d;
  input [9:0]dpra;
  input clk;
  input we;
  output [6:0]dpo;

  wire [9:0]a;
  wire clk;
  wire [6:0]d;
  wire [6:0]dpo;
  wire [9:0]dpra;
  wire we;
  wire [6:0]NLW_U0_qdpo_UNCONNECTED;
  wire [6:0]NLW_U0_qspo_UNCONNECTED;
  wire [6:0]NLW_U0_spo_UNCONNECTED;

  (* C_FAMILY = "kintexuplus" *) 
  (* C_HAS_CLK = "1" *) 
  (* C_HAS_D = "1" *) 
  (* C_HAS_DPO = "1" *) 
  (* C_HAS_DPRA = "1" *) 
  (* C_HAS_QDPO = "0" *) 
  (* C_HAS_QDPO_CE = "0" *) 
  (* C_HAS_QDPO_CLK = "0" *) 
  (* C_HAS_QDPO_RST = "0" *) 
  (* C_HAS_QDPO_SRST = "0" *) 
  (* C_HAS_QSPO = "0" *) 
  (* C_HAS_QSPO_RST = "0" *) 
  (* C_HAS_QSPO_SRST = "0" *) 
  (* C_HAS_SPO = "0" *) 
  (* C_HAS_WE = "1" *) 
  (* C_MEM_TYPE = "4" *) 
  (* C_REG_DPRA_INPUT = "0" *) 
  (* c_addr_width = "10" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1024" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_i_ce = "0" *) 
  (* c_has_qspo_ce = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_parser_type = "1" *) 
  (* c_pipeline_stages = "0" *) 
  (* c_qce_joined = "0" *) 
  (* c_qualify_we = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_a_d_inputs = "0" *) 
  (* c_sync_enable = "1" *) 
  (* c_width = "7" *) 
  lv_table_7x1024_dist_mem_gen_v8_0_13 U0
       (.a(a),
        .clk(clk),
        .d(d),
        .dpo(dpo),
        .dpra(dpra),
        .i_ce(1'b1),
        .qdpo(NLW_U0_qdpo_UNCONNECTED[6:0]),
        .qdpo_ce(1'b1),
        .qdpo_clk(1'b0),
        .qdpo_rst(1'b0),
        .qdpo_srst(1'b0),
        .qspo(NLW_U0_qspo_UNCONNECTED[6:0]),
        .qspo_ce(1'b1),
        .qspo_rst(1'b0),
        .qspo_srst(1'b0),
        .spo(NLW_U0_spo_UNCONNECTED[6:0]),
        .we(we));
endmodule

(* C_ADDR_WIDTH = "10" *) (* C_DEFAULT_DATA = "0" *) (* C_DEPTH = "1024" *) 
(* C_ELABORATION_DIR = "./" *) (* C_FAMILY = "kintexuplus" *) (* C_HAS_CLK = "1" *) 
(* C_HAS_D = "1" *) (* C_HAS_DPO = "1" *) (* C_HAS_DPRA = "1" *) 
(* C_HAS_I_CE = "0" *) (* C_HAS_QDPO = "0" *) (* C_HAS_QDPO_CE = "0" *) 
(* C_HAS_QDPO_CLK = "0" *) (* C_HAS_QDPO_RST = "0" *) (* C_HAS_QDPO_SRST = "0" *) 
(* C_HAS_QSPO = "0" *) (* C_HAS_QSPO_CE = "0" *) (* C_HAS_QSPO_RST = "0" *) 
(* C_HAS_QSPO_SRST = "0" *) (* C_HAS_SPO = "0" *) (* C_HAS_WE = "1" *) 
(* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_MEM_TYPE = "4" *) (* C_PARSER_TYPE = "1" *) 
(* C_PIPELINE_STAGES = "0" *) (* C_QCE_JOINED = "0" *) (* C_QUALIFY_WE = "0" *) 
(* C_READ_MIF = "0" *) (* C_REG_A_D_INPUTS = "0" *) (* C_REG_DPRA_INPUT = "0" *) 
(* C_SYNC_ENABLE = "1" *) (* C_WIDTH = "7" *) (* ORIG_REF_NAME = "dist_mem_gen_v8_0_13" *) 
module lv_table_7x1024_dist_mem_gen_v8_0_13
   (a,
    d,
    dpra,
    clk,
    we,
    i_ce,
    qspo_ce,
    qdpo_ce,
    qdpo_clk,
    qspo_rst,
    qdpo_rst,
    qspo_srst,
    qdpo_srst,
    spo,
    dpo,
    qspo,
    qdpo);
  input [9:0]a;
  input [6:0]d;
  input [9:0]dpra;
  input clk;
  input we;
  input i_ce;
  input qspo_ce;
  input qdpo_ce;
  input qdpo_clk;
  input qspo_rst;
  input qdpo_rst;
  input qspo_srst;
  input qdpo_srst;
  output [6:0]spo;
  output [6:0]dpo;
  output [6:0]qspo;
  output [6:0]qdpo;

  wire \<const0> ;
  wire [9:0]a;
  wire clk;
  wire [6:0]d;
  wire [6:0]dpo;
  wire [9:0]dpra;
  wire we;

  assign qdpo[6] = \<const0> ;
  assign qdpo[5] = \<const0> ;
  assign qdpo[4] = \<const0> ;
  assign qdpo[3] = \<const0> ;
  assign qdpo[2] = \<const0> ;
  assign qdpo[1] = \<const0> ;
  assign qdpo[0] = \<const0> ;
  assign qspo[6] = \<const0> ;
  assign qspo[5] = \<const0> ;
  assign qspo[4] = \<const0> ;
  assign qspo[3] = \<const0> ;
  assign qspo[2] = \<const0> ;
  assign qspo[1] = \<const0> ;
  assign qspo[0] = \<const0> ;
  assign spo[6] = \<const0> ;
  assign spo[5] = \<const0> ;
  assign spo[4] = \<const0> ;
  assign spo[3] = \<const0> ;
  assign spo[2] = \<const0> ;
  assign spo[1] = \<const0> ;
  assign spo[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  lv_table_7x1024_dist_mem_gen_v8_0_13_synth \synth_options.dist_mem_inst 
       (.a(a),
        .clk(clk),
        .d(d),
        .dpo(dpo),
        .dpra(dpra),
        .we(we));
endmodule

(* ORIG_REF_NAME = "dist_mem_gen_v8_0_13_synth" *) 
module lv_table_7x1024_dist_mem_gen_v8_0_13_synth
   (dpo,
    clk,
    d,
    dpra,
    a,
    we);
  output [6:0]dpo;
  input clk;
  input [6:0]d;
  input [9:0]dpra;
  input [9:0]a;
  input we;

  wire [9:0]a;
  wire clk;
  wire [6:0]d;
  wire [6:0]dpo;
  wire [9:0]dpra;
  wire we;

  lv_table_7x1024_sdpram \gen_sdp_ram.sdpram_inst 
       (.a(a),
        .clk(clk),
        .d(d),
        .dpo(dpo),
        .dpra(dpra),
        .we(we));
endmodule

(* ORIG_REF_NAME = "sdpram" *) 
module lv_table_7x1024_sdpram
   (dpo,
    clk,
    d,
    dpra,
    a,
    we);
  output [6:0]dpo;
  input clk;
  input [6:0]d;
  input [9:0]dpra;
  input [9:0]a;
  input we;

  wire [9:0]a;
  wire clk;
  wire [6:0]d;
  wire [6:0]dpo;
  wire \dpo[0]_INST_0_i_1_n_0 ;
  wire \dpo[0]_INST_0_i_2_n_0 ;
  wire \dpo[0]_INST_0_i_3_n_0 ;
  wire \dpo[0]_INST_0_i_4_n_0 ;
  wire \dpo[0]_INST_0_i_5_n_0 ;
  wire \dpo[0]_INST_0_i_6_n_0 ;
  wire \dpo[1]_INST_0_i_1_n_0 ;
  wire \dpo[1]_INST_0_i_2_n_0 ;
  wire \dpo[1]_INST_0_i_3_n_0 ;
  wire \dpo[1]_INST_0_i_4_n_0 ;
  wire \dpo[1]_INST_0_i_5_n_0 ;
  wire \dpo[1]_INST_0_i_6_n_0 ;
  wire \dpo[2]_INST_0_i_1_n_0 ;
  wire \dpo[2]_INST_0_i_2_n_0 ;
  wire \dpo[2]_INST_0_i_3_n_0 ;
  wire \dpo[2]_INST_0_i_4_n_0 ;
  wire \dpo[2]_INST_0_i_5_n_0 ;
  wire \dpo[2]_INST_0_i_6_n_0 ;
  wire \dpo[3]_INST_0_i_1_n_0 ;
  wire \dpo[3]_INST_0_i_2_n_0 ;
  wire \dpo[3]_INST_0_i_3_n_0 ;
  wire \dpo[3]_INST_0_i_4_n_0 ;
  wire \dpo[3]_INST_0_i_5_n_0 ;
  wire \dpo[3]_INST_0_i_6_n_0 ;
  wire \dpo[4]_INST_0_i_1_n_0 ;
  wire \dpo[4]_INST_0_i_2_n_0 ;
  wire \dpo[4]_INST_0_i_3_n_0 ;
  wire \dpo[4]_INST_0_i_4_n_0 ;
  wire \dpo[4]_INST_0_i_5_n_0 ;
  wire \dpo[4]_INST_0_i_6_n_0 ;
  wire \dpo[5]_INST_0_i_1_n_0 ;
  wire \dpo[5]_INST_0_i_2_n_0 ;
  wire \dpo[5]_INST_0_i_3_n_0 ;
  wire \dpo[5]_INST_0_i_4_n_0 ;
  wire \dpo[5]_INST_0_i_5_n_0 ;
  wire \dpo[5]_INST_0_i_6_n_0 ;
  wire \dpo[6]_INST_0_i_1_n_0 ;
  wire \dpo[6]_INST_0_i_2_n_0 ;
  wire \dpo[6]_INST_0_i_3_n_0 ;
  wire \dpo[6]_INST_0_i_4_n_0 ;
  wire \dpo[6]_INST_0_i_5_n_0 ;
  wire \dpo[6]_INST_0_i_6_n_0 ;
  wire [9:0]dpra;
  (* RTL_KEEP = "true" *) wire [6:0]qsdpo_int;
  wire ram_reg_0_63_0_6_i_1_n_0;
  wire ram_reg_0_63_0_6_n_0;
  wire ram_reg_0_63_0_6_n_1;
  wire ram_reg_0_63_0_6_n_2;
  wire ram_reg_0_63_0_6_n_3;
  wire ram_reg_0_63_0_6_n_4;
  wire ram_reg_0_63_0_6_n_5;
  wire ram_reg_0_63_0_6_n_6;
  wire ram_reg_128_191_0_6_i_1_n_0;
  wire ram_reg_128_191_0_6_n_0;
  wire ram_reg_128_191_0_6_n_1;
  wire ram_reg_128_191_0_6_n_2;
  wire ram_reg_128_191_0_6_n_3;
  wire ram_reg_128_191_0_6_n_4;
  wire ram_reg_128_191_0_6_n_5;
  wire ram_reg_128_191_0_6_n_6;
  wire ram_reg_192_255_0_6_i_1_n_0;
  wire ram_reg_192_255_0_6_n_0;
  wire ram_reg_192_255_0_6_n_1;
  wire ram_reg_192_255_0_6_n_2;
  wire ram_reg_192_255_0_6_n_3;
  wire ram_reg_192_255_0_6_n_4;
  wire ram_reg_192_255_0_6_n_5;
  wire ram_reg_192_255_0_6_n_6;
  wire ram_reg_256_319_0_6_i_1_n_0;
  wire ram_reg_256_319_0_6_n_0;
  wire ram_reg_256_319_0_6_n_1;
  wire ram_reg_256_319_0_6_n_2;
  wire ram_reg_256_319_0_6_n_3;
  wire ram_reg_256_319_0_6_n_4;
  wire ram_reg_256_319_0_6_n_5;
  wire ram_reg_256_319_0_6_n_6;
  wire ram_reg_320_383_0_6_i_1_n_0;
  wire ram_reg_320_383_0_6_n_0;
  wire ram_reg_320_383_0_6_n_1;
  wire ram_reg_320_383_0_6_n_2;
  wire ram_reg_320_383_0_6_n_3;
  wire ram_reg_320_383_0_6_n_4;
  wire ram_reg_320_383_0_6_n_5;
  wire ram_reg_320_383_0_6_n_6;
  wire ram_reg_384_447_0_6_i_1_n_0;
  wire ram_reg_384_447_0_6_n_0;
  wire ram_reg_384_447_0_6_n_1;
  wire ram_reg_384_447_0_6_n_2;
  wire ram_reg_384_447_0_6_n_3;
  wire ram_reg_384_447_0_6_n_4;
  wire ram_reg_384_447_0_6_n_5;
  wire ram_reg_384_447_0_6_n_6;
  wire ram_reg_448_511_0_6_i_1_n_0;
  wire ram_reg_448_511_0_6_n_0;
  wire ram_reg_448_511_0_6_n_1;
  wire ram_reg_448_511_0_6_n_2;
  wire ram_reg_448_511_0_6_n_3;
  wire ram_reg_448_511_0_6_n_4;
  wire ram_reg_448_511_0_6_n_5;
  wire ram_reg_448_511_0_6_n_6;
  wire ram_reg_512_575_0_6_i_1_n_0;
  wire ram_reg_512_575_0_6_n_0;
  wire ram_reg_512_575_0_6_n_1;
  wire ram_reg_512_575_0_6_n_2;
  wire ram_reg_512_575_0_6_n_3;
  wire ram_reg_512_575_0_6_n_4;
  wire ram_reg_512_575_0_6_n_5;
  wire ram_reg_512_575_0_6_n_6;
  wire ram_reg_576_639_0_6_i_1_n_0;
  wire ram_reg_576_639_0_6_n_0;
  wire ram_reg_576_639_0_6_n_1;
  wire ram_reg_576_639_0_6_n_2;
  wire ram_reg_576_639_0_6_n_3;
  wire ram_reg_576_639_0_6_n_4;
  wire ram_reg_576_639_0_6_n_5;
  wire ram_reg_576_639_0_6_n_6;
  wire ram_reg_640_703_0_6_i_1_n_0;
  wire ram_reg_640_703_0_6_n_0;
  wire ram_reg_640_703_0_6_n_1;
  wire ram_reg_640_703_0_6_n_2;
  wire ram_reg_640_703_0_6_n_3;
  wire ram_reg_640_703_0_6_n_4;
  wire ram_reg_640_703_0_6_n_5;
  wire ram_reg_640_703_0_6_n_6;
  wire ram_reg_64_127_0_6_i_1_n_0;
  wire ram_reg_64_127_0_6_n_0;
  wire ram_reg_64_127_0_6_n_1;
  wire ram_reg_64_127_0_6_n_2;
  wire ram_reg_64_127_0_6_n_3;
  wire ram_reg_64_127_0_6_n_4;
  wire ram_reg_64_127_0_6_n_5;
  wire ram_reg_64_127_0_6_n_6;
  wire ram_reg_704_767_0_6_i_1_n_0;
  wire ram_reg_704_767_0_6_n_0;
  wire ram_reg_704_767_0_6_n_1;
  wire ram_reg_704_767_0_6_n_2;
  wire ram_reg_704_767_0_6_n_3;
  wire ram_reg_704_767_0_6_n_4;
  wire ram_reg_704_767_0_6_n_5;
  wire ram_reg_704_767_0_6_n_6;
  wire ram_reg_768_831_0_6_i_1_n_0;
  wire ram_reg_768_831_0_6_n_0;
  wire ram_reg_768_831_0_6_n_1;
  wire ram_reg_768_831_0_6_n_2;
  wire ram_reg_768_831_0_6_n_3;
  wire ram_reg_768_831_0_6_n_4;
  wire ram_reg_768_831_0_6_n_5;
  wire ram_reg_768_831_0_6_n_6;
  wire ram_reg_832_895_0_6_i_1_n_0;
  wire ram_reg_832_895_0_6_n_0;
  wire ram_reg_832_895_0_6_n_1;
  wire ram_reg_832_895_0_6_n_2;
  wire ram_reg_832_895_0_6_n_3;
  wire ram_reg_832_895_0_6_n_4;
  wire ram_reg_832_895_0_6_n_5;
  wire ram_reg_832_895_0_6_n_6;
  wire ram_reg_896_959_0_6_i_1_n_0;
  wire ram_reg_896_959_0_6_n_0;
  wire ram_reg_896_959_0_6_n_1;
  wire ram_reg_896_959_0_6_n_2;
  wire ram_reg_896_959_0_6_n_3;
  wire ram_reg_896_959_0_6_n_4;
  wire ram_reg_896_959_0_6_n_5;
  wire ram_reg_896_959_0_6_n_6;
  wire ram_reg_960_1023_0_6_i_1_n_0;
  wire ram_reg_960_1023_0_6_n_0;
  wire ram_reg_960_1023_0_6_n_1;
  wire ram_reg_960_1023_0_6_n_2;
  wire ram_reg_960_1023_0_6_n_3;
  wire ram_reg_960_1023_0_6_n_4;
  wire ram_reg_960_1023_0_6_n_5;
  wire ram_reg_960_1023_0_6_n_6;
  wire we;
  wire NLW_ram_reg_0_63_0_6_DOH_UNCONNECTED;
  wire NLW_ram_reg_128_191_0_6_DOH_UNCONNECTED;
  wire NLW_ram_reg_192_255_0_6_DOH_UNCONNECTED;
  wire NLW_ram_reg_256_319_0_6_DOH_UNCONNECTED;
  wire NLW_ram_reg_320_383_0_6_DOH_UNCONNECTED;
  wire NLW_ram_reg_384_447_0_6_DOH_UNCONNECTED;
  wire NLW_ram_reg_448_511_0_6_DOH_UNCONNECTED;
  wire NLW_ram_reg_512_575_0_6_DOH_UNCONNECTED;
  wire NLW_ram_reg_576_639_0_6_DOH_UNCONNECTED;
  wire NLW_ram_reg_640_703_0_6_DOH_UNCONNECTED;
  wire NLW_ram_reg_64_127_0_6_DOH_UNCONNECTED;
  wire NLW_ram_reg_704_767_0_6_DOH_UNCONNECTED;
  wire NLW_ram_reg_768_831_0_6_DOH_UNCONNECTED;
  wire NLW_ram_reg_832_895_0_6_DOH_UNCONNECTED;
  wire NLW_ram_reg_896_959_0_6_DOH_UNCONNECTED;
  wire NLW_ram_reg_960_1023_0_6_DOH_UNCONNECTED;

  MUXF8 \dpo[0]_INST_0 
       (.I0(\dpo[0]_INST_0_i_1_n_0 ),
        .I1(\dpo[0]_INST_0_i_2_n_0 ),
        .O(dpo[0]),
        .S(dpra[9]));
  MUXF7 \dpo[0]_INST_0_i_1 
       (.I0(\dpo[0]_INST_0_i_3_n_0 ),
        .I1(\dpo[0]_INST_0_i_4_n_0 ),
        .O(\dpo[0]_INST_0_i_1_n_0 ),
        .S(dpra[8]));
  MUXF7 \dpo[0]_INST_0_i_2 
       (.I0(\dpo[0]_INST_0_i_5_n_0 ),
        .I1(\dpo[0]_INST_0_i_6_n_0 ),
        .O(\dpo[0]_INST_0_i_2_n_0 ),
        .S(dpra[8]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[0]_INST_0_i_3 
       (.I0(ram_reg_192_255_0_6_n_0),
        .I1(ram_reg_128_191_0_6_n_0),
        .I2(dpra[7]),
        .I3(ram_reg_64_127_0_6_n_0),
        .I4(dpra[6]),
        .I5(ram_reg_0_63_0_6_n_0),
        .O(\dpo[0]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[0]_INST_0_i_4 
       (.I0(ram_reg_448_511_0_6_n_0),
        .I1(ram_reg_384_447_0_6_n_0),
        .I2(dpra[7]),
        .I3(ram_reg_320_383_0_6_n_0),
        .I4(dpra[6]),
        .I5(ram_reg_256_319_0_6_n_0),
        .O(\dpo[0]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[0]_INST_0_i_5 
       (.I0(ram_reg_704_767_0_6_n_0),
        .I1(ram_reg_640_703_0_6_n_0),
        .I2(dpra[7]),
        .I3(ram_reg_576_639_0_6_n_0),
        .I4(dpra[6]),
        .I5(ram_reg_512_575_0_6_n_0),
        .O(\dpo[0]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[0]_INST_0_i_6 
       (.I0(ram_reg_960_1023_0_6_n_0),
        .I1(ram_reg_896_959_0_6_n_0),
        .I2(dpra[7]),
        .I3(ram_reg_832_895_0_6_n_0),
        .I4(dpra[6]),
        .I5(ram_reg_768_831_0_6_n_0),
        .O(\dpo[0]_INST_0_i_6_n_0 ));
  MUXF8 \dpo[1]_INST_0 
       (.I0(\dpo[1]_INST_0_i_1_n_0 ),
        .I1(\dpo[1]_INST_0_i_2_n_0 ),
        .O(dpo[1]),
        .S(dpra[9]));
  MUXF7 \dpo[1]_INST_0_i_1 
       (.I0(\dpo[1]_INST_0_i_3_n_0 ),
        .I1(\dpo[1]_INST_0_i_4_n_0 ),
        .O(\dpo[1]_INST_0_i_1_n_0 ),
        .S(dpra[8]));
  MUXF7 \dpo[1]_INST_0_i_2 
       (.I0(\dpo[1]_INST_0_i_5_n_0 ),
        .I1(\dpo[1]_INST_0_i_6_n_0 ),
        .O(\dpo[1]_INST_0_i_2_n_0 ),
        .S(dpra[8]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[1]_INST_0_i_3 
       (.I0(ram_reg_192_255_0_6_n_1),
        .I1(ram_reg_128_191_0_6_n_1),
        .I2(dpra[7]),
        .I3(ram_reg_64_127_0_6_n_1),
        .I4(dpra[6]),
        .I5(ram_reg_0_63_0_6_n_1),
        .O(\dpo[1]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[1]_INST_0_i_4 
       (.I0(ram_reg_448_511_0_6_n_1),
        .I1(ram_reg_384_447_0_6_n_1),
        .I2(dpra[7]),
        .I3(ram_reg_320_383_0_6_n_1),
        .I4(dpra[6]),
        .I5(ram_reg_256_319_0_6_n_1),
        .O(\dpo[1]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[1]_INST_0_i_5 
       (.I0(ram_reg_704_767_0_6_n_1),
        .I1(ram_reg_640_703_0_6_n_1),
        .I2(dpra[7]),
        .I3(ram_reg_576_639_0_6_n_1),
        .I4(dpra[6]),
        .I5(ram_reg_512_575_0_6_n_1),
        .O(\dpo[1]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[1]_INST_0_i_6 
       (.I0(ram_reg_960_1023_0_6_n_1),
        .I1(ram_reg_896_959_0_6_n_1),
        .I2(dpra[7]),
        .I3(ram_reg_832_895_0_6_n_1),
        .I4(dpra[6]),
        .I5(ram_reg_768_831_0_6_n_1),
        .O(\dpo[1]_INST_0_i_6_n_0 ));
  MUXF8 \dpo[2]_INST_0 
       (.I0(\dpo[2]_INST_0_i_1_n_0 ),
        .I1(\dpo[2]_INST_0_i_2_n_0 ),
        .O(dpo[2]),
        .S(dpra[9]));
  MUXF7 \dpo[2]_INST_0_i_1 
       (.I0(\dpo[2]_INST_0_i_3_n_0 ),
        .I1(\dpo[2]_INST_0_i_4_n_0 ),
        .O(\dpo[2]_INST_0_i_1_n_0 ),
        .S(dpra[8]));
  MUXF7 \dpo[2]_INST_0_i_2 
       (.I0(\dpo[2]_INST_0_i_5_n_0 ),
        .I1(\dpo[2]_INST_0_i_6_n_0 ),
        .O(\dpo[2]_INST_0_i_2_n_0 ),
        .S(dpra[8]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[2]_INST_0_i_3 
       (.I0(ram_reg_192_255_0_6_n_2),
        .I1(ram_reg_128_191_0_6_n_2),
        .I2(dpra[7]),
        .I3(ram_reg_64_127_0_6_n_2),
        .I4(dpra[6]),
        .I5(ram_reg_0_63_0_6_n_2),
        .O(\dpo[2]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[2]_INST_0_i_4 
       (.I0(ram_reg_448_511_0_6_n_2),
        .I1(ram_reg_384_447_0_6_n_2),
        .I2(dpra[7]),
        .I3(ram_reg_320_383_0_6_n_2),
        .I4(dpra[6]),
        .I5(ram_reg_256_319_0_6_n_2),
        .O(\dpo[2]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[2]_INST_0_i_5 
       (.I0(ram_reg_704_767_0_6_n_2),
        .I1(ram_reg_640_703_0_6_n_2),
        .I2(dpra[7]),
        .I3(ram_reg_576_639_0_6_n_2),
        .I4(dpra[6]),
        .I5(ram_reg_512_575_0_6_n_2),
        .O(\dpo[2]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[2]_INST_0_i_6 
       (.I0(ram_reg_960_1023_0_6_n_2),
        .I1(ram_reg_896_959_0_6_n_2),
        .I2(dpra[7]),
        .I3(ram_reg_832_895_0_6_n_2),
        .I4(dpra[6]),
        .I5(ram_reg_768_831_0_6_n_2),
        .O(\dpo[2]_INST_0_i_6_n_0 ));
  MUXF8 \dpo[3]_INST_0 
       (.I0(\dpo[3]_INST_0_i_1_n_0 ),
        .I1(\dpo[3]_INST_0_i_2_n_0 ),
        .O(dpo[3]),
        .S(dpra[9]));
  MUXF7 \dpo[3]_INST_0_i_1 
       (.I0(\dpo[3]_INST_0_i_3_n_0 ),
        .I1(\dpo[3]_INST_0_i_4_n_0 ),
        .O(\dpo[3]_INST_0_i_1_n_0 ),
        .S(dpra[8]));
  MUXF7 \dpo[3]_INST_0_i_2 
       (.I0(\dpo[3]_INST_0_i_5_n_0 ),
        .I1(\dpo[3]_INST_0_i_6_n_0 ),
        .O(\dpo[3]_INST_0_i_2_n_0 ),
        .S(dpra[8]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[3]_INST_0_i_3 
       (.I0(ram_reg_192_255_0_6_n_3),
        .I1(ram_reg_128_191_0_6_n_3),
        .I2(dpra[7]),
        .I3(ram_reg_64_127_0_6_n_3),
        .I4(dpra[6]),
        .I5(ram_reg_0_63_0_6_n_3),
        .O(\dpo[3]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[3]_INST_0_i_4 
       (.I0(ram_reg_448_511_0_6_n_3),
        .I1(ram_reg_384_447_0_6_n_3),
        .I2(dpra[7]),
        .I3(ram_reg_320_383_0_6_n_3),
        .I4(dpra[6]),
        .I5(ram_reg_256_319_0_6_n_3),
        .O(\dpo[3]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[3]_INST_0_i_5 
       (.I0(ram_reg_704_767_0_6_n_3),
        .I1(ram_reg_640_703_0_6_n_3),
        .I2(dpra[7]),
        .I3(ram_reg_576_639_0_6_n_3),
        .I4(dpra[6]),
        .I5(ram_reg_512_575_0_6_n_3),
        .O(\dpo[3]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[3]_INST_0_i_6 
       (.I0(ram_reg_960_1023_0_6_n_3),
        .I1(ram_reg_896_959_0_6_n_3),
        .I2(dpra[7]),
        .I3(ram_reg_832_895_0_6_n_3),
        .I4(dpra[6]),
        .I5(ram_reg_768_831_0_6_n_3),
        .O(\dpo[3]_INST_0_i_6_n_0 ));
  MUXF8 \dpo[4]_INST_0 
       (.I0(\dpo[4]_INST_0_i_1_n_0 ),
        .I1(\dpo[4]_INST_0_i_2_n_0 ),
        .O(dpo[4]),
        .S(dpra[9]));
  MUXF7 \dpo[4]_INST_0_i_1 
       (.I0(\dpo[4]_INST_0_i_3_n_0 ),
        .I1(\dpo[4]_INST_0_i_4_n_0 ),
        .O(\dpo[4]_INST_0_i_1_n_0 ),
        .S(dpra[8]));
  MUXF7 \dpo[4]_INST_0_i_2 
       (.I0(\dpo[4]_INST_0_i_5_n_0 ),
        .I1(\dpo[4]_INST_0_i_6_n_0 ),
        .O(\dpo[4]_INST_0_i_2_n_0 ),
        .S(dpra[8]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[4]_INST_0_i_3 
       (.I0(ram_reg_192_255_0_6_n_4),
        .I1(ram_reg_128_191_0_6_n_4),
        .I2(dpra[7]),
        .I3(ram_reg_64_127_0_6_n_4),
        .I4(dpra[6]),
        .I5(ram_reg_0_63_0_6_n_4),
        .O(\dpo[4]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[4]_INST_0_i_4 
       (.I0(ram_reg_448_511_0_6_n_4),
        .I1(ram_reg_384_447_0_6_n_4),
        .I2(dpra[7]),
        .I3(ram_reg_320_383_0_6_n_4),
        .I4(dpra[6]),
        .I5(ram_reg_256_319_0_6_n_4),
        .O(\dpo[4]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[4]_INST_0_i_5 
       (.I0(ram_reg_704_767_0_6_n_4),
        .I1(ram_reg_640_703_0_6_n_4),
        .I2(dpra[7]),
        .I3(ram_reg_576_639_0_6_n_4),
        .I4(dpra[6]),
        .I5(ram_reg_512_575_0_6_n_4),
        .O(\dpo[4]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[4]_INST_0_i_6 
       (.I0(ram_reg_960_1023_0_6_n_4),
        .I1(ram_reg_896_959_0_6_n_4),
        .I2(dpra[7]),
        .I3(ram_reg_832_895_0_6_n_4),
        .I4(dpra[6]),
        .I5(ram_reg_768_831_0_6_n_4),
        .O(\dpo[4]_INST_0_i_6_n_0 ));
  MUXF8 \dpo[5]_INST_0 
       (.I0(\dpo[5]_INST_0_i_1_n_0 ),
        .I1(\dpo[5]_INST_0_i_2_n_0 ),
        .O(dpo[5]),
        .S(dpra[9]));
  MUXF7 \dpo[5]_INST_0_i_1 
       (.I0(\dpo[5]_INST_0_i_3_n_0 ),
        .I1(\dpo[5]_INST_0_i_4_n_0 ),
        .O(\dpo[5]_INST_0_i_1_n_0 ),
        .S(dpra[8]));
  MUXF7 \dpo[5]_INST_0_i_2 
       (.I0(\dpo[5]_INST_0_i_5_n_0 ),
        .I1(\dpo[5]_INST_0_i_6_n_0 ),
        .O(\dpo[5]_INST_0_i_2_n_0 ),
        .S(dpra[8]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[5]_INST_0_i_3 
       (.I0(ram_reg_192_255_0_6_n_5),
        .I1(ram_reg_128_191_0_6_n_5),
        .I2(dpra[7]),
        .I3(ram_reg_64_127_0_6_n_5),
        .I4(dpra[6]),
        .I5(ram_reg_0_63_0_6_n_5),
        .O(\dpo[5]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[5]_INST_0_i_4 
       (.I0(ram_reg_448_511_0_6_n_5),
        .I1(ram_reg_384_447_0_6_n_5),
        .I2(dpra[7]),
        .I3(ram_reg_320_383_0_6_n_5),
        .I4(dpra[6]),
        .I5(ram_reg_256_319_0_6_n_5),
        .O(\dpo[5]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[5]_INST_0_i_5 
       (.I0(ram_reg_704_767_0_6_n_5),
        .I1(ram_reg_640_703_0_6_n_5),
        .I2(dpra[7]),
        .I3(ram_reg_576_639_0_6_n_5),
        .I4(dpra[6]),
        .I5(ram_reg_512_575_0_6_n_5),
        .O(\dpo[5]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[5]_INST_0_i_6 
       (.I0(ram_reg_960_1023_0_6_n_5),
        .I1(ram_reg_896_959_0_6_n_5),
        .I2(dpra[7]),
        .I3(ram_reg_832_895_0_6_n_5),
        .I4(dpra[6]),
        .I5(ram_reg_768_831_0_6_n_5),
        .O(\dpo[5]_INST_0_i_6_n_0 ));
  MUXF8 \dpo[6]_INST_0 
       (.I0(\dpo[6]_INST_0_i_1_n_0 ),
        .I1(\dpo[6]_INST_0_i_2_n_0 ),
        .O(dpo[6]),
        .S(dpra[9]));
  MUXF7 \dpo[6]_INST_0_i_1 
       (.I0(\dpo[6]_INST_0_i_3_n_0 ),
        .I1(\dpo[6]_INST_0_i_4_n_0 ),
        .O(\dpo[6]_INST_0_i_1_n_0 ),
        .S(dpra[8]));
  MUXF7 \dpo[6]_INST_0_i_2 
       (.I0(\dpo[6]_INST_0_i_5_n_0 ),
        .I1(\dpo[6]_INST_0_i_6_n_0 ),
        .O(\dpo[6]_INST_0_i_2_n_0 ),
        .S(dpra[8]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[6]_INST_0_i_3 
       (.I0(ram_reg_192_255_0_6_n_6),
        .I1(ram_reg_128_191_0_6_n_6),
        .I2(dpra[7]),
        .I3(ram_reg_64_127_0_6_n_6),
        .I4(dpra[6]),
        .I5(ram_reg_0_63_0_6_n_6),
        .O(\dpo[6]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[6]_INST_0_i_4 
       (.I0(ram_reg_448_511_0_6_n_6),
        .I1(ram_reg_384_447_0_6_n_6),
        .I2(dpra[7]),
        .I3(ram_reg_320_383_0_6_n_6),
        .I4(dpra[6]),
        .I5(ram_reg_256_319_0_6_n_6),
        .O(\dpo[6]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[6]_INST_0_i_5 
       (.I0(ram_reg_704_767_0_6_n_6),
        .I1(ram_reg_640_703_0_6_n_6),
        .I2(dpra[7]),
        .I3(ram_reg_576_639_0_6_n_6),
        .I4(dpra[6]),
        .I5(ram_reg_512_575_0_6_n_6),
        .O(\dpo[6]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \dpo[6]_INST_0_i_6 
       (.I0(ram_reg_960_1023_0_6_n_6),
        .I1(ram_reg_896_959_0_6_n_6),
        .I2(dpra[7]),
        .I3(ram_reg_832_895_0_6_n_6),
        .I4(dpra[6]),
        .I5(ram_reg_768_831_0_6_n_6),
        .O(\dpo[6]_INST_0_i_6_n_0 ));
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \qsdpo_int_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(dpo[0]),
        .Q(qsdpo_int[0]),
        .R(1'b0));
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \qsdpo_int_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(dpo[1]),
        .Q(qsdpo_int[1]),
        .R(1'b0));
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \qsdpo_int_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(dpo[2]),
        .Q(qsdpo_int[2]),
        .R(1'b0));
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \qsdpo_int_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(dpo[3]),
        .Q(qsdpo_int[3]),
        .R(1'b0));
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \qsdpo_int_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(dpo[4]),
        .Q(qsdpo_int[4]),
        .R(1'b0));
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \qsdpo_int_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(dpo[5]),
        .Q(qsdpo_int[5]),
        .R(1'b0));
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \qsdpo_int_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(dpo[6]),
        .Q(qsdpo_int[6]),
        .R(1'b0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "7168" *) 
  (* RTL_RAM_NAME = "synth_options.dist_mem_inst/gen_sdp_ram.sdpram_inst/ram" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "6" *) 
  RAM64M8 #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .INIT_E(64'h0000000000000000),
    .INIT_F(64'h0000000000000000),
    .INIT_G(64'h0000000000000000),
    .INIT_H(64'h0000000000000000)) 
    ram_reg_0_63_0_6
       (.ADDRA(dpra[5:0]),
        .ADDRB(dpra[5:0]),
        .ADDRC(dpra[5:0]),
        .ADDRD(dpra[5:0]),
        .ADDRE(dpra[5:0]),
        .ADDRF(dpra[5:0]),
        .ADDRG(dpra[5:0]),
        .ADDRH(a[5:0]),
        .DIA(d[0]),
        .DIB(d[1]),
        .DIC(d[2]),
        .DID(d[3]),
        .DIE(d[4]),
        .DIF(d[5]),
        .DIG(d[6]),
        .DIH(1'b0),
        .DOA(ram_reg_0_63_0_6_n_0),
        .DOB(ram_reg_0_63_0_6_n_1),
        .DOC(ram_reg_0_63_0_6_n_2),
        .DOD(ram_reg_0_63_0_6_n_3),
        .DOE(ram_reg_0_63_0_6_n_4),
        .DOF(ram_reg_0_63_0_6_n_5),
        .DOG(ram_reg_0_63_0_6_n_6),
        .DOH(NLW_ram_reg_0_63_0_6_DOH_UNCONNECTED),
        .WCLK(clk),
        .WE(ram_reg_0_63_0_6_i_1_n_0));
  LUT5 #(
    .INIT(32'h00000002)) 
    ram_reg_0_63_0_6_i_1
       (.I0(we),
        .I1(a[7]),
        .I2(a[6]),
        .I3(a[9]),
        .I4(a[8]),
        .O(ram_reg_0_63_0_6_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "7168" *) 
  (* RTL_RAM_NAME = "synth_options.dist_mem_inst/gen_sdp_ram.sdpram_inst/ram" *) 
  (* ram_addr_begin = "128" *) 
  (* ram_addr_end = "191" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "6" *) 
  RAM64M8 #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .INIT_E(64'h0000000000000000),
    .INIT_F(64'h0000000000000000),
    .INIT_G(64'h0000000000000000),
    .INIT_H(64'h0000000000000000)) 
    ram_reg_128_191_0_6
       (.ADDRA(dpra[5:0]),
        .ADDRB(dpra[5:0]),
        .ADDRC(dpra[5:0]),
        .ADDRD(dpra[5:0]),
        .ADDRE(dpra[5:0]),
        .ADDRF(dpra[5:0]),
        .ADDRG(dpra[5:0]),
        .ADDRH(a[5:0]),
        .DIA(d[0]),
        .DIB(d[1]),
        .DIC(d[2]),
        .DID(d[3]),
        .DIE(d[4]),
        .DIF(d[5]),
        .DIG(d[6]),
        .DIH(1'b0),
        .DOA(ram_reg_128_191_0_6_n_0),
        .DOB(ram_reg_128_191_0_6_n_1),
        .DOC(ram_reg_128_191_0_6_n_2),
        .DOD(ram_reg_128_191_0_6_n_3),
        .DOE(ram_reg_128_191_0_6_n_4),
        .DOF(ram_reg_128_191_0_6_n_5),
        .DOG(ram_reg_128_191_0_6_n_6),
        .DOH(NLW_ram_reg_128_191_0_6_DOH_UNCONNECTED),
        .WCLK(clk),
        .WE(ram_reg_128_191_0_6_i_1_n_0));
  LUT5 #(
    .INIT(32'h00020000)) 
    ram_reg_128_191_0_6_i_1
       (.I0(we),
        .I1(a[8]),
        .I2(a[6]),
        .I3(a[9]),
        .I4(a[7]),
        .O(ram_reg_128_191_0_6_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "7168" *) 
  (* RTL_RAM_NAME = "synth_options.dist_mem_inst/gen_sdp_ram.sdpram_inst/ram" *) 
  (* ram_addr_begin = "192" *) 
  (* ram_addr_end = "255" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "6" *) 
  RAM64M8 #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .INIT_E(64'h0000000000000000),
    .INIT_F(64'h0000000000000000),
    .INIT_G(64'h0000000000000000),
    .INIT_H(64'h0000000000000000)) 
    ram_reg_192_255_0_6
       (.ADDRA(dpra[5:0]),
        .ADDRB(dpra[5:0]),
        .ADDRC(dpra[5:0]),
        .ADDRD(dpra[5:0]),
        .ADDRE(dpra[5:0]),
        .ADDRF(dpra[5:0]),
        .ADDRG(dpra[5:0]),
        .ADDRH(a[5:0]),
        .DIA(d[0]),
        .DIB(d[1]),
        .DIC(d[2]),
        .DID(d[3]),
        .DIE(d[4]),
        .DIF(d[5]),
        .DIG(d[6]),
        .DIH(1'b0),
        .DOA(ram_reg_192_255_0_6_n_0),
        .DOB(ram_reg_192_255_0_6_n_1),
        .DOC(ram_reg_192_255_0_6_n_2),
        .DOD(ram_reg_192_255_0_6_n_3),
        .DOE(ram_reg_192_255_0_6_n_4),
        .DOF(ram_reg_192_255_0_6_n_5),
        .DOG(ram_reg_192_255_0_6_n_6),
        .DOH(NLW_ram_reg_192_255_0_6_DOH_UNCONNECTED),
        .WCLK(clk),
        .WE(ram_reg_192_255_0_6_i_1_n_0));
  LUT5 #(
    .INIT(32'h00400000)) 
    ram_reg_192_255_0_6_i_1
       (.I0(a[9]),
        .I1(a[7]),
        .I2(a[6]),
        .I3(a[8]),
        .I4(we),
        .O(ram_reg_192_255_0_6_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "7168" *) 
  (* RTL_RAM_NAME = "synth_options.dist_mem_inst/gen_sdp_ram.sdpram_inst/ram" *) 
  (* ram_addr_begin = "256" *) 
  (* ram_addr_end = "319" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "6" *) 
  RAM64M8 #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .INIT_E(64'h0000000000000000),
    .INIT_F(64'h0000000000000000),
    .INIT_G(64'h0000000000000000),
    .INIT_H(64'h0000000000000000)) 
    ram_reg_256_319_0_6
       (.ADDRA(dpra[5:0]),
        .ADDRB(dpra[5:0]),
        .ADDRC(dpra[5:0]),
        .ADDRD(dpra[5:0]),
        .ADDRE(dpra[5:0]),
        .ADDRF(dpra[5:0]),
        .ADDRG(dpra[5:0]),
        .ADDRH(a[5:0]),
        .DIA(d[0]),
        .DIB(d[1]),
        .DIC(d[2]),
        .DID(d[3]),
        .DIE(d[4]),
        .DIF(d[5]),
        .DIG(d[6]),
        .DIH(1'b0),
        .DOA(ram_reg_256_319_0_6_n_0),
        .DOB(ram_reg_256_319_0_6_n_1),
        .DOC(ram_reg_256_319_0_6_n_2),
        .DOD(ram_reg_256_319_0_6_n_3),
        .DOE(ram_reg_256_319_0_6_n_4),
        .DOF(ram_reg_256_319_0_6_n_5),
        .DOG(ram_reg_256_319_0_6_n_6),
        .DOH(NLW_ram_reg_256_319_0_6_DOH_UNCONNECTED),
        .WCLK(clk),
        .WE(ram_reg_256_319_0_6_i_1_n_0));
  LUT5 #(
    .INIT(32'h00020000)) 
    ram_reg_256_319_0_6_i_1
       (.I0(we),
        .I1(a[7]),
        .I2(a[6]),
        .I3(a[9]),
        .I4(a[8]),
        .O(ram_reg_256_319_0_6_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "7168" *) 
  (* RTL_RAM_NAME = "synth_options.dist_mem_inst/gen_sdp_ram.sdpram_inst/ram" *) 
  (* ram_addr_begin = "320" *) 
  (* ram_addr_end = "383" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "6" *) 
  RAM64M8 #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .INIT_E(64'h0000000000000000),
    .INIT_F(64'h0000000000000000),
    .INIT_G(64'h0000000000000000),
    .INIT_H(64'h0000000000000000)) 
    ram_reg_320_383_0_6
       (.ADDRA(dpra[5:0]),
        .ADDRB(dpra[5:0]),
        .ADDRC(dpra[5:0]),
        .ADDRD(dpra[5:0]),
        .ADDRE(dpra[5:0]),
        .ADDRF(dpra[5:0]),
        .ADDRG(dpra[5:0]),
        .ADDRH(a[5:0]),
        .DIA(d[0]),
        .DIB(d[1]),
        .DIC(d[2]),
        .DID(d[3]),
        .DIE(d[4]),
        .DIF(d[5]),
        .DIG(d[6]),
        .DIH(1'b0),
        .DOA(ram_reg_320_383_0_6_n_0),
        .DOB(ram_reg_320_383_0_6_n_1),
        .DOC(ram_reg_320_383_0_6_n_2),
        .DOD(ram_reg_320_383_0_6_n_3),
        .DOE(ram_reg_320_383_0_6_n_4),
        .DOF(ram_reg_320_383_0_6_n_5),
        .DOG(ram_reg_320_383_0_6_n_6),
        .DOH(NLW_ram_reg_320_383_0_6_DOH_UNCONNECTED),
        .WCLK(clk),
        .WE(ram_reg_320_383_0_6_i_1_n_0));
  LUT5 #(
    .INIT(32'h00400000)) 
    ram_reg_320_383_0_6_i_1
       (.I0(a[9]),
        .I1(a[8]),
        .I2(a[6]),
        .I3(a[7]),
        .I4(we),
        .O(ram_reg_320_383_0_6_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "7168" *) 
  (* RTL_RAM_NAME = "synth_options.dist_mem_inst/gen_sdp_ram.sdpram_inst/ram" *) 
  (* ram_addr_begin = "384" *) 
  (* ram_addr_end = "447" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "6" *) 
  RAM64M8 #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .INIT_E(64'h0000000000000000),
    .INIT_F(64'h0000000000000000),
    .INIT_G(64'h0000000000000000),
    .INIT_H(64'h0000000000000000)) 
    ram_reg_384_447_0_6
       (.ADDRA(dpra[5:0]),
        .ADDRB(dpra[5:0]),
        .ADDRC(dpra[5:0]),
        .ADDRD(dpra[5:0]),
        .ADDRE(dpra[5:0]),
        .ADDRF(dpra[5:0]),
        .ADDRG(dpra[5:0]),
        .ADDRH(a[5:0]),
        .DIA(d[0]),
        .DIB(d[1]),
        .DIC(d[2]),
        .DID(d[3]),
        .DIE(d[4]),
        .DIF(d[5]),
        .DIG(d[6]),
        .DIH(1'b0),
        .DOA(ram_reg_384_447_0_6_n_0),
        .DOB(ram_reg_384_447_0_6_n_1),
        .DOC(ram_reg_384_447_0_6_n_2),
        .DOD(ram_reg_384_447_0_6_n_3),
        .DOE(ram_reg_384_447_0_6_n_4),
        .DOF(ram_reg_384_447_0_6_n_5),
        .DOG(ram_reg_384_447_0_6_n_6),
        .DOH(NLW_ram_reg_384_447_0_6_DOH_UNCONNECTED),
        .WCLK(clk),
        .WE(ram_reg_384_447_0_6_i_1_n_0));
  LUT5 #(
    .INIT(32'h00400000)) 
    ram_reg_384_447_0_6_i_1
       (.I0(a[9]),
        .I1(a[8]),
        .I2(a[7]),
        .I3(a[6]),
        .I4(we),
        .O(ram_reg_384_447_0_6_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "7168" *) 
  (* RTL_RAM_NAME = "synth_options.dist_mem_inst/gen_sdp_ram.sdpram_inst/ram" *) 
  (* ram_addr_begin = "448" *) 
  (* ram_addr_end = "511" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "6" *) 
  RAM64M8 #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .INIT_E(64'h0000000000000000),
    .INIT_F(64'h0000000000000000),
    .INIT_G(64'h0000000000000000),
    .INIT_H(64'h0000000000000000)) 
    ram_reg_448_511_0_6
       (.ADDRA(dpra[5:0]),
        .ADDRB(dpra[5:0]),
        .ADDRC(dpra[5:0]),
        .ADDRD(dpra[5:0]),
        .ADDRE(dpra[5:0]),
        .ADDRF(dpra[5:0]),
        .ADDRG(dpra[5:0]),
        .ADDRH(a[5:0]),
        .DIA(d[0]),
        .DIB(d[1]),
        .DIC(d[2]),
        .DID(d[3]),
        .DIE(d[4]),
        .DIF(d[5]),
        .DIG(d[6]),
        .DIH(1'b0),
        .DOA(ram_reg_448_511_0_6_n_0),
        .DOB(ram_reg_448_511_0_6_n_1),
        .DOC(ram_reg_448_511_0_6_n_2),
        .DOD(ram_reg_448_511_0_6_n_3),
        .DOE(ram_reg_448_511_0_6_n_4),
        .DOF(ram_reg_448_511_0_6_n_5),
        .DOG(ram_reg_448_511_0_6_n_6),
        .DOH(NLW_ram_reg_448_511_0_6_DOH_UNCONNECTED),
        .WCLK(clk),
        .WE(ram_reg_448_511_0_6_i_1_n_0));
  LUT5 #(
    .INIT(32'h40000000)) 
    ram_reg_448_511_0_6_i_1
       (.I0(a[9]),
        .I1(a[7]),
        .I2(a[6]),
        .I3(we),
        .I4(a[8]),
        .O(ram_reg_448_511_0_6_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "7168" *) 
  (* RTL_RAM_NAME = "synth_options.dist_mem_inst/gen_sdp_ram.sdpram_inst/ram" *) 
  (* ram_addr_begin = "512" *) 
  (* ram_addr_end = "575" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "6" *) 
  RAM64M8 #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .INIT_E(64'h0000000000000000),
    .INIT_F(64'h0000000000000000),
    .INIT_G(64'h0000000000000000),
    .INIT_H(64'h0000000000000000)) 
    ram_reg_512_575_0_6
       (.ADDRA(dpra[5:0]),
        .ADDRB(dpra[5:0]),
        .ADDRC(dpra[5:0]),
        .ADDRD(dpra[5:0]),
        .ADDRE(dpra[5:0]),
        .ADDRF(dpra[5:0]),
        .ADDRG(dpra[5:0]),
        .ADDRH(a[5:0]),
        .DIA(d[0]),
        .DIB(d[1]),
        .DIC(d[2]),
        .DID(d[3]),
        .DIE(d[4]),
        .DIF(d[5]),
        .DIG(d[6]),
        .DIH(1'b0),
        .DOA(ram_reg_512_575_0_6_n_0),
        .DOB(ram_reg_512_575_0_6_n_1),
        .DOC(ram_reg_512_575_0_6_n_2),
        .DOD(ram_reg_512_575_0_6_n_3),
        .DOE(ram_reg_512_575_0_6_n_4),
        .DOF(ram_reg_512_575_0_6_n_5),
        .DOG(ram_reg_512_575_0_6_n_6),
        .DOH(NLW_ram_reg_512_575_0_6_DOH_UNCONNECTED),
        .WCLK(clk),
        .WE(ram_reg_512_575_0_6_i_1_n_0));
  LUT5 #(
    .INIT(32'h00020000)) 
    ram_reg_512_575_0_6_i_1
       (.I0(we),
        .I1(a[7]),
        .I2(a[6]),
        .I3(a[8]),
        .I4(a[9]),
        .O(ram_reg_512_575_0_6_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "7168" *) 
  (* RTL_RAM_NAME = "synth_options.dist_mem_inst/gen_sdp_ram.sdpram_inst/ram" *) 
  (* ram_addr_begin = "576" *) 
  (* ram_addr_end = "639" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "6" *) 
  RAM64M8 #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .INIT_E(64'h0000000000000000),
    .INIT_F(64'h0000000000000000),
    .INIT_G(64'h0000000000000000),
    .INIT_H(64'h0000000000000000)) 
    ram_reg_576_639_0_6
       (.ADDRA(dpra[5:0]),
        .ADDRB(dpra[5:0]),
        .ADDRC(dpra[5:0]),
        .ADDRD(dpra[5:0]),
        .ADDRE(dpra[5:0]),
        .ADDRF(dpra[5:0]),
        .ADDRG(dpra[5:0]),
        .ADDRH(a[5:0]),
        .DIA(d[0]),
        .DIB(d[1]),
        .DIC(d[2]),
        .DID(d[3]),
        .DIE(d[4]),
        .DIF(d[5]),
        .DIG(d[6]),
        .DIH(1'b0),
        .DOA(ram_reg_576_639_0_6_n_0),
        .DOB(ram_reg_576_639_0_6_n_1),
        .DOC(ram_reg_576_639_0_6_n_2),
        .DOD(ram_reg_576_639_0_6_n_3),
        .DOE(ram_reg_576_639_0_6_n_4),
        .DOF(ram_reg_576_639_0_6_n_5),
        .DOG(ram_reg_576_639_0_6_n_6),
        .DOH(NLW_ram_reg_576_639_0_6_DOH_UNCONNECTED),
        .WCLK(clk),
        .WE(ram_reg_576_639_0_6_i_1_n_0));
  LUT5 #(
    .INIT(32'h00400000)) 
    ram_reg_576_639_0_6_i_1
       (.I0(a[8]),
        .I1(a[9]),
        .I2(a[6]),
        .I3(a[7]),
        .I4(we),
        .O(ram_reg_576_639_0_6_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "7168" *) 
  (* RTL_RAM_NAME = "synth_options.dist_mem_inst/gen_sdp_ram.sdpram_inst/ram" *) 
  (* ram_addr_begin = "640" *) 
  (* ram_addr_end = "703" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "6" *) 
  RAM64M8 #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .INIT_E(64'h0000000000000000),
    .INIT_F(64'h0000000000000000),
    .INIT_G(64'h0000000000000000),
    .INIT_H(64'h0000000000000000)) 
    ram_reg_640_703_0_6
       (.ADDRA(dpra[5:0]),
        .ADDRB(dpra[5:0]),
        .ADDRC(dpra[5:0]),
        .ADDRD(dpra[5:0]),
        .ADDRE(dpra[5:0]),
        .ADDRF(dpra[5:0]),
        .ADDRG(dpra[5:0]),
        .ADDRH(a[5:0]),
        .DIA(d[0]),
        .DIB(d[1]),
        .DIC(d[2]),
        .DID(d[3]),
        .DIE(d[4]),
        .DIF(d[5]),
        .DIG(d[6]),
        .DIH(1'b0),
        .DOA(ram_reg_640_703_0_6_n_0),
        .DOB(ram_reg_640_703_0_6_n_1),
        .DOC(ram_reg_640_703_0_6_n_2),
        .DOD(ram_reg_640_703_0_6_n_3),
        .DOE(ram_reg_640_703_0_6_n_4),
        .DOF(ram_reg_640_703_0_6_n_5),
        .DOG(ram_reg_640_703_0_6_n_6),
        .DOH(NLW_ram_reg_640_703_0_6_DOH_UNCONNECTED),
        .WCLK(clk),
        .WE(ram_reg_640_703_0_6_i_1_n_0));
  LUT5 #(
    .INIT(32'h00400000)) 
    ram_reg_640_703_0_6_i_1
       (.I0(a[8]),
        .I1(a[9]),
        .I2(a[7]),
        .I3(a[6]),
        .I4(we),
        .O(ram_reg_640_703_0_6_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "7168" *) 
  (* RTL_RAM_NAME = "synth_options.dist_mem_inst/gen_sdp_ram.sdpram_inst/ram" *) 
  (* ram_addr_begin = "64" *) 
  (* ram_addr_end = "127" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "6" *) 
  RAM64M8 #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .INIT_E(64'h0000000000000000),
    .INIT_F(64'h0000000000000000),
    .INIT_G(64'h0000000000000000),
    .INIT_H(64'h0000000000000000)) 
    ram_reg_64_127_0_6
       (.ADDRA(dpra[5:0]),
        .ADDRB(dpra[5:0]),
        .ADDRC(dpra[5:0]),
        .ADDRD(dpra[5:0]),
        .ADDRE(dpra[5:0]),
        .ADDRF(dpra[5:0]),
        .ADDRG(dpra[5:0]),
        .ADDRH(a[5:0]),
        .DIA(d[0]),
        .DIB(d[1]),
        .DIC(d[2]),
        .DID(d[3]),
        .DIE(d[4]),
        .DIF(d[5]),
        .DIG(d[6]),
        .DIH(1'b0),
        .DOA(ram_reg_64_127_0_6_n_0),
        .DOB(ram_reg_64_127_0_6_n_1),
        .DOC(ram_reg_64_127_0_6_n_2),
        .DOD(ram_reg_64_127_0_6_n_3),
        .DOE(ram_reg_64_127_0_6_n_4),
        .DOF(ram_reg_64_127_0_6_n_5),
        .DOG(ram_reg_64_127_0_6_n_6),
        .DOH(NLW_ram_reg_64_127_0_6_DOH_UNCONNECTED),
        .WCLK(clk),
        .WE(ram_reg_64_127_0_6_i_1_n_0));
  LUT5 #(
    .INIT(32'h00020000)) 
    ram_reg_64_127_0_6_i_1
       (.I0(we),
        .I1(a[8]),
        .I2(a[7]),
        .I3(a[9]),
        .I4(a[6]),
        .O(ram_reg_64_127_0_6_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "7168" *) 
  (* RTL_RAM_NAME = "synth_options.dist_mem_inst/gen_sdp_ram.sdpram_inst/ram" *) 
  (* ram_addr_begin = "704" *) 
  (* ram_addr_end = "767" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "6" *) 
  RAM64M8 #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .INIT_E(64'h0000000000000000),
    .INIT_F(64'h0000000000000000),
    .INIT_G(64'h0000000000000000),
    .INIT_H(64'h0000000000000000)) 
    ram_reg_704_767_0_6
       (.ADDRA(dpra[5:0]),
        .ADDRB(dpra[5:0]),
        .ADDRC(dpra[5:0]),
        .ADDRD(dpra[5:0]),
        .ADDRE(dpra[5:0]),
        .ADDRF(dpra[5:0]),
        .ADDRG(dpra[5:0]),
        .ADDRH(a[5:0]),
        .DIA(d[0]),
        .DIB(d[1]),
        .DIC(d[2]),
        .DID(d[3]),
        .DIE(d[4]),
        .DIF(d[5]),
        .DIG(d[6]),
        .DIH(1'b0),
        .DOA(ram_reg_704_767_0_6_n_0),
        .DOB(ram_reg_704_767_0_6_n_1),
        .DOC(ram_reg_704_767_0_6_n_2),
        .DOD(ram_reg_704_767_0_6_n_3),
        .DOE(ram_reg_704_767_0_6_n_4),
        .DOF(ram_reg_704_767_0_6_n_5),
        .DOG(ram_reg_704_767_0_6_n_6),
        .DOH(NLW_ram_reg_704_767_0_6_DOH_UNCONNECTED),
        .WCLK(clk),
        .WE(ram_reg_704_767_0_6_i_1_n_0));
  LUT5 #(
    .INIT(32'h40000000)) 
    ram_reg_704_767_0_6_i_1
       (.I0(a[8]),
        .I1(a[7]),
        .I2(a[6]),
        .I3(we),
        .I4(a[9]),
        .O(ram_reg_704_767_0_6_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "7168" *) 
  (* RTL_RAM_NAME = "synth_options.dist_mem_inst/gen_sdp_ram.sdpram_inst/ram" *) 
  (* ram_addr_begin = "768" *) 
  (* ram_addr_end = "831" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "6" *) 
  RAM64M8 #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .INIT_E(64'h0000000000000000),
    .INIT_F(64'h0000000000000000),
    .INIT_G(64'h0000000000000000),
    .INIT_H(64'h0000000000000000)) 
    ram_reg_768_831_0_6
       (.ADDRA(dpra[5:0]),
        .ADDRB(dpra[5:0]),
        .ADDRC(dpra[5:0]),
        .ADDRD(dpra[5:0]),
        .ADDRE(dpra[5:0]),
        .ADDRF(dpra[5:0]),
        .ADDRG(dpra[5:0]),
        .ADDRH(a[5:0]),
        .DIA(d[0]),
        .DIB(d[1]),
        .DIC(d[2]),
        .DID(d[3]),
        .DIE(d[4]),
        .DIF(d[5]),
        .DIG(d[6]),
        .DIH(1'b0),
        .DOA(ram_reg_768_831_0_6_n_0),
        .DOB(ram_reg_768_831_0_6_n_1),
        .DOC(ram_reg_768_831_0_6_n_2),
        .DOD(ram_reg_768_831_0_6_n_3),
        .DOE(ram_reg_768_831_0_6_n_4),
        .DOF(ram_reg_768_831_0_6_n_5),
        .DOG(ram_reg_768_831_0_6_n_6),
        .DOH(NLW_ram_reg_768_831_0_6_DOH_UNCONNECTED),
        .WCLK(clk),
        .WE(ram_reg_768_831_0_6_i_1_n_0));
  LUT5 #(
    .INIT(32'h00400000)) 
    ram_reg_768_831_0_6_i_1
       (.I0(a[7]),
        .I1(a[9]),
        .I2(a[8]),
        .I3(a[6]),
        .I4(we),
        .O(ram_reg_768_831_0_6_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "7168" *) 
  (* RTL_RAM_NAME = "synth_options.dist_mem_inst/gen_sdp_ram.sdpram_inst/ram" *) 
  (* ram_addr_begin = "832" *) 
  (* ram_addr_end = "895" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "6" *) 
  RAM64M8 #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .INIT_E(64'h0000000000000000),
    .INIT_F(64'h0000000000000000),
    .INIT_G(64'h0000000000000000),
    .INIT_H(64'h0000000000000000)) 
    ram_reg_832_895_0_6
       (.ADDRA(dpra[5:0]),
        .ADDRB(dpra[5:0]),
        .ADDRC(dpra[5:0]),
        .ADDRD(dpra[5:0]),
        .ADDRE(dpra[5:0]),
        .ADDRF(dpra[5:0]),
        .ADDRG(dpra[5:0]),
        .ADDRH(a[5:0]),
        .DIA(d[0]),
        .DIB(d[1]),
        .DIC(d[2]),
        .DID(d[3]),
        .DIE(d[4]),
        .DIF(d[5]),
        .DIG(d[6]),
        .DIH(1'b0),
        .DOA(ram_reg_832_895_0_6_n_0),
        .DOB(ram_reg_832_895_0_6_n_1),
        .DOC(ram_reg_832_895_0_6_n_2),
        .DOD(ram_reg_832_895_0_6_n_3),
        .DOE(ram_reg_832_895_0_6_n_4),
        .DOF(ram_reg_832_895_0_6_n_5),
        .DOG(ram_reg_832_895_0_6_n_6),
        .DOH(NLW_ram_reg_832_895_0_6_DOH_UNCONNECTED),
        .WCLK(clk),
        .WE(ram_reg_832_895_0_6_i_1_n_0));
  LUT5 #(
    .INIT(32'h40000000)) 
    ram_reg_832_895_0_6_i_1
       (.I0(a[7]),
        .I1(a[8]),
        .I2(a[6]),
        .I3(we),
        .I4(a[9]),
        .O(ram_reg_832_895_0_6_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "7168" *) 
  (* RTL_RAM_NAME = "synth_options.dist_mem_inst/gen_sdp_ram.sdpram_inst/ram" *) 
  (* ram_addr_begin = "896" *) 
  (* ram_addr_end = "959" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "6" *) 
  RAM64M8 #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .INIT_E(64'h0000000000000000),
    .INIT_F(64'h0000000000000000),
    .INIT_G(64'h0000000000000000),
    .INIT_H(64'h0000000000000000)) 
    ram_reg_896_959_0_6
       (.ADDRA(dpra[5:0]),
        .ADDRB(dpra[5:0]),
        .ADDRC(dpra[5:0]),
        .ADDRD(dpra[5:0]),
        .ADDRE(dpra[5:0]),
        .ADDRF(dpra[5:0]),
        .ADDRG(dpra[5:0]),
        .ADDRH(a[5:0]),
        .DIA(d[0]),
        .DIB(d[1]),
        .DIC(d[2]),
        .DID(d[3]),
        .DIE(d[4]),
        .DIF(d[5]),
        .DIG(d[6]),
        .DIH(1'b0),
        .DOA(ram_reg_896_959_0_6_n_0),
        .DOB(ram_reg_896_959_0_6_n_1),
        .DOC(ram_reg_896_959_0_6_n_2),
        .DOD(ram_reg_896_959_0_6_n_3),
        .DOE(ram_reg_896_959_0_6_n_4),
        .DOF(ram_reg_896_959_0_6_n_5),
        .DOG(ram_reg_896_959_0_6_n_6),
        .DOH(NLW_ram_reg_896_959_0_6_DOH_UNCONNECTED),
        .WCLK(clk),
        .WE(ram_reg_896_959_0_6_i_1_n_0));
  LUT5 #(
    .INIT(32'h40000000)) 
    ram_reg_896_959_0_6_i_1
       (.I0(a[6]),
        .I1(a[8]),
        .I2(a[7]),
        .I3(we),
        .I4(a[9]),
        .O(ram_reg_896_959_0_6_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "7168" *) 
  (* RTL_RAM_NAME = "synth_options.dist_mem_inst/gen_sdp_ram.sdpram_inst/ram" *) 
  (* ram_addr_begin = "960" *) 
  (* ram_addr_end = "1023" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "6" *) 
  RAM64M8 #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .INIT_E(64'h0000000000000000),
    .INIT_F(64'h0000000000000000),
    .INIT_G(64'h0000000000000000),
    .INIT_H(64'h0000000000000000)) 
    ram_reg_960_1023_0_6
       (.ADDRA(dpra[5:0]),
        .ADDRB(dpra[5:0]),
        .ADDRC(dpra[5:0]),
        .ADDRD(dpra[5:0]),
        .ADDRE(dpra[5:0]),
        .ADDRF(dpra[5:0]),
        .ADDRG(dpra[5:0]),
        .ADDRH(a[5:0]),
        .DIA(d[0]),
        .DIB(d[1]),
        .DIC(d[2]),
        .DID(d[3]),
        .DIE(d[4]),
        .DIF(d[5]),
        .DIG(d[6]),
        .DIH(1'b0),
        .DOA(ram_reg_960_1023_0_6_n_0),
        .DOB(ram_reg_960_1023_0_6_n_1),
        .DOC(ram_reg_960_1023_0_6_n_2),
        .DOD(ram_reg_960_1023_0_6_n_3),
        .DOE(ram_reg_960_1023_0_6_n_4),
        .DOF(ram_reg_960_1023_0_6_n_5),
        .DOG(ram_reg_960_1023_0_6_n_6),
        .DOH(NLW_ram_reg_960_1023_0_6_DOH_UNCONNECTED),
        .WCLK(clk),
        .WE(ram_reg_960_1023_0_6_i_1_n_0));
  LUT5 #(
    .INIT(32'h80000000)) 
    ram_reg_960_1023_0_6_i_1
       (.I0(we),
        .I1(a[7]),
        .I2(a[6]),
        .I3(a[9]),
        .I4(a[8]),
        .O(ram_reg_960_1023_0_6_i_1_n_0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
