-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Nov  9 13:34:45 2021
-- Host        : HURA-JUNHO running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               c:/Users/hura/Documents/Xilinx_xfile/Noeun/noeun_a_19ver_01/noeun_a_19ver.srcs/sources_1/ip/mgc_table_26x/mgc_table_26x_sim_netlist.vhdl
-- Design      : mgc_table_26x
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xcku5p-ffvb676-1-i
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity mgc_table_26x_sdpram is
  port (
    dpo : out STD_LOGIC_VECTOR ( 25 downto 0 );
    we : in STD_LOGIC;
    a : in STD_LOGIC_VECTOR ( 6 downto 0 );
    clk : in STD_LOGIC;
    d : in STD_LOGIC_VECTOR ( 25 downto 0 );
    dpra : in STD_LOGIC_VECTOR ( 6 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of mgc_table_26x_sdpram : entity is "sdpram";
end mgc_table_26x_sdpram;

architecture STRUCTURE of mgc_table_26x_sdpram is
  signal \^dpo\ : STD_LOGIC_VECTOR ( 25 downto 0 );
  signal qsdpo_int : STD_LOGIC_VECTOR ( 25 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of qsdpo_int : signal is "true";
  signal ram_reg_0_63_0_6_i_1_n_0 : STD_LOGIC;
  signal ram_reg_0_63_0_6_n_0 : STD_LOGIC;
  signal ram_reg_0_63_0_6_n_1 : STD_LOGIC;
  signal ram_reg_0_63_0_6_n_2 : STD_LOGIC;
  signal ram_reg_0_63_0_6_n_3 : STD_LOGIC;
  signal ram_reg_0_63_0_6_n_4 : STD_LOGIC;
  signal ram_reg_0_63_0_6_n_5 : STD_LOGIC;
  signal ram_reg_0_63_0_6_n_6 : STD_LOGIC;
  signal ram_reg_0_63_14_20_n_0 : STD_LOGIC;
  signal ram_reg_0_63_14_20_n_1 : STD_LOGIC;
  signal ram_reg_0_63_14_20_n_2 : STD_LOGIC;
  signal ram_reg_0_63_14_20_n_3 : STD_LOGIC;
  signal ram_reg_0_63_14_20_n_4 : STD_LOGIC;
  signal ram_reg_0_63_14_20_n_5 : STD_LOGIC;
  signal ram_reg_0_63_14_20_n_6 : STD_LOGIC;
  signal ram_reg_0_63_21_25_n_0 : STD_LOGIC;
  signal ram_reg_0_63_21_25_n_1 : STD_LOGIC;
  signal ram_reg_0_63_21_25_n_2 : STD_LOGIC;
  signal ram_reg_0_63_21_25_n_3 : STD_LOGIC;
  signal ram_reg_0_63_21_25_n_4 : STD_LOGIC;
  signal ram_reg_0_63_7_13_n_0 : STD_LOGIC;
  signal ram_reg_0_63_7_13_n_1 : STD_LOGIC;
  signal ram_reg_0_63_7_13_n_2 : STD_LOGIC;
  signal ram_reg_0_63_7_13_n_3 : STD_LOGIC;
  signal ram_reg_0_63_7_13_n_4 : STD_LOGIC;
  signal ram_reg_0_63_7_13_n_5 : STD_LOGIC;
  signal ram_reg_0_63_7_13_n_6 : STD_LOGIC;
  signal ram_reg_64_127_0_6_i_1_n_0 : STD_LOGIC;
  signal ram_reg_64_127_0_6_n_0 : STD_LOGIC;
  signal ram_reg_64_127_0_6_n_1 : STD_LOGIC;
  signal ram_reg_64_127_0_6_n_2 : STD_LOGIC;
  signal ram_reg_64_127_0_6_n_3 : STD_LOGIC;
  signal ram_reg_64_127_0_6_n_4 : STD_LOGIC;
  signal ram_reg_64_127_0_6_n_5 : STD_LOGIC;
  signal ram_reg_64_127_0_6_n_6 : STD_LOGIC;
  signal ram_reg_64_127_14_20_n_0 : STD_LOGIC;
  signal ram_reg_64_127_14_20_n_1 : STD_LOGIC;
  signal ram_reg_64_127_14_20_n_2 : STD_LOGIC;
  signal ram_reg_64_127_14_20_n_3 : STD_LOGIC;
  signal ram_reg_64_127_14_20_n_4 : STD_LOGIC;
  signal ram_reg_64_127_14_20_n_5 : STD_LOGIC;
  signal ram_reg_64_127_14_20_n_6 : STD_LOGIC;
  signal ram_reg_64_127_21_25_n_0 : STD_LOGIC;
  signal ram_reg_64_127_21_25_n_1 : STD_LOGIC;
  signal ram_reg_64_127_21_25_n_2 : STD_LOGIC;
  signal ram_reg_64_127_21_25_n_3 : STD_LOGIC;
  signal ram_reg_64_127_21_25_n_4 : STD_LOGIC;
  signal ram_reg_64_127_7_13_n_0 : STD_LOGIC;
  signal ram_reg_64_127_7_13_n_1 : STD_LOGIC;
  signal ram_reg_64_127_7_13_n_2 : STD_LOGIC;
  signal ram_reg_64_127_7_13_n_3 : STD_LOGIC;
  signal ram_reg_64_127_7_13_n_4 : STD_LOGIC;
  signal ram_reg_64_127_7_13_n_5 : STD_LOGIC;
  signal ram_reg_64_127_7_13_n_6 : STD_LOGIC;
  signal NLW_ram_reg_0_63_0_6_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_63_14_20_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_63_21_25_DOF_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_63_21_25_DOG_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_63_21_25_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_63_7_13_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_64_127_0_6_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_64_127_14_20_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_64_127_21_25_DOF_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_64_127_21_25_DOG_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_64_127_21_25_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_64_127_7_13_DOH_UNCONNECTED : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \dpo[0]_INST_0\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \dpo[10]_INST_0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \dpo[11]_INST_0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \dpo[12]_INST_0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \dpo[13]_INST_0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \dpo[14]_INST_0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \dpo[15]_INST_0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \dpo[16]_INST_0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \dpo[17]_INST_0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \dpo[18]_INST_0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \dpo[19]_INST_0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \dpo[1]_INST_0\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \dpo[20]_INST_0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \dpo[21]_INST_0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \dpo[22]_INST_0\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \dpo[23]_INST_0\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \dpo[24]_INST_0\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \dpo[25]_INST_0\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \dpo[2]_INST_0\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \dpo[3]_INST_0\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \dpo[4]_INST_0\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \dpo[5]_INST_0\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \dpo[6]_INST_0\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \dpo[7]_INST_0\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \dpo[8]_INST_0\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \dpo[9]_INST_0\ : label is "soft_lutpair4";
  attribute KEEP : string;
  attribute KEEP of \qsdpo_int_reg[0]\ : label is "yes";
  attribute equivalent_register_removal : string;
  attribute equivalent_register_removal of \qsdpo_int_reg[0]\ : label is "no";
  attribute KEEP of \qsdpo_int_reg[10]\ : label is "yes";
  attribute equivalent_register_removal of \qsdpo_int_reg[10]\ : label is "no";
  attribute KEEP of \qsdpo_int_reg[11]\ : label is "yes";
  attribute equivalent_register_removal of \qsdpo_int_reg[11]\ : label is "no";
  attribute KEEP of \qsdpo_int_reg[12]\ : label is "yes";
  attribute equivalent_register_removal of \qsdpo_int_reg[12]\ : label is "no";
  attribute KEEP of \qsdpo_int_reg[13]\ : label is "yes";
  attribute equivalent_register_removal of \qsdpo_int_reg[13]\ : label is "no";
  attribute KEEP of \qsdpo_int_reg[14]\ : label is "yes";
  attribute equivalent_register_removal of \qsdpo_int_reg[14]\ : label is "no";
  attribute KEEP of \qsdpo_int_reg[15]\ : label is "yes";
  attribute equivalent_register_removal of \qsdpo_int_reg[15]\ : label is "no";
  attribute KEEP of \qsdpo_int_reg[16]\ : label is "yes";
  attribute equivalent_register_removal of \qsdpo_int_reg[16]\ : label is "no";
  attribute KEEP of \qsdpo_int_reg[17]\ : label is "yes";
  attribute equivalent_register_removal of \qsdpo_int_reg[17]\ : label is "no";
  attribute KEEP of \qsdpo_int_reg[18]\ : label is "yes";
  attribute equivalent_register_removal of \qsdpo_int_reg[18]\ : label is "no";
  attribute KEEP of \qsdpo_int_reg[19]\ : label is "yes";
  attribute equivalent_register_removal of \qsdpo_int_reg[19]\ : label is "no";
  attribute KEEP of \qsdpo_int_reg[1]\ : label is "yes";
  attribute equivalent_register_removal of \qsdpo_int_reg[1]\ : label is "no";
  attribute KEEP of \qsdpo_int_reg[20]\ : label is "yes";
  attribute equivalent_register_removal of \qsdpo_int_reg[20]\ : label is "no";
  attribute KEEP of \qsdpo_int_reg[21]\ : label is "yes";
  attribute equivalent_register_removal of \qsdpo_int_reg[21]\ : label is "no";
  attribute KEEP of \qsdpo_int_reg[22]\ : label is "yes";
  attribute equivalent_register_removal of \qsdpo_int_reg[22]\ : label is "no";
  attribute KEEP of \qsdpo_int_reg[23]\ : label is "yes";
  attribute equivalent_register_removal of \qsdpo_int_reg[23]\ : label is "no";
  attribute KEEP of \qsdpo_int_reg[24]\ : label is "yes";
  attribute equivalent_register_removal of \qsdpo_int_reg[24]\ : label is "no";
  attribute KEEP of \qsdpo_int_reg[25]\ : label is "yes";
  attribute equivalent_register_removal of \qsdpo_int_reg[25]\ : label is "no";
  attribute KEEP of \qsdpo_int_reg[2]\ : label is "yes";
  attribute equivalent_register_removal of \qsdpo_int_reg[2]\ : label is "no";
  attribute KEEP of \qsdpo_int_reg[3]\ : label is "yes";
  attribute equivalent_register_removal of \qsdpo_int_reg[3]\ : label is "no";
  attribute KEEP of \qsdpo_int_reg[4]\ : label is "yes";
  attribute equivalent_register_removal of \qsdpo_int_reg[4]\ : label is "no";
  attribute KEEP of \qsdpo_int_reg[5]\ : label is "yes";
  attribute equivalent_register_removal of \qsdpo_int_reg[5]\ : label is "no";
  attribute KEEP of \qsdpo_int_reg[6]\ : label is "yes";
  attribute equivalent_register_removal of \qsdpo_int_reg[6]\ : label is "no";
  attribute KEEP of \qsdpo_int_reg[7]\ : label is "yes";
  attribute equivalent_register_removal of \qsdpo_int_reg[7]\ : label is "no";
  attribute KEEP of \qsdpo_int_reg[8]\ : label is "yes";
  attribute equivalent_register_removal of \qsdpo_int_reg[8]\ : label is "no";
  attribute KEEP of \qsdpo_int_reg[9]\ : label is "yes";
  attribute equivalent_register_removal of \qsdpo_int_reg[9]\ : label is "no";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0_63_0_6 : label is "";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0_63_0_6 : label is 3328;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0_63_0_6 : label is "synth_options.dist_mem_inst/gen_sdp_ram.sdpram_inst/ram";
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0_63_0_6 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0_63_0_6 : label is 63;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0_63_0_6 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0_63_0_6 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0_63_0_6 : label is 6;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0_63_14_20 : label is "";
  attribute RTL_RAM_BITS of ram_reg_0_63_14_20 : label is 3328;
  attribute RTL_RAM_NAME of ram_reg_0_63_14_20 : label is "synth_options.dist_mem_inst/gen_sdp_ram.sdpram_inst/ram";
  attribute ram_addr_begin of ram_reg_0_63_14_20 : label is 0;
  attribute ram_addr_end of ram_reg_0_63_14_20 : label is 63;
  attribute ram_offset of ram_reg_0_63_14_20 : label is 0;
  attribute ram_slice_begin of ram_reg_0_63_14_20 : label is 14;
  attribute ram_slice_end of ram_reg_0_63_14_20 : label is 20;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0_63_21_25 : label is "";
  attribute RTL_RAM_BITS of ram_reg_0_63_21_25 : label is 3328;
  attribute RTL_RAM_NAME of ram_reg_0_63_21_25 : label is "synth_options.dist_mem_inst/gen_sdp_ram.sdpram_inst/ram";
  attribute ram_addr_begin of ram_reg_0_63_21_25 : label is 0;
  attribute ram_addr_end of ram_reg_0_63_21_25 : label is 63;
  attribute ram_offset of ram_reg_0_63_21_25 : label is 0;
  attribute ram_slice_begin of ram_reg_0_63_21_25 : label is 21;
  attribute ram_slice_end of ram_reg_0_63_21_25 : label is 25;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0_63_7_13 : label is "";
  attribute RTL_RAM_BITS of ram_reg_0_63_7_13 : label is 3328;
  attribute RTL_RAM_NAME of ram_reg_0_63_7_13 : label is "synth_options.dist_mem_inst/gen_sdp_ram.sdpram_inst/ram";
  attribute ram_addr_begin of ram_reg_0_63_7_13 : label is 0;
  attribute ram_addr_end of ram_reg_0_63_7_13 : label is 63;
  attribute ram_offset of ram_reg_0_63_7_13 : label is 0;
  attribute ram_slice_begin of ram_reg_0_63_7_13 : label is 7;
  attribute ram_slice_end of ram_reg_0_63_7_13 : label is 13;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_64_127_0_6 : label is "";
  attribute RTL_RAM_BITS of ram_reg_64_127_0_6 : label is 3328;
  attribute RTL_RAM_NAME of ram_reg_64_127_0_6 : label is "synth_options.dist_mem_inst/gen_sdp_ram.sdpram_inst/ram";
  attribute ram_addr_begin of ram_reg_64_127_0_6 : label is 64;
  attribute ram_addr_end of ram_reg_64_127_0_6 : label is 127;
  attribute ram_offset of ram_reg_64_127_0_6 : label is 0;
  attribute ram_slice_begin of ram_reg_64_127_0_6 : label is 0;
  attribute ram_slice_end of ram_reg_64_127_0_6 : label is 6;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_64_127_14_20 : label is "";
  attribute RTL_RAM_BITS of ram_reg_64_127_14_20 : label is 3328;
  attribute RTL_RAM_NAME of ram_reg_64_127_14_20 : label is "synth_options.dist_mem_inst/gen_sdp_ram.sdpram_inst/ram";
  attribute ram_addr_begin of ram_reg_64_127_14_20 : label is 64;
  attribute ram_addr_end of ram_reg_64_127_14_20 : label is 127;
  attribute ram_offset of ram_reg_64_127_14_20 : label is 0;
  attribute ram_slice_begin of ram_reg_64_127_14_20 : label is 14;
  attribute ram_slice_end of ram_reg_64_127_14_20 : label is 20;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_64_127_21_25 : label is "";
  attribute RTL_RAM_BITS of ram_reg_64_127_21_25 : label is 3328;
  attribute RTL_RAM_NAME of ram_reg_64_127_21_25 : label is "synth_options.dist_mem_inst/gen_sdp_ram.sdpram_inst/ram";
  attribute ram_addr_begin of ram_reg_64_127_21_25 : label is 64;
  attribute ram_addr_end of ram_reg_64_127_21_25 : label is 127;
  attribute ram_offset of ram_reg_64_127_21_25 : label is 0;
  attribute ram_slice_begin of ram_reg_64_127_21_25 : label is 21;
  attribute ram_slice_end of ram_reg_64_127_21_25 : label is 25;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_64_127_7_13 : label is "";
  attribute RTL_RAM_BITS of ram_reg_64_127_7_13 : label is 3328;
  attribute RTL_RAM_NAME of ram_reg_64_127_7_13 : label is "synth_options.dist_mem_inst/gen_sdp_ram.sdpram_inst/ram";
  attribute ram_addr_begin of ram_reg_64_127_7_13 : label is 64;
  attribute ram_addr_end of ram_reg_64_127_7_13 : label is 127;
  attribute ram_offset of ram_reg_64_127_7_13 : label is 0;
  attribute ram_slice_begin of ram_reg_64_127_7_13 : label is 7;
  attribute ram_slice_end of ram_reg_64_127_7_13 : label is 13;
begin
  dpo(25 downto 0) <= \^dpo\(25 downto 0);
\dpo[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ram_reg_64_127_0_6_n_0,
      I1 => dpra(6),
      I2 => ram_reg_0_63_0_6_n_0,
      O => \^dpo\(0)
    );
\dpo[10]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ram_reg_64_127_7_13_n_3,
      I1 => dpra(6),
      I2 => ram_reg_0_63_7_13_n_3,
      O => \^dpo\(10)
    );
\dpo[11]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ram_reg_64_127_7_13_n_4,
      I1 => dpra(6),
      I2 => ram_reg_0_63_7_13_n_4,
      O => \^dpo\(11)
    );
\dpo[12]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ram_reg_64_127_7_13_n_5,
      I1 => dpra(6),
      I2 => ram_reg_0_63_7_13_n_5,
      O => \^dpo\(12)
    );
\dpo[13]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ram_reg_64_127_7_13_n_6,
      I1 => dpra(6),
      I2 => ram_reg_0_63_7_13_n_6,
      O => \^dpo\(13)
    );
\dpo[14]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ram_reg_64_127_14_20_n_0,
      I1 => dpra(6),
      I2 => ram_reg_0_63_14_20_n_0,
      O => \^dpo\(14)
    );
\dpo[15]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ram_reg_64_127_14_20_n_1,
      I1 => dpra(6),
      I2 => ram_reg_0_63_14_20_n_1,
      O => \^dpo\(15)
    );
\dpo[16]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ram_reg_64_127_14_20_n_2,
      I1 => dpra(6),
      I2 => ram_reg_0_63_14_20_n_2,
      O => \^dpo\(16)
    );
\dpo[17]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ram_reg_64_127_14_20_n_3,
      I1 => dpra(6),
      I2 => ram_reg_0_63_14_20_n_3,
      O => \^dpo\(17)
    );
\dpo[18]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ram_reg_64_127_14_20_n_4,
      I1 => dpra(6),
      I2 => ram_reg_0_63_14_20_n_4,
      O => \^dpo\(18)
    );
\dpo[19]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ram_reg_64_127_14_20_n_5,
      I1 => dpra(6),
      I2 => ram_reg_0_63_14_20_n_5,
      O => \^dpo\(19)
    );
\dpo[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ram_reg_64_127_0_6_n_1,
      I1 => dpra(6),
      I2 => ram_reg_0_63_0_6_n_1,
      O => \^dpo\(1)
    );
\dpo[20]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ram_reg_64_127_14_20_n_6,
      I1 => dpra(6),
      I2 => ram_reg_0_63_14_20_n_6,
      O => \^dpo\(20)
    );
\dpo[21]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ram_reg_64_127_21_25_n_0,
      I1 => dpra(6),
      I2 => ram_reg_0_63_21_25_n_0,
      O => \^dpo\(21)
    );
\dpo[22]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ram_reg_64_127_21_25_n_1,
      I1 => dpra(6),
      I2 => ram_reg_0_63_21_25_n_1,
      O => \^dpo\(22)
    );
\dpo[23]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ram_reg_64_127_21_25_n_2,
      I1 => dpra(6),
      I2 => ram_reg_0_63_21_25_n_2,
      O => \^dpo\(23)
    );
\dpo[24]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ram_reg_64_127_21_25_n_3,
      I1 => dpra(6),
      I2 => ram_reg_0_63_21_25_n_3,
      O => \^dpo\(24)
    );
\dpo[25]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ram_reg_64_127_21_25_n_4,
      I1 => dpra(6),
      I2 => ram_reg_0_63_21_25_n_4,
      O => \^dpo\(25)
    );
\dpo[2]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ram_reg_64_127_0_6_n_2,
      I1 => dpra(6),
      I2 => ram_reg_0_63_0_6_n_2,
      O => \^dpo\(2)
    );
\dpo[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ram_reg_64_127_0_6_n_3,
      I1 => dpra(6),
      I2 => ram_reg_0_63_0_6_n_3,
      O => \^dpo\(3)
    );
\dpo[4]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ram_reg_64_127_0_6_n_4,
      I1 => dpra(6),
      I2 => ram_reg_0_63_0_6_n_4,
      O => \^dpo\(4)
    );
\dpo[5]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ram_reg_64_127_0_6_n_5,
      I1 => dpra(6),
      I2 => ram_reg_0_63_0_6_n_5,
      O => \^dpo\(5)
    );
\dpo[6]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ram_reg_64_127_0_6_n_6,
      I1 => dpra(6),
      I2 => ram_reg_0_63_0_6_n_6,
      O => \^dpo\(6)
    );
\dpo[7]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ram_reg_64_127_7_13_n_0,
      I1 => dpra(6),
      I2 => ram_reg_0_63_7_13_n_0,
      O => \^dpo\(7)
    );
\dpo[8]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ram_reg_64_127_7_13_n_1,
      I1 => dpra(6),
      I2 => ram_reg_0_63_7_13_n_1,
      O => \^dpo\(8)
    );
\dpo[9]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ram_reg_64_127_7_13_n_2,
      I1 => dpra(6),
      I2 => ram_reg_0_63_7_13_n_2,
      O => \^dpo\(9)
    );
\qsdpo_int_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \^dpo\(0),
      Q => qsdpo_int(0),
      R => '0'
    );
\qsdpo_int_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \^dpo\(10),
      Q => qsdpo_int(10),
      R => '0'
    );
\qsdpo_int_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \^dpo\(11),
      Q => qsdpo_int(11),
      R => '0'
    );
\qsdpo_int_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \^dpo\(12),
      Q => qsdpo_int(12),
      R => '0'
    );
\qsdpo_int_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \^dpo\(13),
      Q => qsdpo_int(13),
      R => '0'
    );
\qsdpo_int_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \^dpo\(14),
      Q => qsdpo_int(14),
      R => '0'
    );
\qsdpo_int_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \^dpo\(15),
      Q => qsdpo_int(15),
      R => '0'
    );
\qsdpo_int_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \^dpo\(16),
      Q => qsdpo_int(16),
      R => '0'
    );
\qsdpo_int_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \^dpo\(17),
      Q => qsdpo_int(17),
      R => '0'
    );
\qsdpo_int_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \^dpo\(18),
      Q => qsdpo_int(18),
      R => '0'
    );
\qsdpo_int_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \^dpo\(19),
      Q => qsdpo_int(19),
      R => '0'
    );
\qsdpo_int_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \^dpo\(1),
      Q => qsdpo_int(1),
      R => '0'
    );
\qsdpo_int_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \^dpo\(20),
      Q => qsdpo_int(20),
      R => '0'
    );
\qsdpo_int_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \^dpo\(21),
      Q => qsdpo_int(21),
      R => '0'
    );
\qsdpo_int_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \^dpo\(22),
      Q => qsdpo_int(22),
      R => '0'
    );
\qsdpo_int_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \^dpo\(23),
      Q => qsdpo_int(23),
      R => '0'
    );
\qsdpo_int_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \^dpo\(24),
      Q => qsdpo_int(24),
      R => '0'
    );
\qsdpo_int_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \^dpo\(25),
      Q => qsdpo_int(25),
      R => '0'
    );
\qsdpo_int_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \^dpo\(2),
      Q => qsdpo_int(2),
      R => '0'
    );
\qsdpo_int_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \^dpo\(3),
      Q => qsdpo_int(3),
      R => '0'
    );
\qsdpo_int_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \^dpo\(4),
      Q => qsdpo_int(4),
      R => '0'
    );
\qsdpo_int_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \^dpo\(5),
      Q => qsdpo_int(5),
      R => '0'
    );
\qsdpo_int_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \^dpo\(6),
      Q => qsdpo_int(6),
      R => '0'
    );
\qsdpo_int_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \^dpo\(7),
      Q => qsdpo_int(7),
      R => '0'
    );
\qsdpo_int_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \^dpo\(8),
      Q => qsdpo_int(8),
      R => '0'
    );
\qsdpo_int_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \^dpo\(9),
      Q => qsdpo_int(9),
      R => '0'
    );
ram_reg_0_63_0_6: unisim.vcomponents.RAM64M8
    generic map(
      INIT_A => X"155554AAAAA00000",
      INIT_B => X"E66666CCCCC00000",
      INIT_C => X"F878790F0F000000",
      INIT_D => X"FF807E0FF0000000",
      INIT_E => X"F8FF81F000000000",
      INIT_F => X"0000000000000000",
      INIT_G => X"0000000000000000",
      INIT_H => X"0000000000000000"
    )
        port map (
      ADDRA(5 downto 0) => dpra(5 downto 0),
      ADDRB(5 downto 0) => dpra(5 downto 0),
      ADDRC(5 downto 0) => dpra(5 downto 0),
      ADDRD(5 downto 0) => dpra(5 downto 0),
      ADDRE(5 downto 0) => dpra(5 downto 0),
      ADDRF(5 downto 0) => dpra(5 downto 0),
      ADDRG(5 downto 0) => dpra(5 downto 0),
      ADDRH(5 downto 0) => a(5 downto 0),
      DIA => d(0),
      DIB => d(1),
      DIC => d(2),
      DID => d(3),
      DIE => d(4),
      DIF => d(5),
      DIG => d(6),
      DIH => '0',
      DOA => ram_reg_0_63_0_6_n_0,
      DOB => ram_reg_0_63_0_6_n_1,
      DOC => ram_reg_0_63_0_6_n_2,
      DOD => ram_reg_0_63_0_6_n_3,
      DOE => ram_reg_0_63_0_6_n_4,
      DOF => ram_reg_0_63_0_6_n_5,
      DOG => ram_reg_0_63_0_6_n_6,
      DOH => NLW_ram_reg_0_63_0_6_DOH_UNCONNECTED,
      WCLK => clk,
      WE => ram_reg_0_63_0_6_i_1_n_0
    );
ram_reg_0_63_0_6_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => we,
      I1 => a(6),
      O => ram_reg_0_63_0_6_i_1_n_0
    );
ram_reg_0_63_14_20: unisim.vcomponents.RAM64M8
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"000001FFFFFFFFFF",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000",
      INIT_E => X"0000000000000000",
      INIT_F => X"0000000000000000",
      INIT_G => X"0000000000000000",
      INIT_H => X"0000000000000000"
    )
        port map (
      ADDRA(5 downto 0) => dpra(5 downto 0),
      ADDRB(5 downto 0) => dpra(5 downto 0),
      ADDRC(5 downto 0) => dpra(5 downto 0),
      ADDRD(5 downto 0) => dpra(5 downto 0),
      ADDRE(5 downto 0) => dpra(5 downto 0),
      ADDRF(5 downto 0) => dpra(5 downto 0),
      ADDRG(5 downto 0) => dpra(5 downto 0),
      ADDRH(5 downto 0) => a(5 downto 0),
      DIA => d(14),
      DIB => d(15),
      DIC => d(16),
      DID => d(17),
      DIE => d(18),
      DIF => d(19),
      DIG => d(20),
      DIH => '0',
      DOA => ram_reg_0_63_14_20_n_0,
      DOB => ram_reg_0_63_14_20_n_1,
      DOC => ram_reg_0_63_14_20_n_2,
      DOD => ram_reg_0_63_14_20_n_3,
      DOE => ram_reg_0_63_14_20_n_4,
      DOF => ram_reg_0_63_14_20_n_5,
      DOG => ram_reg_0_63_14_20_n_6,
      DOH => NLW_ram_reg_0_63_14_20_DOH_UNCONNECTED,
      WCLK => clk,
      WE => ram_reg_0_63_0_6_i_1_n_0
    );
ram_reg_0_63_21_25: unisim.vcomponents.RAM64M8
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000",
      INIT_E => X"0000000000000000",
      INIT_F => X"0000000000000000",
      INIT_G => X"0000000000000000",
      INIT_H => X"0000000000000000"
    )
        port map (
      ADDRA(5 downto 0) => dpra(5 downto 0),
      ADDRB(5 downto 0) => dpra(5 downto 0),
      ADDRC(5 downto 0) => dpra(5 downto 0),
      ADDRD(5 downto 0) => dpra(5 downto 0),
      ADDRE(5 downto 0) => dpra(5 downto 0),
      ADDRF(5 downto 0) => dpra(5 downto 0),
      ADDRG(5 downto 0) => dpra(5 downto 0),
      ADDRH(5 downto 0) => a(5 downto 0),
      DIA => d(21),
      DIB => d(22),
      DIC => d(23),
      DID => d(24),
      DIE => d(25),
      DIF => '0',
      DIG => '0',
      DIH => '0',
      DOA => ram_reg_0_63_21_25_n_0,
      DOB => ram_reg_0_63_21_25_n_1,
      DOC => ram_reg_0_63_21_25_n_2,
      DOD => ram_reg_0_63_21_25_n_3,
      DOE => ram_reg_0_63_21_25_n_4,
      DOF => NLW_ram_reg_0_63_21_25_DOF_UNCONNECTED,
      DOG => NLW_ram_reg_0_63_21_25_DOG_UNCONNECTED,
      DOH => NLW_ram_reg_0_63_21_25_DOH_UNCONNECTED,
      WCLK => clk,
      WE => ram_reg_0_63_0_6_i_1_n_0
    );
ram_reg_0_63_7_13: unisim.vcomponents.RAM64M8
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000",
      INIT_E => X"0000000000000000",
      INIT_F => X"0000000000000000",
      INIT_G => X"0000000000000000",
      INIT_H => X"0000000000000000"
    )
        port map (
      ADDRA(5 downto 0) => dpra(5 downto 0),
      ADDRB(5 downto 0) => dpra(5 downto 0),
      ADDRC(5 downto 0) => dpra(5 downto 0),
      ADDRD(5 downto 0) => dpra(5 downto 0),
      ADDRE(5 downto 0) => dpra(5 downto 0),
      ADDRF(5 downto 0) => dpra(5 downto 0),
      ADDRG(5 downto 0) => dpra(5 downto 0),
      ADDRH(5 downto 0) => a(5 downto 0),
      DIA => d(7),
      DIB => d(8),
      DIC => d(9),
      DID => d(10),
      DIE => d(11),
      DIF => d(12),
      DIG => d(13),
      DIH => '0',
      DOA => ram_reg_0_63_7_13_n_0,
      DOB => ram_reg_0_63_7_13_n_1,
      DOC => ram_reg_0_63_7_13_n_2,
      DOD => ram_reg_0_63_7_13_n_3,
      DOE => ram_reg_0_63_7_13_n_4,
      DOF => ram_reg_0_63_7_13_n_5,
      DOG => ram_reg_0_63_7_13_n_6,
      DOH => NLW_ram_reg_0_63_7_13_DOH_UNCONNECTED,
      WCLK => clk,
      WE => ram_reg_0_63_0_6_i_1_n_0
    );
ram_reg_64_127_0_6: unisim.vcomponents.RAM64M8
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000001FFFFFFFFF",
      INIT_C => X"0000001FFFFFFFFF",
      INIT_D => X"0000001FFFFFFFFF",
      INIT_E => X"0000001FFFFFFFFF",
      INIT_F => X"0000000000000000",
      INIT_G => X"0000000000000000",
      INIT_H => X"0000000000000000"
    )
        port map (
      ADDRA(5 downto 0) => dpra(5 downto 0),
      ADDRB(5 downto 0) => dpra(5 downto 0),
      ADDRC(5 downto 0) => dpra(5 downto 0),
      ADDRD(5 downto 0) => dpra(5 downto 0),
      ADDRE(5 downto 0) => dpra(5 downto 0),
      ADDRF(5 downto 0) => dpra(5 downto 0),
      ADDRG(5 downto 0) => dpra(5 downto 0),
      ADDRH(5 downto 0) => a(5 downto 0),
      DIA => d(0),
      DIB => d(1),
      DIC => d(2),
      DID => d(3),
      DIE => d(4),
      DIF => d(5),
      DIG => d(6),
      DIH => '0',
      DOA => ram_reg_64_127_0_6_n_0,
      DOB => ram_reg_64_127_0_6_n_1,
      DOC => ram_reg_64_127_0_6_n_2,
      DOD => ram_reg_64_127_0_6_n_3,
      DOE => ram_reg_64_127_0_6_n_4,
      DOF => ram_reg_64_127_0_6_n_5,
      DOG => ram_reg_64_127_0_6_n_6,
      DOH => NLW_ram_reg_64_127_0_6_DOH_UNCONNECTED,
      WCLK => clk,
      WE => ram_reg_64_127_0_6_i_1_n_0
    );
ram_reg_64_127_0_6_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => we,
      I1 => a(6),
      O => ram_reg_64_127_0_6_i_1_n_0
    );
ram_reg_64_127_14_20: unisim.vcomponents.RAM64M8
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000",
      INIT_E => X"0000000000000000",
      INIT_F => X"0000000000000000",
      INIT_G => X"0000000000000000",
      INIT_H => X"0000000000000000"
    )
        port map (
      ADDRA(5 downto 0) => dpra(5 downto 0),
      ADDRB(5 downto 0) => dpra(5 downto 0),
      ADDRC(5 downto 0) => dpra(5 downto 0),
      ADDRD(5 downto 0) => dpra(5 downto 0),
      ADDRE(5 downto 0) => dpra(5 downto 0),
      ADDRF(5 downto 0) => dpra(5 downto 0),
      ADDRG(5 downto 0) => dpra(5 downto 0),
      ADDRH(5 downto 0) => a(5 downto 0),
      DIA => d(14),
      DIB => d(15),
      DIC => d(16),
      DID => d(17),
      DIE => d(18),
      DIF => d(19),
      DIG => d(20),
      DIH => '0',
      DOA => ram_reg_64_127_14_20_n_0,
      DOB => ram_reg_64_127_14_20_n_1,
      DOC => ram_reg_64_127_14_20_n_2,
      DOD => ram_reg_64_127_14_20_n_3,
      DOE => ram_reg_64_127_14_20_n_4,
      DOF => ram_reg_64_127_14_20_n_5,
      DOG => ram_reg_64_127_14_20_n_6,
      DOH => NLW_ram_reg_64_127_14_20_DOH_UNCONNECTED,
      WCLK => clk,
      WE => ram_reg_64_127_0_6_i_1_n_0
    );
ram_reg_64_127_21_25: unisim.vcomponents.RAM64M8
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000",
      INIT_E => X"0000000000000000",
      INIT_F => X"0000000000000000",
      INIT_G => X"0000000000000000",
      INIT_H => X"0000000000000000"
    )
        port map (
      ADDRA(5 downto 0) => dpra(5 downto 0),
      ADDRB(5 downto 0) => dpra(5 downto 0),
      ADDRC(5 downto 0) => dpra(5 downto 0),
      ADDRD(5 downto 0) => dpra(5 downto 0),
      ADDRE(5 downto 0) => dpra(5 downto 0),
      ADDRF(5 downto 0) => dpra(5 downto 0),
      ADDRG(5 downto 0) => dpra(5 downto 0),
      ADDRH(5 downto 0) => a(5 downto 0),
      DIA => d(21),
      DIB => d(22),
      DIC => d(23),
      DID => d(24),
      DIE => d(25),
      DIF => '0',
      DIG => '0',
      DIH => '0',
      DOA => ram_reg_64_127_21_25_n_0,
      DOB => ram_reg_64_127_21_25_n_1,
      DOC => ram_reg_64_127_21_25_n_2,
      DOD => ram_reg_64_127_21_25_n_3,
      DOE => ram_reg_64_127_21_25_n_4,
      DOF => NLW_ram_reg_64_127_21_25_DOF_UNCONNECTED,
      DOG => NLW_ram_reg_64_127_21_25_DOG_UNCONNECTED,
      DOH => NLW_ram_reg_64_127_21_25_DOH_UNCONNECTED,
      WCLK => clk,
      WE => ram_reg_64_127_0_6_i_1_n_0
    );
ram_reg_64_127_7_13: unisim.vcomponents.RAM64M8
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000",
      INIT_E => X"0000000000000000",
      INIT_F => X"0000000000000000",
      INIT_G => X"0000000000000000",
      INIT_H => X"0000000000000000"
    )
        port map (
      ADDRA(5 downto 0) => dpra(5 downto 0),
      ADDRB(5 downto 0) => dpra(5 downto 0),
      ADDRC(5 downto 0) => dpra(5 downto 0),
      ADDRD(5 downto 0) => dpra(5 downto 0),
      ADDRE(5 downto 0) => dpra(5 downto 0),
      ADDRF(5 downto 0) => dpra(5 downto 0),
      ADDRG(5 downto 0) => dpra(5 downto 0),
      ADDRH(5 downto 0) => a(5 downto 0),
      DIA => d(7),
      DIB => d(8),
      DIC => d(9),
      DID => d(10),
      DIE => d(11),
      DIF => d(12),
      DIG => d(13),
      DIH => '0',
      DOA => ram_reg_64_127_7_13_n_0,
      DOB => ram_reg_64_127_7_13_n_1,
      DOC => ram_reg_64_127_7_13_n_2,
      DOD => ram_reg_64_127_7_13_n_3,
      DOE => ram_reg_64_127_7_13_n_4,
      DOF => ram_reg_64_127_7_13_n_5,
      DOG => ram_reg_64_127_7_13_n_6,
      DOH => NLW_ram_reg_64_127_7_13_DOH_UNCONNECTED,
      WCLK => clk,
      WE => ram_reg_64_127_0_6_i_1_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity mgc_table_26x_dist_mem_gen_v8_0_13_synth is
  port (
    dpo : out STD_LOGIC_VECTOR ( 25 downto 0 );
    we : in STD_LOGIC;
    a : in STD_LOGIC_VECTOR ( 6 downto 0 );
    clk : in STD_LOGIC;
    d : in STD_LOGIC_VECTOR ( 25 downto 0 );
    dpra : in STD_LOGIC_VECTOR ( 6 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of mgc_table_26x_dist_mem_gen_v8_0_13_synth : entity is "dist_mem_gen_v8_0_13_synth";
end mgc_table_26x_dist_mem_gen_v8_0_13_synth;

architecture STRUCTURE of mgc_table_26x_dist_mem_gen_v8_0_13_synth is
begin
\gen_sdp_ram.sdpram_inst\: entity work.mgc_table_26x_sdpram
     port map (
      a(6 downto 0) => a(6 downto 0),
      clk => clk,
      d(25 downto 0) => d(25 downto 0),
      dpo(25 downto 0) => dpo(25 downto 0),
      dpra(6 downto 0) => dpra(6 downto 0),
      we => we
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity mgc_table_26x_dist_mem_gen_v8_0_13 is
  port (
    a : in STD_LOGIC_VECTOR ( 6 downto 0 );
    d : in STD_LOGIC_VECTOR ( 25 downto 0 );
    dpra : in STD_LOGIC_VECTOR ( 6 downto 0 );
    clk : in STD_LOGIC;
    we : in STD_LOGIC;
    i_ce : in STD_LOGIC;
    qspo_ce : in STD_LOGIC;
    qdpo_ce : in STD_LOGIC;
    qdpo_clk : in STD_LOGIC;
    qspo_rst : in STD_LOGIC;
    qdpo_rst : in STD_LOGIC;
    qspo_srst : in STD_LOGIC;
    qdpo_srst : in STD_LOGIC;
    spo : out STD_LOGIC_VECTOR ( 25 downto 0 );
    dpo : out STD_LOGIC_VECTOR ( 25 downto 0 );
    qspo : out STD_LOGIC_VECTOR ( 25 downto 0 );
    qdpo : out STD_LOGIC_VECTOR ( 25 downto 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 7;
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 128;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is "./";
  attribute C_FAMILY : string;
  attribute C_FAMILY of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is "kintexuplus";
  attribute C_HAS_CLK : integer;
  attribute C_HAS_CLK of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 1;
  attribute C_HAS_D : integer;
  attribute C_HAS_D of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 1;
  attribute C_HAS_DPO : integer;
  attribute C_HAS_DPO of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 1;
  attribute C_HAS_DPRA : integer;
  attribute C_HAS_DPRA of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 1;
  attribute C_HAS_I_CE : integer;
  attribute C_HAS_I_CE of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 0;
  attribute C_HAS_QDPO : integer;
  attribute C_HAS_QDPO of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 0;
  attribute C_HAS_QDPO_CE : integer;
  attribute C_HAS_QDPO_CE of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 0;
  attribute C_HAS_QDPO_CLK : integer;
  attribute C_HAS_QDPO_CLK of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 0;
  attribute C_HAS_QDPO_RST : integer;
  attribute C_HAS_QDPO_RST of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 0;
  attribute C_HAS_QDPO_SRST : integer;
  attribute C_HAS_QDPO_SRST of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 0;
  attribute C_HAS_QSPO : integer;
  attribute C_HAS_QSPO of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 0;
  attribute C_HAS_QSPO_CE : integer;
  attribute C_HAS_QSPO_CE of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 0;
  attribute C_HAS_QSPO_RST : integer;
  attribute C_HAS_QSPO_RST of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 0;
  attribute C_HAS_QSPO_SRST : integer;
  attribute C_HAS_QSPO_SRST of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 0;
  attribute C_HAS_SPO : integer;
  attribute C_HAS_SPO of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 0;
  attribute C_HAS_WE : integer;
  attribute C_HAS_WE of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 1;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is "mgc_table_26x.mif";
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 4;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 1;
  attribute C_PIPELINE_STAGES : integer;
  attribute C_PIPELINE_STAGES of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 0;
  attribute C_QCE_JOINED : integer;
  attribute C_QCE_JOINED of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 0;
  attribute C_QUALIFY_WE : integer;
  attribute C_QUALIFY_WE of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 1;
  attribute C_REG_A_D_INPUTS : integer;
  attribute C_REG_A_D_INPUTS of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 0;
  attribute C_REG_DPRA_INPUT : integer;
  attribute C_REG_DPRA_INPUT of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 0;
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is 26;
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of mgc_table_26x_dist_mem_gen_v8_0_13 : entity is "dist_mem_gen_v8_0_13";
end mgc_table_26x_dist_mem_gen_v8_0_13;

architecture STRUCTURE of mgc_table_26x_dist_mem_gen_v8_0_13 is
  signal \<const0>\ : STD_LOGIC;
begin
  qdpo(25) <= \<const0>\;
  qdpo(24) <= \<const0>\;
  qdpo(23) <= \<const0>\;
  qdpo(22) <= \<const0>\;
  qdpo(21) <= \<const0>\;
  qdpo(20) <= \<const0>\;
  qdpo(19) <= \<const0>\;
  qdpo(18) <= \<const0>\;
  qdpo(17) <= \<const0>\;
  qdpo(16) <= \<const0>\;
  qdpo(15) <= \<const0>\;
  qdpo(14) <= \<const0>\;
  qdpo(13) <= \<const0>\;
  qdpo(12) <= \<const0>\;
  qdpo(11) <= \<const0>\;
  qdpo(10) <= \<const0>\;
  qdpo(9) <= \<const0>\;
  qdpo(8) <= \<const0>\;
  qdpo(7) <= \<const0>\;
  qdpo(6) <= \<const0>\;
  qdpo(5) <= \<const0>\;
  qdpo(4) <= \<const0>\;
  qdpo(3) <= \<const0>\;
  qdpo(2) <= \<const0>\;
  qdpo(1) <= \<const0>\;
  qdpo(0) <= \<const0>\;
  qspo(25) <= \<const0>\;
  qspo(24) <= \<const0>\;
  qspo(23) <= \<const0>\;
  qspo(22) <= \<const0>\;
  qspo(21) <= \<const0>\;
  qspo(20) <= \<const0>\;
  qspo(19) <= \<const0>\;
  qspo(18) <= \<const0>\;
  qspo(17) <= \<const0>\;
  qspo(16) <= \<const0>\;
  qspo(15) <= \<const0>\;
  qspo(14) <= \<const0>\;
  qspo(13) <= \<const0>\;
  qspo(12) <= \<const0>\;
  qspo(11) <= \<const0>\;
  qspo(10) <= \<const0>\;
  qspo(9) <= \<const0>\;
  qspo(8) <= \<const0>\;
  qspo(7) <= \<const0>\;
  qspo(6) <= \<const0>\;
  qspo(5) <= \<const0>\;
  qspo(4) <= \<const0>\;
  qspo(3) <= \<const0>\;
  qspo(2) <= \<const0>\;
  qspo(1) <= \<const0>\;
  qspo(0) <= \<const0>\;
  spo(25) <= \<const0>\;
  spo(24) <= \<const0>\;
  spo(23) <= \<const0>\;
  spo(22) <= \<const0>\;
  spo(21) <= \<const0>\;
  spo(20) <= \<const0>\;
  spo(19) <= \<const0>\;
  spo(18) <= \<const0>\;
  spo(17) <= \<const0>\;
  spo(16) <= \<const0>\;
  spo(15) <= \<const0>\;
  spo(14) <= \<const0>\;
  spo(13) <= \<const0>\;
  spo(12) <= \<const0>\;
  spo(11) <= \<const0>\;
  spo(10) <= \<const0>\;
  spo(9) <= \<const0>\;
  spo(8) <= \<const0>\;
  spo(7) <= \<const0>\;
  spo(6) <= \<const0>\;
  spo(5) <= \<const0>\;
  spo(4) <= \<const0>\;
  spo(3) <= \<const0>\;
  spo(2) <= \<const0>\;
  spo(1) <= \<const0>\;
  spo(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
\synth_options.dist_mem_inst\: entity work.mgc_table_26x_dist_mem_gen_v8_0_13_synth
     port map (
      a(6 downto 0) => a(6 downto 0),
      clk => clk,
      d(25 downto 0) => d(25 downto 0),
      dpo(25 downto 0) => dpo(25 downto 0),
      dpra(6 downto 0) => dpra(6 downto 0),
      we => we
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity mgc_table_26x is
  port (
    a : in STD_LOGIC_VECTOR ( 6 downto 0 );
    d : in STD_LOGIC_VECTOR ( 25 downto 0 );
    dpra : in STD_LOGIC_VECTOR ( 6 downto 0 );
    clk : in STD_LOGIC;
    we : in STD_LOGIC;
    dpo : out STD_LOGIC_VECTOR ( 25 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of mgc_table_26x : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of mgc_table_26x : entity is "mgc_table_26x,dist_mem_gen_v8_0_13,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of mgc_table_26x : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of mgc_table_26x : entity is "dist_mem_gen_v8_0_13,Vivado 2019.1";
end mgc_table_26x;

architecture STRUCTURE of mgc_table_26x is
  signal NLW_U0_qdpo_UNCONNECTED : STD_LOGIC_VECTOR ( 25 downto 0 );
  signal NLW_U0_qspo_UNCONNECTED : STD_LOGIC_VECTOR ( 25 downto 0 );
  signal NLW_U0_spo_UNCONNECTED : STD_LOGIC_VECTOR ( 25 downto 0 );
  attribute C_FAMILY : string;
  attribute C_FAMILY of U0 : label is "kintexuplus";
  attribute C_HAS_CLK : integer;
  attribute C_HAS_CLK of U0 : label is 1;
  attribute C_HAS_D : integer;
  attribute C_HAS_D of U0 : label is 1;
  attribute C_HAS_DPO : integer;
  attribute C_HAS_DPO of U0 : label is 1;
  attribute C_HAS_DPRA : integer;
  attribute C_HAS_DPRA of U0 : label is 1;
  attribute C_HAS_QDPO : integer;
  attribute C_HAS_QDPO of U0 : label is 0;
  attribute C_HAS_QDPO_CE : integer;
  attribute C_HAS_QDPO_CE of U0 : label is 0;
  attribute C_HAS_QDPO_CLK : integer;
  attribute C_HAS_QDPO_CLK of U0 : label is 0;
  attribute C_HAS_QDPO_RST : integer;
  attribute C_HAS_QDPO_RST of U0 : label is 0;
  attribute C_HAS_QDPO_SRST : integer;
  attribute C_HAS_QDPO_SRST of U0 : label is 0;
  attribute C_HAS_QSPO : integer;
  attribute C_HAS_QSPO of U0 : label is 0;
  attribute C_HAS_QSPO_RST : integer;
  attribute C_HAS_QSPO_RST of U0 : label is 0;
  attribute C_HAS_QSPO_SRST : integer;
  attribute C_HAS_QSPO_SRST of U0 : label is 0;
  attribute C_HAS_SPO : integer;
  attribute C_HAS_SPO of U0 : label is 0;
  attribute C_HAS_WE : integer;
  attribute C_HAS_WE of U0 : label is 1;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of U0 : label is 4;
  attribute C_REG_DPRA_INPUT : integer;
  attribute C_REG_DPRA_INPUT of U0 : label is 0;
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 7;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 128;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_i_ce : integer;
  attribute c_has_i_ce of U0 : label is 0;
  attribute c_has_qspo_ce : integer;
  attribute c_has_qspo_ce of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "mgc_table_26x.mif";
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 1;
  attribute c_pipeline_stages : integer;
  attribute c_pipeline_stages of U0 : label is 0;
  attribute c_qce_joined : integer;
  attribute c_qce_joined of U0 : label is 0;
  attribute c_qualify_we : integer;
  attribute c_qualify_we of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 1;
  attribute c_reg_a_d_inputs : integer;
  attribute c_reg_a_d_inputs of U0 : label is 0;
  attribute c_sync_enable : integer;
  attribute c_sync_enable of U0 : label is 1;
  attribute c_width : integer;
  attribute c_width of U0 : label is 26;
begin
U0: entity work.mgc_table_26x_dist_mem_gen_v8_0_13
     port map (
      a(6 downto 0) => a(6 downto 0),
      clk => clk,
      d(25 downto 0) => d(25 downto 0),
      dpo(25 downto 0) => dpo(25 downto 0),
      dpra(6 downto 0) => dpra(6 downto 0),
      i_ce => '1',
      qdpo(25 downto 0) => NLW_U0_qdpo_UNCONNECTED(25 downto 0),
      qdpo_ce => '1',
      qdpo_clk => '0',
      qdpo_rst => '0',
      qdpo_srst => '0',
      qspo(25 downto 0) => NLW_U0_qspo_UNCONNECTED(25 downto 0),
      qspo_ce => '1',
      qspo_rst => '0',
      qspo_srst => '0',
      spo(25 downto 0) => NLW_U0_spo_UNCONNECTED(25 downto 0),
      we => we
    );
end STRUCTURE;
