// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Jul 27 17:44:11 2021
// Host        : HURA-JUNHO running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               C:/Users/hura/Documents/Xilinx_xfile/Noeun/noeun_a_19ver/noeun_a_19ver.srcs/sources_1/ip/mult_u32u8/mult_u32u8_stub.v
// Design      : mult_u32u8
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku5p-ffvb676-1-i
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "mult_gen_v12_0_15,Vivado 2019.1" *)
module mult_u32u8(CLK, A, B, P)
/* synthesis syn_black_box black_box_pad_pin="CLK,A[7:0],B[31:0],P[39:0]" */;
  input CLK;
  input [7:0]A;
  input [31:0]B;
  output [39:0]P;
endmodule
