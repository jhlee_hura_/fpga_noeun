// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Thu Oct  7 10:27:41 2021
// Host        : HURA-JUNHO running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               C:/Users/hura/Documents/Xilinx_xfile/Noeun/noeun_a_19ver_01/noeun_a_19ver.srcs/sources_1/ip/add_s18s18/add_s18s18_sim_netlist.v
// Design      : add_s18s18
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku5p-ffvb676-1-i
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "add_s18s18,c_addsub_v12_0_13,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_addsub_v12_0_13,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module add_s18s18
   (A,
    B,
    CLK,
    S);
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [17:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef" *) input [17:0]B;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF s_intf:c_out_intf:sinit_intf:sset_intf:bypass_intf:c_in_intf:add_intf:b_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 250000000, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 s_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME s_intf, LAYERED_METADATA undef" *) output [17:0]S;

  wire [17:0]A;
  wire [17:0]B;
  wire CLK;
  wire [17:0]S;
  wire NLW_U0_C_OUT_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "kintexuplus" *) 
  (* c_a_type = "0" *) 
  (* c_a_width = "18" *) 
  (* c_add_mode = "0" *) 
  (* c_b_constant = "0" *) 
  (* c_b_type = "0" *) 
  (* c_b_value = "000000000000000000" *) 
  (* c_b_width = "18" *) 
  (* c_bypass_low = "0" *) 
  (* c_has_bypass = "0" *) 
  (* c_has_c_in = "0" *) 
  (* c_has_c_out = "0" *) 
  (* c_latency = "2" *) 
  (* c_out_width = "18" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  add_s18s18_c_addsub_v12_0_13 U0
       (.A(A),
        .ADD(1'b1),
        .B(B),
        .BYPASS(1'b0),
        .CE(1'b1),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_U0_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADD_MODE = "0" *) (* C_AINIT_VAL = "0" *) (* C_A_TYPE = "0" *) 
(* C_A_WIDTH = "18" *) (* C_BORROW_LOW = "1" *) (* C_BYPASS_LOW = "0" *) 
(* C_B_CONSTANT = "0" *) (* C_B_TYPE = "0" *) (* C_B_VALUE = "000000000000000000" *) 
(* C_B_WIDTH = "18" *) (* C_CE_OVERRIDES_BYPASS = "1" *) (* C_CE_OVERRIDES_SCLR = "0" *) 
(* C_HAS_BYPASS = "0" *) (* C_HAS_CE = "0" *) (* C_HAS_C_IN = "0" *) 
(* C_HAS_C_OUT = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "2" *) 
(* C_OUT_WIDTH = "18" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "kintexuplus" *) (* ORIG_REF_NAME = "c_addsub_v12_0_13" *) 
(* downgradeipidentifiedwarnings = "yes" *) 
module add_s18s18_c_addsub_v12_0_13
   (A,
    B,
    CLK,
    ADD,
    C_IN,
    CE,
    BYPASS,
    SCLR,
    SSET,
    SINIT,
    C_OUT,
    S);
  input [17:0]A;
  input [17:0]B;
  input CLK;
  input ADD;
  input C_IN;
  input CE;
  input BYPASS;
  input SCLR;
  input SSET;
  input SINIT;
  output C_OUT;
  output [17:0]S;

  wire \<const0> ;
  wire [17:0]A;
  wire [17:0]B;
  wire CLK;
  wire [17:0]S;
  wire NLW_xst_addsub_C_OUT_UNCONNECTED;

  assign C_OUT = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_AINIT_VAL = "0" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "kintexuplus" *) 
  (* c_a_type = "0" *) 
  (* c_a_width = "18" *) 
  (* c_add_mode = "0" *) 
  (* c_b_constant = "0" *) 
  (* c_b_type = "0" *) 
  (* c_b_value = "000000000000000000" *) 
  (* c_b_width = "18" *) 
  (* c_bypass_low = "0" *) 
  (* c_has_bypass = "0" *) 
  (* c_has_c_in = "0" *) 
  (* c_has_c_out = "0" *) 
  (* c_latency = "2" *) 
  (* c_out_width = "18" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  add_s18s18_c_addsub_v12_0_13_viv xst_addsub
       (.A(A),
        .ADD(1'b0),
        .B(B),
        .BYPASS(1'b0),
        .CE(1'b0),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_xst_addsub_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
hkYW+OZm6k9gF5yAUfXGm/n8kfXYD6tjFQYha968Ws0SqrM/NNAjCrrtMG8kIqTbkipnmceefxNr
sB0PtSpUrw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
NEPpD4CxNBVJLV3hg1agn83QnqiCz3YuR89MlVuNyQGERKVJ+uGolFDqHFzBKLQArFTiHBWivkzK
A2DQ42XdOxp30NKOgHjrjgmF+fZMjDs24rn3Ue1INLHwTS5RT84Kih7Jx/7R0dl03/COJq+33l9u
7l+ArdY7mLwqqI9iIjU=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
cfBwEwc95LpKuxDGqpON2gGac620iHNKrm/QNXYg3/OFA5ZQNdpdhRz4vCTQRVbOg7b1nIox6GR8
TD/cf0JW38RU0NuY+TR6CkFT19NCdy67gR6JTDdXifhr/zTKjOL5gvp0XjT9PSLwwPyDirNX4TMa
9y9X5pf4gEnt0dikHNgySZO+Qpr30MP7n6oAjuxowlf45cfmPqZthYPnIjBSCdQGBPfSF+kZ2F1N
XCDEja5xE4CQshPPodH5njadc6kj7/qp9C4PfKcyNtDug+qsws9UK25Z2IFc8vk6/15HlIkQHkXv
Wq0iHaPLidqh3035FinHyPD/FDnfGGa5Oa2qcg==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
QmjD3HAHcP+h0RsjR0iH8h2N6drNxei50nfQN9RC8HobMEaARq/6rKjZEhHXMSCStQeCMhyVKRmN
HM7ZrqMf3W0s/8U4QMqp3M1VuYXVjEe2PCIpvtRcMY3JngdSWOydG2dH6dDA16ehxinMKgIr0TjA
PXA+lfyX6yTs1FWrne/6ufrl6ZAPpNG7EDKQ2aHqSm8DEXT1BJYMblBfAjAajwaJmPEu1aDlQeNo
onryTiFJkKP92pcZLCCufZL8ZAJ5uMvZZxiZRsiLd0BnCfOe3rl9AON+q53U+iK11EvAkpIBT+Cc
VYb5NqVAVaqXbQrqo3+YHEW5ft3fM9kZnlFDew==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
FkHW107swc8fPv4xOTlQJU6PWERObturlywl6rsGCswc/v367bmQ1Maze/8QdmUPjEYwhAcHKVMu
7U4o3CvYhmrDpYiUQdQQ0B7gAbMZbJ8MFY5jRxn7KYDk+Bi9Ov8092IdW1a51FPWEVPmF4Kn6z4E
DSqpQDL58qieEUnrU2Ltb4GLJc3NrWTLvnbvRtHUUuQWTMZTQ7WqX4iH2dZ/EICpbRjlAF50iMAS
YHuuFTRKXcIFQlKYRyeQV4nyaA5JGbb3RC3N/Q2IZjdSXqQ9EOpmdhttpxReCnsdJiD/pPCtf7ZN
d/TheLy1Va2FZR+p4MozZorVui5/FtcCwKy6aw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
NgZm/7Jvy0UZQRVxBVxeZ/odxMd59IlnRFHjM+6Bof6o6u4Qy4u9MOoQ3Sr2paPuGq+B+5EhdcD8
a5WGiurBrPW0qF+L2CoUJsDqz0WonRehZECQynibSUlmctvvMyr790pwb+C78gtW47p8uALYdUCJ
NhcDkV8fE3jFdDEYmfQ=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
TxQNdd2WOThZCBZEYNgXxai4jK9AqWD/GRadYnarEfzmLUfcNDUoG7DxVWHCdTVuW8i2qZpouT1H
FUHt76rzZk8vI2tFLfUbKyTaRqik1aYwOCp1ZdqbgqQEDhBRWJjGxcJuxZbSQ2z8IUgiJ0eT148+
nf9UmzvYS1jrIsN/a7K4EjyRNMk0V917y85rxdk7itlisaUw4Cm72z9slByFtALj6/077uPjcK9U
mbWm7PbXk8PT44eQeaJl990wlWvD5/8BZS6AHqjg8520Xs+jftSeB6aNqTiYxfp21FJqmexwo7cG
G3BH/DRHhP7ZIsXHqSaJJFo20Nx9VgpLuF5t2g==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
ZOwlgb1vHaQjfN/VA78KPfnWXELYSWoxV+ZC2bmccJlvC/2RqiirOzxrr/vpEc8McwYjHo+0SfHd
cXOffFX43mN6TCGomgGxU1VNNPeQBCg9a4pEhl5HlQY6JLa0NitCAs/TgasDobcRHQoq0dPhOK3e
YJ6Xkd0RcSWCAhG8WKHosidPkqfdnN8Ol2ODGt5Hd8XrRAqpvFc3OQFniqW/ZwUXoP/ZinV29sOf
35ASMBT0DJv0ZH55rlf4FlUNQ5/jSyF+YfzBxnWDk0AiUOGCY2CtjFugdxw4mID6xH5jVFtXLNNV
EWiUw0DJU8Ycszb92Vb3Gk/+S502qXXq+KkWnA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
i/VmFBtSU/Ik/wyGnojWF9Nnbo+NlHxJuKSwoAR6lX1FIvQplSCFfj3pRes2O+SxHjCUyraLw8Te
mda0DdOAeILriG31cvTrXkhXPpDPQcbWj60UXtqg9m9g9bVgHoGTLYR4wXFeKoF5xApeKPYpCoox
X035zuzQMHTo5a1lxC4vvi2qwh+oXDunTb2MtT0SNTfCRn5jGR9CW0+LfzLA7fFVRkvcXu495cf4
3sYyjIKmtGbKvja9Jd7PWBbYfkhXpDpZc2m2Oc4xiOVRzm7xfM4Gpi8N9oT45hjK/gfzmcBiUlcx
XRIkBjan+ITTildO6+SYntQo1btkpB+hESaxxQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 25232)
`pragma protect data_block
b1H0Sv71UoAq+hit1AP3A34UO0BpfPTcg5QX0+utjXjnyOUksw4E1v7XT5EvOdorzxtdfhAjd2T8
ofPfhEaREB58rc9lasZcGkFO/wog6s5TvAx/yYFMSd1iZqCE2SHu0c95nNYSTR8m29vDwmgpOgdY
J5RPDDXEb/+SZUlq+caPsG5VBAhno8hSSUxcSMYoG997cC+SgVA9vFfWktr0icfqSy9hcT22hCgs
0tvnCeVHWmO40y1yqAeuvz5uS47DmXxpIv8mGKkCaN7mH+jAHJ7TqX1klLplwHhH5v4BVaFIiR4e
+1kXniBeGqom81k167g/9mq6QU9otjZVcikHVLe8sDQ/P9rGVGEXDmafZKb85Yc3bCmUSTIBU8TZ
771JkQe/9Ojh/vRh0iBpPgBXiyn13yAKAMoU/Onoi8TgkTjcqvm4KqUfgSvMivtC/Fv/f4rSNzOz
1TR12ySAfsIVmmweqOS6fr5NXaCDUj2nGbpOxhm9nfMs8sCpktcxhje3sZllGVINXzUp5CckXEkz
qI6hZXZqN5dq4GXvazlMkMcBX6WNOopMoGhyyLA03WV5d5AkdwPqSp6yBaxOrLTJBouEXus/Bf+1
34YFsTcpVox5ogKsK3w6RE5/Y+U6znC8ny843ut8RFO9vWCgZ4+AdR3UC7O+i3bDO1niIYKR0eHk
wo38w+OaalpwFWCgog27D2wLojy59WX6RBoeRn2V9Lf/XqmgJH/dx5wkVQgU03aNhs/Ffa/iyGFD
yWJ6DYZ7xjPYb3HeFpplBOomeuHBWgY4DIpagj2jxLP1omFyyOvzKDegn0kAu/uvO73kkFPhYn3V
VhiuzpbNtwePC0LZTayUagFfI7Ivk3z+WLYgzQ5nUDHsNajhkUzWMq+D/0S7iU3jg2KcAhbC14WY
8OOqHCo49EOik1v5bzRbz56Z28NaGAgT3iSCPwEv0ApfklHqj5vObp8t7WljT1Tn91nDAiqUutdE
85LsSSy25CBHpWp2zrWSrvWwOPwOpsz6QdNDktLflRmmttFXMm59YY6OXv2mkLYfD6DFwBVgrkjP
ZXWNla0ZgezLzfPdIFt7/Al0g7FBk81FlZHVvI3rbMJsROqdGKLPpvWw2DFdH+WMyJHOeoNgYdjj
KMGMwazRYDoVyzKsyuw8VcaDPFMjgx6yMsM/nh9O7OhvdT8ozowIfTaLIScBRjmmK71QHIsNVFlP
mhi8p4NoQznE/olrbInQ6AoxeHZxczihU0X6AHsQvYmn403yKrGBVYzgivIiQiei8xYMfdFfD+ax
On4otAqrbnHkP1xxlEUx9V6hcdFvuXgSEgbFauZ/bcTS80imCb1NZ93mMzpb4eKtq4cfuPvzcSQz
jEEUWyVwszLlTRo4fPf5Sc4egEjHzyFujg6+Ln1yzPMdvDFs34C0TxSJp6XgS50jxcs70yl8b8xg
xgy0t1CaIITm7tbMJsM/CLzYlOVib4CZCPjWODvT8YhSOhafD7mw+F4X8QS4PzBYORxmkiGN8/es
r3GdX5EHJiYQReN2IFuVVhrm7UNdleNjnDtvV9ghGYakiac989yhGKeOP4MzFIX1soIKBl3uAgRq
NTbLs5Gaf6BWCz0ZkQnK1rChe4oPRl9Oj4AOEyRhj7tzrC8hyaFSHKhRiTkf0U8Zy4vBoq6bYmAK
tcoJJ0knLTxVNZSGkOOcpRRefzEdXf7R+zgDEE9waTeBakKezdAekuQUwNPJNVZE7L09qxKX/cET
h02KowXJyMCxAi/ObqkS5jo59G6iKhSUxpyF/J4w9LeBFODmRC3/20iQ487qDUF3BlJZF0CXCC++
yApfwdKwFDf5LtjJ9R8paJWlAucCmAwZPS3X/byO9AFLz2R4m8/JJE9uILSV651b0/zxWyK96jLT
1Tinzueh3kdhxchmbnOh2W/EntqK2b+wszTgL9Dg+RpPTd1GsTSot8MagHNQwIohgUtxvf1NujJm
SEe/KNZWqCZQ8ko2yLznoI3sFo3++rWGCPITyxg2nbV0R1ponNa4JLZDeblk3QU+40Ny5ORP7/XC
I8Za961HMzvzDe21Q7x6NAzTshEOd7WXQ4Nk0JS2yhC2dhjkJoR687/1XiZbDO3XQmJ1zgnD0aod
1ETuEllfWxjeXCYjaqvTNCQtLgUVI8I1t6BxNgSStX22awd+XmW9ugDdoKbwaSwY97VciPZ+n8ex
FKGQbQLqo+0+9XiM4dSuQ7k1rGSHqqty/gvrRoZltLWis2m1AHn75UB7E8hkaqAortiipZM5DunN
xQP2+9QU+NVc7sVwnW30Bfn8fRs/MrFZcg9y5nhwJc1ojsEgQVM34dbV2p99gu2nrhWQBmwuhxPG
MNJ9uBElLWN8moqWmvcqzY13uT07nacIyLkX55SKgN5Bh9UYvD+E2m74/jg207gxepOd7jAAFXUn
xmJaicfKf5U3KzVNqW8iGP85OZ1AH3neC+TEHEzEM9grOQjUX+s3pFneY+f+JTQMvrfd1TeujDs/
qmC243LO5Fa3JovzqCX4bcb5LyP+8A4aMqyBfzztFgVmzYyp1MBnBE7VC/kcwC9EKnMTQWPgX5Di
4902+Ily8/A4b3UWK974UJImusSw75YiInfbzTALuldZSDD8a1Nhiu8Rv+VUDBnig9p15vP7PBrj
fUjl4/vO5D+bWYtnwQ4UBXETCJlVnwHF6x9myBnYHjAKGs7nfwztD9WmjTp9nhj11+4hOCAzXLyR
g5NcH74NFKEdPK1Y6FDjCK0qyy6FPaxcOQIIRsKZU1exRU1aPCjR+v10Hkth7rPREQkFnmynaVJa
XpiHrj17k87TA6LMKlNdSz8vgNtQx4riweHk3qAu4jjb2B8RXf0wMm2FawaGoRJ9WlgaQF8m5lsh
xXYutXfLSJJ/pz2BD1l6zQ0UKRdkHz8fnpuWRpvr7cXGgTxJCvBUN1qb8nt+7hWhqvFrShVKjfmu
UInsGB3uzCKGwMUdqugrsBuMuSpjg+q+Q3FwBN2Y1tsO25GvThPLBrKxsrFmY0CsBQ5Pm+3XRpNk
dc8xD6k9FH3HLyX5pjFfpAgtxr0h92u9Kd3jFrqCozyxHC5ZxAfnjAHkl0vlgwlD49KGAQ4pTc0a
vLEDpy9lc4x8UNLgnE5ABDG4RwaPHsLgQBCQhWu7WgURjhVy5deBBXt4rAps1eGcRvac0nn17dLg
NetaXCqpTZxOrOicNR6Qslni+/dTewqwB6T0EtlfWRrIw4Q3BwCzhNb9SS9xmY8n+j8rEdoTqK/u
mUdENgtmBW1c2NpaF40F6DHIv2Sf6F7vQ23g6+8krfroSWNMGakrb8gBFamlysdggAdTh7+oFTGN
atAVMxT5snSq5DwtMgdt99IuvO7VrSw9OVzDku6CxYbK5xEkE/07w7isez9dRCaRcRarINAMCtY4
zhVz4uhIPdaloyuhmUH+Wdnh9PLBVK6y3am5Tcqi7XQPO/JYrx+G9toBkgF0GVHoOUApGqFKWfc8
VaatVZDZl3kf+pr0UALwp7V6g5y+juBRFCcnLN5V5QqiUF/dMpA4VAjNPjNAXnp4+yOOiG18jKUz
ySW8J5VJ2F5Y6im9CwgSZ8yh2HnchoHuphVkt6y9FrXb58HUJPyN4TpY/fz0G76vfgWcjfdu0ho1
Hl2CGL4A6JemoAzuWmxgn7d9G98mKP9IzBapk0ZiMU5j/LghRAEG13npquJXBEHDM5Z3lFOrQ5jI
k9W/FQerGym44vC7pqvY+Vc1Lll4E8/55bBNCfyJ2XrN3j67hjjem0xiHUeeyZ5cr++uCYE0g7x4
+AYKofFhveB68gpzUrOYAcOaNFaR47H8SbkbHSQwc8HhjbdCFKgnRNm+DNChoaAhW/UKaNdhTI/8
aJQ1mdN2+/4MpCzPKo2BVaQkPcmgwgKxRNKmrVvhOBWy4uYAgg8woqd9diEbZDUO9MlInc3Pp5f1
9S0DCNCaiLeSbk/LWEf0hrNjpbZ6pGyXxAd8b3kUZv88fzMkv+IILwMmgG6F751fulBiztz15VhT
bsmNIqvsFZ9LfkMJSD7U36X/kH5ynV3Wv89QYFhhP1cLJdsHP02MJPvUy0jrdg/3mP5vjmzIPMbt
E57n0whPXpxdX2L+75CaY7MYKqQS7/y8Q1XUtWvIAOlL+8NFpwKGwEKXti2/6zfGHJnIFFDQg1Ak
t/bnU5BA1dVmMUKXSOaTbU/kUxHYO66FphlyY1QOssDJEMegJ3QqinNBDGZtfuA/AlhOcowDRqBl
ASdXyq4AdHsBwqFjrz3uX7HxPrpVO2y/GIm1DSswC4CpI6wmNgJKPsPAhqv3PAObQrM5uSMREP4a
41GUkiwk82hsAIv4QP2Ax7YKw/QmdKWd9KwZGZfaHEjdRVbWji1rmHoSIYtbFLl4VxudIyLLnyZE
CXQnaSD+iP2NO2mzlE2wPRJvHyau1k/6586k/n+D2S/VeyITTJVGH2SVP0iznc8XaSXg0NE2JcDO
dR8NnI7woldAKyYnCKc330G5ZVA+nCb76MafywRRDh0XFthWUu+X8ViriOM7/eYed5Rp3OSTTY00
GlaQMJwr4saEGvJeLF5+OsVvf+bJgzNlG2HOtkc8e5YWDyF9ETFtzwNeygFNXtV1DTsQC81XKdfR
nuIdaqYsGGXK4wwmevjk9EQgjexKhsoB9D7QEAwOOikHu7STmhRnIFsj+41/AKBuQ/BgQPSbkbIJ
zLcMho8OjRKY6xxb7yOXO7jzK3a09kMmSviy1li6VJUnzYs9lIPd3dddtoZwZ8/pQ4D/so3l0mBF
FyA422t3EQoHmSv7ARlhJ0dEUI5coX4Ayr3JTHTu7P6q7bOpuBfjdqcB5Ag7qnzbivhvy5lMR8mk
cDVCJDPqOFiUBoHdniO86BbHFw7lGGBQywtt9gvplGmOwH5Me8/H7M/bphumIEPgkbHUuC4OCEAO
TRZFXvoxwoGRfHV5JpEBiOCGHmQ37Zm3IhNUMeOQiYYik+46cMbDNz5wxu+rLgd8ljiMeNtdAG6N
UdSpQ3Brze+FbJrSSOmBbF0HMHF/R7qARBPg84OYS7TL50czLnC8L+arjG3qsvi/kL1U+xxEb0wM
Mpmy7tezulYOViUDwg8ChKhra5PHPYHA3+4Ft/Pe2nZI50VSKX/1cAz42tsud/yXoUHZIdDGm/Kx
RPKus//mtjnaU6rxHMN8MaCZT9QMiFDuFzd2PssABjQHVdDt+3IUMhYp3K5v5c6RM5LuZHl/s34w
zqr62Z0JK6XgzvgSogxcfA+xeO3fxllc34fjSbqX4WcwaV7DgzXwTjXBswR1UoMRtfTlaQ2mKuvG
k5vEWtlVStvTVvJM2pf93UMgAWDbcPk9XSrLhstcb6WEaJzArsKwW0eNzCfuduNYrSoOjneuRKLU
RYwRxKTlx3jlREkMPYyFbNNoygdSaRz0lCk0eJeMp7T2sV1Kl32bIWih6X9deSVVmsfnr7vms289
RSGLrf0JKGNU1yM3OykGPCX1PMeSoQzGI4MfA9jWtpbfavlZAQvX94oFiFtiVa8ybdHSYX3l42nE
WRw008AdkEHEnuJGZEZuc2MPWlZq8ecQ7M5kpDeexQRFPM3XOkDY84HLERpFTBbEJEGgjBB5thJI
iCuJWYMo3P4LtJZE4gMAT4K9BsV1iiJsz2U7T8fe6mGD1m+8y+2F8IIbE8qxAsyqn+7rRE3Effvw
wB/2nJ7WK2uP54fQHMsx4DTGa7hGwaVge3JueXHVmnuGNNoDc68q2BM6NJU3kuAHBnolu1UI/nMH
6KIS2Po8DeCEiRcgLljmEhfCz1YnWNnHkMBPpWD7kVadZoij01xCLYLKHVsQZsGqKNkSpVSzmk1s
nxNS3SHtxhqsxjoEIkvrBGQcXvSx0ETE6E47qfyz/Mkx9qf9Nndkin/XxnJ5MUx72/N5INWUPL6q
iGaPj98Lf6UDhHldyY2GsA6GiGaEhOScIA0mUaSSuV+VAvkz9yf4XYB6jn1hmBjK3jdOzgmGDkRt
V8pwerjBNHfQtZ2CR4Cz45xwvlXv6JGAZEn0tori/MsH8oTuSf1jzfwy7ftoiFxZDq4q16zS/bU5
4rO8wAquidrc1b4YL6JyPg1C2kHQDprhETjno267jY7TdKOhi5S1qg0bz5e5Oj+6+SNXnzaBQKyX
BODclexsSRsyG+AK1VqrTDYdICuhHlULxegxSDoiewitt6bwdfM4gDh1c82bKIQqbcBfKGJ3NgVQ
Vpa5M5L4jCe5eS4GzDdO9p22SWTninJHA47pwy2iCd39idqxF1Ajsk3kAvqMPu/okS0eAUWGHys5
alOwLBVRny2N/5HlUhSgb//C/rBbnQgpK6B8kdv2JodsCUyBqbBOH4UDcMJuTFBYy3r/H4PVjZ+7
DPy0qjo5Pw1tFOIeCUhE5rHF/guRh8WVXhShnmxSd679HT0zc4CbbJEZwjLRp2sr3fnQjXIJ3pXo
dDoxoB1yBgRAIXyCnBw+67wUsiZGKsEPNZXJJaogUym3uYOueFJ/D5qYfXw/8XJWpsJaKytHLHvy
5uyM/KkTFGuZ1PlDBteT2FUmck/lzaldGooAqS8acbRtQUtVEqadNeSR0AZqbdh/aFYGCFIZ+LXm
nIWjTjPwCP3dQoiLx5ogIMz9KGFqjsUwWtX39geMOENY8sQe0yo0Iwg8ulov4JozheCdm78dL3IW
MLeMfyweRzE8alRusobJSbL4kwt9NFGs5dnnAGGSw3hgvPjQrsJhR+P5VHO0RKstI42Qm9RRfi1n
AB9swyPqoDHwMx2LSDgIPhyx1WYsk+tp+svNpPmPNvReoNKB+SejK3ven0IAGjsJEt7btOktmQRx
Eq7SUuDQzT8jp8tc/gfL1skKHDw/shL2Q49lck1wwOEaGG60OsWSEz/GkRh5ORxBlJsvyniKdBer
petn8/c5pXFZqfBlIgxaMXlTu0aX5+oirB8YZXCs+Bp5d/dIVmVloX+C4W9PNo+qk9slrHGD87jT
CkyHpV9WvCYeC5QJTOc+DXHZp0sPKaIku9pxz4xJDwqF6XKhagmHNLL3FdqZ4dp1jAkzBH/fxXok
AtFYgrVCdpyfa/MZDakk6yFuwosVU6stirNB63dZi7nUWVggZZwZq5wvqvqzr4c776+K0ftt8Xeb
4EvLZ1Md9rWBq+w5MKqTkQMHVvJTs+myTJu74NT0ckXZifscVcfEZizjTPpH3n+Qun7dXwv0IqFu
FFtJWdj+iMBrd7S7E6EDoTqe7FwS0e6ttQIl9n0/nPEphbtd7TYjuA8OOl8KYxZI+Nf8wr+ZnNR0
yG9iErzhwagKGueVBqqu9LOZ8iPsf+/ad1YSFGCbQNeICnXkUL/c+umxvfdUla6aEoG/slCbGz6w
sRJBAGUOg9kJ+4aWh9m1l3RMEFGkXu8cxa+McC8vivXuCEV2NwHVKU1ka1H94iAtlN0cWtr/6Hl9
M/k4aB1FeaFdJuOrOyHQpX4BUsN81s5RxCcW8Bavf+P/Ajem+69XlLldIxg8Xj7EaVBGNQwks3Pm
dy3bPCvq8XL5DKdeA01OpNcN2FH0sx24c838HDy2vCRiAsEah3hRF/8clZO4VeAf7FWIZwzUpe/6
KRvNOwKSDY6h2uGpQCK/HM2pMgz/Qe/0TkEivuBxZYNy8otIAqYoQy/PLu4mRfuszwn0qzSCGnPm
f+Ph9CNGYwOdm39fvTVFmFvQFBDEKA39Oh4MPpXYlFgtydgzCQM9P0b50cXiLOu10uxSMyRQV6fM
QkpaKk8QSiWZFLxfSj4bqT7+HpT9WkUuvsq4hyMTPd6UkCheVEabbovtKPvafkQXfM2gTUVsR/ki
mmbC27DcXOh+YY7qqwFZR1zy/LlCV33STANn17rn7iHJwNr0yFj8fSgVyCg2UVIXTZLpLRS9xpM9
lXiqNgTiSTtBkoducOJY7+awzlboMux101idj2dVHPdrEc73oYyS7CTqRIchlO5qodVQLO1bQQRC
SIqyEMYeDQ3TVXQrXFPW7Cw5qpvxTq/whaPMeWPDFgETJKNKn8x2cibsPghsv8TFoNfKUQSHxoeo
3hYczy9oYio/6rqNn6Gr+GFcXQkfemms20mnRv78Zgth/wDbVlBajKCQ1GKCTIY4rXRGZoXkEEZy
c6veZ9+nA+6P5aplWskgT9c5ddQ7EewvGpv+JmXM7mfgQeChJn+VzxhOqYN8l3AUJevVSLu6vqgT
3ZaSugh7tRjc404K6kEVroHdcw2/NWn7jxoYksr+vp5f+k2vEAgJYqFA9SepGh/TTRgRHeGkuZZG
2ArSGP8jKHooqDEPF7M1ibOZaOUoHMsfCNu9UWOazBL7SLc42tafznZGdL2DA60cM1EiSxlCxTwK
EC5bA7BySElmqPLVJXq/WcHppM1h/m3p9kcWlnEPKu0vyvWS4LVQiwr81BcXOeOJGp8B0MDCJ8cV
yHk1cx5Xl+p4AuUh3yeT034TXfrKZTSagL1FQhmJvZuYdOWBay9NJw4YjDkoqWFcPIhJbOR1qQKA
3pxwnGzku+49Dt+6sark4/laBa3Nbye10RJVi6V2oCdGRHsX5WZame1l48wn7jTTAUHfS8BBbH8+
8YwmvWR1qMmlOO2FTD3DKQlOjbVRS7LnIO03wJllZC/Pe0aYQb/y3OLr/LMBrG1c8JA5EcoQogG9
U3pr4f5X7b4s0OF2XoBsz1nV2mYVG1cHNDwhcNNOwHt3lwvI8YKS2HoSEx3cocsrCQnY1PtRwbf9
T4pXkOBD8D9YSnULKwJepMRCKsmyq8+mPlwY3vL/uQhGC6AXZ28IW9CchqgxD6c4fJNqze3D1NA2
obHest2bvu8HkGFzgCXMzCmAEpbH7zS13XFcN4qSp5w3yUqI3FRsoEPSahF0pGb36pL/OtZgj5D4
HKL+44vvWyI9/ZitKxuDPpz7p0fBp11dsErJozdIBfTEv5skLk7L91HrE5jOcfgbHPmqcyk6t/YZ
snYYt2UXHdChvcnaY1LpnZmy2yJsIgDDJOUzxPgOQ0RZf3GABZSaqjKtuOznwmAbjh3fPpSBCvQ/
7ObO1ft0sUULUQIkx2Semc/dJkuRSuqSRMo69MMyI3LXlAPJP4PxrIeFamtKASsz89iifWbOCJMo
GLnsHWgaqtbOqweYFyH1TsUPFmx8r2pWWg3bIrq+Yp4gSsJkdMCpfPsctwGZRbzMkuIRQ/DSl0Ps
aLcERAMxcAkGvMdEq1qJr0TB5DHQm2oYwr6sqqSmA+gUWQ/WHMEsM4xROl63gUqP32ObdSr8pNaf
SzHMSlYIAmzK1nYsTJn3Bf3Sg8Ml15LUkb3GGqUVyqBM0pOsJkw6PMDGMGnaC8NL9Z2AkIm7pmj0
VhB+ODWlkDi8S9ynvVHQlUD9U1UJ85s+opZ14LQIfVCHsc8FUBjBZs3RFDeXJkrAzMjWD7lnWM7E
MbHWhrML7ZEWc2dI67hUOLWGvVr6jUW/ZrLixKMfSrR0zv50NCk2zH4bV55Y1KOOUpnWPnaYI8mM
rfFmHIbEqzbBVbhc7Jq18LbiIYh3PeyL3gbf4iS6OC8aNRCvyS0pR/B2wDqwlFBH75ck+T9ICvgE
/FbjWVVpG+ktycdmWu4rcfC746pIVrv6YOXB1A17DohzngQhBLVIudhK7fyfrDGvMO7qOaexuOsL
xFiPAAsw1XYjI8s+dX98ZN8hInz60IarjDej7lhvRSB+awXDFEgj/c3RvUSuuxxCsIbWxKiL4s3b
bd685Fk7jgZu9l8coG+zB3up6zeWELtzQ1B+5wivrYlpkQU0gcpjapv5QK3f24m7Lnx7x8fPbo+v
3pS8ReCe95U16EuIgzFeW/BR1nlS95SBOijP0na7DOI9wW4/I8GoR+NH0THii6vgnbckZjUeMf1W
4Ru7Wk26zK28xvht1r/2LBf2wJJ7QepUYpFYv8+3EAgn0rmqD8GNqpaXiZ1x/aXk9ulngHemOoEH
ezhMFXsYHL5jIZdFVSkoexxRM4QD2EwahZCM6wTGLrlBqrdUw7+lXLoRpvKk6sZjaYw9VYuVkK49
Up03J3KCntqf9Ng46TTnl8EC+nUvY5leW5hPyUtSlZQD9g51KX68sV8HlFOJkNwtkPA/MSxzozTX
vSyY7J5RLxVXH+NdBL4q57B3QDWfXUEo0rOQvx35mbPqUB5CcyNn7xSehMUV+RL1dfXLOxxkyj59
kcWrEYaM/+BzP2lHhyVilKAs9Q61Rxp5gP8yTcn+gShVUDtn5DeVDKHignSdsQ090PhWGGpKG49J
ijlv/NksToiQ3Gln1gobxbS5fXmYV/pcGkbBIMzw23+/LKHYS621rDs/EEZ4Rb6unHu7TooaGos+
YsF4fRWdZ3RQSdITlyiIeTN4tnNZ2CE6Q1bo0Cw5NSxuv/8CsAX+yGlFQuE7a1twA9pHiouRYFYj
YlSQHFKSY+Wp6kfpd8nacjA2sk5yq1OBWe5pw3Z0PpR7QF9DdLOv5ixAIg49O4+7NYvFsVv/Zbzg
hGnDWMPTd/DP8EbJxjHl9PNhat9ZVOSJ5GN6gGCIGYyHndzjjlQZTSPTiuyXS8jZ2nb7xPlbdmVK
YcuW7LnevdhWxH8wnPljywIhYsLpn5kum2Xuo2IuS6Zo87q1/kamJWwi406+AL+BWBYQiJcEzlwD
1iMT/osx/hPW9mk7DEu06iGJuuLlvjrn6GCBX8Lx3DVgJ5QiKo2/Hll0vpgaiMaAozZRFTYc2g02
VKTwDdVmfioSBuWDuN+z31d3i9TdH8pQ31zy5ScTQ08muhLkALGRBBq65atfoPvd24jvBfZh8GvQ
p4U0p1cJeKk3WLbRO3Z2KBqQlNipv8PzqJmwpGCqW/KFDcueneZVdkRdG2m4gV7O+hEYkrrQz5fn
5kxv1mxrYKiUT2E5YboUJIULvQB7HymuxwXTwBgxV5+pEOK3TVlbLvUfnouqr6ohfuiKLujZbk2G
MgEsOt15ku5B6mppKPGcH5rQtyFlAwKX0ndxOzSLRChG8N02bXMWH4gAN4qgHprmvMiue7e0JdMD
Bi0nX9+wpLpPITKwcAbiHnxxMwRfh3CryVpTFX3pDo+yo7YBBL/MoPICSC4YQeM0XIre09/6VMye
Lz8VYg7ve/KurCA1Pv7zW28s9CqzKKh6u8LSmqW7hDfS9jSd3IwwytB9qKnCEhxPZoLcZuM94Smr
ePJcCuJW3EHJ17r8EY6UHKjbmn0AjEGgI3WMqolyQ+GG4qKjBnRjI2FRxIYE+ikjR5XNFAvyGW0Y
La3q4n7Dq0xsCYyKANMk1lvB8Qjd6aczAbCI/H+pQxUiadPgZXjuMTclMN423Mj7bXFk8Zlm3bQ3
+DFB+DSrzHCdeTi0M61gEWtEsyiQ/VeQk5eKid9q5h/4h8XED5wlhjHGc48tvkCo1f1L9OJFmrmd
aHPOi52QltoNzhqZEHgiP+XWeDrEyWbnF/6uwYB6GUv8E5rL4KK5Wk7hlnd6IcCU21UkfpL2dwo3
KqC1KG8MUfsUBwxBHUXl2okhB3v4vGa2JncyQHjxfuEv7XJWcbQWxvbuMX7LckZ8Cr9crmfgGTVI
4Zy4JPauHZQraiOZaY/eaxBozfNPh6AM1p0/b/dyuVESWgRsKd7RBnkf+hN13nOQ0xHsu9KXgpnB
hZbew0XR1DObk+V2y7UiU/vEIaWuMu1dMr0vuC7dPoXcWML3S0770raRmfD913VyGCVSJMSsFW3N
rfAmjTLRdtizD+KUm2IqxP/GQ9QTz2F0IMCLT07wzhhiNvfxhf24jawLMW4BH+ctJ3Z9z9yyBq2w
Gi2kBIcRKvj0Pf6dunyq3BG+uOGbKUfOmKnm2/JTvlcUGVX8dFbHsIp4su3bITLic8V6xy3X6HW5
Zl80/lw3unOhGD832gn7FJAaHSC/zbWz8UIGYvOYIxhEYM/lSCPuH366YPSmP3ODooGz4gr/oQ1Y
Py8acw1YwpNLaZ+mk5Vz5n8L2w/BF/Zh0Zu6jCouV5oGJt8k+Hq52q8j8l8ZScAObSsrRztxRxCq
s9kPq42+VUO9XgME4BeKC1T70l9vaxRFplA34H7WWnIQ8CjJ7ja+jvKR0qxn2iuLe279b7QOE8G/
MHjbV9mE9y95pVGw/K3zVosqLvkr0X1asjMKCXt7+bDDDDiWqEUtT6xcByN/xGtuaZCJ867LWvgZ
7ZnLgxnGfQd3Upfl/R/zPn/XowJ4ahm9WInoIvOk8zexujom5TL6X75q5by8GwIY9GHx1bGAMms/
xKu8ZXmfX9vhj5pY54hv27HdFEGM2+0SoxZyGpsWwK0mqWlTyJEzOZ3AJvx5ehGVjlAhwGIh5KHj
P41ktBkVsqs1lZxHvWO+nKyvvA84q3QFy2fRzT/5wbpSKYMOQzTh7mI6rrvxW7vbE2UiYGGQoMj/
b+XV2Ro1shpyJDSOMrTaiyYZFtvtKeAdT+6BsbepyOXCMyjTS7n57nqIgRLJo9oTlGce7KQ8MGUn
gr4ie1bSjyfxmPRPNWtMA5a6R3B+v/Q149CD9gUOFBnKfocDN47oy1ZsWcyGOVW7T87irnOV4p3Z
lvmG2XNgpmM92aLw9hsMGxHELX1aDh2fKhQ7by3LV4pHRPTyGfU0bH35EZ8gkhadGGV9ZVuzpKGq
jp5v+Y7MF7XtvMFLUKX+S+NaQRpgFnO8LTFgq1mraRJASvOD/bIh4rlqXMZsEP6bDa3V9pyWoKo9
9WZYPTPL0Ddn8/+9wzkS3xPPvVhzZ6/I5dAg8RhQJHm9JQbR7V6JBfQ3Y52eJYwox+OlGR2deuUd
2GjD/HCO/n5EwpBNLH6fhNws/1TbBMW9PZxOOY60mZ1mRuZoVi6AAV8ujhL8yY1G/gMavYT2Dr76
P9Vr4m3Kqg5TADQ3WNuXGur8O4a51HDpWc5y84PAYt5++k6/eR4A0mmjbxzSI/+8tvl2LxmHv+rw
1qIEhvIJgaDBseHM4P5Jll16X6INY5YpQhawxlapgLlefVjKXq3IS4cLj/38uyqt7CxSWYILnCPN
ZD0Jo6aPxWW49NHu0OJiZy21M6zfUvzuudVHMDdIx9jI6xCLJp8dNLA/Iw5VFZaDkEnGgaJ4xy5r
CAHcs70MJDxYL5X7ho2t3sn4YILBIbB9dET2b15lxINd3OE+5ypdzPGKbt7IXEWtsqE6QxdowV1h
rx/omnm9YYNIJYyIMl1sik8LdmVqYx7pYueVpLx9nBSUanDA/W2nLRuSF25R1SNYIp9m8VrMD2xJ
fzrsW1IbVWsQKocAPsxw17Qbs554YtSQ8TNLBXJ72H19e8aJ9fGNzKVMwGP8hfqwKbguGqqve+M7
O3eexxdLOcvHUmf8QNHA7MIJLl/e311bQ7G/2atQ7UD+wiEy6S5uQRMJXSfPX6HwDzyab0T8K/EG
8Seeem/ZGx4qpwG0AyGHjKwWL5H8MvV78qxCFuiIkMu2Ys5fLY9NuEj1+qrNG5KCJNpsbL3VtJac
BnsxhmL2v9YaJXcIJI6sTkc4JKOIkpayoCPkwJFuU9RWxR/0r4N2bNhQ6++AONbAX2n+kRfGZEcQ
JFYNiv2V+zyNDRqTxucyJAvTmchJOPZC0g6ElmPDdriyrejsifpeSzCOKXs9ne8gpvjGVbOcepof
XO8ECuHEeXug7ceOtN5DIGYjmwDjOiBuK3DF3hZy8CJKiwu3cJcLrfmlVn8eufzFloq8BwYpn5aq
kEYzYAICuOKQZ7dSXkcmZWeZeX+wM0vnEyowIPDh5kSeCR49gsp7ewxpJUiz0UibKnZYPLlhZQt8
0US9Y4w8v61GA+sYwr7f/phTGe9006adqh2KKTkwbScfe2h+bflrHIWidIzrliIKPHM9U/qcomIc
X5Wf9hrYCOzSj+7ddB9Qt9JfnuymAyUXAuDIkCfehJJ9WxQZ0ttCgILF4dnFdbsSbEM35cgoYuG4
9WZcCgWFDdgwuq53sx1vZvFxjoIdJfxY07mARceJKchpCH0zBq13CSWC0NeegeBdtSXsm2yUjB50
8Lre3Ifwx7ZFetRu4tauKonAdDjhYf2OKWbIBg9bkYt4bYNFktdHG7bsjX9tsUoOwRqMfXF9m3HW
YW0rX6lF9pKQX0l6wOac+EaiEChcnr8iSDvTmSjAu1i6ASlVTj/Y2+AmOIH/1s+HKAXX7mvq9uIB
7mWf17HuxPKgtkCFJLdMOX+UYuLUS6Rr2KOvHwhOTBUz/OCRc+1VWuKkvIgTcQw36w2gCiVna043
8P39SJt2aP0GpXmcpk890I63OKbi8NX6L0ZO+ewhtkVEB//Po77z2iLjXCWrOjOH5NnWedsQFiI4
PI41KuaHbR3rdU3YG6+bqXaox7Iu1oHSFhARdAZ+gIPyIpppKwqaF/hfTVTydu6U9xny7Oh/uorH
JCZki3yku7OzPtv++qO3Nxd+tynQhW697SwZglrZ6+jVR/4zkqiRkGJOlRpI0CyspuVf0s8WL+RQ
PSB6PCgUa87/Wnww5hls8G0tqyMxsLMYqViaEha0UJCKW4MjvnlH5tHSrr4qbbv00zSV8nxIJ749
6ZjCrNZVuUfRUhrlBH+I/zRMXrdJuWS16k0mb3Cmn24RXQziLrQfk85ePdD64K0Idp0DnSkrWLZ6
xd9pHcaGoVJF+H/ky5JAZ3EZ1YNvBqDWKMzExOgtpB9L6h39K9NRQakpgQ61Hd5XEAzp8CsLWK7P
sAqQt5oKZA4roSWgqvFNpdVtqU1cWGQmWeu7LNGq77HRmCYFBki3CsAPBSrXentaC//aVc8bxgdG
uvKKv/r0P456eCJl5qMkfR/sdEBon1+IBYgj9LGOkPX5KsP2Etl4yItW21iYIqICJRbnA6Jd6W1s
Zo/KAA2WCxNRE9rcvIjInUPgipDZBZ1I0+78AFOjvLCt7uPulNfUacAU/j61fnw3710XV28wuauo
lGI2gRRbtD58fbRNeWz5hlLEaOWMSEnAlVtdol+gyAJVqRj77PE/SnbyrU4go7x37CjGzTHkmUB+
2hqiuWijq9Ch/GW0a1iBwgFmwOmo3n9QZa9dcjRoQrShlQb+5EqvtERUrltnRwG0cQEKWpI02DDT
2GFDbgwndsiK9EsKxsZ9lM4J8KidixGivTJziA6Thv+2neYH+MSRBBV6IdW7SCkNsK9oRBEK9ZrE
eU0lyNDO1rOpS01+wFW4afB368J5QZSnQVBeHYNUaXya1JJrUY3MJ9gEsWhlOX+0K/UPximQ/WCA
CMimqAg60Y/SZGe8HUisBoSxsINXhr5rQan/BAdOEkKA26GGWig10uTK2IKd5pDh6JtJZXlG38/e
DunhcGjJPuvR4lyga6Z9pfVeKjBJ68ULQyXOX3hWP4nAkbHjgDMX7AbaEenwQ6Db8S9qxdjOcNvq
kphcDMvvwPjRt0xehZ5Vwf5v4ZBLd8x0Gq+zwsMPXci7xBSHlah1VHbeeU0TTRdFOmdWWPIJjGnh
/ktG3oR26BcuSIW4UXbszs4wZau9V4yxL0ikkdX2AcHjOgo+jYG+tsVEkk6sGGTDE/CURf/BOIwi
pNBrNko/Wrjs6TcWu2eEZFtTpJ2AasdQtpuLcTaU1mxQb+JNyY9ybRWAxlMrOtnzp79t4BObsG63
NzIC6ijpCMbHbyiAbFUebTesLJFwGTURsB4aLX56SOdcDlyXAJ9bI5qDmLwffSPr5EWg89IOO+Ah
s2/JiL3lY6fwDfDITKZrPrOG0ayvwiWP/eBnHsEpTI9LkvOnanBao7t+ovQKKO8OaDwTpBUFKGit
2IzrBDXKP/aLp0co7RYr9o69Y7pJ5GH2j5QbLEHN8YntukQ8+QCX5CXQZdhQRcEDiXTV/5K0FwPZ
LKq7DaW4KxEdK6IiY8emvQUQZnf5jIQK37GrUab3vN7Nxd+298SaMva/8cM6Ha82jrD696ElfHY6
fbqRTidQ5f0N6F5Rvv6eBoxam2DyqdQkJAmOIqP9r+g8V9yaNJJpNxGPx39iSmMixUGWbZuLCgMo
KXl0T5r1TLMVXDVnFW+UMWwwgeg8afCaxh0xE5MLcv7Cr9cN+ZS7yBWAgHA1tfwMvneHyAcla8SO
eCBjAocJ0/WOYKH4YHFXK7bY8b6oSWE15FM8I/YE9oevlkqBdZcDSgLuSA5wOdQoVw9NxCp8U2f+
3BVquG3I7jw5109uapI5pa7OubFRLsBkSswQ/wnaMMW/EJwGcCvEjdLlz6GnEdQKXTsHYZsftEA3
/WaKCtM3YWfY8HGcDLI0tpEVl0ebdwVd4PeB1kpN5G5A39btQ3Lcjx7FLRB/XCsQX07GoqhoLeGC
xV41uG3lnfzIsbxYCmh+MVXVc51RoXOo8Et5kAQ0CT35r1hlF+KVDpbVSgUsEbNvIf7rAFYGNm/M
EL+sXCHxqxj4YlZbrnNR/0DXakTJ/td2HGSMLdvNVuSO9ewpIU8Vrg8Qt09dUkGZvh/kRW1uLPSA
uVD945WRTd8gMJU16cRt/WVGgPndKxpnEVeOtSoGJTSLYMhKU+aTTzssntTxMwT4jKrH0hFcgWBJ
/ntWGBGlHCraxdajwPZ1GnQCxitT98k+wu4iwINJX2jOFFdTl1CNSZCQ+tQcsRaKptEd5YzGjPdm
8ZwpPMe2iAMsop3C3xvFrKQ98JMTGf+5b0dEjZ1CK5JWGwMY8TbMNjIPQ8xuROzy/0BxD98gTMys
Mwy968Zm/h5MTEPZiL/FityoXyfhsozuZWvNiGuO46Xv/URc5FYPGBcUPbZH88ygyHiUCx62INOz
yPRJfo6aXqEBJZi45sc6VrFx/1XkEMc1Njopiyv3PBglnpwPtg4FFolgdFWKJfTOluTVGAPMo8Ov
UFVdwDsfg5sq1nRF39dQ04eKNGN2oDr9ZlSKbgrfmrw20DaYvi3IncZ6YU1aQ9Sl0csWNX5AyFo/
hk/6A4rgAwxrwhS0qurWS2FMAw3Cpk/9APWapp5/iVJmKBsRk5TE30Afpn6wziYqvlMnBjXeEBBg
Y1BhEqyXhXb0QLpRnxCXhzrfYitVv5fdsaASWkHd3r9AeyqJtYJqsuVS8YSkKVvN36XYQeVARByj
xqg5WzvnahX3UaRH78icD4nwspRNuvNheRlrpppGBCc2L/EmvaZ+uGQD0OhCh0nH812bzL6CWFYj
OlYnuE+nhgaJUbZK4s4DlX5ChDUqAi7M2wahFeRgPzz6gpGgwPElbH68KwI05GddJ3h7l3q83Xbs
6bZJowCjqE2Ya7IJmS4QhLMLl4+6MDxoT6SOafq0cn7kDLwoqMfmB1BDqO7DVFyuchRAmLbZFqmr
sxDVcOBCQIagB8XAYwnMdgjY6YbO0dmX5bOUh0NU9rklDP3c2eR8thtGUbykNEgtU8EBFfKv7loL
aSr7lp8NKZRPmAf+Z75vq11IxZy2xoEHvVTqkFbCbuLL/lYb79bq3Hurhw169MDZ2sLQv438KCdj
XTReAKDVUaN3/KmsC6sVM8ue6FgWed1GMwE/bnxKwSzJqoCtBJJ2fH9RTw3y1ImACE4WLjZcr4dT
amsFp3FfSp4E9NF0X+IMfQ/ClLYNuYhnAEV181Gw+HxGsOZyyWU5f5efMWLky32c5xB1j9mkBA5z
7ffo0it5dVC1O/Qj+TttMsP1rEVO6Y5fJVN8A9qlUCUS3ZdYn8Z4hJusZg1IKiXYRh39johS5ho6
c/Z/LlF5KUJjmxybiWGmWCYUHUXgkMvIJQEZElC5xQrFFlXDFkyGjToqcH4viTXFntP7kJNiR67Q
s95bGfYBGlK7/HzF8ZnrJIJuefiuoA4Z8JLpZ1Z/mdPbA+S5B1Iga4iQAaU5XcPfX5nyqE61kS35
PsGwLBSua3VLQVL5PF4oWRA6PV/Hqbrd835IAD2zXSCqVd8WsTIXfbGOUGTXxXnxnOwI5msuP1K3
V0dQhdGXXV0vTUYyOwuPedBoy7a6felH8c51tykaEn7ug0aCBvUgId7TrxFqY7wIZ//Hzgkuzc40
nIBJJuTsbyq5g9ZgEjdCAPy140Y72+VEjeE+VcDyvq2shjLV7K25bJtJBqGRVp2t37mlJR+m1QTb
GKwCh5pTWE18KkyUP5cWenyqlMElH5N6lqEJ7kGnv9H2iIzy6w/69uSWdAepwdgtc9f6ZweC2N6Q
pNnw8UvH0aYPGqS2PPLWyXOxjjvxSIbKCkOIRSmro+FHU/lazuPzKtwpP46n87TI/q9N5V0+YHeM
mZcXwCGF2g6afJRu20TGFvNs4mNHB6BvbFLsbo8nvj/ZQw4tA5Nxq8uDHLTzmTfGqdwaDYKauRc1
0s7jtoFod/+yzzY1nr0BDmhUeKeJ+UB7y9FnAKTLxwVor6gAOB5Oo+10OIPYl7Q3tY8Iyy3rEh/K
7Av5ywm/sPoupDSkqvCBEDlIyHbTnYtUoaBjUfVesYAe/z7sep3X/Gfar0hStLyVNEZUkDpBbM67
K35gwA+WsfmogRPbzcL/Br3XZMswkyK4bwOjZ08Q0GmLF0g9ufrxZnnOkCWtfThrytV0DE4J+TQe
mXHURLFHj6b6z0RoUx55Idbz1W24Pit8w8GNUXa4bXhVLzM3mUpdEjTevgFwFzIZxlXftVZKKase
7H2lat5i94OKsQtaMEv0i3a83SBjUrFO9p67O1i6M3wCYLOhgCB3pVljk9qKblvqCqMKQlcp4Fk9
ZHaszT7QaFCknzJgpWLTkw89xKlJ8weuKy//q/6AXVxBwL5vwrK+YSJqnZMIfVq4Q73UYdW8on65
QelJhoJP1eqxh37Y0YAneZ7RMKrBKJ6C59/iC3sfvnIvhU2ReVMlNTVCNQY+PYfsLH0+3ENFJ181
AUWP3rHIXyb1JU6F77P4pc5krQLUC+maKZ6RSYTJrwLm9ekXqWUW7GQCDSe3uaUN9E25YWj5TPf6
qt1XXDRqvkSQfZ8QMVZECyXRqxAZfWOBlfze/yjbWuao39Z6WJPOlETbvfCayRfdhPVfD98McQzm
7FuiYgbyVQ/oXe3sbsvChcZvJn8fCNineiaP3wmKcFTT0KcuioGhz6Pa1EeYJIpbQsnMptFLz3Cs
YPM/OYv4+eCrwGUzIXbobHIusyNBtp2sy+tkK3HJIuULsZqpd85rjIu5eirUryOKaPBpZF75xmaL
jj4jrnghPzQMxhBXXxGZ3Ak2qzsgXyvJBE2HAe3gwaElkuYUIsl5P/H/3t2ryWUjvNKESjLAfGfO
5ws/NxD/EFTcHk8/rLpyAkgKzIEi5GcFqUHsNFjQtfJutPOGWsPVFQDK0Wq9ujx7jgzGUaGVmuFn
ISuO1E7pjNHVQRnJcauo7H+VonxEfbYhcptpNi6y3lvVIn3/OO9BQdWpIvTp3Z/pLR0IO1E/9UU5
F+OMCijsRY8Uare5IYRlMUsQUfAVBYXHlKWfS1OUch4ELrwc7CzDEecxRlbm/NHIBfDKWEemeR6w
K5cObTILvopp6eas35w2AJbfVjOIHtoLzsPiJkGIhXJLliYWiOp9BvTIjATqd9rv3v1PFc4IxMMc
YXgqOXCXAW2dJyMGCYdd6bexsCzvvfWK9X/IJsPgDehk8IwxTldBViRPz7AwWNQsDdmvFvZCf9ul
5GC88YXyoT5rjUEX1Le4hD0zfeYrpljG47YuJ3ixQfhjTP47vzt4epwbvrt2t4+Hm2PkHSnia7X9
NXlR8Yq1ZfdK9N4QzbfkWW40vHNqFX0BtSQduHNzQ7hpiSvF7GAHGFUgWS/RZke6jVAe1F7+OXWU
BcI34J6QhEWQvQy5Nb9WftZ860oEChyTsd4tv8XSznXhq4vLalkQU7jftotwD4YfAZYUiff9+5tf
iB7MUY2WBMOmWYUywRpE9ffsm0Szf8P+TMiTGWEJpJbdlDkuZW4oGIZw1PR4kbpJ8ut80VDcRGAa
JwwpgibnWYNkK2sV4s47TOE91tdugvpb9Do6XZd7pLpUmNekr31fT7Ay3oTvU3PoV3fKwolGPamz
8ygWCgagNYrphzjtNpnTNCZb+RHPf6vqHF/t8LoUoRkhD6Lr0uZOz6r8RPcqMCMmSkB0/shTmZ0G
9P3WHB9frele0VAqPTlc7cNtqTE9MKxKAl12aA126UZE9rSKHY+kKhbZ/WODYiPvZV7wUHlGWjZJ
BG5OCKczXzyFdkFxagDvm7U+WglQKLQ2pj5l1ZBKJ79lMcj8UjNwEo0djh2ZdC4d/Mk2Mt0Pz2Wt
dJb7EpEEtHWTPf8DKIYQcINso5oVlG7G3aCgZRi0dUbD+sqkjujeozEXyIn91rA1+FTFZbmLnHcG
Qq3PBtSsFx3wwqEUaL3j/8p47UjtzRsoy4zp16ulpubmEFWQBsHmBbRp+fvuBPNG3PskSYwJsl0D
zO/EfNtyyZZZGFOAMw2KhGCcR0/8HzOmFDcpV3DN1/RJngieZKGDjUJCf22+1Mwvk8HLbCSPQA2u
PyP5yftJxcw2gjSPWUJxLvCZPmYHA79bAXvLbDjSqA8ljVSkV8B84/aQvnHkX6RxUXK3qzH1myjl
/Gv6dLd0fgk4cHRbjmOgN47S8s9fyxnAXFpJbTfT/+YO+Umz5qC77to0u7Z/Po5De5NA1coZyoLw
TureKx8pylSfdRfF52i3P6QVHUcpUTdiNQh7ZY68lrONzrKdZDVY3N08uaUTqyQ5J5HhvLA1Tq2f
c33Mx0l89UiTUR+YYwZ4+hRA2KSsoT9/2k7YhKKxmmWMXHjN/3yT49mU0E1DaQumhKFWDlP6oC2L
XHJZmcJpoQwuGJFcj0YgMlGq9cHBijUFnirQkhZtjSkPgftbwEEQhz7ej6ExyCoHXNi61XSJGl4K
A9Is8LG/0seTBgQjqprDJH/Q8iaJiwffJUTGRUjiY6oKTXvmvADOAHVqEHVTpdpvVhTNdnxNi2vM
l+43Sx/NX7R9cW8eaJaBcxa3FpQVt5hTLhsT0KH8lx+GFnchXjHXynwgpc8sddtfFp3ck6BN75NL
GC1ERs2yP4N+Q1bgoMxrm6WUbV9KS/ZmemQjAFXMJtB7+RzMndlYmVfQQXHbiAKuHJCEKWSDR7SW
Yfi21/OD8InQpxA1p1STyzo8gMM2m8AEqFiqGi5uxYPpLS/1nAcY/r8mLGbIdUUZlrTO3Kn4Z6Zv
d5fsNg3fNO71EMfUImyWMfjF1cU9S5H+Kjnh+rFIXezY5htNqNehmT2/QJ1/hssjtVjnCFhxxy/9
8DBWC6OqG5/LOW3lQdGyL7INUvjhP1fk0mq9NlFp3WG1oUT5ioJvGHVElvOwEG7nMgvhPsnO8MDS
5L5+P9o9n1IqPNj8snL6ncYDM3GIvq+ud6Q+YEwv+saEol2Jxx+N4ZVF06iRAVpYZNdJJEXirJli
E4ngowb1/aJSDlebWIqXwFiJ6dI3IE5pxXOhuHdIu50cCM9ZGoedRbneqsDBQuKI0PXxFLQX9QII
OJTgXkgOqtSHJZh5TKwzPranNNxhUPWtBLLc+y01VPyFzIo+UHb6XmIBV+jWixhET3N/BE6FOpcK
3sd/dw3KZdJSzKYNB0YhVtMPwKKLXDbhpLJ0yqS/0p31cVjngfQJDaDZDggkXfvnpIuAYVrwo9nu
HGok2nHHdMt7P5RSGL4iIa2vNcBs9xKQcnubG0bW8tGz/f9l6iqNraz9dVmX3cZ56jMQJt5luUb9
JkfxYj0hRUBS2K7VZVBlZsoSF0gdTbKhEouqtIOSpXgnpzQHCIv3jvPvit644Tv+JAHnMOm17cSi
hGvbVoqdERmITxDb/5YvdLAF/T+cfuzfJmmn68pin6dt+4tIFWM7S/rOU8qHyjAQsWVplP3bbpoz
Gz+rW6SNZHyLIlrUvOGoyC8TQ9nRhkInNTNdHMe0GYVQv4bIwuQ+4zjbNurFggoa6naGJDe2SQ3k
U2Ibrwck3I7tprxrUFjdQyDxDrEsmE6XAZ2MstHG31jM5GR64XkDHe2oCasgz/ghiLysTieV0BCC
vGI4gslJTqCrCzp9H+r9UpqL/+YGyQqAT4OfKk4uOFHisTBYNmBlwlVF1xzsRo6nXF3FQe/T05tj
g2QQKSEkz8kK/Da8lZ+FVv/8gOKdRA8VAg5NQQIKNhXjinAAAj0XZLdpCvv/MeZKZeLBOH0BidWJ
bRG6SuQBmJLw9+QairUPdB/wMSDPu4EouijCsJxOlaC0lEakr2+kxvk4lKGXEGVSsz94k22u7D3c
hey45T6XdUslVJrYiW0oH3Cux3SMu3uynuSE19QFo9m1jLMXmE1tIz+cT5HFTSGTvI1Rz0TjJcB+
bB5w4M0FZs0nf6+ZphW5UWutvaQWUhQ3lMaSysQqfrdpH5eFA+JxGn8XszFygtFxiOrEBMnnnl5r
4Zxsw6E25R+kIOdGPwgo3R3o5gBFc3lWDL02kMLOYoj1PwLG9uQQmInhZc4xMwvMoSALWCS4RAdO
UqcLg/WbqIE7O8BgT18zLwN7w0ZImZ1FoHu9WpTfThLknUyMC0I758PErWhaoW0+TT17IxGnKyLK
hHWiUo+5mhR9yzYg+5vJM+6qFf91d0YlRCknVWIGuPqxf5RemF5C/DWON9kOG1yaVFUV/4LndlZY
qA52PEcFk3L8vYdCBtpvGnLFUv1VNI6Uzc+p3EhwQt5MwO4YgRA+CFN1bHFlRbL1+JA9jIUplGYy
h7NzqPiSpsxkgVRU9jq3+NH4XEPWrCdj1MwX7H0xNQoNdLxu7RV0cviHtRqzZ7SA+IM/SDCAO7zz
8D/QFevIF3UvwKu+3g8WM+WBGZOgoIS8SNLiuWncJ8Oc71P1foeK7hgR7z2aFOsGyRjpDLSkeg2i
ubX2S0gtukwbYU4wprlYlDP0+irIutBGYm2YFpLWtmPLz7OT3r2j+X2QlYjP/3tvXOCi/owMXYKH
TUW+ZKoJdh+fdWVlq9KbTgCCyzBwalzK8WAD3KuVPo6FcsfUJABE0Kkb000n9HL7D1BJ2uZlaNiz
Uz3se7zvVYvWlGNb9b1Nz8pjGtfB0A6AG9B7FSXmchYUz5iLG+I6kyPqfBTRk2lPqGWRCJ/l1EbR
ZaZXmfdFE6oY3Ojg9SsKZWi1Z9qFdVz2CP72VGkUi2S8U23g8wyFCXJfZdKXNGchxGFX8rubcnYP
Wha9DjxND1JFZSPpCQcrxaFrgmTXNQQj7gtvOeL3gzMMvRsCWj/4seUStQJMwL2ASqYflEaPaTS9
vnA/u0l6xALnw75+ID6AdlZ5ObpY0nNibs6I83YjssiqS4Ro2AZn3MSHSK9bx+6upB0XWzAVMQ92
V0n93KTiJ0GaCGtkFMcZU25anV2Qn/axzTKJV3YmmIywJBHVcEdivRovtWRjuLB4eYpkD7EYSizw
1CtFiu3sEab0gfFye9DH5/3YChd1JvMHUkWCrP+ujb9BT4+K3s9XiBSth7rt8VuRIu4g0DnNaKO2
G6oWuBIInSTF8iH9kSro6MePj6UNjyV7pQ+ZmZsfuFXcZ6GtFB2OAT+fRERIm1HVy2CEbcWfi6mo
bndDRwrv++GJ4pFlceNrEPzKVeiiDavjJ2EpBNwNKYn+sR1e3azuzQ2h5YbdU8vm7FVyv0s6qcnE
elt1pB6csWnxek3rggYNYvBQkqitdOc+Vv+yNjeYXX60T4wKcrcBqt3X+1INWW3Dw3femy4DWAN6
6dqUtukAEyAjDnzQszftZ2PrHG99bweikxtBjK49cKoHo3YR96T30uOFiIS+Oy0p5efar0HGbzGI
IL2EczQDqiiJrNSV7Fg5UpbNkCVY6JGZY/32tRG2/r5TbrYpLsZWWoZ3L4f2yuekB4dqS86SwGfI
83iBEl/EYBmcp4yzHR+eM2klCcsQ/1yFLsRNN1DpsOjFqok2Ujv3R1ZCH1qSibB/fEFl/wI4RI5U
NaCIaj5W3zn8+/kUIHeKET4q8FMqBM/7lmEsdnH2XJlXJtTlp9G56W4YjJrjyG7zFRa0k3JOLISQ
Jv0v4LNNieRPB7v3a7i97nWXPSvImoQFEgZbfzBkUXSREcxKofVgpbVc2UtkAvB0imZuwtTIu0hJ
OdT2m3HaixM67xArk2C1CVSeJGvcD7rkDDNUnkOkddbeiNdABJvImLLl4Ft5K2wy2Msa4b7cm1ab
aqwavat+SJMTxQCFUu3rT1OmbNzbMOeSffZ6320+TloqtwA3w5tjYX0uPUZ1ffsIY0ouFk98d5dx
87atZdzM6+ahKnRCiB/R3q7xezD/WAHLtAAO085ReNhZxMoCyEqqoIcnOKwV/y4I3zY6WnAId6bC
fnaIr3m04s7aN5Lm4F+5gm1jqG3DtZvoYRW1VMfphcLCfkI9fe5gUfp3c9edhDQ/ushIUPmDGdtF
Pez9r5G9tad8GWy/tUbSkK2iB5EwZMxstbODWvtO+Gvb9IUi9maLWfIlFciU/U/KgV9SxijYAhtL
7042kQKpEE7GRVGUOdPbu/OvchibaSQYASAhY/FW8OizyspxZrFnysZ7ZYY5a7zeFgRItYCZEWrd
Xsi4gE0qf9k2G9OmCEEUTWMHAMpSignw2DsveOwzX9sQ+JzQE7NV2jnyNI6Pmz/0HAdm8HyMe7/5
AD2+0ghWJK2CCtdM3LcU8+eFSVr4xH4F5wAx2mnH0zfixczDORuz3d6yqJzTeCIlRPBeZozCrfiv
V36age2Dj1Ri1WeBZejr4rjpnbB/qqYHCfHykRxOgTXErL7ClL1OaKOmQjwS0MCKqOPLaBWUBBAm
rX3yXwQ/q5kMHYTfqXDxnmq75Jlhop+ML4GpWm8p2MDeKjYsvXRvi4/uAs17a4GrCNVkyuadCONv
c4QrbP8rvH2WhVdAM2iUJk8ACn6q95e0VpKYoS3bXGnw154BvkLkhovTDTr5ERXjWrnxWsFQ103i
FpYt1JU+/kpiatua1RBsRS61URdo7v43fHuUAJ3V6eSAMsAxrIDRi1TOlqkvRptTAbe/1sm7TYhY
F7A705W+8C2naLvR2S+DfddRvyN0R3wiqb5E7QFjd2nfzOhZ9T8mSJ+G332TmislO/8qoS73GTOX
9e+1we9/QzuO4o6jnOELXhgqm7Fj6El7cJ5Fc5KQ1jSVREMDxTyG+bmkpdM2h5mvIrtvqCCyBGeX
cDHbh5Mpi1L9zu14Cd8bsUtJkScZ0r01qk6MZVAAm7h8GMMfRY60LLnYfIXdQMpbNwA8xis+CM3O
XwbI2lo/X8xPZ4oKRy8P9Ep+Pn0UU5r8ieZ+G54cfOk3G0iz+sr1ZvbV5jxShxiCsG2YZJSCNuiu
uYIHUSw3bPhwWOxLCZu8u3Qa6CZn0rWvgcCYcrz/L2qSpCyClYP5SKgiVS6AlTGppA0I20Z7tpmd
4hZMeJWjpaXnYZVD1qpwaX9aByEkAt3oRxx85YP2KVVAPxP4XlWNnbT93e+asu2z4nGBn+Zo4p5Z
AO4R8kP4gb/rk8FmVucT8k3RFjoxBG8JIqMHTKWMCfXor5VFKIOJGI56OJibTS1PRjrzoETk0OTt
wltVCGR4/TfkWpKIaysNt3Qm7qmAWlURodK45Fubh6GB4q4ORxAGHpjBuEbleau2a0G8N4brIfg3
6bYU3K2kRa3omFCTjjRpqeb27SC08EXfmlzyeJ7QEywFnPSR2SBBOao3dR/vP0Dme3/ZXQPQL+Rh
cW3VLgru26kNG8Qi4Sjz8VqK7ctxJ/qqh4tqyto2/BtjBFFUkq0TmkOWVOf/3GIVJFIEz9pnXcpu
h+0XZpoG6Sh7obG4U5KMGpwH0MmnQf/ssIILv4bog/UyLgW4u1FMkpXl4TxNEK/7EXMNuw4AKJsf
TiW4vXNZTmVICHkeHQdTq4BSQlHyF7knk8l7mgtmlj5r9BFDIfbYNNRztypqWA2+0/U7DPcCAjXO
mwz4A+hxDX4jr2AiGU1v2enhbrUcHE2/+HNUfqzJfzimJyhHW9LJ/L1QmDdivPOwZ32dvNd87bPT
1Gu1G9cFD0xdd7nhhYnc0UR6TNAEcDjGLj/q02mfjvXjR+3nUI6L3r4+PHuLNoOgz1Kg5Nd6LXGS
MNnA510ndZLihMjayEWJH+QS2lpOeiBGRMMi3qHSIy74D48Rhn1InLTlql24VETaguheEs61BtQA
nLNwu6fERxbET7APOxPfwhcXG6TIKI1T3VMejDn0yhEtPGRS/eSwEMNk42I2q06/LapzNOwFpRbR
9kBngZuz2lFzP/JM2eu/fAYz+popjiDygB6DK2Znl6HRHSydCOBYL7PclSLybS4bvaTxq67zegN6
qrMC9wDrC0VrQ1oPB2fuLJtqO8DDzG1bPML7RYCs4HhU+31fhguLLN21xIZgJwP0YzrIuxBGHHCA
zrsUAukqgsaHjCDgEap0rug22mYR4pZaKJRn4bIiRw5bydfyCPo4IM5FAycbSUmzn6P5oZo4VK29
+mA3roHC3XYkIoYCkLopb7VNI0/JIFyENLnD8vWlA5RBy7j+oS7+GjoPKeZg6fBXc6kQxrbNYcty
OOqa2N0bxlUby1UwN/xyl4Lv4Prhe3n10kQxCzF8AaPO7aLYW25bLp16T24H0R+1Wp8doRBloZcI
j55pHxqoifTFTcox7xxo6fiZLeEfL2BMsU5xLXLeAzS8uUdFYlzALox4a8xWR+rSdX5Ra9ui7h/l
cFIOfwo2uzXrE6/BrEsDm5YlLHJce9gxr79gP2eSAZvuMhh+PuklTDlHcHF8CGMT+e3WG1MHadOt
5HMdWTLxX19WWSe7Wru7cLqXxjWIxIFpBQ/0kxQd5Gpp7gi+iPut8srs0YHpNRQOQ93xx7zS/T/0
KoEL4GKFPBr9x7r1MU0YbfyqsUXMHAWJz7zFk8g7hPZoqplbOrzPNlKGujRasUdItRlKsOxHeu+f
X/O+v4lXWfAGkgcYXLyTVMnaEW0M31wfn20VapwiwLGGwSC5kjSMuBiFuNZmZrxcqsjodtrzUoZl
1Xvh1ruLYjqKJI85jfSFIBPmcBjr4AwTpNc0xk5k3y+BQ897ID2/jnJumZg5xEp4vckDBcoNSnxu
Rfs3uKP+fTfPeEF8SUMuCflqCXx88b2trwCVLrI1s+a0xFs3xMcOhVkjaC9YPhI6cM+ywFE8PyGX
6wEhx9w+a87aiLTJUcyOIuzW5JgMJgokgirW6JvhVYfX6kxzREVGOKOpRE2b6+M39+GqoGsyM6ko
sJCaHjgfEZbfBHuWEg0cMPc0T9Z8jov+gkb1s22C9ejiz+mZifo/dQQU26PcILLPUzswL+6EnVzX
tOPndBJ8+Fyx8n2MSCqJrBfrqHahAQR0yaIR6vQSUDyOdOlbN7+2p/0Nx9Uecb3yWyBkMgaO/x1Y
WoxlV1tYJl0JyBenBGX5UWVQDyA+PRazoXfZveMVKui28q0aZdCdovnfGXluHmuZX+zB5WqnImH5
bqApfH+LsQ0oFKCFOqdCh77JHpHJBQooRpWw0azfGDiB5pziSfzolXLeZYbU95z3DZRs+xrMcolJ
nnRTOkQtNKt1qfjofjL43Lwa4ZWkqm7Y91ts5iocgC9H/E485VvHw4LtJJ5FNtuzRIkAmKXbNfpT
pQTPoYcYkjLFDyOA58wTc3wgHaN4Vp7iCUhOPk+Z/QYJQmGDi8BE3W+lC0JmB17vdQrROa7IkjHD
lxThJrBNP2tBwlU0WGHMZ8w5kZ4IXAzdDwd7LjzTocbpvPGAUSl9VPBmlSa6cXP1DGM1ZdRQFBSA
clSaRicqaCX0ueaEUxhCBj9eGtWaWwuFBR23NIlpDDKfXTVvd+EonM03oB0omdgOuesDMRIBjv/1
qhvZR5v+M3+H4iFkPJDGBxM5yzi0cl+tUlNisUbWf4Co39T+Gw2BMkHkTYb6c/MtH3mBsUuuBS6A
xqNyXqH8KDId8xpdcE6KSv6S6ge2nhWd8swMWQbj6WlCgdQGLZP6ekoFn17wSeK3e21cGJnOUnRD
tZ3snd4vKl/QmF1Nhu4LMxgMpeEu7OWWK8I/lnrqBJZfgbBRccW22mQ61dSeRZ6AIsyGWT0Upbxw
9Lwe819BPkji9rkRHl3rChOJfiKbW1BTAXfpwSCEl7pATtWTtzM6RYI0QoIO4VOI/2rfHup+YTNn
m7WgbrOqXVhmfHnX4BVYIsCklPnjAcbnFL+Iy+b4gX5TwHb5n94SVsEgtSvYQVrGzTbf3ZGQW7kz
TdjMH10mmcT5uOrWzIYOZQrpj2C1lmzCGlbAMXYMIKq+q4zNz8lteTK3sPCKH14LZOjskJTS+GYw
+hEw47FA8LokTDdh2KNo2p5E7KoNA0AVYRtmcE5B3AEQYuXfz6/ejyjyzmEoqyWlcz8rdu06k6sK
HXDdx7z2O4Pep8IfrnditdeB19TxRuck2mMIbj9TVPiPyTB04y4EA4JjRbJVr6P25CRrhE49lFJU
KEG9Iotx5OO9SVsAlO9sHPqw4uRBYXgVIbRbnn01ahrk8zSoAt6xTop9mQpfqcjN/DnZ717ZJcBo
q04R6qtmbKyTjVX/sU8W0heyQIltEuuj1SmITILQqx4wrpOq6uE+EkQ3cErprlBmlZBzC9PYZz7w
Wdm+u4Zd8/+v8TSxjyaBCFNRhBvgi5zK4/Y0iJFMGebEbCEH00ES0qaHnQBKaQrfq4tBZ0ndlVvL
rO1maeaQUi47rvybRdyY0SHOLKFNGbpqheLIz0xeiv447nmQ8Y+auLpLbH8O5XhSIk7AK/zwNcdo
wEDyAemsSpUNIt+YZUUuY+iEleYHKh+WIoJxmx4TeM6riiT1S6CBvxFpSKE7jwd6pB6GKl4/eIUM
/bq4SI5B+bBGpcMYCLu8vCyBCmnCtA5GP1OrA1Ahv3wBt3EAyAAk8pWX2gyDLgxC98raeu8LbSg/
YJRlJfz1i3P4fKYZT3zQn75Gx7RG8BlIyg2GkBSWuvYKFqvCjbwkHp4an+FZbw9PFa+U3w2iqmUH
piBisywYeCK3HxzHg3fCb/9lclQPwgEnOj2d1fVb2JETjMleNmnHe6Pq5V64LUNQ1fUJ0NLH+yoM
bqs5dURcTEP6Gbbp8IcRRcPEiKyslu/kTCEkmACAbm8gz64bauxYT46Jay5fQ3U2Y9M4mofTQOU1
+Ns3jYyQnQVd5oMqIFrZy+U5vWZ/YPTJhpIHgWL2akOPGOgS85TfOxTasVOYKA5yoQ0HuN8E/Zrk
Vwyu9B3s+qvRE4+dQTgv+igCO9wnuOFIcAgM7tMwvoXtPCme72cV9K0Ma8axHnSWBTStSJx49Lsn
eIyrjxzjdSbwczp0g+1ponJ1yh3YzXDQYsYE5q73TVraf+6CunCgIYKq/VPHsKf5HDTNgC0GcNRS
BHVpGh9jc0Aj7n8n26gSqunYwSWlbOA7yDj6xofgHN558qSmRyccAyL75ZbNn4OBMUYp6J73dr0z
HPIRpDsY7bCFpHrMaPDr9YzhU1BBywWibtL/LhFR9viYM2W8s8fqfTgNjqZL1eQdrrEIu/k/Sn9c
cneZCDXSybUHOf7vnnlLdbeKRGl7gU5i1FrXd/6H3n85ojdSVcPVLVGF3I3wDnzdOb3/NIB4IMyb
F5WuvRk3UrZDXQ5XIAeLu9QwTJ+NveoINTbKZI71iWCVGe/RRBbQgeKWuqW7jwHRsBsP/vl44SLF
6AaktgvnKdzw6EwNFuB734q7ndsTiiX9BRRi1xdqZTLd/yEZybr9/eOiM88LlGl4xxn6lHf5Ou/y
Ble0O42c+tVEr9iQvE14mBA/k/PnHfm+89XO3S59wq/e+mdm1c+WQ9nYzUayJUyNZrHkrQURmgAe
vvE0Fzf9wxi9Ium2Jzd6E8KFuMPb6xX3wn3otHKPgJ75qPIrc1q479yjibl+22XDo5Tw9y2H29Fy
JFfqHYxg3iKkNfnrj0r/VnDWiFYP1IIJfF/GWFR382TeH/pXm8hXM2oA1n+ga2YDtYfHycvkAMM9
4Dtwbb33V/v1mpphpRNtdrAVKRSWZaNwdDkePF6TONLVR3Qs0qrZpq0baZllI2Hn8tBLDLXmQxu2
TD+m1Pu48Tnv/+qymvzjrQGxTbyetoaadt/9V5uT9zasRt+Imu6ftsQvTKkGsvFjTgTQ+SrgabaX
faGeVRTisHTy2Ye/xmUZzAJ3RWESYF4ewGqc4SvVHcXRbsPA5ZMyW9PBLMsQA8HeH6HJzhAnL8cP
/Bqu1Oz7H7U5SBriCjAvMMA6SB/WRxkIcwgmP8xaPokKSJEKgR3hLIq9UVU6kZcJ4XptJZlY4hMi
Ctkj0Qn9Ek4+3ZxM2ad7PZjTytcZVgdZzJhFLtxGy+PVTY+1/7v/SFQSSyTprHSmEm9zQ2YUG8wD
2wiMrT3IwhWIThfvdMpTKei35rKIQUUnYW4TYCybcgNpWO3JWqkz9+V8QB2phYWfaiCKwaZFalWE
rQLrdAg86AMZ1EYJcxS11h7T6puuSUEM087gdbIjm9gOTGgieDfJtqnwCf2YF/yth5vVAV1YbTEa
DlDdWKKPIaYRmj/ji6PozP8MafFuUypnnlcn9/xPRcZmvjd6Eh1no0LH+Mwp+CIvatrhnuKH2FiS
JN6FU4Wm+I341tlrvvV+XZRH62o/5wzTWmgOfd0riylaxfOqTDOI+XjcXNxfYIOsSXe+hhk+u9Rg
V7g+Ui3NHDg+a+bGRva1Y1Mx6XB6divIDGicmuNPwDAT2WzG503xLU5PJxi1ayMAK5Tzarc26A1Y
9rqvFwe1G+iBAk3aWGE2uLNIR5joEjWqETVqhpqt/YEBNREx7iUXtmviPx5DgOdzoBHvM28IxOWU
vym5QYHkQinik+bZmSq6m7PGk6ub1dh2OiotyHTQklcfcGpV2dxEDBR/WvUJBYvzRcTCXxDV7l4q
FcpF/o6wwSysz+OWbOqqLYJkGnRQFzCRQKTzOqbY8F51IQVMslFAgl6dhwFbYBuXzAke7AsSkFjX
IKJp+1wbbouTJiSL8vLIAK3JwcAFpSUCQiSXRDeaqj4kn0vhn4V1+Gwcs6LLzFr9B/JE1Wo1ags1
54YFviqQrUwTgur0qr/iuaj8vM2xQ8HJ/CpXXLKXpuu/Zo1FOLgJhTIYLgWxUyUwFdK6x11/d6fX
1dCxWGDbjg2i/g0YrCSvHZdzrtzURqMdvEOHQy/LyN09AzMVU7iEbHhPCAsz+E/5gKf8YMdZOL8O
xhdz4ZtsV3UNzMuebDOPN1aw+vwhIYvzJXz7Z818uc+R2PlvrqrSc4CEXfQX/gd5tjUESXTUNbPv
SmZq3+E5GWwjDYzJG9tiwNqpQpV+EvEqJdta9Wy36uXeXcWmCi3Lp8DdCd0cxcPnvXkgKZPAVz9W
c1AKN6y0ftxavs8wYncLLUjIBRSXLtpUXSCTHOM6pjMfyp5DIidDis30D1yaPkB9XKgtS3uKCoen
EgpSGGqOIkBQgyShe1hShznb3MUIRCrGO3wtGtKrj6RbehnyeVs5kOzVBEgH2QApoyuok7+VPDrU
E4VTpyB5zFbRWb4aenmJOAzM6oCyEAOA9D/+Zfm0DbJObIoiZmb2Ash8J9PxLjmILuZQM2ku2aVJ
QOZQMbv7FLvc5+q3SgtmJQaaPoFRwaiKiJc/Ahj1cutdDFVdRnAMAa1aph9bNLgxKrYJr6Jpp8SJ
TqvVIJ7+CWUVypFORnNtXPtpv+gCzNTL8svi/NvTV71d9hIHVCa1lbdwQruYFBeZAimVjhR0Ef6L
oINyueJQrkEORV4YfLOQEp3aKlLuTUwEcAimkEhMUiGdsS/N2fSQ/jsCJ0s13eu78LUy8wlfnC7q
ebKwRz/Kh29CJEiMHY3PPJyCAKTTybiHt4XuIQKhCu52aHPJOMP5kTPfH8uV/uqxeE3ydd+4HouV
ivl9ocnglsKBgo3Ncx1C2c+vd9kDEtIZt3+85n/C6OOgJLegdPTpTBLO2RO61gZkqbeIE9st3bYB
IaxKnHpH1oiuNaofzIuFQxR+W+DkaGlzS6avlbwG5G4tPdQfx3be6irLGM6z3OsyS0b/ri+GJPOu
OtbdRF6xPSiBjKcDFG9tTyMrZGh6eMvOVy7EfkcpmVxyy4FIMMdho1QwogXPYxwMHB9eL6EEQYRT
SLccbEmKC200N1xxtzzk2FrknMb5SQgGen1IcheEi5NfDPMm2Smudlue25gRrZK9/UB6TyhVNqQH
YF/0ApIW9z4nk4FdgMVBF3k9BODWiy73+FUYULOgKR+7+0Qa3zgb5RxMjf7PkdzANR4TsMYRjFWE
34UHW9NWSO/pycxxoqAeNL0r1gwpkFMM+NOAJ0H44wMiKkxXo6fL60iqVJ1feMX3+WbolyGjwXJn
HWF9fF6dq3vTeuZEIKNLnyaFaO+n9mutIHk+zAUEdrHOxCPTDYmBB6mC6RjYWCCGRlvTW9naKSrk
486pKtGgTs6g2AY+5+nEFGGyeEtWeJ/gog6tg2oWkjCIKWeCofI0cG1oJRfGBpMCDDCFzmq8abke
viJV+mvePeE5mumstgEONpNOFpYqjxzWQ+8WYAw9ZdHRvAjOmu64/s4VMsM8mOVjvdeKRAdr6Fm6
b39oF19BUyPtkXImx+yjjAy97bdc+D3ZAgVH/mchF5No+IuSAPr1e5KvBaripBqefA24scX6i50T
7Q0dw3n5Uc/4VY+dflKdJbl8GgYl6bQV6ahHTB4emZcebrYlnn5En9gsh8/bUTFDge/20zaViQ3C
oGkeUmUTe1LuLR1SCq4a+q/nuuLzt2U3xVBdLmrYhFRuIBPA5duenwlvNPczIgDXi/+CAknJJ+GK
SMlnkRA/KCRIH/LhBRAw9WB8FRMN3paINdw7KyFYsz7KANW5v1FtjybbgEm6juQK0LzUrddJ3NMG
oee+Wofxf1MJETgBWvRRWLQh5LaWD+3pxX1bEOJG9zlpnC4Na0JLso++B4S8lEa9XnewtMk3o1uo
CurXqPeBcEiSpveRiyf/V++w/IRjtk9IVbNBChimLfDAYPPmOnmYPpdZj7sONcaA1syVLEplyddV
u21llKlQ42SqVqd3TXPbZ4LEiaMsViEsIbez2uHk9AWdKwhWmSfOFLBzUCvFX5ENsa3SZGpMkXvJ
h8K+zf8KejdQ6J3+cR1Ta3tTDC5wggRutIPoLoioXyaFzRgsiPO5Vll1vFbVnJKY2l25xjfiXhKP
Suxx+tgUl+20aFMTEvBqIMr2VPXFk5rJh39hpSP+FmSuW9+zr5It0h9OGtSPXsksr30rXjaKsXTV
IKJUulQnmmKKJbIJsLpR/yzzLcf/SY7DUftGLfN9q2Vvn10UnNWvz+jx7nxNraGqoSgVujKslqe7
NEH2IJm+jd+CZvCY9YH3E0sMP+MxqkxpDAHkmu3Rt6k5cvqoY8UCIf/gD9YO7rcErFMVO61chua0
l1fkQrxbGIseUGYRel0CekxeWX/MuBKy/l530Jl5y0dLSRSVKiKaKliFk7TO+gWOI+TpoMMK3jqg
qEb3tPhMy79BldyXHllvh8kfWkgS+UefnMz0gPEqq5rf2YatvJezr/dJDrwOS+aSDHJV9LlhU2YM
pmjMz9fobZvpbrwyFVHrmop7+dIE2eSya5IhT+FWJRKnF9PeqtfvPGDetisBQeLoS7D666eYa/tz
9UMgdKGxA47IUDhxdqkBIRQX/uK5SvTHCgoP0BGo6GooulwOA6mCfA1pVQ02Nt+kmGn3uVjEDqoX
ivHLM56ATNiTQJrneQ4wXJa/8TUJ3d8x+L2H2AudVnVwwBpgUFvsidRcnCwKrhY3WquH2uK4hgxn
x5gTCDBBgmas5u4qVzq1kgCvBphZ9SpMcE214gK0B73mxyHhmP8=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
