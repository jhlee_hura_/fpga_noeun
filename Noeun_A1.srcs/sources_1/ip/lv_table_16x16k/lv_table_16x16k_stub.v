// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Thu Oct  7 10:35:30 2021
// Host        : HURA-JUNHO running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               C:/Users/hura/Documents/Xilinx_xfile/Noeun/noeun_a_19ver_01/noeun_a_19ver.srcs/sources_1/ip/lv_table_16x16k/lv_table_16x16k_stub.v
// Design      : lv_table_16x16k
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku5p-ffvb676-1-i
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "blk_mem_gen_v8_4_3,Vivado 2019.1" *)
module lv_table_16x16k(clka, ena, wea, addra, dina, clkb, addrb, doutb)
/* synthesis syn_black_box black_box_pad_pin="clka,ena,wea[0:0],addra[13:0],dina[15:0],clkb,addrb[13:0],doutb[15:0]" */;
  input clka;
  input ena;
  input [0:0]wea;
  input [13:0]addra;
  input [15:0]dina;
  input clkb;
  input [13:0]addrb;
  output [15:0]doutb;
endmodule
