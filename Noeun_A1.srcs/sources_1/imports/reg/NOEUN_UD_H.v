
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/07/28 10:04:50
// Design Name: 
// Module Name: NOEUN_H
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

// ?΄λ¦? λ§λ€κΈ?
// a='EdgeRF iron(GA) 8GHz (HR-EdgeRFi-01)';
// len_b=ceil(length(a)/4)*4; b=zeros(1,len_b);b(1:length(a))=a;
// c=dec2hex(typecast(uint8(double(b)),'uint32'))
//
// char(typecast(uint32(hex2dec(c)),'uint8'))' % ??Έ?©
// reshape(char(typecast(uint32(hex2dec(c)),'uint8')),4,len_b/4)' % ??Έ?©
// ---------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------
// SYSTEM Constant
`define VERSION_DATE_DATA 				32'h1DE10001 	// dec2hex(fix(now)-datenum(2000,01,01)) // scilab : dec2hex(int(now()) - datenum(2000,01,01))
`define COPYRIGHTER 					32'h41525548 	// HURA
`define PRODUCT 						32'h65676445
`define PRODUCT1 						32'h69204652
`define PRODUCT2 						32'h286E6F72
`define PRODUCT3 						32'h20294147
`define PRODUCT4 						32'h7A484738
`define PRODUCT5 						32'h52482820
`define PRODUCT6 						32'h6764452D
`define PRODUCT7 						32'h69465265
`define PRODUCT8 						32'h2931302D
`define PRODUCT9 						32'h00000000
`define PRODUCT10 						32'h00000000
`define PRODUCT11 						32'h00000000
// EdgeRF - Spectrum Monitoring
// 0x000 : BS, GA
// 0x100 : SM(Signal Monitoring)
// 0x200 : H.AI(Hura AI Sensor)
`define EDGERF_FUNC						32'h00000001
// Mercury C1 - Guam 2021? λ²μ 
`define EDGERF_TYPE		32'h00000101
// ---------------------------------------------------------------------------------

// Write FIFO Program full (for DIN RDY) - 0,1?΄λ©? PF λ¬΄μ?κ³? FULL, ALMOST_FULL ?? RDY ? ?Έκ°? κΊΌμ§
// RTS? κ²½μ° RTS OUT ? Latencyκ°? ??΄ Margin?Όλ‘? 64 - 5 = 59 ? ?
`define S0_FIFO_PF_THRESH				59
`define S1_FIFO_PF_THRESH				59
`define S2_FIFO_PF_THRESH				59
`define S3_FIFO_PF_THRESH				59
`define S4_FIFO_PF_THRESH				59
`define S5_FIFO_PF_THRESH				59

// M?_DURATION λ§λ€ ?κ°μ© (1/M?_DURATION ?΄ ? ? ?¨) ... κ°μ 1/n ?©μ³μ 1?΄ ?κΈ? μ‘°μ 
// RTS? κ²½μ° burstκ°? κΈΈμ΄ Duration? μ§§κ² 2λ‘?.
`define M0_DURATION						9	// 11%		// S0 // Common
`define M1_DURATION						9	// 11%		// to PCIE DMA
`define M2_DURATION						9	// 11%		// S1 Spectrum
`define M3_DURATION						3	// 33%		// S2 RTS
`define M4_DURATION						9	// 11%		// S3 BIGRF
`define M5_DURATION						9	// 11%		// S4 Spectrogram
`define M6_DURATION						9	// 11%		// Reserved
// 512bit ?¨?? Len λ₯? λ³΄λ΄λ©? ?€? κ²μΌλ‘? round robin
`define M0_BURST_LEN					10
`define M1_BURST_LEN					10
`define M2_BURST_LEN					10
`define M3_BURST_LEN					10
`define M4_BURST_LEN					10
`define M5_BURST_LEN					10
`define M6_BURST_LEN					10

