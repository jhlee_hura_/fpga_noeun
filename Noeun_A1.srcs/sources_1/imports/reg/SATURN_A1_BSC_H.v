
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/11/30 16:54:10
// Design Name: 
// Module Name: SATURN_A1_BSC_H
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


`define NUMBER_OF_RX_LANES 4
`define NUMBER_OF_TX_LANES 4
`define NUMBER_OF_REFCLK_BUFFERS 2
`define ADC_RESOLUTION 16
