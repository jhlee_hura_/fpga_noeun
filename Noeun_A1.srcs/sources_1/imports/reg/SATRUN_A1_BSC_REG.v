
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/07/28 10:03:55
// Design Name: 
// Module Name: SATRUN_A1_BSC_REG
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------
// Cλ‘? μΉ΄νΌλ°©λ²
// 1.μ£ΌμκΉμ? μΉ΄νΌ
// replace (ctrl+H)
// 16'h	 -> 0x
// 32'h	 -> 0x
// 'h	 -> 0x
// `define -> #define
// 'd	 ->
// ` ->
// //-- ->
// ---------------------------------------------------------------------------------
// ?΄λ¦? λ§λ€κΈ?
// a='MERCURY Cx Digital Board(HR-MERC-Cx-01)';
// len_b=ceil(length(a)/4)*4; b=zeros(1,len_b);b(1:length(a))=a;
// c=dec2hex(typecast(uint8(double(b)),'uint32'))
//
// char(typecast(uint32(hex2dec(c)),'uint8'))' % ??Έ?©
// reshape(char(typecast(uint32(hex2dec(c)),'uint8')),4,len_b/4)' % ??Έ?©
// ---------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------
// Board and FW Constant

`define BSC_VERSION_DATE_DATA			32'h1D8D000C	// dec2hex(fix(now)-datenum(2000,01,01)) // scilab : dec2hex(int(now()) - datenum(2000,01,01))
`define BOARD_MAKER						32'h41525548	// HURA
`define BOARD_NAME						32'h4352454D
`define BOARD_NAME1						32'h20595255
`define BOARD_NAME2						32'h44207843
`define BOARD_NAME3						32'h74696769
`define BOARD_NAME4						32'h42206C61
`define BOARD_NAME5						32'h6472616F
`define BOARD_NAME6						32'h2D524828
`define BOARD_NAME7						32'h4352454D
`define BOARD_NAME8						32'h2D78412D
`define BOARD_NAME9						32'h00297878
`define BOARD_NAME10					32'h00000000
`define BOARD_NAME11					32'h00000000

`define BOARD_NAME2_C1					32'h44203143
`define BOARD_NAME2_C2					32'h44203243
`define BOARD_NAME8_C1					32'h2D31412D
`define BOARD_NAME8_C2					32'h2D32412D
`define BOARD_NAME9_01					32'h00293130
`define BOARD_NAME9_02					32'h00293230
// ----------------------------------------------------------------------
// ADDRESS RANGE
// ----------------------------------------------------------------------
// system domain register
`define BSC_REG1_ADDR_START				16'h0000
`define BSC_REG1_ADDR_END				16'h07FF
// signal domain address
`define BSC_REG2_ADDR_START				16'h0800
`define BSC_REG2_ADDR_END				16'h0CFF
// pcie domain address
`define BSC_PCIE_ADDR_START				16'h0D00
`define BSC_PCIE_ADDR_END				16'h0FFF

// ----------------------------------------------------------------------
// SYSTEM DOMAIN REGISTER
// ----------------------------------------------------------------------
// --------- Board Information Register ---------
`define BOARD_MAKER_REG					`BSC_REG1_ADDR_START + 16'h0000
`define BSC_VERSION_REG					`BSC_REG1_ADDR_START + 16'h0004
`define BOARD_NAME_REG					`BSC_REG1_ADDR_START + 16'h0008
`define BOARD_NAME1_REG					`BSC_REG1_ADDR_START + 16'h000C
`define BOARD_NAME2_REG					`BSC_REG1_ADDR_START + 16'h0010
`define BOARD_NAME3_REG					`BSC_REG1_ADDR_START + 16'h0014
`define BOARD_NAME4_REG					`BSC_REG1_ADDR_START + 16'h0018
`define BOARD_NAME5_REG					`BSC_REG1_ADDR_START + 16'h001C
`define BOARD_NAME6_REG					`BSC_REG1_ADDR_START + 16'h0020
`define BOARD_NAME7_REG					`BSC_REG1_ADDR_START + 16'h0024
`define BOARD_NAME8_REG					`BSC_REG1_ADDR_START + 16'h0028
`define BOARD_NAME9_REG					`BSC_REG1_ADDR_START + 16'h002C
`define BOARD_NAME10_REG				`BSC_REG1_ADDR_START + 16'h0030
`define BOARD_NAME11_REG				`BSC_REG1_ADDR_START + 16'h0034
`define CLK_MUX_REG						`BSC_REG1_ADDR_START + 16'h0038
`define TEST_PAD_REG					`BSC_REG1_ADDR_START + 16'h003C
`define PCB_VER_REG						`BSC_REG1_ADDR_START + 16'h0040
`define BOARD_STAT_REG					`BSC_REG1_ADDR_START + 16'h0044

// --------- ADC Register ---------
`define ADC_IODELAY_BASE_ADD			`BSC_REG1_ADDR_START + 16'h0070

//`define ADCA_IODELAY_3_0_REG			`ADC_IODELAY_BASE_ADD + 16'h0010
//`define ADCA_IODELAY_7_4_REG			`ADC_IODELAY_BASE_ADD + 16'h0014
//`define ADCA_IODELAY_11_8_REG			`ADC_IODELAY_BASE_ADD + 16'h0018
//`define ADCA_IODELAY_15_12_REG			`ADC_IODELAY_BASE_ADD + 16'h001C
//`define ADCA_IODELAY_13_12_REG			`ADCA_IODELAY_15_12_REG

// --------- Board Register ---------
`define DIGI_PERIP_BASE_ADD				`BSC_REG1_ADDR_START + 16'h0100
`define ADCA_SPI_WR_REG					`DIGI_PERIP_BASE_ADD + 16'h0004
`define ADCA_SPI_RD_REG					`DIGI_PERIP_BASE_ADD + 16'h0008
`define BSC_LED_REG					`DIGI_PERIP_BASE_ADD + 16'h000C
`define TMP_IN_RD_REG						`DIGI_PERIP_BASE_ADD + 16'h00C0
`define TMP_EX_RD_REG						`DIGI_PERIP_BASE_ADD + 16'h00C4
`define TMP_AD_REG						`DIGI_PERIP_BASE_ADD + 16'h00C8
`define HMCPLL_SPI_WR_REG				`DIGI_PERIP_BASE_ADD + 16'h00D0
`define HMCPLL_SPI_RD_REG				`DIGI_PERIP_BASE_ADD + 16'h00D4
`define GPSDO_INFO_REG					`DIGI_PERIP_BASE_ADD + 16'h00DC
`define PA_SHDN_REG						`DIGI_PERIP_BASE_ADD + 16'h00F0
`define PWR_FAN_HIGH_DURA_REG			`DIGI_PERIP_BASE_ADD + 16'h00F8
`define PWR_FAN_LOW_DURA_REG			`DIGI_PERIP_BASE_ADD + 16'h00FC

`define UART_RD_REG						`DIGI_PERIP_BASE_ADD + 16'h0110	// 0x210
`define UART_WR_REG						`DIGI_PERIP_BASE_ADD + 16'h0114
`define UART_STATUS_REG					`DIGI_PERIP_BASE_ADD + 16'h0118
`define UART_CFG_REG					`DIGI_PERIP_BASE_ADD + 16'h011C

//`define EXT_GPIO_MODE_REG				`DIGI_PERIP_BASE_ADD + 16'h0120
`define EXT_GPIO_DATA_REG				`DIGI_PERIP_BASE_ADD + 16'h0128	// 0x228
`define EXT_GPIO_DIR_REG				`DIGI_PERIP_BASE_ADD + 16'h012C

// ----------------------------------------------------------------------
// SIGNAL DOMAIN REGISTER
// ----------------------------------------------------------------------
// --------- Memory Information REG ---------
// ??¬ ?°?΄?°κ°? ?°?¬μ§?κ³? ?? λ²μ? ??Έ
// Long Data? κ²½μ° ??¬ MEM Pointerλ‘? DRAM? ?°?¬μ§? ?°?΄?° κ°?? ??Έκ°??₯
`define MEM_POI_BASE_ADD				`BSC_REG2_ADDR_START + 16'h0000
`define S0_POINTER_REG					`MEM_POI_BASE_ADD + 16'h0000
`define S1_POINTER_REG					`MEM_POI_BASE_ADD + 16'h0004
`define S2_POINTER_REG					`MEM_POI_BASE_ADD + 16'h0008
`define S3_POINTER_REG					`MEM_POI_BASE_ADD + 16'h000C
`define S4_POINTER_REG					`MEM_POI_BASE_ADD + 16'h0010
`define S5_POINTER_REG					`MEM_POI_BASE_ADD + 16'h0014

// --------- ADC Input Setting REG-----------------
`define ADC_INPUT_MODE_BASE_ADD			`BSC_REG2_ADDR_START + 16'h0040
//`define ADCA_MODE_REG					`ADC_INPUT_MODE_BASE_ADD + 16'h0000

// --------- PPS TIME REG-----------------
`define PPS_TIME_BASE_ADD				`BSC_REG2_ADDR_START + 16'h0100		// 0x900
`define DATE_REG						`PPS_TIME_BASE_ADD + 16'h0000
`define TIME_REG						`PPS_TIME_BASE_ADD + 16'h0004
`define SYSONSEC_REG					`PPS_TIME_BASE_ADD + 16'h0008

// ----------------------------------------------------------------------
// PCIE DOMAIN REGISTER
// ----------------------------------------------------------------------
`define PCIE_DMA_BASE_ADD				`BSC_PCIE_ADDR_START

`define DMA_DATA_SEL_REG				`PCIE_DMA_BASE_ADD + 16'h0000
`define DMA_FIFO_EMPTY_REG				`PCIE_DMA_BASE_ADD + 16'h0004

`define PCIE_DMA_C2H_0_CTRL_REG		`PCIE_DMA_BASE_ADD + 16'h0080
`define PCIE_DMA_C2H_0_SRCADDR_REG		`PCIE_DMA_BASE_ADD + 16'h0084
`define PCIE_DMA_C2H_0_LEN_REG			`PCIE_DMA_BASE_ADD + 16'h0088
`define PCIE_DMA_C2H_0_STAT_REG		`PCIE_DMA_BASE_ADD + 16'h008C
`define PCIE_DMA_C2H_0_BUSY_CNT_REG	`PCIE_DMA_BASE_ADD + 16'h0090
`define PCIE_DMA_C2H_0_RUN_CNT_REG		`PCIE_DMA_BASE_ADD + 16'h0094
`define PCIE_DMA_C2H_0_PACKET_CNT_REG	`PCIE_DMA_BASE_ADD + 16'h0098
`define PCIE_DMA_C2H_0_DESC_CNT_REG	`PCIE_DMA_BASE_ADD + 16'h009C

`define PCIE_DMA_H2C_0_STAT_REG		`PCIE_DMA_BASE_ADD + 16'h00AC


`define PCIE_DMA_C2H_SRCADDR_DEF		0
`define DMA_LEN_DEF					4096
`define C2H_RST_PCIECLK					10	
// ---------------------------------------------------------------------------------
// DDR Memory Map (1bit=64bit, 512bit?¨?λ‘? ??λ¦¬λ 0 or 8) for DMA
// ---------------------------------------------------------------------------------
`define FPGA_MEM_BASE_ADDR				'h00000000
`define FPGA_MEM_END_ADDR				'h0FFFFFF8

// ---------------------------------------------------------------------------------
// MASK and Bit shift & CONSTANT VALUE
// ---------------------------------------------------------------------------------
`define PRODUCT_WORD_MAX_LENGTH			12
// CLK define
`define CLKPERSEC						184320000
`define SYSCLKPERSEC					125000000
//--#define PCIECLKMHZ						125
`define GRST_CYCLE_SYSCLK				10
`define C2H_RST_MIGCLK					8
`define CLKSEL_RST_SYSCLK				8

// ADC IO DELAY (ADCx_IODELAY_3_0_REG)
//--#define ADC_IO_DELAY0_MSK				0x0000001F
//--#define ADC_IO_DELAY1_MSK				0x00001F00
//--#define ADC_IO_DELAY2_MSK				0x001F0000
//--#define ADC_IO_DELAY3_MSK				0x1F000000
//--#define ADC_IO_DELAY0_SHF				0
//--#define ADC_IO_DELAY1_SHF				8
//--#define ADC_IO_DELAY2_SHF				16
//--#define ADC_IO_DELAY3_SHF				24
// ADC IO DELAY (ADCx_IODELAY_4_7_REG)
//--#define ADC_IO_DELAY4_MSK				0x0000001F
//--#define ADC_IO_DELAY5_MSK				0x00001F00
//--#define ADC_IO_DELAY6_MSK				0x001F0000
//--#define ADC_IO_DELAY7_MSK				0x1F000000
//--#define ADC_IO_DELAY4_SHF				0
//--#define ADC_IO_DELAY5_SHF				8
//--#define ADC_IO_DELAY6_SHF				16
//--#define ADC_IO_DELAY7_SHF				24
// ADC IO DELAY (ADCx_IODELAY_11_8_REG)
//--#define ADC_IO_DELAY8_MSK				0x0000001F
//--#define ADC_IO_DELAY9_MSK				0x00001F00
//--#define ADC_IO_DELAY10_MSK				0x001F0000
//--#define ADC_IO_DELAY11_MSK				0x1F000000
//--#define ADC_IO_DELAY8_SHF				0
//--#define ADC_IO_DELAY9_SHF				8
//--#define ADC_IO_DELAY10_SHF				16
//--#define ADC_IO_DELAY11_SHF				24

// ADC IO DELAY (ADCx_IODELAY_15_12_REG)
//--#define ADC_IO_DELAY12_MSK				0x0000001F
//--#define ADC_IO_DELAY13_MSK				0x00001F00
//--#define ADC_IO_DELAY14_MSK				0x001F0000
//--#define ADC_IO_DELAY15_MSK				0x1F000000
//--#define ADC_IO_DELAY12_SHF				0
//--#define ADC_IO_DELAY13_SHF				8
//--#define ADC_IO_DELAY14_SHF				16
//--#define ADC_IO_DELAY15_SHF				24
// ADC IO DELAY (ADCx_IODELAY_OR_REG)
//--#define ADCA_IO_DELAYOR_MSK				0x0000001F
//--#define ADCA_IO_DELAYOR_SHF				0

// ADC SPI MASK
//--#define ADC_SPI_RW_MSK					0x00800000
//--#define ADC_SPI_ADDR_MSK				0x001FFF00
//--#define ADC_SPI_DATA_MSK				0x000000FF
//--#define ADC_SPI_RW_SHF					23
//--#define ADC_SPI_ADDR_SHF				8
//--#define ADC_SPI_DATA_SHF				0

// ADC MODE
`define ADC_EDGESEL_MSK					'h1
`define ADC_PINMODE_MSK					'h2
`define ADC_EDGESEL_NOR					'h0
`define ADC_EDGESEL_OFFSET				`ADC_EDGESEL_MSK
`define ADC_PINMODE_MUX					'h0
`define ADC_PINMODE_INTERLEAVED			`ADC_PINMODE_MSK

// DMA DATA Selectiong
`define PCIE_DMA_SRC_TEST1				'h0
`define PCIE_DMA_SRC_TEST2				'h1
`define PCIE_DMA_SRC_FIFO				'h2
`define PCIE_DMA_SRC_FIFO1				'h3
`define PCIE_DMA_SRC_MIG				'h3

// ---------PCIE REG-----------------
// PCIE DMA Initial(PCIE_DMA_RST_REG)
//--#define PCIE_DMA_INIT					0x1

//--#define PCIE_DMA_C2H_START_MSK			0x00000001
//--#define PCIE_DMA_C2H_DONE_MSK			0x00000100
// PCIE DMA START
//--#define PCIE_DMA_C2H_START				PCIE_DMA_C2H_START_MSK

// Weighted Round Robin
//--#define PCIE_DMA_C2H_WRR_MSK			0x000000FF
//--#define PCIE_DMA_H2C_WRR_MSK			0x00FF0000
// DEFAULT
`define PCIE_DMA_C2H_LEN_DEF			320
`define PCIE_DMA_C2H_TLP_DEF			32		// Don't change, Tuned value for this board
`define PCIE_DMA_C2H_WRRCNT_DEF			16
`define PCIE_DMA_H2C_WRRCNT_DEF			16

`define PCIE_DMA_C2H_SRCADDR_DEF		0

// ---------------------------------------------------------------------------------
// DEFAULT VALUE
// ---------------------------------------------------------------------------------

// ADC_MODE(ADCx_MODE_REG)
`define ADC_MODE_DEF					`ADC_EDGESEL_NOR

// PWR_FAN1_HIGH_DURA_REG
`define FAN_HIGH_DURA_DEF			4500		// 5000 ? λ°°λΆ : duty 20% ?΄λ©? High 1000, Low 4000, 40%?΄λ©? High 2000, Low 3000
// PWR_FAN1_LOW_DURA_REG
`define FAN_LOW_DURA_DEF			500

