
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/07/28 10:06:41
// Design Name: 
// Module Name: NOEUN_REG
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
// --------------------------------------------------------------------------------

// replace (ctrl+H)
// 16'h  -> 0x
// 32'h  -> 0x
// 'h    -> 0x
// -32'sd -> -
// `define -> #define
// 'd    ->
// ` ->
// //-- ->
// ---------------------------------------------------------------------------------
// ----------------------------------------------------------------------

// system domain address
`define UD_REG1_ADDR_START				16'h1000
`define	UD_REG1_ADDR_END				`UD_REG1_ADDR_START + 16'h0FFF
// signal domain address
`define	UD_REG2_ADDR_START				16'h2000
`define	UD_REG2_ADDR_END				`UD_REG2_ADDR_START + 16'hDFFF      // UD_REG2_ADDR_END Must be FFFF 

// ----------------------------------------------------------------------
// SYSTEM DOMAIN REGISTER
// ---------------------------------------------------------------------
// --------- Project Information Register ---------
`define COPYRIGHTER_REG					`UD_REG1_ADDR_START + 16'h0000
`define	VERSION_REG						`UD_REG1_ADDR_START + 16'h0004
`define PRODUCT_REG						`UD_REG1_ADDR_START + 16'h0008
`define PRODUCT1_REG					`UD_REG1_ADDR_START + 16'h000C
`define PRODUCT2_REG					`UD_REG1_ADDR_START + 16'h0010
`define PRODUCT3_REG					`UD_REG1_ADDR_START + 16'h0014
`define PRODUCT4_REG					`UD_REG1_ADDR_START + 16'h0018
`define PRODUCT5_REG					`UD_REG1_ADDR_START + 16'h001C
`define PRODUCT6_REG					`UD_REG1_ADDR_START + 16'h0020
`define PRODUCT7_REG					`UD_REG1_ADDR_START + 16'h0024
`define PRODUCT8_REG					`UD_REG1_ADDR_START + 16'h0028
`define PRODUCT9_REG					`UD_REG1_ADDR_START + 16'h002C
`define PRODUCT10_REG					`UD_REG1_ADDR_START + 16'h0030
`define PRODUCT11_REG					`UD_REG1_ADDR_START + 16'h0034
`define	BYTESWAP_REG					`UD_REG1_ADDR_START + 16'h003C
`define SYS_TYPE_REG					`UD_REG1_ADDR_START + 16'h0040
`define FUNCTION_REG					`UD_REG1_ADDR_START + 16'h0044

// --------- GAIN(MGC) Register ---------
`define RF_GC_BASE_ADD					`UD_REG1_ADDR_START + 16'h0100

//`define RF_GC_CH_REG					`RF_GC_BASE_ADD + 16'h0000
`define RF_MGC_EN_REG					`RF_GC_BASE_ADD + 16'h0004
`define RF_MGC_REG						`RF_GC_BASE_ADD + 16'h0008
`define RF_MGC_TB_REG					`RF_GC_BASE_ADD + 16'h000C
`define RF_GC_USER_LNASW_REG					`RF_GC_BASE_ADD + 16'h0010
`define RF_GC_USER_SATT_REG				`RF_GC_BASE_ADD + 16'h0014
`define RF_GC_USER_ATT_CHA_1_REG				`RF_GC_BASE_ADD + 16'h0018
`define RF_GC_USER_ATT_CHA_2_REG				`RF_GC_BASE_ADD + 16'h001C
`define RF_GC_USER_ATT_CHB_1_REG				`RF_GC_BASE_ADD + 16'h0020
`define RF_GC_USER_ATT_CHB_2_REG				`RF_GC_BASE_ADD + 16'h0024

// Gain Setting and Switch setting
`define RF_GATT1_REG						`RF_GC_BASE_ADD + 16'h0030
`define RF_GATT2_REG					`RF_GC_BASE_ADD + 16'h0034
`define RF_HF_SATT_REG					`RF_GC_BASE_ADD + 16'h0038
`define RF_HF_GATT_REG					`RF_GC_BASE_ADD + 16'h003C
`define RF_HF_LN_REG					`RF_GC_BASE_ADD + 16'h0040

// --------- SYN Register ---------
`define RF_FREQ_BASE_ADD				`UD_REG1_ADDR_START + 16'h0200
`define RF_SYN_BASE_ADD					`UD_REG1_ADDR_START + 16'h0230
`define RF_INTF_SYNBD_M4_PLL_CMD_REG					`RF_SYN_BASE_ADD + 16'h0000
`define RF_INTF_SYNBD_M4_PLL_WR_REG					`RF_SYN_BASE_ADD + 16'h0004
`define RF_INTF_SYNBD_M4_PLL_RD_REG					`RF_SYN_BASE_ADD + 16'h0008
`define RF_INTF_SYNBD_M4_PLL_CTRL_INTERVAL_REG					`RF_SYN_BASE_ADD + 16'h000C
`define RF_INTF_SYNBD_M2_SYNC_DISTRIBUTOR_REG					`RF_SYN_BASE_ADD + 16'h0010         
`define RF_INTF_SYNBD_M2_SYNC_DISTRIBUTOR_RD_REG					`RF_SYN_BASE_ADD + 16'h0014
`define RF_INTF_SYNBD_M5_PLL_ATT1_REG					`RF_SYN_BASE_ADD + 16'h0018      
`define RF_INTF_SYNBD_M5_PLL_ATT2_REG					`RF_SYN_BASE_ADD + 16'h001C   
`define RF_INTF_SYNBD_M5_PLL_ATT3_REG					`RF_SYN_BASE_ADD + 16'h0020         
`define RF_INTF_SYNBD_M5_PLL_ATT4_REG					`RF_SYN_BASE_ADD + 16'h0024   
// --------- Frequency Switching & Detect Register ---------
`define RF_FREQ_SW_BASE_ADD				`UD_REG1_ADDR_START + 16'h0260

`define RF_INTF_SYNBD_M1_SR_WR_REG  		`RF_FREQ_SW_BASE_ADD + 16'h001C
`define RF_INTF_DCBD_M3_SR_WR_REG			`RF_FREQ_SW_BASE_ADD + 16'h0028

`define RF_INTF_DCBD_M6_DETECT_RD_REG  		`RF_FREQ_SW_BASE_ADD + 16'h0018
`define RF_INTF_DCBD_M7_DETECT_RD_REG  		`RF_FREQ_SW_BASE_ADD + 16'h001C

// --------- Status control ---------
`define RF_STS_BASE_ADD					`UD_REG1_ADDR_START + 16'h0300
`define RF_INTF_DCBD_M5_SRL_WR_REG					`RF_STS_BASE_ADD + 16'h0004
`define RF_INTF_DCBD_M5_SRL_RD_REG					`RF_STS_BASE_ADD + 16'h0008
`define RF_INTF_SYNBD_M3_SRL_WR_REG  				`RF_STS_BASE_ADD + 16'h0024
`define RF_INTF_SYNBD_M3_SRL_RD_REG  				`RF_STS_BASE_ADD + 16'h0024

// --------- RF_I2C control ---------
`define RF_I2C_BASE_ADD					`UD_REG1_ADDR_START + 16'h0340
`define RF_INTF_DCBD_M4_TMP_AD_REG				`RF_I2C_BASE_ADD + 16'h0000
`define RF_INTF_DCBD_M4_TMP_RD_REG				`RF_I2C_BASE_ADD + 16'h0004
`define RF_INTF_SYNBD_M6_TMP_AD_REG					`RF_I2C_BASE_ADD + 16'h0008
`define RF_INTF_SYNBD_M6_TMP_RD_REG					`RF_I2C_BASE_ADD + 16'h000C

// --------- LV Control and Information ---------
// LV Cal Control and information for Ach
`define LV_CAL_BASE_ADD				`UD_REG1_ADDR_START + 16'h0400

`define LV_IDX_BASE_ADD			    `LV_CAL_BASE_ADD + 16'h0000

`define LV_CAL_EN_REG				    `LV_IDX_BASE_ADD + 16'h0004
`define LV_FREQ_IDX_REG			    `LV_IDX_BASE_ADD + 16'h0008

`define LV_SATT_IDX_REG			    `LV_IDX_BASE_ADD + 16'h000C
`define LV_LNA_REG					    `LV_IDX_BASE_ADD + 16'h0010
`define LV_GATT_VALUE_REG				`LV_IDX_BASE_ADD + 16'h0014
`define LV_ATT_IDX_CHA_1_REG			`LV_IDX_BASE_ADD + 16'h0018
`define LV_ATT_IDX_CHA_2_REG			`LV_IDX_BASE_ADD + 16'h001C
`define LV_ATT_IDX_CHB_1_REG			`LV_IDX_BASE_ADD + 16'h0020
`define LV_ATT_IDX_CHB_2_REG			`LV_IDX_BASE_ADD + 16'h0024
`define LV_TEMP_IDX_CHA_REG			`LV_IDX_BASE_ADD + 16'h0028
`define LV_TEMP_IDX_CHB_REG			`LV_IDX_BASE_ADD + 16'h002C
`define LV_SUM_CAL_CHA_REG				`LV_IDX_BASE_ADD + 16'h0030
`define LV_SUM_CAL_CHB_REG				`LV_IDX_BASE_ADD + 16'h0034




// LV Table Write for Ach
`define LV_TB_BASE_ADD				    `LV_CAL_BASE_ADD + 16'h0040

`define LV_TB_SATT_REG				    `LV_TB_BASE_ADD + 16'h0000
`define LV_TB_LNA_REG				    `LV_TB_BASE_ADD + 16'h0004
`define LV_TB_GATT_REG			        `LV_TB_BASE_ADD + 16'h0008
`define LV_TB_ATT_CHA_1_REG			`LV_TB_BASE_ADD + 16'h000C
`define LV_TB_ATT_CHA_2_REG			`LV_TB_BASE_ADD + 16'h0010
`define LV_TB_BW_CHA_REG				`LV_TB_BASE_ADD + 16'h0014
`define LV_TB_TEMP_CHA_REG				`LV_TB_BASE_ADD + 16'h0018
`define LV_TB_ATT_CHB_1_REG			`LV_TB_BASE_ADD + 16'h001C
`define LV_TB_ATT_CHB_2_REG			`LV_TB_BASE_ADD + 16'h0020
`define LV_TB_BW_CHB_REG				`LV_TB_BASE_ADD + 16'h0024
`define LV_TB_TEMP_CHB_REG				`LV_TB_BASE_ADD + 16'h0028











// ----------------------------------------------------------------------
// SIGNAL DOMAIN REGISTER
// ----------------------------------------------------------------------
// Input mux control
`define	INPUT_MUX_REG					`UD_REG2_ADDR_START + 16'h0000
`define	RF_LAT_CNT_REG					`UD_REG2_ADDR_START + 16'h000C
// Test Signal Generator
`define TEST_SIGA_BASE_ADD				`UD_REG2_ADDR_START + 16'h0010
`define TEST_SIGA_SCAL_REG				`TEST_SIGA_BASE_ADD + 16'h0000
`define TEST_SIGA_PINC_REG				`TEST_SIGA_BASE_ADD + 16'h0004
`define TEST_SIGA_POFF_REG				`TEST_SIGA_BASE_ADD + 16'h0008

`define TEMP_REG						`UD_REG2_ADDR_START + 16'h003C

// DDC SEL
`define DDC_SEL_BASE_ADD				`UD_REG2_ADDR_START + 16'h0040
`define DDC1A_DATA_SEL_REG				`DDC_SEL_BASE_ADD + 16'h0000
`define DDC1B_DATA_SEL_REG				`DDC_SEL_BASE_ADD + 16'h0004

// DDC 1 Control
`define DDC1A_BASE_ADD					`UD_REG2_ADDR_START + 16'h0050
`define DDC1A_DDS_PINC_REG				`DDC1A_BASE_ADD + 16'h0000
`define DDC1A_IQ_CHANGE_REG				`DDC1A_BASE_ADD + 16'h0004
`define DDC1A_BW_REG						`DDC1A_BASE_ADD + 16'h0008

// DDC 2 control
`define DDC1B_BASE_ADD					`UD_REG2_ADDR_START + 16'h0060
`define DDC1B_DDS_PINC_REG				`DDC1B_BASE_ADD + 16'h0000
`define DDC1B_IQ_CHANGE_REG				`DDC1B_BASE_ADD + 16'h0004
`define DDC1B_BW_REG						`DDC1B_BASE_ADD + 16'h0008
`define DDC1B_CH_FILT_REG				`DDC1B_BASE_ADD + 16'h000C

// Common mem control
`define COMM_DATA_BASE_ADD				`UD_REG2_ADDR_START + 16'h00E0
`define COMM_DATA_CTRL_REG				`COMM_DATA_BASE_ADD + 16'h0000
`define COMM_DATA_FORM_REG				`COMM_DATA_BASE_ADD + 16'h0004
`define COMM_DATA_LEN_REG				`COMM_DATA_BASE_ADD + 16'h0008
`define COMM_FIFO_STAT_REG				`COMM_DATA_BASE_ADD + 16'h0010
`define COMM_FIFO_PF_REG				`COMM_DATA_BASE_ADD + 16'h0014

// ----------------------------------------------------------------------
// TIME DOMAIN
`define TIME_BASE_ADD					`UD_REG2_ADDR_START + 16'h0100
`define TDOA_START_REG					`TIME_BASE_ADD + 16'h0010
`define TDOA_SNAPSHOT_REG				`TIME_BASE_ADD + 16'h0018
`define TDOA_INTERVAL_REG				`TIME_BASE_ADD + 16'h001C

// ----------------------------------------------------------------------
// FFT1
`define FFT1_BASE_ADD					`UD_REG2_ADDR_START + 16'h0200
//`define FFT1_MODE_REG					`FFT1_BASE_ADD + 16'h0000
`define FFT1_WIN_WR_REG					`FFT1_BASE_ADD + 16'h0010
`define FFT1_ST_REG						`FFT1_BASE_ADD + 16'h0014
`define FFT1_TIME_OFFSET_REG			`FFT1_BASE_ADD + 16'h0018
`define FFT1_TIME_INTERVAL_REG			`FFT1_BASE_ADD + 16'h001C
`define FFT1_IFCAL_EN_REG				`FFT1_BASE_ADD + 16'h0020
`define FFT1_IFCAL_WR_IFBW_REG			`FFT1_BASE_ADD + 16'h0024
`define FFT1_IFCAL_WR_REG				`FFT1_BASE_ADD + 16'h0028

// ----------------------------------------------------------------------
// Spectrum 1
`define SPEC1_BASE_ADD					`UD_REG2_ADDR_START + 16'h0280
`define SPEC1_CTRL_REG					`SPEC1_BASE_ADD + 16'h0000
`define SPEC1_USER_LAT_REG				`SPEC1_BASE_ADD + 16'h0010
`define SPEC1_AVR_NUM_REG				`SPEC1_BASE_ADD + 16'h0014
`define SPEC1_TRACE_MODE_REG			`SPEC1_BASE_ADD + 16'h0018
`define SPEC1_FIFO_STAT_REG				`SPEC1_BASE_ADD + 16'h0030
`define SPEC1_PF_THRESH_REG				`SPEC1_BASE_ADD + 16'h0034

// ----------------------------------------------------------------------
// RTS
`define RTS_BASE_ADD					`UD_REG2_ADDR_START + 16'h0300
`define RTS_CTRL_REG					`RTS_BASE_ADD + 16'h0000
`define RTS_MODE_REG					`RTS_BASE_ADD + 16'h0004
`define RTS_FRAME_NUM_REG				`RTS_BASE_ADD + 16'h0010
`define RTS_OBSERVE_NUM_REG				`RTS_BASE_ADD + 16'h0014
`define RTS_DISCARD_NUM_REG				`RTS_BASE_ADD + 16'h0018
`define RTS_ALMOST_DONE_NUM_REG			`RTS_BASE_ADD + 16'h001C
//`define RTS_FRAME_RDY_REG				`RTS_BASE_ADD + 16'h0020
`define RTS_AXIS_OFFSET_REG				`RTS_BASE_ADD + 16'h0024
`define RTS_PRE_FIFO_STAT_REG			`RTS_BASE_ADD + 16'h0030
`define RTS_PRE_FIFO_PF_THRES_REG		`RTS_BASE_ADD + 16'h0034

// RTG
`define RTG_BASE_ADD					`UD_REG2_ADDR_START + 16'h0380
`define RTG_CTRL_REG					`RTG_BASE_ADD + 16'h0000
`define RTG_MODE_REG					`RTG_BASE_ADD + 16'h0004
`define RTG_FRAME_NUM_REG				`RTG_BASE_ADD + 16'h0010
`define RTG_AVG_NUM_REG					`RTG_BASE_ADD + 16'h0014
`define RTG_TRACE_MODE_REG				`RTG_BASE_ADD + 16'h0018
`define RTG_DETECT_MODE_REG				`RTG_BASE_ADD + 16'h001C
`define RTG_SYNC_INTERVAL_REG			`RTG_BASE_ADD + 16'h0020
`define RTG_SYNC_OFFSET_REG				`RTG_BASE_ADD + 16'h0024
`define RTG_FIFO_STAT_REG				`RTG_BASE_ADD + 16'h0030
`define RTG_PF_THRESH_REG				`RTG_BASE_ADD + 16'h0034

// ----------------------------------------------------------------------
// H.AI Data
`define HAI_BASE_ADD					`UD_REG2_ADDR_START + 16'h0600
`define HAI_CTRL_REG					`HAI_BASE_ADD + 16'h0000
`define HAI_USER_LAT_REG				`HAI_BASE_ADD + 16'h000C
`define HAI_AVR_NUM_REG					`HAI_BASE_ADD + 16'h0010
`define HAI_SPEC_SIZE_REG				`HAI_AVR_NUM_REG
`define HAI_PW_STEP_REG					`HAI_BASE_ADD + 16'h0014
`define HAI_PHASE_STEP_REG				`HAI_BASE_ADD + 16'h0018
`define HAI_FIFO_STAT_REG				`HAI_BASE_ADD + 16'h0020

// ----------------------------------------------------------------------
// AUDIO
`define AUD_BASE_ADD					`UD_REG2_ADDR_START + 16'h0400
`define AUD_MODE_REG					`AUD_BASE_ADD + 16'h0000
// AUDIO DDC
`define AUD_DDC_BASE_ADD				`AUD_BASE_ADD + 16'h0010
`define AUD_DDC_DDS_PINC_REG			`AUD_DDC_BASE_ADD + 16'h0000
`define AUD_DDC_IQ_CHANGE_REG			`AUD_DDC_BASE_ADD + 16'h0004
`define AUD_DDC_BW_REG					`AUD_DDC_BASE_ADD + 16'h0008
`define AUD_DDC_CH_FILT_REG				`AUD_DDC_BASE_ADD + 16'h000C
// AUDIO SQUEL
`define AUD_SQUELCH_EN_REG				`AUD_BASE_ADD + 16'h0020
`define AUD_SQUELCH_REG					`AUD_BASE_ADD + 16'h0024
// AUDIO DEMOD
`define AUD_BFO_PINC_REG				`AUD_BASE_ADD + 16'h0030
`define AUD_SSB_PINC_REG				`AUD_BASE_ADD + 16'h0034
`define AUD_TONE_SCALE_REG				`AUD_BASE_ADD + 16'h0038
`define AUD_TONE_OFFSET_REG				`AUD_BASE_ADD + 16'h003C
// AUDIO AGC
`define AUD_AGC_EN_REG					`AUD_BASE_ADD + 16'h0040
//`define AUD_AGC_REF_MAG_REG			`AUD_BASE_ADD + 16'h0044
`define AUD_AGC_ATTACK_STEP_REG			`AUD_BASE_ADD + 16'h0048
`define AUD_AGC_HOLD_REG				`AUD_BASE_ADD + 16'h004C
// AUDIO AFC
`define AUD_AFC_EN_REG					`AUD_BASE_ADD + 16'h0050
`define AUD_AFC_FREQ_OFFSET_REG			`AUD_BASE_ADD + 16'h0054
// AUDIO FIFO
`define AUD_FIFO_STAT_REG				`AUD_BASE_ADD + 16'h0058
`define AUD_FIFO_PF_REG					`AUD_BASE_ADD + 16'h005C
// Audio BW Enum
//--#define AUD_DDC_BW_NUM 						5
//--const float m_faudbw_enums[AUD_DDC_BW_NUM] = 		{1000000,1000000, 250000, 100000, 25000};
//--const float m_faudbw_fs_enums[AUD_DDC_BW_NUM]  = 	{1280000,1280000, 320000, 128000, 32000};

// FIFO Status
//#define FIFO_START						0x00000001 // FIFO Enable
//--#define FIFO_MRS						0x00000002 // FIFO RESET
//--#define FIFO_EF_STT						0x00000100 // FIFO Empty Status
//--#define FIFO_AE_STT						0x00000200 // FIFO Alomst empty Status
//--#define FIFO_PE_STT						0x00000400 // FIFO Program empty Status
//--#define FIFO_PF_STT						0x00000800 // FIFO Program Full Status
//--#define FIFO_AF_STT						0x00001000 // FIFO Almost Full Status
//--#define FIFO_FF_STT						0x00002000 // FIFO Full Status
// ---------------------------------------------------------------------------------
// DDR Memory Map (1bit=64bit, 512bit?¨?λ‘? ??λ¦¬λ 0 or 8) for DMA
// ---------------------------------------------------------------------------------
/* `define S1_BASE_ADDR					`FPGA_MEM_BASE_ADDR + 'h00000000
`define S1_END_ADDR	 					`FPGA_MEM_BASE_ADDR + 'h00FFFFF8
`define S2_BASE_ADDR					`FPGA_MEM_BASE_ADDR + 'h01000000
`define S2_END_ADDR	 					`FPGA_MEM_BASE_ADDR + 'h01FFFFF8
`define S3_BASE_ADDR					`FPGA_MEM_BASE_ADDR + 'h02000000
`define S3_END_ADDR	 					`FPGA_MEM_BASE_ADDR + 'h03FFFFF8
`define S4_BASE_ADDR					`FPGA_MEM_BASE_ADDR + 'h04000000
`define S4_END_ADDR	 					`FPGA_MEM_BASE_ADDR + 'h06FFFFF8
`define S5_BASE_ADDR					`FPGA_MEM_BASE_ADDR + 'h07000000
`define S5_END_ADDR	 					`FPGA_MEM_BASE_ADDR + 'h07FFFFF8
`define S0_BASE_ADDR					`FPGA_MEM_BASE_ADDR + 'h08000000
`define S0_END_ADDR	 					`FPGA_MEM_END_ADDR

`define COMM_DATA_BASE_ADDR				`S0_BASE_ADDR
`define COMM_DATA_END_ADDR 				`S0_END_ADDR
`define SPEC_BASE_ADDR					`S1_BASE_ADDR
`define SPEC_END_ADDR					`S1_END_ADDR
`define RTS_BASE_ADDR					`S2_BASE_ADDR
`define RTS_END_ADDR					`S2_END_ADDR
`define HAI_BIG_BASE_ADDR				`S3_BASE_ADDR
`define HAI_BIG_END_ADDR				`S3_END_ADDR
`define RTG_BASE_ADDR					`S4_BASE_ADDR
`define RTG_END_ADDR					`S4_END_ADDR 

// ---------------------------------------------------------------------------------
// MIG Configuration
// Using BSC mercury_bsc
`define S0_DIN_WIDTH					64
`define S1_DIN_WIDTH					32		// Spectrum
`define S2_DIN_WIDTH					512		// RTS
`define S3_DIN_WIDTH					128		// HAIedge
`define S4_DIN_WIDTH					16		// Spectrogram
`define S5_DIN_WIDTH					32		// Reserved */

// ---------------------------------------------------------------------------------
// MIG Configuration
// Using BSC mercury_bsc
`define F0_DIN_WIDTH					16
`define F1_DIN_WIDTH					64

// ---------------------------------------------------------------------------------
// MASK and Bit shift & CONSTANT VALUE
// ---------------------------------------------------------------------------------
// BUSY MASK
//--#define BUSY_MSK						0x80000000
// Enable
//`define ENABLE							'h1
`define DISABLE							'h0

// RF Temp. Sensor 					// Not Used in FPGA
//--#define RF_TMP_ADDR						0
//--#define RF_TMP_DATA_DIV					256

// Windowing Shift(SPEC1_WIN_WR_REG)
//--#define WIN_DATA_SHF					0
//--#define WIN_ADDR_SHF					18

// FFT CAL (SPEC1_FFTCAL_WR_REG)
//--#define FFTCAL_DATA_SHF					0
//--#define FFTCAL_ADDR_SHF					18
//--#define FFTCAL_NORMAL					0x1000

//--#define MGC_TB_ADDR_SHF					16
//--#define MGC_TB_HFTABLE_MSK				0x10000000
//--#define LV_FREQ_HF_MSK					0x10000000

// INPUT_MUX(INPUT_MUX_REG)  // Not Used in FPGA code
`define INPUT_MUX_AA_MSK				'h01
`define INPUT_MUX_AA_ADC				'h0
`define INPUT_MUX_AA_TEST				'h1


// BW Enum
//--#define DDC_BW_NUM 						24
//--const float m_fbw_enums[DDC_BW_NUM] = 		{72000000, 50000000, 48000000, 36000000, 24000000, 16000000, 12000000, 8000000, 4000000, 3200000, 1600000, 800000, 640000, 320000, 160000, 128000,  80000, 64000, 32000, 16000, 12800,  8000, 6400, 3200};
//--const float m_fbw_fs_enums[DDC_BW_NUM]  = 	{92160000, 61440000, 61440000, 46080000, 30720000, 20480000, 15360000,10240000, 5120000, 4096000, 2048000,1024000, 819200, 409600, 204800, 163840, 102400, 81920, 40960, 20480, 16384, 10240, 8192, 4096};

// BW NPPS μ£ΌκΈ° // FPGA Only
`define BW_NPPS							`CLKPERSEC / 1024

//Common Data_Enable (COMM_DATA_CTRL_REG )
//--#`define COMM_DATA_START					0x1

//Common Data Select (COMM_DATA_FORM_REG)
`define COMM_DT_TEST					5'h0
`define COMM_DT_DDC1A_24					5'h1
`define COMM_DT_DDC1B_24					5'h2
`define COMM_DT_DDC2A_24					5'h3
`define COMM_DT_DDC2B_24					5'h4
//`define COMM_DT_SPEC					5'h7
//`define COMM_DT_RTG						5'h8
//`define COMM_DT_BIGRF					5'h9
//`define COMM_DT_TDOA					5'ha
`define COMM_DT_ADCAA					5'h10
`define COMM_DT_ADCAB					5'h11
// ---------------------------------------------------------------------------------
// TDOA
`define TDOA_SNAPSHOT_DEF				65536
`define TDOA_TRIG_CLK_DEF				`CLKPERSEC

// ---------------------------------------------------------------------------------
// Spectrum
`define NFFT							8192
`define NFFT2							2048
//--#define FFT_PER_SEC						22500

// Audio demod
`define AUD_DEM_OFF						'h0
`define AUD_DEM_AM						'h1
`define AUD_DEM_FM						'h2
`define AUD_DEM_USB						'h3
`define AUD_DEM_LSB						'h4
`define AUD_DEM_CW						'h5
`define AUD_DEM_TONE					'h6

// ---------------------------------------------------------------------------------
// RTS (Persistant)
// Data Length // Not Used in FPGA
//--#define RTS_8B_LEN						163968	// 1281*512*8/32
//--#define RTS_LEN							RTS_8B_LEN

// RTS FRAME Ready (RTSA_FRAME_RDY_REG  , RTSB_FRAME_RDY_REG)
`define RTS_TRANS_RDY					1

// ---------------------------------------------------------------------------------
// Data Length(16bits) // Not Used in FPGA
//--#define RTG_HEADER_LEN					8	//Header(0x8000, 0x8001) + SEC(2) + nanoSec(2) + line?(1) + FFTperLINE(1) + 1281 * RTG_FRAME_NUM_REG)
// RTG Header
`define RTG_HEADER16_1					16'h8000
`define RTG_HEADER16_2					16'h8001
//`define RTG_HEADER32					32'h7f86eae6
//--#define RTG_FFT_TIME					NFFT / CLKPERSEC

// RTG Spec Detect Mode (RTG_DETECT_MODE_REG)
`define SAMPLE_DET						2'd0
`define PEAK_DET						2'd1
`define NEGPEAK_DET						2'd2
`define RMS_DET							2'd3
//`define AUTOPEAK_DET					3'd4

// Trace Mode (xxx_TRACE_MODE_REG)
`define SAMPLE_TRACE					2'd0
`define MAX_TRACE						2'd1
`define MIN_TRACE						2'd2
`define MEAN_TRACE						2'd3

// RTG INTERVAL Mode (RTG_SYNC_INTERVAL_REG)
`define RTG_TRIG_CLK_DEF				`CLKPERSEC / 10
// ---------------------------------------------------------------------------------
// DEFAULT VALUE
// ---------------------------------------------------------------------------------
// INPUT_MUX(INPUT_MUX_REG)
`define INPUT_MUX_DEF					`INPUT_MUX_AA_ADC
// RF_LAT_CNT_REG
`define	RF_LAT_CNT_DEF					60000		// 325 us , 55000 ?΄λ©? OK ?΄? λ§μ§

// RF_FAN
`define FAN_HIGH_DURA_DEF				4500		// 5000 ? λ°°λΆ : duty 20% ?΄λ©? High 1000, Low 4000, 40%?΄λ©? High 2000, Low 3000
`define FAN_LOW_DURA_DEF				500

// RF_MGC_EN_REG
`define	MGC_EN_DEF						`DISABLE
`define	MGC_DEF							107

// RF_SYN_REF_REG (FPGA Only)
`define	RF_SYN_REF_DEF					`ENABLE
// RF_SYN_CTRL_INTERVAL_REG (FPGA Only)
`define	RF_SYN_CTRL_INTERVAL_DEF		5		// 8ns * 10 = 80ns, 0 ?΄?΄? OK (??? 4clk ? ? κ°κ²©??)

// ---------------------------------------------------------------------------------
// Test Signal DDS
`define TEST_SCAL_DEF					'h0FFF
`define TEST_PINC_DEF 					'h4000091A
`define TEST_POFF_DEF 					'h0

// DDC data sel
`define DDC1_INPUT_CH_DEF 				'h0
`define DDC2_INPUT_CH_DEF 				'h0

// DDC
`define DDS_PINC_DEF 					'h40000000
`define DDC_IQCHG_DEF 					'd1
`define BW_DEF 							4
`define CH_FILT_DEF 					75

// Common Data Select (COMM_DATA_FORM_REG)
`define COMM_DATA_FORM_DEF				`COMM_DT_DDC1A_24
`define COMM_DATA_LEN_DEF				65536					// 32bits
`define COMM_PF_THRES_DEF				`COMM_DATA_LEN_DEF/2		// 64bits
// ------------------------------------------------------------------------------------
// Spectrum
// FFT Average for Spec or DF (SPx_FFT_AVR_NUM_REG)
`define FFT_AVR_NUM_DEF					'd8
`define SP1_FIFO_PF_THRES_DEF			'd2048

// RTS Mode(RTS_MODE_DEF)
`define RTS_MODE_DEF					'd0
// RTS Persist Num (RTS_FRAME_NUM_REG)
`define RTS_FRAME_NUM_DEF				'd2250	// 10fps
// RTS_ALMOST_DONE_NUM_REG
`define RTS_ALMOST_DONE_NUM_DEF			'd2025	// 10ms ? 

// RTG FFT block Num (RTG_FRAME_NUM_REG)
`define RTG_FRAME_NUM_DEF				'd1000
// RTG Average Num (RTG_AVG_NUM_REG)
`define RTG_AVR_NUM_DEF					'd225	// 10ms
// RTG_PF_THRESH_REG
`define RTG_FIFO_PF_THRES_DEF			'd512
// xxx_TRACE_MODE_REG
`define TRACE_MODE_DEF					`MEAN_TRACE

// ------------------------------------------------------------------------------------
// H.AI Data
`define HAI_SPEC_SIZE_DEF				'd256
`define HAI_PW_STEP_DEF					'd1
`define HAI_PHASE_STEP_DEF				'd1

// ---------------------------------------------------------------------------------
// AUDIO
`define AUD_MODE_DEF					`AUD_DEM_OFF
`define AUD_DDC_DDS_PINC_DEF			'h40000000
`define AUD_DDC_IQCHG_DEF				'd1
`define AUD_DDC_BW_DEF					'd2
`define AUD_DDC_CH_FILT_DEF				'd75
`define AUD_DDC_CH_FILT_BYPASS_DEF		`DISABLE
`define AUD_SQUELCH_EN_DEF				`DISABLE
`define AUD_SQUELCH_DEF					'd0
`define AUD_BFO_PINC_DEF				'd17476
`define AUD_SSB_PINC_DEF				'd37283
`define AUD_TONE_SCALE_DEF				'd350
`define AUD_TONE_OFFSET_DEF				-32'sd6991
`define AUD_AGC_EN_DEF					`ENABLE
`define AUD_AGC_ATTACK_STEP_DEF			'h1000
`define AUD_AGC_HOLD_DEF				'd200		// 2sec
`define AUD_AFC_EN_DEF					`ENABLE
`define AUD_PF_THRES_DEF				'd1600		// 50ms