##-----------------------------------------------------------------------------                PIN XDC
##          
##          
##           
#---Bank64(1.8V)
#set_property PACKAGE_PIN AA18
#set_property PACKAGE_PIN Y18
#set_property PACKAGE_PIN AA17
#set_property PACKAGE_PIN Y17
#set_property PACKAGE_PIN AC17
#set_property PACKAGE_PIN AB17
#set_property PACKAGE_PIN AB20
#set_property PACKAGE_PIN AA20
#set_property PACKAGE_PIN AB19
#set_property PACKAGE_PIN AA19
#set_property PACKAGE_PIN Y21
#set_property PACKAGE_PIN Y20
#set_property PACKAGE_PIN AC16
#set_property PACKAGE_PIN AE18
#set_property PACKAGE_PIN AE16
#set_property PACKAGE_PIN AD16
#set_property PACKAGE_PIN AF17
#set_property PACKAGE_PIN AE17
#set_property PACKAGE_PIN AD18
#set_property PACKAGE_PIN AC18
#set_property PACKAGE_PIN AF19
#set_property PACKAGE_PIN AF18
#set_property PACKAGE_PIN AD19
#set_property PACKAGE_PIN AE20
#set_property PACKAGE_PIN AD20
#set_property PACKAGE_PIN AC21
#set_property PACKAGE_PIN AB21
#set_property PACKAGE_PIN AE21
#set_property PACKAGE_PIN AD21
#set_property PACKAGE_PIN AB22
#set_property PACKAGE_PIN AA22
#set_property PACKAGE_PIN AC23
#set_property PACKAGE_PIN AC22
#set_property PACKAGE_PIN AE23
#set_property PACKAGE_PIN AD23
#set_property PACKAGE_PIN AF22
#set_property PACKAGE_PIN AE22
#set_property PACKAGE_PIN AF20
#set_property PACKAGE_PIN AF23
#set_property PACKAGE_PIN AC24
#set_property PACKAGE_PIN AB24
#set_property PACKAGE_PIN AD25
#set_property PACKAGE_PIN AD24
#set_property PACKAGE_PIN AD26
#set_property PACKAGE_PIN AC26
#set_property PACKAGE_PIN AF25
#set_property PACKAGE_PIN AF24
#set_property PACKAGE_PIN AB26
#set_property PACKAGE_PIN AE25
#set_property PACKAGE_PIN AE26
#set_property PACKAGE_PIN AE25


#---Bank65(1.8V)
#set_property PACKAGE_PIN N22
#set_property PACKAGE_PIN N21
#set_property PACKAGE_PIN P19
#set_property PACKAGE_PIN N19
#set_property PACKAGE_PIN P23   CFG_D5
#set_property PACKAGE_PIN N23   CFG_D4
#set_property PACKAGE_PIN R21   CFG_D7
#set_property PACKAGE_PIN R20   CFG_D6
#set_property PACKAGE_PIN P21
#set_property PACKAGE_PIN P20
#set_property PACKAGE_PIN R23
#set_property PACKAGE_PIN R22
#set_property PACKAGE_PIN T19
#set_property PACKAGE_PIN N26   CFG_CSI_B
#set_property PACKAGE_PIN R26
#set_property PACKAGE_PIN R25   [get_ports {TMP_ALERT_EX}]
set_property PACKAGE_PIN P26 [get_ports TMP_SDA_EX]
set_property PACKAGE_PIN P25 [get_ports TMP_SCL_EX]
#set_property PACKAGE_PIN V26
#set_property PACKAGE_PIN U26
#set_property PACKAGE_PIN P24
#set_property PACKAGE_PIN N24
#set_property PACKAGE_PIN U25
#set_property PACKAGE_PIN T25
#set_property PACKAGE_PIN U24
#set_property PACKAGE_PIN T24
#set_property PACKAGE_PIN W24
#set_property PACKAGE_PIN V24
set_property PACKAGE_PIN W23 [get_ports TMP_SDA_IN]
set_property PACKAGE_PIN V23 [get_ports TMP_SCL_IN]
#set_property PACKAGE_PIN W26
#set_property PACKAGE_PIN W25
#set_property PACKAGE_PIN AA25
#set_property PACKAGE_PIN AA24
#set_property PACKAGE_PIN Y26
#set_property PACKAGE_PIN Y25
#set_property PACKAGE_PIN Y23
#set_property PACKAGE_PIN Y22
#set_property PACKAGE_PIN AA23
#set_property PACKAGE_PIN W21
#set_property PACKAGE_PIN W20
#set_property PACKAGE_PIN W19
#set_property PACKAGE_PIN T23
set_property PACKAGE_PIN T22 [get_ports FPGA_INT]
#set_property PACKAGE_PIN V22
#set_property PACKAGE_PIN V21
#set_property PACKAGE_PIN U20
#set_property PACKAGE_PIN T20
#set_property PACKAGE_PIN U22
#set_property PACKAGE_PIN U21
#set_property PACKAGE_PIN V19
#set_property PACKAGE_PIN U19
set_property IOSTANDARD LVCMOS18 [get_ports TMP_SDA_EX]
set_property IOSTANDARD LVCMOS18 [get_ports TMP_SCL_EX]

set_property IOSTANDARD LVCMOS18 [get_ports TMP_SDA_IN]
set_property IOSTANDARD LVCMOS18 [get_ports TMP_SCL_IN]

set_property IOSTANDARD LVCMOS18 [get_ports FPGA_INT]
#---Bank66(1.8V)
#set_property PACKAGE_PIN B26
#set_property PACKAGE_PIN B25
#set_property PACKAGE_PIN C26
#set_property PACKAGE_PIN D26
#set_property PACKAGE_PIN C24
#set_property PACKAGE_PIN D23
#set_property PACKAGE_PIN D25
#set_property PACKAGE_PIN D24
#set_property PACKAGE_PIN E23
#set_property PACKAGE_PIN F23
#set_property PACKAGE_PIN E26
#set_property PACKAGE_PIN E25
#set_property PACKAGE_PIN F22
#set_property PACKAGE_PIN G22
#set_property PACKAGE_PIN H22
#set_property PACKAGE_PIN H21
#set_property PACKAGE_PIN G26
#set_property PACKAGE_PIN H26
#set_property PACKAGE_PIN F25
#set_property PACKAGE_PIN F24
#set_property PACKAGE_PIN J26
#set_property PACKAGE_PIN J25
#set_property PACKAGE_PIN H24
#set_property PACKAGE_PIN H23
#set_property PACKAGE_PIN G25
#set_property PACKAGE_PIN G24
#set_property PACKAGE_PIN J24
#set_property PACKAGE_PIN J23
#set_property PACKAGE_PIN K23
#set_property PACKAGE_PIN K22
#set_property PACKAGE_PIN L25
#set_property PACKAGE_PIN L24
#set_property PACKAGE_PIN K26
#set_property PACKAGE_PIN K25
#set_property PACKAGE_PIN M26
#set_property PACKAGE_PIN M25
#set_property PACKAGE_PIN L23
#set_property PACKAGE_PIN L22
#set_property PACKAGE_PIN M24
#set_property PACKAGE_PIN M22
#set_property PACKAGE_PIN K20
#set_property PACKAGE_PIN L20
#set_property PACKAGE_PIN J21
#set_property PACKAGE_PIN K21
#set_property PACKAGE_PIN L19
#set_property PACKAGE_PIN M19
#set_property PACKAGE_PIN J20
#set_property PACKAGE_PIN J19
#set_property PACKAGE_PIN M21
#set_property PACKAGE_PIN M20
#set_property PACKAGE_PIN K18
#set_property PACKAGE_PIN L18

#---Bank67(1.8V)
#set_property PACKAGE_PIN B22
#set_property PACKAGE_PIN C22
#set_property PACKAGE_PIN A25
#set_property PACKAGE_PIN A24
#set_property PACKAGE_PIN B21
#set_property PACKAGE_PIN C21
#set_property PACKAGE_PIN B24
#set_property PACKAGE_PIN C23
#set_property PACKAGE_PIN D21
#set_property PACKAGE_PIN E21
#set_property PACKAGE_PIN A23
#set_property PACKAGE_PIN A22
#set_property PACKAGE_PIN E22
#set_property PACKAGE_PIN B16
#set_property PACKAGE_PIN A20
#set_property PACKAGE_PIN A19
#set_property PACKAGE_PIN A15
#set_property PACKAGE_PIN B15
#set_property PACKAGE_PIN A18
#set_property PACKAGE_PIN A17
#set_property PACKAGE_PIN B17
#set_property PACKAGE_PIN C17
set_property PACKAGE_PIN B19 [get_ports CLK_C_P]
set_property PACKAGE_PIN B20 [get_ports CLK_C_N]
set_property PACKAGE_PIN C18 [get_ports SYSREF_B_P]
set_property PACKAGE_PIN C19 [get_ports SYSREF_B_N]
#set_property PACKAGE_PIN D20
#set_property PACKAGE_PIN D19
#set_property PACKAGE_PIN E18 [get_ports SYNCINB_P]
#set_property PACKAGE_PIN D18 [get_ports SYNCINB_N]
set_property PACKAGE_PIN E20 [get_ports ADC_SDATA]
set_property PACKAGE_PIN F20 [get_ports ADC_SCLK]
set_property PACKAGE_PIN G21 [get_ports ADC_CS_B]
set_property PACKAGE_PIN G20 [get_ports ADC_SDOUT]
set_property PACKAGE_PIN F19 [get_ports OVRB]
set_property PACKAGE_PIN F18 [get_ports OVRA]
#set_property PACKAGE_PIN H19
#set_property PACKAGE_PIN H18
#set_property PACKAGE_PIN G19
#set_property PACKAGE_PIN F17
#set_property PACKAGE_PIN C16
#set_property PACKAGE_PIN D16
#set_property PACKAGE_PIN G16
#set_property PACKAGE_PIN H16
#set_property PACKAGE_PIN E17
#set_property PACKAGE_PIN E16
#set_property PACKAGE_PIN D15
#set_property PACKAGE_PIN E15
#set_property PACKAGE_PIN G17
#set_property PACKAGE_PIN H17
#set_property PACKAGE_PIN F15
#set_property PACKAGE_PIN G15

set_property IOSTANDARD LVDS [get_ports CLK_C_N]
set_property IOSTANDARD LVDS [get_ports CLK_C_P]
set_property DIFF_TERM_ADV TERM_100 [get_ports CLK_C_N]
set_property DIFF_TERM_ADV TERM_100 [get_ports CLK_C_P]

set_property IOSTANDARD LVDS [get_ports SYSREF_B_N]
set_property IOSTANDARD LVDS [get_ports SYSREF_B_P]
set_property DIFF_TERM_ADV TERM_100 [get_ports SYSREF_B_N]
set_property DIFF_TERM_ADV TERM_100 [get_ports SYSREF_B_P]

#set_property IOSTANDARD LVDS [get_ports SYNCINB_N]
#set_property IOSTANDARD LVDS [get_ports SYNCINB_P]
#set_property DIFF_TERM_ADV TERM_100 [get_ports SYNCINB_N] delete
#set_property DIFF_TERM_ADV TERM_100 [get_ports SYNCINB_P] delete

set_property IOSTANDARD LVCMOS18 [get_ports ADC_SDATA]
set_property IOSTANDARD LVCMOS18 [get_ports ADC_SCLK]
set_property IOSTANDARD LVCMOS18 [get_ports ADC_CS_B]
set_property IOSTANDARD LVCMOS18 [get_ports ADC_SDOUT]
set_property IOSTANDARD LVCMOS18 [get_ports OVRB]
set_property IOSTANDARD LVCMOS18 [get_ports OVRA]

#---Bank84(3.3V)
set_property PACKAGE_PIN W13 [get_ports SP_RUN3_N]
set_property PACKAGE_PIN W12 [get_ports SP_RUN2_N]
set_property PACKAGE_PIN AA13 [get_ports SP_RUN1_N]
set_property PACKAGE_PIN Y13 [get_ports SP_RUN0_N]
set_property PACKAGE_PIN W15 [get_ports {EXT_GPIO[3]}]
set_property PACKAGE_PIN W14 [get_ports {EXT_GPIO[2]}]
set_property PACKAGE_PIN Y16 [get_ports {EXT_GPIO[1]}]
set_property PACKAGE_PIN W16 [get_ports {EXT_GPIO[0]}]
set_property PACKAGE_PIN AB14 [get_ports GPS_LOCK]
set_property PACKAGE_PIN AA14 [get_ports GPS_HOLDOVER]
set_property PACKAGE_PIN AA15 [get_ports GPS_PPS]
#set_property PACKAGE_PIN Y15 [get_ports {FPGA_INT}]
set_property PACKAGE_PIN AB16 [get_ports LMK_SDIO]
set_property PACKAGE_PIN AB15 [get_ports LMK_SCK]
set_property PACKAGE_PIN AC14 [get_ports LMK_CS_B]
set_property PACKAGE_PIN AC13 [get_ports ADCCLK_SCK]
set_property PACKAGE_PIN AD14 [get_ports ADCCLK_SDO]
set_property PACKAGE_PIN AD13 [get_ports ADCCLK_SDI]
set_property PACKAGE_PIN AE15 [get_ports ADCCLK_SEN]
set_property PACKAGE_PIN AD15 [get_ports FAN_CTRL]
set_property PACKAGE_PIN AF13 [get_ports F_UART_RXD]
set_property PACKAGE_PIN AE13 [get_ports F_UART_TXD]
set_property PACKAGE_PIN AF15 [get_ports LMK_SYNC]
set_property PACKAGE_PIN AF14 [get_ports LMK_RESET]

set_property IOSTANDARD LVCMOS33 [get_ports SP_RUN3_N]
set_property IOSTANDARD LVCMOS33 [get_ports SP_RUN2_N]
set_property IOSTANDARD LVCMOS33 [get_ports SP_RUN1_N]
set_property IOSTANDARD LVCMOS33 [get_ports SP_RUN0_N]

set_property IOSTANDARD LVCMOS33 [get_ports {EXT_GPIO[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {EXT_GPIO[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {EXT_GPIO[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {EXT_GPIO[3]}]

set_property IOSTANDARD LVCMOS33 [get_ports GPS_LOCK]
set_property IOSTANDARD LVCMOS33 [get_ports GPS_HOLDOVER]
set_property IOSTANDARD LVCMOS33 [get_ports GPS_PPS]


#---Bank86(3.3V)
set_property PACKAGE_PIN B11 [get_ports RF_PIN23]
set_property PACKAGE_PIN C11 [get_ports RF_PIN22]
set_property PACKAGE_PIN A10 [get_ports RF_PIN21]
set_property PACKAGE_PIN B10 [get_ports RF_PIN20]
set_property PACKAGE_PIN A9 [get_ports RF_PIN19]
set_property PACKAGE_PIN B9 [get_ports RF_PIN18]
set_property PACKAGE_PIN C9 [get_ports RF_PIN17]
set_property PACKAGE_PIN D9 [get_ports RF_PIN16]
set_property PACKAGE_PIN D10 [get_ports RF_PIN15]
set_property PACKAGE_PIN D11 [get_ports RF_PIN14]
set_property PACKAGE_PIN E10 [get_ports RF_PIN13]
set_property PACKAGE_PIN E11 [get_ports RF_PIN12]
set_property PACKAGE_PIN F9 [get_ports RF_PIN11]
set_property PACKAGE_PIN F10 [get_ports RF_PIN10]
set_property PACKAGE_PIN G9 [get_ports RF_PIN9]
set_property PACKAGE_PIN G10 [get_ports RF_PIN8]
set_property PACKAGE_PIN G11 [get_ports RF_PIN7]
set_property PACKAGE_PIN H11 [get_ports RF_PIN6]
set_property PACKAGE_PIN H9 [get_ports RF_PIN5]
set_property PACKAGE_PIN J9 [get_ports RF_PIN4]
set_property PACKAGE_PIN J10 [get_ports RF_PIN3]
set_property PACKAGE_PIN J11 [get_ports RF_PIN2]
set_property PACKAGE_PIN K9 [get_ports RF_PIN1]
set_property PACKAGE_PIN K10 [get_ports RF_PIN0]


set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN0]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN1]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN2]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN3]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN4]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN5]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN6]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN7]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN8]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN9]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN10]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN11]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN12]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN13]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN14]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN15]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN16]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN17]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN18]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN19]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN20]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN21]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN22]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN23]

#---Bank87(3.3V)
set_property PACKAGE_PIN A14 [get_ports RF_PIN47]
set_property PACKAGE_PIN B14 [get_ports RF_PIN46]
set_property PACKAGE_PIN A12 [get_ports RF_PIN45]
set_property PACKAGE_PIN A13 [get_ports RF_PIN44]
set_property PACKAGE_PIN B12 [get_ports RF_PIN43]
set_property PACKAGE_PIN C12 [get_ports RF_PIN42]
set_property PACKAGE_PIN C13 [get_ports RF_PIN41]
set_property PACKAGE_PIN C14 [get_ports RF_PIN40]
set_property PACKAGE_PIN D13 [get_ports RF_PIN39]
set_property PACKAGE_PIN D14 [get_ports RF_PIN38]
set_property PACKAGE_PIN E12 [get_ports RF_PIN37]
set_property PACKAGE_PIN E13 [get_ports RF_PIN36]
set_property PACKAGE_PIN F13 [get_ports RF_PIN35]
set_property PACKAGE_PIN F14 [get_ports RF_PIN34]
set_property PACKAGE_PIN F12 [get_ports RF_PIN33]
set_property PACKAGE_PIN G12 [get_ports RF_PIN32]
set_property PACKAGE_PIN J14 [get_ports RF_PIN31]
set_property PACKAGE_PIN J15 [get_ports RF_PIN30]
set_property PACKAGE_PIN G14 [get_ports RF_PIN29]
set_property PACKAGE_PIN H14 [get_ports RF_PIN28]
set_property PACKAGE_PIN H13 [get_ports RF_PIN27]
set_property PACKAGE_PIN J13 [get_ports RF_PIN26]
set_property PACKAGE_PIN H12 [get_ports RF_PIN25]
set_property PACKAGE_PIN J12 [get_ports RF_PIN24]

set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN24]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN25]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN26]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN27]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN28]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN29]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN30]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN31]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN32]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN33]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN34]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN35]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN36]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN37]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN38]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN39]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN40]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN41]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN42]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN43]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN44]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN45]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN46]
set_property IOSTANDARD LVCMOS33 [get_ports RF_PIN47]

set_property PACKAGE_PIN A3 [get_ports {SERDOUT_N[3]}]
set_property PACKAGE_PIN B1 [get_ports {SERDOUT_N[2]}]
set_property PACKAGE_PIN C3 [get_ports {SERDOUT_N[1]}]
set_property PACKAGE_PIN D1 [get_ports {SERDOUT_N[0]}]

set_property IOSTANDARD LVCMOS33 [get_ports F_UART_RXD]
set_property IOSTANDARD LVCMOS33 [get_ports F_UART_TXD]

set_property IOSTANDARD LVCMOS33 [get_ports LMK_CS_B]
set_property IOSTANDARD LVCMOS33 [get_ports LMK_RESET]
set_property IOSTANDARD LVCMOS33 [get_ports LMK_SCK]
set_property IOSTANDARD LVCMOS33 [get_ports LMK_SDIO]
set_property IOSTANDARD LVCMOS33 [get_ports LMK_SYNC]
set_property IOSTANDARD LVCMOS33 [get_ports ADCCLK_SCK]
set_property IOSTANDARD LVCMOS33 [get_ports ADCCLK_SDI]
set_property IOSTANDARD LVCMOS33 [get_ports ADCCLK_SDO]
set_property IOSTANDARD LVCMOS33 [get_ports ADCCLK_SEN]
set_property IOSTANDARD LVCMOS33 [get_ports FAN_CTRL]

##-----------------------------------------------------------------------------                 DMA and PCIe XDC
##
## Project    : The Xilinx PCI Express DMA
## File       : xilinx_xdma_pcie_x0y0.xdc
## Version    : 4.1
##-----------------------------------------------------------------------------
#
# User Configuration
# Link Width   - x2
# Link Speed   - Gen2
# Family       - kintexuplus
# Part         - xcku5p
# Package      - ffvb676
# Speed grade  - -1
#
# PCIe Block INT - 0
# PCIe Block STR - X0Y0
#
###############################################################################
# User Time Names / User Time Groups / Time Specs
###############################################################################
##
## Free Running Clock is Required for IBERT/DRP operations.
##
#############################################################################################################
create_clock -period 10.000 -name sys_clk [get_ports sys_clk_p]
#
#############################################################################################################
set_false_path -from [get_ports sys_rst_n]
set_property PULLUP true [get_ports sys_rst_n]
set_property IOSTANDARD LVCMOS18 [get_ports sys_rst_n]
#
#set_property PACKAGE_PIN T22 [get_ports sys_rst_n]
#set_property PACKAGE_PIN AF11 [get_ports sys_rst_n]
#
set_property CONFIG_VOLTAGE 1.8 [current_design]
#
#############################################################################################################
set_property PACKAGE_PIN T6 [get_ports sys_clk_n]
set_property PACKAGE_PIN T7 [get_ports sys_clk_p]
#
#############################################################################################################

#############################################################################################################
#


#
# BITFILE/BITSTREAM compress options
#
#set_property BITSTREAM.CONFIG.EXTMASTERCCLK_EN div-1 [current_design]
#set_property BITSTREAM.CONFIG.BPI_SYNC_MODE Type1 [current_design]
#set_property CONFIG_MODE BPI16 [current_design]
#set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
#set_property BITSTREAM.CONFIG.UNUSEDPIN Pulldown [current_design]
#
#
set_false_path -to [get_pins -hier {*sync_reg[0]/D}]